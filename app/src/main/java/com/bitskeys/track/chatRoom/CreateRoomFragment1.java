package com.bitskeys.track.chatRoom;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.users.ChatUsersListActivity;

/**
 * Created by Manish on 21-02-2016.
 */
public class CreateRoomFragment1 extends Fragment {

    private static String FILENAME = "GBC CreateRoomFragment1";

    private EditText chatRoomText;
    ImageButton submit_button;
    ImageButton cancel_button;
    String chatRoomName;

    View myFragmentView;


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_createroom1, container, false);
        return myFragmentView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        chatRoomText = (EditText) myFragmentView.findViewById(R.id.chat_RoomName);
        submit_button = (ImageButton) (myFragmentView.findViewById(R.id.next_room_button));
        cancel_button = (ImageButton) (myFragmentView.findViewById(R.id.cancel_room_button));
    }

    public void onCreateChatRoom(ChatUsersListActivity activity)
    {
        mCallback = activity;
        chatRoomName = null;
        chatRoomText.setText("");
                /*
                chatRoomText.setOnClickListener(new View.OnClickListener() {
                                                    public void onClick(View arg0) {
                                                        Log.d(FILENAME, "onClickListener");
                                                    }
                                                });*/
        chatRoomText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    //return sendChatMessage();
                    chatRoomName = chatRoomText.getText().toString();
                }
                return false;
            }
        });

        submit_button.setSelected(false);
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                //deleteUserActionFragment();
                //onNavigateToUsersListFromMap();
                Log.d(FILENAME, "onClick");
                chatRoomName = chatRoomText.getText().toString();
                if (chatRoomName != null && chatRoomName.length() >0)
                    mCallback.createChatRoom(chatRoomName);
            }
        });

        cancel_button.setSelected(false);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //deleteUserActionFragment();
                //onNavigateToMap();
                //Toast.makeText(getApplicationContext(), "Loading maps...", Toast.LENGTH_LONG).show();
                Log.d(FILENAME, "onClick");
                //mMapFragment.refresh();
                mCallback.cancelCreateChatRoom();
            }
        });
    }

    public void onDeleteChatRoom(ChatUsersListActivity activity)
    {
        mCallback = activity;
        chatRoomName = null;
        chatRoomText.setText("");
                /*
                chatRoomText.setOnClickListener(new View.OnClickListener() {
                                                    public void onClick(View arg0) {
                                                        Log.d(FILENAME, "onClickListener");
                                                    }
                                                });*/
        chatRoomText.setOnKeyListener(new View.OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    //return sendChatMessage();
                    chatRoomName = chatRoomText.getText().toString();
                }
                return false;
            }
        });

        submit_button.setSelected(false);
        submit_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {

                //deleteUserActionFragment();
                //onNavigateToUsersListFromMap();
                Log.d(FILENAME, "onClick");
                chatRoomName = chatRoomText.getText().toString();
                if (chatRoomName != null && chatRoomName.length() >0)
                    mCallback.deleteChatRoom(chatRoomName);
            }
        });

        cancel_button.setSelected(false);
        cancel_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                //deleteUserActionFragment();
                //onNavigateToMap();
                //Toast.makeText(getApplicationContext(), "Loading maps...", Toast.LENGTH_LONG).show();
                Log.d(FILENAME, "onClick");
                //mMapFragment.refresh();
                mCallback.cancelCreateChatRoom();
            }
        });
    }

    CreateRoomFrag1CallBack mCallback;
    public interface CreateRoomFrag1CallBack{
        public void createChatRoom(String roomName);
        public void deleteChatRoom(String roomName);
        public void cancelCreateChatRoom();
    }

}
