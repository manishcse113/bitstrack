package com.bitskeys.track.chatRoom;

import com.bitskeys.track.login.ChatConnection;
import com.google.android.gms.games.multiplayer.realtime.Room;

import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.MultiUserChat;

import java.util.HashMap;

/**
 * Created by Mr.Neeraj on 26-05-2016.
 */
public class RoomListenersCache {

    public static HashMap<String,RoomListeners> workingListeners = new HashMap<>();
    public static HashMap<String,RoomListeners> pendingListeners = new HashMap<>();

    public static boolean initializeRoomListeners(String room)
    {
        RoomListeners roomL = workingListeners.get(room);
        if(roomL != null)
        {
            // it is already established
            return true;
        }


        roomL = pendingListeners.get(room);
        if(roomL != null)
        {
            // already available in pending listeners, running job should retry to establish listeners
            return true;
        }

        roomL = new RoomListeners(room);
        MultiUserChat muc = null;
        XMPPTCPConnection mConnection = ChatConnection.getInstance().getConnection();
        if(mConnection != null)
        {
            muc = MultiChat.FormMultiUserChat(mConnection,room);
            MultiChat.setUpGroupChatInActionListeners(muc,roomL);
            MultiChat.setUpRoomInvitationListener(mConnection,roomL);
            workingListeners.put(room,roomL);
            return true;
        }
        else
        {
            pendingListeners.put(room,roomL);
            return false;
        }
    }
}
