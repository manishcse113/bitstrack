package com.bitskeys.track.chatRoom;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.login.ChatConnection;
import com.bitskeys.track.users.ChatUsersListActivity;
import com.bitskeys.track.users.UserObject;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.Affiliate;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Manish on 20-02-2016.
 */
public class CreateRoomFragment extends ListFragment {
    private EditText chatRoomText;
    ImageButton submit_button;
    ImageButton cancel_button;
    ListView usersListinRoom;
    String mRoomName;
    public static List selectedUserObjList = new ArrayList();

    CreateRoomAdapter createRoomAdapter;
    final String FILENAME = "GBC CreateRoomFragment";

    View myFragmentView;
    View mheaderView;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        myFragmentView = inflater.inflate(R.layout.fragment_createroom, container, false);
        mheaderView = inflater.inflate(R.layout.create_chatroom_header, null);

        usersListinRoom = (ListView)(myFragmentView.findViewById(R.id.list));

        return myFragmentView;
    }



    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if (mheaderView != null)
        {
            this.getListView().addHeaderView(mheaderView);
        }
        // Don't forget to now call setListAdapter()
        //this.setListAdapter(listAdapter);
    }

    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
    }

    public void updateFragment(ChatUsersListActivity context, String roomName, Map<String, String> mapobj) {
        Log.d(FILENAME, "addUsers");
        mRoomName = roomName;
        mCallback = context;
        selectedUserObjList.clear();
        if (myFragmentView != null)
        {
            List userObjList = createUserObjectList(mapobj,context);
            Log.d(FILENAME, "Creating new UserList");
            createRoomAdapter = new CreateRoomAdapter(context, R.id.list, userObjList);
            if (mheaderView!=null) {
                chatRoomText = (EditText) mheaderView.findViewById(R.id.chat_RoomName);
                submit_button = (ImageButton) (mheaderView.findViewById(R.id.create_room_button));
                cancel_button = (ImageButton) (mheaderView.findViewById(R.id.cancel_room_button));
                chatRoomText.setText(roomName);
                chatRoomText.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                            //return sendChatMessage();
                        }
                        return false;
                    }
                });
                submit_button.setSelected(false);
                submit_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {

                        //prepare users names for the group:
                        for (Object userObject : createRoomAdapter.userObjList) {
                            if (((UserObject) (userObject)).isAddedToGroup() == true) {
                                selectedUserObjList.add(userObject);
                            }
                        }
                        mCallback.submitChatRoom(mRoomName, selectedUserObjList);
                        Log.d(FILENAME, "onClick");
                    }
                });

                cancel_button.setSelected(false);
                cancel_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        mCallback.cancelChatRoomFrag();
                        Log.d(FILENAME, "onClick");
                    }
                });
            }
            setListAdapter(createRoomAdapter);
        }
    }


    public void editGroupFragment(ChatUsersListActivity context, String roomName, Map<String, String> mapobj) {
        Log.d(FILENAME, "editGroupFragment");
        mRoomName = roomName;
        mCallback = context;
        selectedUserObjList.clear();
        if (myFragmentView != null)
        {
            Log.d(FILENAME, "Editing new UserList");

            //Update the userList - as per the members of this group.
            List<UserObject> userObjList = createUserObjectList(mapobj,context);
            XMPPTCPConnection connection = ChatConnection.getInstance().getConnection();
            if (connection == null || !connection.isConnected())
            {
                return;
            }
            List<Affiliate> usersInRoomList = MultiChat.getMembersOfChatRoom(connection,roomName);
            //setList of selected users in room in userObjList

            if (usersInRoomList!=null)
            {
                for (Affiliate userAffiliation : usersInRoomList) {
                    for (UserObject userObj : userObjList) {
                        String userName = userObj.userName;
                        if (userAffiliation.getNick().equals(userName)) {
                            userObj.setSelected(true);
                        }
                    }
                }
            }

            createRoomAdapter = new CreateRoomAdapter(context, R.id.list, userObjList);
            if (mheaderView!=null) {
                chatRoomText = (EditText) mheaderView.findViewById(R.id.chat_RoomName);
                submit_button = (ImageButton) (mheaderView.findViewById(R.id.create_room_button));
                cancel_button = (ImageButton) (mheaderView.findViewById(R.id.cancel_room_button));
                chatRoomText.setText(roomName);
                chatRoomText.setOnKeyListener(new View.OnKeyListener() {
                    public boolean onKey(View v, int keyCode, KeyEvent event) {
                        if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                        }
                        return false;
                    }
                });
                //submit_button.setSelected(false);
                submit_button.setOnClickListener(new View.OnClickListener()
                {
                    @Override
                    public void onClick(View arg0) {
                        //prepare users names for the group:
                        for (Object userObject : createRoomAdapter.userObjList) {
                            if (((UserObject) (userObject)).isAddedToGroup() == true) {
                                selectedUserObjList.add(userObject);
                            }
                        }
                        mCallback.submitEditedChatRoom(mRoomName, selectedUserObjList);
                        Log.d(FILENAME, "onClick");
                    }
                });

                cancel_button.setSelected(false);
                cancel_button.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View arg0) {
                        mCallback.cancelChatRoomFrag();
                        Log.d(FILENAME, "onClick");
                    }
                });
            }
            setListAdapter(createRoomAdapter);
        }
    }

    //CreateUserObjectList
    public List createUserObjectList(Map<String, String> map,Context context)
    {
        List userObjList= new ArrayList();
        //Map<String, String> map = listobj.mUserPresenceMap;
        int i = 0;
        String userName;
        String presence;
        Integer presenceId;
        for (Map.Entry<String, String> entry : map.entrySet()) {
            //userNameListTemp[i] = entry.getKey();
            userName = entry.getKey();
            presence = entry.getValue();
            if (presence.equals(ChatXMPPService.OFFLINE))
            {
                presenceId = R.drawable.ic_3offline_icon;
            }
            else if (presence.equals(ChatXMPPService.ONLINE))
            {
                presenceId = R.drawable.ic_1online_icon;
            }
            else
            {
                presenceId = R.drawable.ic_2away;
            }
            //Neeraj Changed "...No location" to ""
            UserObject userObj = new UserObject(userName,ChatXMPPService.getDisplayName(userName), ChatXMPPService.getAvatarId(userName), presenceId, "" , false, true);
            Bitmap bitmap = Utility.getSavedBitmapForUser(userName, context);
            if(bitmap != null)
                userObj.setMyPhoto(bitmap);

            userObjList.add(userObj);
            i++;
        }
        Log.d(FILENAME, "New/Update Users count## " + i);
        return userObjList;
    }



    CreateRoomFragCallBack mCallback;
    public interface CreateRoomFragCallBack{
        public void submitChatRoom(String roomName, List<UserObject> userObjList);
        public void submitEditedChatRoom(String roomName, List<UserObject> userObjList);
        public void cancelChatRoomFrag();
    }
}
