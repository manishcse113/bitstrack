package com.bitskeys.track.chatRoom;

import org.jivesoftware.smackx.muc.Affiliate;

import java.util.HashMap;
import java.util.List;

/**
 * Created by Mr.Neeraj on 27-05-2016.
 */
public class ChatRoomInfo {

    static public enum MembersPermission {
        ADMIN,OWNER,OUTCAST,MEMEBER
    }
    String roomName;
    String roomJid;
    String groupName;

    public  static Integer ROOM_ADMIN = 0;
    public static Integer ROOM_OWNER = 1;
    public static Integer ROOM_OUTCAST = 2;
    public static Integer ROOM_MEMEBER = 3;

    HashMap<String,Integer> affiliate = new HashMap<>();
    HashMap<String,String> groupMap = new HashMap<>();


    public ChatRoomInfo(String roomName,String roomJid) {
        this.roomName = roomName;
        this.roomJid = roomJid;
        affiliate = new HashMap<>();
    }

    public  void addMemberInfo(String member,int perm,String groupName)
    {
        if(affiliate.containsKey(member))
        {
            affiliate.remove(member);
            affiliate.put(member,perm);
        }
        else
        {
            affiliate.put(member,perm);
        }
        if(groupMap.containsKey(member))
        {
            groupMap.remove(member);
            groupMap.put(member,groupName);
        }
        else
        {
            groupMap.put(member,groupName);
        }
    }
}
