package com.bitskeys.track.chatRoom;

import java.io.Serializable;
import java.util.HashMap;

/**
 * Created by Manish on 3/6/2016.
 */
public class RoomMaps extends HashMap<String, String>  implements Serializable {
    // public HashMap<String, String> mUserPresenceMap = new HashMap<String, String>();
    //public HashMap<String, String> mRoomJidMap = new HashMap<String, String>();
    //public HashMap<String,String> mRoomNameToDisplayNameMap = new HashMap<String,String>();//Jid, Display Name
    //public HashMap<String,String> mDisplayNameToRoomNameMap = new HashMap<String,String>();//Display Name, Jid


    public void addRoomNameToMap(String roomName,String jid)
    {
        if(roomName == null)
            return;
        String input = roomName.toLowerCase();
        this.put(input,jid);
    }
    /*

    public void addDisplayNameToMap(String displayName, String roomName)
    {
        if(displayName == null)
            return;
        String input = displayName.toLowerCase();
        mDisplayNameToRoomNameMap.put(input,roomName);
    }

    public String getDisplayName(String roomName)
    {
        if(roomName == null)
            return null;
        String input = roomName.toLowerCase();
        String str =mRoomNameToDisplayNameMap.get(input);
        return str;
    }

    public String getRoomName(String displayName)
    {
        if(displayName == null)
            return null;
        String input = displayName.toLowerCase();
        String str = mDisplayNameToRoomNameMap.get(input);
        return str;
    }*/
}
