package com.bitskeys.track.chatRoom;

import android.util.Log;

import com.bitskeys.track.RestAPI.OpenfireRestAPI;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.login.ChatConnection;

import org.jivesoftware.smack.MessageListener;
import org.jivesoftware.smack.PresenceListener;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.Affiliate;
import org.jivesoftware.smackx.muc.InvitationListener;
import org.jivesoftware.smackx.muc.InvitationRejectionListener;
import org.jivesoftware.smackx.muc.MultiUserChat;
import org.jivesoftware.smackx.muc.MultiUserChatManager;
import org.jivesoftware.smackx.muc.SubjectUpdatedListener;
import org.jivesoftware.smackx.muc.UserStatusListener;
import org.jivesoftware.smackx.xdata.Form;
import org.jivesoftware.smackx.xdata.FormField;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

/**
 * Created by swarnkarn on 2/15/2016.
 */
public class MultiChat {


    private static String FILENAME = "GBC MultiChat";

    public enum AFFILIATION_TYPE{
        AFFILIATION_NONE,
        AFFILIATION_OWNER,
        AFFILIATION_MEMBER,
        AFFILIATION_ADMIN,
    };

    public static String getServiceName(XMPPTCPConnection connection)
    {
        return "conference." + connection.getServiceName();
    }

    public static String getMyJid(XMPPTCPConnection connection)
    {
        return ChatXMPPService.mUserName + "@" + getServiceName(connection);
    }


    //owners list contains JIDs of users who are owner
    //chatRoomJid = roomName@service example myroom@conference.jabber.org
    //chatRoom = roomName
    public static boolean testCreateChatRoom()
    {
        boolean result = false;
        XMPPTCPConnection connection = ChatConnection.getInstance().getConnection();
        if(connection == null)
            return result;

        connection.setPacketReplyTimeout(10000);

        List<String> owners = new ArrayList<String>();
        owners.add("+11000");
        owners.add("+12000");
        owners.add("+919899406606");
        if(createChatRoom(connection, "test_chat_room", owners, "Ballu")==0)
            result = true;

        inviteUsersWithAffiliation(connection,"test_chat_room",owners,AFFILIATION_TYPE.AFFILIATION_OWNER);

        setUpRoomInvitationListener(connection,null);
        return result;
    }

    public static MultiUserChat  FormMultiUserChat(XMPPTCPConnection connection, String chatRoom)
    {
        if(connection == null || chatRoom == null || (chatRoom.length() == 0) )
        {
            Log.e(FILENAME, "connection or chat room is null");
            return null;
        }

        String chatRoomJid = chatRoom + "@" + getServiceName(connection);
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getInstanceFor returned null");
            return null;
        }
        MultiUserChat muc = manager.getMultiUserChat(chatRoomJid);
        return muc;
    }
    public static int  createChatRoom(XMPPTCPConnection connection, String chatRoom,List<String> owners,String myNickName)
    {
        Log.d(FILENAME, "createChatRoom");
        if(connection == null || chatRoom == null || (chatRoom.length() == 0) )
        {
            Log.e(FILENAME, "connection or chat room is null");
            return -1;
        }

        int result = 0;

        String chatRoomJid = chatRoom + "@" + getServiceName(connection);
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getInstanceFor returned null");
            return -1;
        }

        MultiUserChat muc = manager.getMultiUserChat(chatRoomJid);
        if(muc == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getMultiUserChat returned null");
            return -1;
        }

        try {
            muc.create(myNickName);
            Form form = muc.getConfigurationForm();
            Form submitForm = form.createAnswerForm();

            for (Iterator fields = form.getFields().iterator(); fields.hasNext();) {
                FormField field = (FormField) fields.next();
                if (!FormField.Type.hidden.equals(field.getType()) && field.getVariable() != null) {
                    // Sets the default value as the answer
                    submitForm.setDefaultAnswer(field.getVariable());
                }
            }
//            submitForm.setAnswer("muc#roomconfig_roomowners", owners);
            if (submitForm.getField("muc#roomconfig_roomname")!=null)
                submitForm.setAnswer("muc#roomconfig_roomname", "Test Room Subject");

            if (submitForm.getField("muc#roomconfig_roomdesc")!=null)
                submitForm.setAnswer("muc#roomconfig_roomdesc", "Test Room Subject");

            if (submitForm.getField("muc#roomconfig_changesubject")!=null)
                submitForm.setAnswer("muc#roomconfig_changesubject", true);

            if (submitForm.getField("muc#roomconfig_anonymity")!=null)
                submitForm.setAnswer("muc#roomconfig_anonymity","anonymous");

            if (submitForm.getField("muc#roomconfig_publicroom")!=null)
                submitForm.setAnswer("muc#roomconfig_publicroom", false);

            if (submitForm.getField("muc#roomconfig_persistentroom")!=null)
                submitForm.setAnswer("muc#roomconfig_persistentroom", true);

            if (submitForm.getField("muc#roomconfig_whois")!=null)
                submitForm.setAnswer("muc#roomconfig_whois", Arrays.asList("anyone"));

            if (submitForm.getField("muc#roomconfig_historylength")!=null)
                submitForm.setAnswer("muc#roomconfig_historylength", 0);

            /*

            if (submitForm.getField("muc#maxhistoryfetch")!=null)
                submitForm.setAnswer("muc#maxhistoryfetch", 0);
                */

            if (submitForm.getField("muc#roomconfig_enablelogging")!=null)
                submitForm.setAnswer("muc#roomconfig_enablelogging", false);

            /*
            if (submitForm.getField("muc#maxhistoryfetch")!=null)
                submitForm.setAnswer("muc#maxhistoryfetch", 0);
                */
            muc.sendConfigurationForm(submitForm);

            for(int i=0;i<owners.size();i++)
            {
                String userJid = owners.get(i) + "@" + getServiceName(connection);
                //muc.grantOwnership(userJid);
                muc.grantMembership(userJid);
                muc.invite(userJid,"Invitation to room" + chatRoom);

            }
        }
        catch (Exception ex)
        {
            String message  = ex.getMessage();
            Log.d("CreateChatRoomException",message);

            result = -2;
        }

        return result;
    }

    public static int  joinTheChatRoomFromMesgCmd(XMPPTCPConnection connection, String chatRoom)
    {
        Log.d(FILENAME, "leaveTheChatRoom");
        if(connection == null || chatRoom == null || (chatRoom.length() == 0) )
            return -1;

        int result = 0;

        String chatRoomJid = chatRoom + "@" + getServiceName(connection);
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getInstanceFor returned null");
            return -1;
        }

        MultiUserChat muc = manager.getMultiUserChat(chatRoomJid);
        if(muc == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getMultiUserChat returned null");
            return -1;
        }

        try {
            if(!muc.isJoined())
            {
                muc.join(ChatXMPPService.mDisplayName);
            }
            muc.addMessageListener(new MessageListener() {
                @Override
                public void processMessage(Message message) {

                }
            });
        }
        catch (Exception ex)
        {
            result = -2;
        }

        return result;
    }


    public static int  leaveTheChatRoom(XMPPTCPConnection connection, String chatRoom)
    {
        Log.d(FILENAME, "leaveTheChatRoom");
        if(connection == null || chatRoom == null || (chatRoom.length() == 0) )
            return -1;

        int result = 0;

        String chatRoomJid = chatRoom + "@" + getServiceName(connection);
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getInstanceFor returned null");
            return -1;
        }

        MultiUserChat muc = manager.getMultiUserChat(chatRoomJid);
        if(muc == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getMultiUserChat returned null");
            return -1;
        }

        try {
            if(muc.isJoined())
            {
                muc.leave();
            }

        }
        catch (Exception ex)
        {
            result = -2;
        }

        return result;
    }


    public static List<Affiliate>  getMembersOfChatRoom(XMPPTCPConnection connection, String chatRoom)
    {
        Log.d(FILENAME, "getMembersOfChatRoom");
        List<Affiliate> list = null;

        if(connection == null || chatRoom == null || (chatRoom.length() == 0) )
        {
            Log.d(FILENAME, "connection, or roonName is null");
            return list;
        }


        String chatRoomJid = chatRoom + "@" + getServiceName(connection);
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getInstanceFor returned null");
            return list;
        }

        MultiUserChat muc = manager.getMultiUserChat(chatRoomJid);
        if(muc == null)
        {
            Log.d(FILENAME," MultiUserChatManager.getMultiUserChat returned null");
            return list;
        }

        if(!muc.isJoined())
        {
            Log.d(FILENAME, "room not joined - joining again");
            try
            {
                muc.join(ChatXMPPService.mUserName);
            }
            catch(Exception e)
            {
                String msg = e.getMessage();
                Log.d(FILENAME, "muc - joining exception " + msg);
            }
            //return list;
        }


        try {
            List<Affiliate> adminsMembers = muc.getAdmins();
            List<Affiliate> ownerMembers = muc.getOwners();
            List<Affiliate> members = muc.getMembers();
            List<Affiliate> outcastsMembers = muc.getOutcasts();
            for( int i = 0;i<adminsMembers.size();i++)
            {
                if(list == null)
                {
                    list = adminsMembers;
                    break;
                }
                else
                {
                    list.add(adminsMembers.get(i));
                }

            }

            for( int i = 0;i<ownerMembers.size();i++)
            {
                if(list == null)
                {
                    list = ownerMembers;
                    break;
                }
                else
                {
                    list.add(ownerMembers.get(i));
                }

            }

            for( int i = 0;i< members.size();i++)
            {


                if(list == null)
                {
                    list = members;
                    break;
                }
                else
                {
                    list.add(members.get(i));
                }
            }

            //Commenting below code, outcast means members who have been banned out of room
//            for( int i = 0;i<outcastsMembers.size();i++)
//            {
//
//                if(list == null)
//                {
//                    list = outcastsMembers;
//                    break;
//                }
//                else
//                {
//                    list.add(outcastsMembers.get(i));
//                }
//            }


        }
        catch (Exception ex)
        {
            String msg = ex.getMessage();
            Log.d(FILENAME, "Unable to get members:" + msg);
        }

        return list;
    }

    /*
    Verified, working fine: Neeraj
     */
    public static boolean  destroyChatRoom(XMPPTCPConnection connection, String chatRoom)
    {
        Log.d(FILENAME, "destroyChatRoom");
        if(connection == null || chatRoom == null || (chatRoom.length() == 0) )
            return false;


        String chatRoomJid = chatRoom + "@" + getServiceName(connection);
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getInstanceFor returned null");
            return false;
        }

        MultiUserChat muc = manager.getMultiUserChat(chatRoomJid);
        if(muc == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getMultiUserChat returned null");
            return false;
        }

        if(!muc.isJoined())
        {
            Log.d(FILENAME, "Not joined");
            return false;
        }


        try {
            muc.destroy("",chatRoomJid);
            return true;
        }
        catch (Exception ex)
        {
            String msg = ex.getMessage();
            Log.d("inviteUsersAsOwner",msg);
        }

        return false;
    }


    /*
    Verified, working fine : Neeraj
     */
    
    public static int  inviteUsersWithAffiliation(XMPPTCPConnection connection, String chatRoom,List<String> users,AFFILIATION_TYPE type)
    {
        Log.d(FILENAME, "inviteUsersWithAffiliation");
        if(connection == null || chatRoom == null || (chatRoom.length() == 0)|| users == null||(users.size() ==0) )
            return -1;

        int result = 0;

        String chatRoomJid = chatRoom + "@" + getServiceName(connection);
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getInstanceFor returned null");
            return -1;
        }

        MultiUserChat muc = manager.getMultiUserChat(chatRoomJid);
        if(muc == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getMultiUserChat returned null");
            return -1;
        }

        if(!muc.isJoined())
        {
            return -1;
        }


        try {
            for(int i =0;i<users.size();i++){
                String userJid = users.get(i) + "@" + getServiceName(connection);
                if(type == AFFILIATION_TYPE.AFFILIATION_ADMIN)
                {
                    muc.grantAdmin(userJid);
                    muc.invite(userJid,"Invitation to join " + chatRoom);
                }else if(type == AFFILIATION_TYPE.AFFILIATION_MEMBER)
                {
                    muc.grantMembership(userJid);
                    muc.invite(userJid,"Invitation to join " + chatRoom);
                }else if(type == AFFILIATION_TYPE.AFFILIATION_OWNER)
                {
                    muc.grantOwnership(userJid);
                    muc.invite(userJid,"Invitation to join " + chatRoom);
                }
                else
                {
                    continue;
                }

            }

        }
        catch (Exception ex)
        {
            String msg = ex.getMessage();
            Log.d("inviteUsersAsOwner",msg);
            result = -2;
        }

        return result;
    }


    public static int  kickUserFromTheChatRoom(XMPPTCPConnection connection, String chatRoom,String userNickName)
    {
        Log.d(FILENAME, "kickUserFromTheChatRoom");
        if(connection == null || chatRoom == null || (chatRoom.length() == 0) )
            return -1;

        int result = 0;

        String chatRoomJid = chatRoom + "@" + getServiceName(connection);
        //String userJid = userName + "@" + getServiceName();
        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getInstanceFor returned null");
            return -1;
        }

        MultiUserChat muc = manager.getMultiUserChat(chatRoomJid);
        if(muc == null)
        {
            Log.d("MULTIUSERCHAT"," MultiUserChatManager.getMultiUserChat returned null");
            return -1;
        }


        try {
            muc.kickParticipant(userNickName,"");
        }
        catch (Exception ex)
        {
            result = -2;
        }

        return result;
    }

    public interface RoomInvitationCallback
    {
        public void OnRoomInvitationRecieve(MultiUserChat room,String inviter,String reason,String password,Message message);
        public void OnRemoveRoomInvitationRecieve(MultiUserChat room,String inviter,String reason,String password,Message message);
    }

    public interface GroupChatInActionCallback
    {
        public void processParticipantPresence(Presence presence);
        public void processSubjectChange(String subject,String from);
        public void processRemoveMessage(Message message);
        public void processInvitationDeclined(String invitee, String reason);
        public void processRecievedMessage(Message message);
    }

    public static  void setUpGroupChatInActionListeners(MultiUserChat room, final GroupChatInActionCallback mCallback)
    {
        Log.d(FILENAME, "setUpGroupChatInActionListeners");
        room.addParticipantListener(new PresenceListener() {
            @Override
            public void processPresence(Presence presence) {
                Log.d(FILENAME, "processPresence");
                if(mCallback != null)
                    mCallback.processParticipantPresence(presence);
            }
        });

        room.addSubjectUpdatedListener(new SubjectUpdatedListener() {
            @Override
            public void subjectUpdated(String subject, String from) {
                Log.d(FILENAME, "subjectUpdated");
                if(mCallback != null)
                    mCallback.processSubjectChange(subject,from);
            }
        });

        room.removeMessageListener(new MessageListener() {
            @Override
            public void processMessage(Message message) {
                Log.d(FILENAME, "removeMessageListener processMessage");
                if(mCallback != null)
                    mCallback.processRemoveMessage(message);
            }
        });

        room.addInvitationRejectionListener(new InvitationRejectionListener() {
            @Override
            public void invitationDeclined(String invitee, String reason) {
                Log.d(FILENAME, "invitationDeclined");
                if(mCallback != null)
                    mCallback.processInvitationDeclined(invitee,reason);
            }
        });

        room.addMessageListener(new MessageListener() {
            @Override
            public void processMessage(Message message) {
                Log.d(FILENAME, "processMessage");
                String from = message.getFrom();
                String args[] = from.split("/");
                if(args.length == 2)
                {
                    if( args[1].equalsIgnoreCase(ChatXMPPService.mUserName))
                    {
                        /* This is a loopback message */
                        return;
                    }
                }
                if(mCallback != null)
                    mCallback.processRecievedMessage(message);
            }
        });



        room.addUserStatusListener(new UserStatusListener() {
            @Override
            public void kicked(String actor, String reason) {
                Log.d(FILENAME, "kicked");

            }

            @Override
            public void voiceGranted() {
                Log.d(FILENAME, "voiceGranted");
            }

            @Override
            public void voiceRevoked() {
                Log.d(FILENAME, "voiceRevoked");
            }

            @Override
            public void banned(String actor, String reason) {
                Log.d(FILENAME, "banned");
            }

            @Override
            public void membershipGranted() {
                Log.d(FILENAME, "membershipGranted");
            }

            @Override
            public void membershipRevoked() {
                Log.d(FILENAME, "membershipRevoked");
            }

            @Override
            public void moderatorGranted() {
                Log.d(FILENAME, "moderatorGranted");
            }

            @Override
            public void moderatorRevoked() {
                Log.d(FILENAME, "moderatorRevoked");
            }

            @Override
            public void ownershipGranted() {
                Log.d(FILENAME, "ownershipGranted");
            }

            @Override
            public void ownershipRevoked() {
                Log.d(FILENAME, "ownershipRevoked");
            }

            @Override
            public void adminGranted() {
                Log.d(FILENAME, "adminGranted");
            }

            @Override
            public void adminRevoked() {
                Log.d(FILENAME, "adminRevoked");
            }
        });
    }

    public static MultiUserChat getMultiUserChatInstanceForRoom(String roomName,String myNickName, XMPPTCPConnection connection)
    {
        Log.d(FILENAME, "getMultiUserChatInstanceForRoom");
        boolean ifRoomExists = false;
        if(connection == null)
        {
            Log.d(FILENAME, "connection is null");
            return null;
        }

        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d(FILENAME, "manager is null");
            return null;
        }

        String chatRoomJid = roomName + "@" + getServiceName(connection);
        MultiUserChat multiChat = manager.getMultiUserChat(chatRoomJid);

        /* Joining the room is must, otherwise listeners would not work */
        if(multiChat != null)
        {
            try {
                if (multiChat.isJoined() == false) {
                    Log.d(FILENAME, "Joining room");
                    multiChat.join(myNickName);
                }
                ifRoomExists = true;
            }
            catch (Exception ex)
            {
                String msg = ex.getMessage();
                Log.d(FILENAME, "OnRoomJoin " + msg);
            }
            return multiChat;
        }

        /* In case unable to join, there is not signification of object of MultiUserChat i.e. multiChat */
        if(ifRoomExists)
        {
            return multiChat;
        }
        else
        {
            Log.d(FILENAME, "room doesn't exists");
            return null;
        }
    }


    /*
      roomNameJID : room@service
     */
    public static MultiUserChat getMultiUserChatInstanceForRoomJID(String roomNameJID,String myNickName, XMPPTCPConnection connection)
    {
        Log.d(FILENAME, "getMultiUserChatInstanceForRoomJID");
        boolean ifRoomExists = false;
        if(connection == null)
        {
            Log.d(FILENAME, "connection is null");
            return null;
        }

        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d(FILENAME, "manager is null");
            return null;
        }

        MultiUserChat multiChat = manager.getMultiUserChat(roomNameJID);

        /* Joining the room is must, otherwise listeners would not work */
        if(multiChat != null)
        {
            try {
                multiChat.join(myNickName);
                ifRoomExists = true;
            }
            catch (Exception ex)
            {
                String msg = ex.getMessage();
                Log.d(FILENAME, "OnRoomJoin" + msg);
            }
        }

        /* In case unable to join, there is not signification of object of MultiUserChat i.e. multiChat */
        if(ifRoomExists) {
            return multiChat;
        }
        else
        {
            Log.d(FILENAME, "room doesn't exists");
            return null;
        }
    }

    /*
    returns the set of roomJId i.e like room@muc.jabber.org
     */
    public static Set<String> getJoinedRooms(XMPPTCPConnection connection)
    {
        Log.d(FILENAME, "getJoinedRooms");
        if(connection == null)
        {
            Log.d(FILENAME, "connection is null");
            return null;
        }

        MultiUserChatManager manager = MultiUserChatManager.getInstanceFor(connection);
        if(manager == null)
        {
            Log.d(FILENAME, "manager is null");
            return null;
        }

        Set<String> rooms = manager.getJoinedRooms();
        return rooms;
    }
    public static void setUpRoomInvitationListener(XMPPTCPConnection connection, final RoomInvitationCallback mCallback)
    {
        Log.d(FILENAME, "setUpRoomInvitationListener");
        MultiUserChatManager.getInstanceFor(connection).addInvitationListener(new InvitationListener() {
              @Override
              public void invitationReceived(XMPPConnection conn, MultiUserChat room, String inviter, String reason, String password, Message message) {
                  Log.d(FILENAME, "invitationReceived");
                  //Tested, this works
                  try {
                      room.join(OpenfireRestAPI.getDisplayName());
                      if (mCallback != null) {
                          Log.d(FILENAME, "mCallback is null");
                          mCallback.OnRoomInvitationRecieve(room, inviter, reason, password, message);
                      }
                  } catch (Exception ex) {
                      String msg = ex.getMessage();
                      Log.d("GroupChatInvite", msg);
                  }

                                                                                      //Leaving following commented code as it is a test code
//                try {
//                    room.join("Maharaj");
//                    setUpGroupChatInActionListeners(room, null);
//                    room.sendMessage(new Message("Hellow"));
//                }
//                catch (Exception ex)
//                {
//                    String msg = ex.getMessage();
//                    Log.d("MultiUserChat",msg);
//
//
//                }
                                                                                  }
                                                                              }

        );

            MultiUserChatManager.getInstanceFor(connection).

            removeInvitationListener(new InvitationListener() {
                                         @Override
                                         public void invitationReceived(XMPPConnection conn, MultiUserChat room, String
                                                 inviter, String reason, String password, Message message) {
                                             Log.d(FILENAME, "invitationReceived");
                                             if (mCallback != null)
                                             {
                                                 Log.d(FILENAME, "callback is null");
                                                 mCallback.OnRemoveRoomInvitationRecieve(room, inviter, reason, password, message);
                                             }

                                         }

                                     }

            );
        }


    }
