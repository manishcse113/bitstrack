package com.bitskeys.track.chatRoom;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.TextView;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.gen.BitMapWorkerRequest;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.users.UserObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Manish on 20-02-2016.
 */
public class CreateRoomAdapter extends ArrayAdapter {

    public static List userObjList = new ArrayList();
    Context context;
    final String FILENAME = "GBC CreateRoomAdapter";
    int groupid;

    public CreateRoomAdapter(Context context, int vg, List userObjList) {
        super(context, vg, userObjList);
        this.context = context;
        groupid = vg;
        this.userObjList.clear();
        this.userObjList.addAll(userObjList);
        sortUserObjList();
    }


    public void sortUserObjList() {
        //first sort based on displayname
        Collections.sort(userObjList, new Comparator<UserObject>() {
            @Override
            public int compare(final UserObject object1, final UserObject object2) {
                return object1.userDisplayName.compareTo(object2.userDisplayName);
            }
        });

        //now sort on selected
        Collections.sort(userObjList, new Comparator<UserObject>() {
            @Override
            public int compare(final UserObject object1, final UserObject object2)
            {
                if (object1.isSelected == object2.isSelected)
                {
                    return 0;
                }
                else
                {
                    return 1;
                }
            }
        });
        //userObjList.t
    }

    //@Override
    public void add(UserObject object) {
        userObjList.add(object);
        super.add(object);
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // View itemView = inflater.inflate(groupid, parent, false);
        //View fragment_list_view = inflater.inflate(R.layout.fragment_chatusers_list, null);
        //Log.d(FILENAME, "getView position#"+ position + " view#"+convertView);
        View row = convertView;
        if (row == null) {
            //LayoutInflater inflater = (LayoutInflater) this.getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            row = inflater.inflate(R.layout.createroomlistlayout, parent, false);

            //row = inflater.inflate(R.layout.createroomlistlayout, (ViewGroup) fragment_list_view, false);
            populateUserListLayout(inflater, row, position);

        } else {
            //You get here in case of scroll
            populateUserListLayout(inflater, row, position);

        }
        //((ViewGroup) fragment_list_view).addView(row);
        return row;
    }

    public View populateUserListLayout(LayoutInflater inflater, final View user_view, final int position) {
        //Log.d(FILENAME, "inflateUserListLayout");
        try {

            ImageView avatarView = (ImageView) user_view.findViewById(R.id.avatar);
            TextView textuserName = (TextView) user_view.findViewById(R.id.username);
            String userDisplayName = textuserName.getText().toString();
            userDisplayName = ((UserObject) (userObjList.get(position))).userDisplayName;

            final String remoteUserName = ChatXMPPService.getUserName(userDisplayName);


            final UserObject uObj = (UserObject) (userObjList.get(position));
            CheckBox cb = (CheckBox) user_view.findViewById(R.id.checkBox1);

            if(cb!=null) {
                if (uObj.isAddedToGroup()) {
                    //cb.setSelected(true);
                    cb.setChecked(true);
                } else {
                    cb.setChecked(false);
                    //cb.setSelected(false);
                }
                cb.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View v) {
                        CheckBox cb = (CheckBox) v;
                        if (uObj.isAddedToGroup()) {
                            //cb.setSelected(false);
                            uObj.setAddtogroup(false);
                        } else {
                            //cb.setSelected(true);
                            uObj.setAddtogroup(true);
                        }
                    }
                });
            }

            avatarView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });

            //Bitmap avatarBitmap = Utility.getSavedBitmapForUser(remoteUserName, context);
            Bitmap avatarBitmap = uObj.getMyPhoto();
            if (avatarBitmap != null) {
                avatarView.setImageBitmap(null);
                avatarView.setImageBitmap(avatarBitmap);
            } else {
                avatarView.setImageResource(R.drawable.avatar);
                BitMapWorkerRequest.AddAndStartBitMapWorker(context.getResources(),
                        remoteUserName,
                        null,
                        avatarView.getWidth(),
                        avatarView.getHeight(),
                        null,
                        null,
                        null,
                        null,
                        null,
                        R.id.avatar, user_view.getContext(), null);
            }

            textuserName = (TextView) user_view.findViewById(R.id.username);
            userDisplayName = ((UserObject) (userObjList.get(position))).userDisplayName; //(String)userDisplayNamelist.get(position);
            // Convert the first Character to uppercase.

            String formatDisplayName = Utility.FormatStringToUserName(userDisplayName);

            if (userDisplayName != null && userDisplayName.length() > 0) {
//                textuserName.setText(userDisplayName);
                textuserName.setText(formatDisplayName);
            } else {
                textuserName.setText(((UserObject) (userObjList.get(position))).userName);//(String)userNamelist.get(position));
            }
            textuserName.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Log.d(FILENAME, "onLongClick");
                    ((Activity) context).openContextMenu(v);
                    return true;
                }
            });

            textuserName.setOnClickListener(new View.OnClickListener() {
                                                public void onClick(View arg0) {
                                                    Log.d(FILENAME, "onClickListener");
                                                    String displayName = ((TextView) (arg0)).getText().toString();
                                                    String lremoteUserName = ChatXMPPService.getUserName(displayName);
                                                    return;
                                                }
                                            }
            );


        } catch (Exception e) {

            Log.d(FILENAME, " Exception " + e.getMessage());
        }
        return user_view;
    }
}