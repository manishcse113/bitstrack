package com.bitskeys.track.chatRoom;

import android.util.Log;

import org.jivesoftware.smack.packet.Message;
import org.jivesoftware.smack.packet.Presence;
import org.jivesoftware.smackx.muc.MultiUserChat;

/**
 * Created by Mr.Neeraj on 26-05-2016.
 */
public class RoomListeners implements MultiChat.RoomInvitationCallback, MultiChat.GroupChatInActionCallback {

    String roomName = null;

    @Override
    public void processParticipantPresence(Presence presence) {

    }

    @Override
    public void processSubjectChange(String subject, String from) {

    }

    @Override
    public void processRemoveMessage(Message message) {

    }

    @Override
    public void processInvitationDeclined(String invitee, String reason) {

    }

    @Override
    public void processRecievedMessage(Message message) {

        Log.d("Message","Recieved");
    }

    public RoomListeners(String roomName) {
        this.roomName = roomName;
    }

    @Override
    public void OnRoomInvitationRecieve(MultiUserChat room, String inviter, String reason, String password, Message message) {

    }

    @Override
    public void OnRemoveRoomInvitationRecieve(MultiUserChat room, String inviter, String reason, String password, Message message) {

    }
}

