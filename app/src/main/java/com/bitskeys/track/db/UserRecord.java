package com.bitskeys.track.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;
import java.util.Vector;

public class UserRecord extends DBObject
{
	private static String FILENAME = "UserRecord";
	private final static String DB_TABLE = "UserRecords";

	private final static String COLUMN_USER_NAME = "user_name";
	private final static String COLUMN_GROUP_NAME = "group_name";
	private final static String COLUMN_EMAILID = "emailId";
	private final static String COLUMN_AVATAR = "avatar";
	private final static String COLUMN_DISPLAY_NAME = "disp_name";

	private final static String[] ALL_COLUMNS = new String[] {
		COLUMN_ID,
			COLUMN_USER_NAME,
			COLUMN_GROUP_NAME,
			COLUMN_EMAILID,
			COLUMN_AVATAR,
			COLUMN_DISPLAY_NAME
		/*
		COLUMN_PHONE_NUMBER,
		COLUMN_VALIDATION_CODE_1,
		COLUMN_VALIDATION_CODE_2,
		COLUMN_CREATION_TIMESTAMP,
		COLUMN_AUTH_PASSWORD
		*/
	};

	private final static String CREATE_TABLE =
			"CREATE TABLE " + DB_TABLE + " (" +
			COLUMN_ID + " integer, " +
			COLUMN_USER_NAME  + " text primary key, " +
			COLUMN_GROUP_NAME + " text, " +
			COLUMN_EMAILID + " text, " +
			COLUMN_AVATAR + " text, " +
					COLUMN_DISPLAY_NAME + " text " +

					/*
			COLUMN_PHONE_NUMBER + " text, " +
			COLUMN_VALIDATION_CODE_1 + " text, " +
			COLUMN_VALIDATION_CODE_2 + " text, " +
			COLUMN_CREATION_TIMESTAMP + " integer not null, " +
			COLUMN_AUTH_PASSWORD + " text not null	*/
			");";

	String userName = null;
	String displayName = null;
	String groupName = null;
	String emailId = null;
	String  avatar = null;
	/*
	String phoneNumber = null;
	String validationCode1 = null;
	String validationCode2 = null;
	long creationTimestamp = 0;
	String authPassword = null;
	*/

	public static void onCreate(SQLiteDatabase db) {
		db.execSQL(CREATE_TABLE);
	}

	public static void onUpgrade(SQLiteDatabase db, int oldVersion,
			int newVersion) {
		if(oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
			onCreate(db);
		}
	}

	public UserRecord() {
		//setCreationTimestamp(System.currentTimeMillis() / 1000);
	}
	
	public static List<UserRecord> loadFromDatabase(SQLiteDatabase db) {
		Vector<UserRecord> v = new Vector<UserRecord>();
		
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, null,
				null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;
	}
	
	public static UserRecord loadFromDatabase(SQLiteDatabase db, long id) {
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_ID + " = " + id,
				null, null, null, null);
		cursor.moveToFirst();
		UserRecord req = null;
		if(!cursor.isAfterLast())
			 req = fromCursor(cursor);
		cursor.close();
		return req;
	}

	//userName All chat records
	public static List<UserRecord>  loadFromDatabase(SQLiteDatabase db, String userName) {
		Vector<UserRecord> v = new Vector<UserRecord>();
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_USER_NAME +  " =?", new String[]{userName},
				null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;
	}

	public String getDisplayName()
	{
		return displayName;
	}

	public void setDisplayName(String displayName)
	{
		this.displayName = displayName;
	}
	private static UserRecord fromCursor(Cursor cursor) {
		UserRecord req = new UserRecord();
		int i = 0;
		req.setId(cursor.getLong(i++));
		req.setUserName(cursor.getString(i++));
		req.setGroupName(cursor.getString(i++));
		req.setEmailId(cursor.getString(i++));
		req.setAvatar(cursor.getString(i++));
		req.setDisplayName(cursor.getString(i++));


		//req.

		/*
		req.setId(cursor.getLong(i++));
		req.setPhoneNumber(cursor.getString(i++));
		req.setValidationCode1(cursor.getString(i++));
		req.setValidationCode1(cursor.getString(i++));
		req.setCreationTimestamp(cursor.getLong(i++));
		req.setAuthPassword(cursor.getString(i++));
		*/
		
		return req;
	}

	public final static String USER_RECORD_ID = "userRecord";
	
	@Override
	protected String getTableName() {
		return DB_TABLE;
	}
	
	@Override
	protected void putValues(ContentValues values) {
		values.put(COLUMN_USER_NAME, getUserName());
		values.put(COLUMN_GROUP_NAME, getGroupName());
		values.put(COLUMN_EMAILID, getEmailId());
		values.put(COLUMN_AVATAR, getAvatar());
		values.put(COLUMN_DISPLAY_NAME,getDisplayName());

		/*
		values.put(COLUMN_PHONE_NUMBER, getPhoneNumber());
		values.put(COLUMN_VALIDATION_CODE_1, getValidationCode1());
		values.put(COLUMN_VALIDATION_CODE_2, getValidationCode2());
		values.put(COLUMN_CREATION_TIMESTAMP, getCreationTimestamp());
		values.put(COLUMN_AUTH_PASSWORD, getAuthPassword());
		*/
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGroupName()
	{
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getEmailId()
	{
		return emailId;
	}

	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getAvatar()
	{
		return avatar;
	}

	public void setAvatar(String avatar) {
		this.avatar = avatar;
	}

	/*
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getValidationCode1() {
		return validationCode1;
	}

	public void setValidationCode1(String validationCode1) {
		this.validationCode1 = validationCode1;
	}

	public String getValidationCode2() {
		return validationCode2;
	}

	public void setValidationCode2(String validationCode2) {
		this.validationCode2 = validationCode2;
	}

	public long getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(long creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getAuthPassword() {
		return authPassword;
	}

	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}
	*/

	@Override
	public String getTitleForMenu() {
		return getId() + "";
	}

	@Override
	public String getKeyForIntent() {
		return USER_RECORD_ID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		/*
		result = prime * result
				+ ((authPassword == null) ? 0 : authPassword.hashCode());
		result = prime * result
				+ (int) (creationTimestamp ^ (creationTimestamp >>> 32));
		result = prime * result
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result
				+ ((validationCode1 == null) ? 0 : validationCode1.hashCode());
		result = prime * result
				+ ((validationCode2 == null) ? 0 : validationCode2.hashCode());
		*/
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof UserRecord))
			return false;
		UserRecord other = (UserRecord) obj;

		if (userName == null) {
			if (other.userName!=null)
				return false;
		} else if (!userName.equals(other.userName)) {
			return false;
		}

		if (groupName == null) {
			if (other.groupName!=null)
				return false;
		} else if (!groupName.equals(other.groupName)) {
			return false;
		}

		if (emailId == null) {
			if (other.emailId!=null)
				return false;
		} else if (!emailId.equals(other.emailId)) {
			return false;
		}

		if (avatar == null) {
			if (other.avatar!=null)
				return false;
		} else if (!avatar.equals(other.avatar)) {
			return false;
		}

		if (displayName == null) {
			if (other.displayName!=null)
				return false;
		} else if (!displayName.equals(other.displayName)) {
			return false;
		}


		/*
		if (authPassword == null) {
			if (other.authPassword != null)
				return false;
		} else if (!authPassword.equals(other.authPassword))
			return false;
		if (creationTimestamp != other.creationTimestamp)
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (validationCode1 == null) {
			if (other.validationCode1 != null)
				return false;
		} else if (!validationCode1.equals(other.validationCode1))
			return false;
		if (validationCode2 == null) {
			if (other.validationCode2 != null)
				return false;
		} else if (!validationCode2.equals(other.validationCode2))
			return false;
		*/
		return true;
	}


	public static void storeUniqueNameRecordNew(Context context, String userName,String displayName, String groupName, String emailId, String avatar) {

		DataSource db = new DataSource(context);
		try {
			db.open();
			ContentValues data=new ContentValues();
			data.put(COLUMN_ID,0); //putting ID as 0 for now:
			data.put(COLUMN_USER_NAME,userName);
			data.put(COLUMN_GROUP_NAME,groupName);
			data.put(COLUMN_EMAILID, emailId);
			data.put(COLUMN_AVATAR,avatar);
			data.put(COLUMN_DISPLAY_NAME,displayName);
			//db.db.update(DB_TABLE, data, COLUMN_USER_NAME + " like " + "'" + userName + "'", null);
			String sqlQuery =
			"insert or replace into "+DB_TABLE+ " (" +
					COLUMN_ID + "," +
					COLUMN_USER_NAME + "," +
							COLUMN_GROUP_NAME + "," +
							COLUMN_EMAILID + "," +
							COLUMN_AVATAR + "," +
							COLUMN_DISPLAY_NAME +
							") values " +
							"(" +
								"( select " + COLUMN_ID + " from " + DB_TABLE + " where " + COLUMN_USER_NAME + " = " + "'" + userName + "'" + ")," +
					"'" + userName +  "'" + "," +
					"'" + 		groupName + "'" + "," +
					"'" + 		emailId + "'" + "," +
					"'" + 			avatar + "'" + "," +
					"'" + 			displayName + "'" +
							");";
			db.db.execSQL(sqlQuery);
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
			db.close();
		}
	}


	public static void storeNameRecord(Context context, String userName,String displayName, String groupName, String emailId, String avatar) {
		UserRecord req = new UserRecord();
		req.setUserName(userName);
		req.setGroupName(groupName);
		req.setEmailId(emailId);
		req.setAvatar(avatar);
		req.setDisplayName(displayName);
		DataSource db = new DataSource(context);

		try {
			db.open();
			db.persistUserRecord(req);
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
		}
	}

	public static void storeUniqueNameRecord(Context context, String userName,String displayName, String groupName, String emailId, String avatar) {
		UserRecord req = new UserRecord();
		req.setUserName(userName);
		req.setGroupName(groupName);
		req.setEmailId(emailId);
		req.setAvatar(avatar);
		req.setDisplayName(displayName);
		DataSource db = new DataSource(context);

		try {
			db.open();

			boolean recordExists = false;
			List<UserRecord> reqs = db.getUserRecords(userName);
			for (UserRecord _req : reqs) {
				if (_req.getUserName().equalsIgnoreCase(userName) == true) {
					recordExists = true;
					break;
				}
			}
			if (recordExists == false) {
				db.persistUserRecord(req);
			}
			db.close();
		}
		catch(Exception exception)
		{
			exception.printStackTrace();
			Log.e(FILENAME, "Exception in opeing db");
			db.close();
		}
	}

	public static void clearFromDB(Context context, String userName)
	{
		DataSource db = new DataSource(context);
		db.open();
		List<UserRecord> reqs = db.getUserRecords(userName);
		for(UserRecord _req : reqs) {
			db.deleteUserRecord(_req);
		}
		db.close();
	}

	public static void clearFromDB(Context context)
	{
		DataSource db = new DataSource(context);
		db.open();
		List<UserRecord> reqs = db.getUserRecords();
		//for(UserRecord _req : reqs) {
			deleteUser(db.db);
		//}
		db.close();
	}

	public static void deleteUser(SQLiteDatabase db) {
		//if(getId() != -1) {
			db.execSQL("DELETE FROM " + DB_TABLE +
					";");
		//}
		//setId(-1);
	}

}
