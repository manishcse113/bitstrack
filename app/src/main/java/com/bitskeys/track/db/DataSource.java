package com.bitskeys.track.db;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;

import com.bitskeys.track.chatRoom.ChatRoomInfo;

import java.util.HashMap;
import java.util.List;

//Manish: Added for CTalk

public class DataSource {

	// Database fields
	public SQLiteDatabase db;
	private DatabaseHelper helper;

	private static String FILENAME = "GBC DataSource";

	public DataSource(Context context) {
		helper = new DatabaseHelper(context);
	}

	public SQLiteDatabase getDb() {return db;}

	public void open() throws SQLException {
		db = helper.getWritableDatabase();
	}

	public void close() {
		helper.close();
	}

	public ChatRecord getChatRecord(long id) {

		//Log.d(FILENAME, "getChatRecord");
		return ChatRecord.loadFromDatabase(db, id);
	}

	public List<ChatRecord> getChatRecords() {
		//Log.d(FILENAME, "getChatRecords");

		return ChatRecord.loadFromDatabase(db);
	}

	public List<ChatRecord> getChatRecords(String userName) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName);
	}

	public List<ChatRecord> getChatRecords(String userName, String receiptStatus) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName, receiptStatus);
	}

	public List<ChatRecord> getChatRecords(String userName, String remoteUsername, boolean isGroup) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName, remoteUsername,isGroup);
	}

	public List<ChatRecord> getChatRecords(String userName, String remoteUsername, boolean isGroup, Long startDate) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabase(db, userName, remoteUsername, isGroup, startDate);
	}

	public List<ChatRecord> getChatRecords(String userName, String remoteUsername, boolean isGroup, String receiptId) {
		//Log.d(FILENAME, "getChatRecords");
		return ChatRecord.loadFromDatabaseReceiptId(db, userName, remoteUsername, isGroup, receiptId);
	}

	public void persistChatRecord(ChatRecord chatRecord) {
		//Log.d(FILENAME, "persistChatRecord");
		chatRecord.commit(db);
	}

	public void deleteChatRecord(ChatRecord chatRecord) {
		//Log.d(FILENAME, "deleteChatRecord");
		chatRecord.delete(db);
	}

	public void updateChatRecord (String userName, String remoteUserName, String receiptId, String receiptStatus)
	{
		//Log.d(FILENAME, "updateChatRecord");
		ChatRecord.LoadAndUpdateChatRecord(db, userName, remoteUserName, false, receiptId, receiptStatus);
	}

	public void updateChatRecord (String userName, String remoteUserName, String receiptId, String receiptStatus, long timeStamp)
	{
		//Log.d(FILENAME, "updateChatRecord");
		ChatRecord.LoadAndUpdateChatRecord(db, userName, remoteUserName, false, receiptId, receiptStatus, timeStamp);
	}



	public MyRecord getMyRecord(long id) {

		//Log.d(FILENAME, "getMyRecord");
		return MyRecord.loadFromDatabase(db, id);
	}

	public List<MyRecord> getMyRecords() {
		//Log.d(FILENAME, "getMyRecords");
		return MyRecord.loadFromDatabase(db);
	}

	public List<MyRecord> getMyRecords(String userName) {
		//Log.d(FILENAME, "getMyRecords");
		return MyRecord.loadFromDatabase(db, userName);
	}
	public void persistMyRecord(MyRecord myRecord) {

		//Log.d(FILENAME, "persistMyRecord");
		myRecord.commit(db);
	}

	public void deleteMyRecord(MyRecord myRecord) {
		//Log.d(FILENAME, "deleteMyRecord");
		myRecord.delete(db);
	}


	public void updateMyRecord (String userName, long trackingPreference)
	{
		//Log.d(FILENAME, "updateChatRecord");
		MyRecord.LoadAndUpdateMyRecord(db, userName, trackingPreference);
	}

	public UserRecord getUserRecord(long id) {
		//Log.d(FILENAME, "getUserRecord");
		return UserRecord.loadFromDatabase(db, id);
	}

	public List<UserRecord> getUserRecords() {
		//Log.d(FILENAME, "getUserRecords");
		return UserRecord.loadFromDatabase(db);
	}

	public List<UserRecord> getUserRecords(String userName) {
		//Log.d(FILENAME, "getUserRecords");
		return UserRecord.loadFromDatabase(db, userName);
	}
	public void persistUserRecord(UserRecord userRecord) {
		//Log.d(FILENAME, "persistUserRecords");
		userRecord.commit(db);
	}

	public void deleteUserRecord(UserRecord userRecord) {
		//Log.d(FILENAME, "delteUserRecords");
		userRecord.delete(db);
	}


	public List<ProfileImageRecord> getProfileImageRecords() {
		return ProfileImageRecord.loadFromDatabase(db);
	}

	public List<ProfileImageRecord> getProfileImageRecords(String userName) {
		return ProfileImageRecord.loadFromDatabase(db, userName);
	}

	public void persistProfileImageRecord(ProfileImageRecord profileImageRecord)
	{
		profileImageRecord.commit(db);
	}


	public void deleteProfileImageRecord(ProfileImageRecord profileImageRecord) {
		profileImageRecord.delete(db);
	}


	public LocationRecord getLocationRecord(long id) {
		return LocationRecord.loadFromDatabase(db, id);
	}

	public List<LocationRecord> getLocationRecords() {
		return LocationRecord.loadFromDatabase(db);
	}

	public List<LocationRecord> getLocationRecords(String userName) {
		return LocationRecord.loadFromDatabase(db, userName);
	}

	public void persistLocationRecord(LocationRecord locationRecord) {
		locationRecord.commit(db);
	}

	public void deleteLocationRecord(LocationRecord locationRecord) {
		locationRecord.delete(db);
	}


	public MyLocationRecord getMyLocationRecord(long id) {
		return MyLocationRecord.loadFromDatabase(db, id);
	}

	public List<MyLocationRecord> getMyLocationRecords(String recordedTime) {
		return MyLocationRecord.loadFromDatabase(db, recordedTime);
	}

	public List<MyLocationRecord> getMyLocationRecords() {
		return MyLocationRecord.loadFromDatabase(db);
	}

	public void persistMyLocationRecord(MyLocationRecord myLocationRecord) {
		myLocationRecord.commit(db);
	}

	public void deleteMyLocationRecord(MyLocationRecord myLocationRecord) {
		myLocationRecord.delete(db);
	}

	public RoomRecord getRoomRecord(long id) {
		return RoomRecord.loadFromDatabase(db, id);
	}

	public List<RoomRecord> getRoomRecords() {
		return RoomRecord.loadFromDatabase(db);
	}
	public ChatRoomInfo getChatRoomInfo(String roomName)
	{
		if(db == null)
			db  = helper.getWritableDatabase();

		return RoomRecordNew.loadFromDatabase(db,roomName);
	}

	public HashMap<String,ChatRoomInfo> getChatRoomInfoForAllChatRooms()
	{
		return RoomRecordNew.loadFromDatabase(db);
	}


	public List<RoomRecord> getRoomRecords(String userName) {
		return RoomRecord.loadFromDatabase(db, userName);
	}
	public void persistRoomRecord(RoomRecord roomRecord) {
		roomRecord.commit(db);
	}

	public void deleteRoomRecord(RoomRecord roomRecord) {
		roomRecord.delete(db);
	}


	/*
	public SIPIdentity getSIPIdentity(long id) {
		return SIPIdentity.loadFromDatabase(db, id);
	}
	
	public List<SIPIdentity> getSIPIdentities() {
		return SIPIdentity.loadFromDatabase(db);
	}
	
	public void persistSIPIdentity(SIPIdentity sipIdentity) {
		sipIdentity.commit(db);
	}
	
	public void deleteSIPIdentity(SIPIdentity sipIdentity) {
		sipIdentity.delete(db);
	}


	public List<PTTChannel> getPTTChannels() {
		return PTTChannel.loadFromDatabase(db);
	}

	public void deletePTTChannel(PTTChannel object) {
		object.delete(db);
	}

	public void persistPTTChannel(PTTChannel object) {
		object.commit(db);
	}

	public PTTChannel getPTTChannel(long id) {
		return PTTChannel.loadFromDatabase(db, id);
	}
	
	public List<ENUMSuffix> getENUMSuffixes() {
		return ENUMSuffix.loadFromDatabase(db);
	}

	public void deleteENUMSuffix(ENUMSuffix object) {
		object.delete(db);
	}

	public ENUMSuffix getENUMSuffix(long id) {
		return ENUMSuffix.loadFromDatabase(db, id);
	}

	public void persistENUMSuffix(ENUMSuffix object) {
		object.commit(db);
	}
	
	//Manish: Added for CTalk, removed sIP5060
	public CTalkRegisterRequest getCTalkRegisterRequest(long id) {
		return CTalkRegisterRequest.loadFromDatabase(db, id);
	}
	
	public List<CTalkRegisterRequest> getCTalkRegisterRequests() {
		return CTalkRegisterRequest.loadFromDatabase(db);
	}
	
	public void persistCTalkRegisterRequest(CTalkRegisterRequest cTalkRegisterRequest) {
		cTalkRegisterRequest.commit(db);
	}
	
	public void deleteCTalkRegisterRequest(CTalkRegisterRequest cTalkRegisterRequest) {
		cTalkRegisterRequest.delete(db);
	}
	
	
	
	public SIP5060ProvisioningRequest getSIP5060ProvisioningRequest(long id) {
		return SIP5060ProvisioningRequest.loadFromDatabase(db, id);
	}
	
	public List<SIP5060ProvisioningRequest> getSIP5060ProvisioningRequests() {
		return SIP5060ProvisioningRequest.loadFromDatabase(db);
	}
	
	public void persistSIP5060ProvisioningRequest(SIP5060ProvisioningRequest sIP5060ProvisioningRequest) {
		sIP5060ProvisioningRequest.commit(db);
	}
	
	public void deleteSIP5060ProvisioningRequest(SIP5060ProvisioningRequest sIP5060ProvisioningRequest) {
		sIP5060ProvisioningRequest.delete(db);
	}
	
	*/
}
