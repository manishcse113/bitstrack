package com.bitskeys.track.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;
import java.util.Vector;

/**
 * Created by Manish on 17-01-2016.
 */
public class ProfileImageRecord extends DBObject
{
    private static String FILENAME = "GBC ProfileImageRecord";
    private final static String DB_TABLE = "ProfileRecord";
    private final static String COLUMN_USER_NAME = "user_name";
    private final static String COLUMN_PROFILE_IMAGE = "profile_image";
    private final static String COLUMN_PROFILE_FIRST_NAME = "first_name";
    private final static String COLUMN_PROFILE_LAST_NAME = "last_name";
    private final static String COLUMN_PROFILE_ORG_NAME = "org_name";
    private final static String COLUMN_PROFILE_EMAIL_ID = "email_id";

    private final static String[] ALL_COLUMNS = new String[]{
            COLUMN_ID,
            COLUMN_USER_NAME,
            COLUMN_PROFILE_IMAGE,
            COLUMN_PROFILE_FIRST_NAME,
            COLUMN_PROFILE_LAST_NAME,
            COLUMN_PROFILE_ORG_NAME,
            COLUMN_PROFILE_EMAIL_ID
    };

    @Override
    public String getKeyForIntent()
    {
        return PROFILE_IMAGE_RECORD_ID;
    }

    private final static String CREATE_TABLE =
            "CREATE TABLE " + DB_TABLE + " (" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    COLUMN_USER_NAME + " text, " +
                    COLUMN_PROFILE_IMAGE + " BLOB, " +
                    COLUMN_PROFILE_FIRST_NAME + " text, " +
                    COLUMN_PROFILE_LAST_NAME + " text, " +
                    COLUMN_PROFILE_ORG_NAME + " text, " +
                    COLUMN_PROFILE_EMAIL_ID + " text " +
                    ");";

    String userName = null;
    byte[] imageBytes = null;
    String firstName = null;
    String lastName = null;
    String orgName = null;
    String emailId = null;

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    @Override
    public String getTitleForMenu() {
        return getId() + "";
    }


    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName)
    {
        this.userName = userName;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes)
    {
        this.imageBytes = imageBytes;
    }

    public long getId()
    {
        return id;
    }
    public void setId(long id)
    {
        this.id = id;
    }

    public static void onCreate(SQLiteDatabase db)
    {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion,
                                 int newVersion) {
        if (oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
            onCreate(db);
        }
    }

//
//    public static byte[] loadProfileImageFromDatabase(SQLiteDatabase db,String userName)
//    {
//        Cursor cursor = db.query(DB_TABLE,
//                ALL_COLUMNS,
//                COLUMN_USER_NAME + " = " + userName,
//                null,
//                null,
//                null,
//                null);
//
//        if(cursor != null)
//        {
//            String imageStrData = cursor.getString(2);//0 for Id, 1 for userName, 2 for imageByteStr
//            byte[] imageByte = null;
//
//            try
//            {
//                imageByte = imageStrData.getBytes("UTF8");
//                cursor.close();
//                return imageByte;
//            }
//            catch (Exception ex)
//            {
//                String msg = ex.getMessage();
//                Log.d("DEBUG", msg);
//            }
//            cursor.close();
//        }
//        return null;
//    }

//    public  byte[] getImageByte()
//    {
//        String imgStr = getImageByteStr();
//        byte[] imageByte = null;
//        if(imgStr != null && imgStr.length() >0)
//        {
//            try
//            {
//                imageByte = imgStr.getBytes("UTF8");
//            }
//            catch (Exception ex)
//            {
//                String msg = ex.getMessage();
//                Log.d("DEBUG", msg);
//            }
//        }
//        return imageByte;
//    }


    private static ProfileImageRecord fromCursor(Cursor cursor) {
        ProfileImageRecord req = new ProfileImageRecord();
        int i = 0;
        req.setId(cursor.getLong(i++));
        req.setUserName(cursor.getString(i++));
        req.setImageBytes(cursor.getBlob(i++));
        req.setFirstName(cursor.getString(i++));
        req.setLastName(cursor.getString(i++));
        req.setOrgName(cursor.getString(i++));
        req.setEmailId(cursor.getString(i++));
        return req;
    }



    public final static String PROFILE_IMAGE_RECORD_ID = "ProfileImageRecord";

    @Override
    protected String getTableName() {
        return DB_TABLE;
    }

    @Override
    protected void putValues(ContentValues values) {
        values.put(COLUMN_USER_NAME, getUserName());
        values.put(COLUMN_PROFILE_IMAGE, getImageBytes());
        values.put(COLUMN_PROFILE_FIRST_NAME, getFirstName());
        values.put(COLUMN_PROFILE_LAST_NAME, getLastName());
        values.put(COLUMN_PROFILE_ORG_NAME, getOrgName());
        values.put(COLUMN_PROFILE_EMAIL_ID, getEmailId());
    }


    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        return result;
    }



    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof ProfileImageRecord))
            return false;
        ProfileImageRecord other = (ProfileImageRecord) obj;

        if (userName == null) {
            if (other.userName!=null)
                return false;
        } else if (!userName.equals(other.userName)) {
            return false;
        }

        if (imageBytes == null) {
            if (other.imageBytes == null)
                return false;
        } else if (!imageBytes.equals(other.imageBytes)) {
            return false;
        }

        if (firstName == null) {
            if (other.firstName!=null)
                return false;
        } else if (!firstName.equals(other.firstName)) {
            return false;
        }

        if (lastName == null) {
            if (other.lastName!=null)
                return false;
        } else if (!lastName.equals(other.lastName)) {
            return false;
        }

        if (orgName == null) {
            if (other.orgName!=null)
                return false;
        } else if (!orgName.equals(other.orgName)) {
            return false;
        }

        if (emailId == null) {
            if (other.emailId!=null)
                return false;
        } else if (!emailId.equals(other.emailId)) {
            return false;
        }

        return true;
    }

    public static void storeProfileImageRecord(Context context, String userName,String firstName,String lastName,
                                               String emailId, String orgName,byte[] imageByte) {
        ProfileImageRecord req = new ProfileImageRecord();
        req.setUserName(userName);
        req.setImageBytes(imageByte);
        req.setFirstName(firstName);
        req.setLastName(lastName);
        req.setEmailId(emailId);
        req.setOrgName(orgName);

        /* Clear all previous records with userNam */
        clearFromDB(context,userName);

        /* Now save the record */
        DataSource db = new DataSource(context);
        try {
            db.open();
            db.persistProfileImageRecord(req);
        }

        catch(Exception e)
        {
            Log.e(FILENAME,"Exception in opening db in storeProfileImageRecord" );
            e.printStackTrace();
        }
        db.close();
    }


    public static List<ProfileImageRecord> loadFromDatabase(SQLiteDatabase db) {
        Vector<ProfileImageRecord> v = new Vector<ProfileImageRecord>();
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, null,
                null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }
    public static List<ProfileImageRecord>  loadFromDatabase(SQLiteDatabase db, String userName)
    {
        Vector<ProfileImageRecord> v = new Vector<ProfileImageRecord>();
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_USER_NAME +  " =?", new String[]{userName},
                null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }
    public static void clearFromDB(Context context, String userName)
    {
        DataSource db = new DataSource(context);
        try {
            db.open();
            List<ProfileImageRecord> reqs = db.getProfileImageRecords(userName);
            for (ProfileImageRecord _req : reqs) {
                db.deleteProfileImageRecord(_req);
            }
        }
        catch(Exception e)
        {
            Log.e(FILENAME,"Exception in opening db in clearFromDB" );
            e.printStackTrace();
        }
        db.close();
    }

    public static void clearFromDB(Context context)
    {
        DataSource db = new DataSource(context);
        try
        {
            db.open();
            List<ProfileImageRecord> reqs = db.getProfileImageRecords();
            for(ProfileImageRecord _req : reqs) {
                db.deleteProfileImageRecord(_req);
            }
        }
        catch(Exception e)
        {
            Log.e(FILENAME,"Exception in opening db in clearFromDB" );
            e.printStackTrace();
        }
        db.close();
    }

}

