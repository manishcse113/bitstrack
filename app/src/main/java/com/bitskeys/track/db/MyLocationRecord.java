package com.bitskeys.track.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;
import java.util.Vector;

/**
 * Created by Manish on 27-01-2016.
 */
public class MyLocationRecord extends DBObject {

    private final static String DB_TABLE = "MyLocationRecord";

    private final static String COLUMN_USER_NAME = "user_name";
    private final static String COLUMN_GROUP_NAME = "group_name";
    private final static String COLUMN_DISPLAY_NAME = "disp_name";
    private final static String COLUMN_LATITUDE = "latitude";
    private final static String COLUMN_LONGITUDE = "longitude";
    private final static String COLUMN_RECORDED_TIME = "recorded_time";

    private final static String[] ALL_COLUMNS = new String[]{
            COLUMN_ID,
            COLUMN_USER_NAME,
            COLUMN_GROUP_NAME,
            COLUMN_DISPLAY_NAME,
            COLUMN_LATITUDE,
            COLUMN_LONGITUDE,
            COLUMN_RECORDED_TIME
    };

    private final static String CREATE_TABLE =
            "CREATE TABLE " + DB_TABLE + " (" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    COLUMN_USER_NAME + " text, " +
                    COLUMN_GROUP_NAME + " text, " +
                    COLUMN_DISPLAY_NAME + " text, " +
                    COLUMN_LATITUDE + " integer not null, " +
                    COLUMN_LONGITUDE + " integer not null, " +
                    COLUMN_RECORDED_TIME + " text " +
                    ");";
    String userName = null;
    String displayName = null;
    String groupName = null;
    long latitude = 0;
    long longitude = 0;
    String recordedTime = null;

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion,
                                 int newVersion) {
        if (oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
            onCreate(db);
        }
    }

    public static List<MyLocationRecord> loadFromDatabase(SQLiteDatabase db) {
        Vector<MyLocationRecord> v = new Vector<MyLocationRecord>();

        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, null,
                null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    public static MyLocationRecord loadFromDatabase(SQLiteDatabase db, long id) {
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_ID + " = " + id,
                null, null, null, null);
        cursor.moveToFirst();
        MyLocationRecord req = null;
        if(!cursor.isAfterLast())
            req = fromCursor(cursor);
        cursor.close();
        return req;
    }

    //userName All chat records
    public static List<MyLocationRecord>  loadFromDatabase(SQLiteDatabase db, String recordedTime) {
        Vector<MyLocationRecord> v = new Vector<MyLocationRecord>();
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_RECORDED_TIME +  " =?", new String[]{recordedTime},
                null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    public String getDisplayName()
    {
        return displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    private static MyLocationRecord fromCursor(Cursor cursor) {
        MyLocationRecord req = new MyLocationRecord();
        int i = 0;
        req.setId(cursor.getLong(i++));
        req.setUserName(cursor.getString(i++));
        req.setGroupName(cursor.getString(i++));
        req.setDisplayName(cursor.getString(i++));
        req.setLatitude(cursor.getLong(i++));
        req.setLongitude(cursor.getLong(i++));
        req.setRecordedTime(cursor.getString(i++));
        return req;
    }

    public final static String MY_LOCATION_RECORD_ID = "MylocationRecord";

    @Override
    protected String getTableName() {
        return DB_TABLE;
    }

    @Override
    protected void putValues(ContentValues values) {
        values.put(COLUMN_USER_NAME, getUserName());
        values.put(COLUMN_GROUP_NAME, getGroupName());
        values.put(COLUMN_DISPLAY_NAME, getDisplayName());
        values.put(COLUMN_LATITUDE, getLatitude());
        values.put(COLUMN_LONGITUDE, getLongitude());
        values.put(COLUMN_RECORDED_TIME, getRecordedTime());
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public long getLatitude()
    {
        return latitude;
    }

    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }


    public long getLongitude()
    {
        return longitude;
    }

    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }




    public String getRecordedTime() {
        return recordedTime;
    }

    public void setRecordedTime(String recordedTime) {
        this.recordedTime = recordedTime;
    }

    @Override
    public String getTitleForMenu() {
        return getId() + "";
    }

    @Override
    public String getKeyForIntent() {
        return MY_LOCATION_RECORD_ID;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof MyLocationRecord))
            return false;
        MyLocationRecord other = (MyLocationRecord) obj;

        if (userName == null) {
            if (other.userName!=null)
                return false;
        } else if (!userName.equals(other.userName)) {
            return false;
        }

        if (groupName == null) {
            if (other.groupName!=null)
                return false;
        } else if (!groupName.equals(other.groupName)) {
            return false;
        }

        if (displayName == null) {
            if (other.displayName!=null)
                return false;
        } else if (!displayName.equals(other.recordedTime)) {
            return false;
        }

        if (latitude != other.latitude) {
            return false;
        }


        if (longitude != other.longitude) {
            return false;
        }


        if (recordedTime == null) {
            if (other.recordedTime!=null)
                return false;
        } else if (!recordedTime.equals(other.recordedTime)) {
            return false;
        }

        return true;
    }

    public static void storeMyLocationRecord(Context context, String userName,String groupName, String displayName, long latitude, long longitude, String recordedTime) {
        MyLocationRecord req = new MyLocationRecord();
        req.setUserName(userName);
        req.setGroupName(groupName);
        req.setDisplayName(displayName);
        req.setLatitude(latitude);
        req.setLongitude(longitude);
        req.setRecordedTime(recordedTime);

        DataSource db = new DataSource(context);
        db.open();

        /*
        boolean recordExists = false;
        List<LocationRecord> reqs = db.getLocationRecords(userName);
        for(LocationRecord _req : reqs) {
            if (_req.getUserName().equalsIgnoreCase(userName) == true)
            {
                recordExists = true;
                break;
            }
        }
        if (recordExists == false) {
        */

        db.persistMyLocationRecord(req);
        //}

        db.close();
    }


    public static void clearFromDB(Context context, String recordedTime)
    {
        DataSource db = new DataSource(context);
        db.open();
        List<MyLocationRecord> reqs = db.getMyLocationRecords(recordedTime);
        for(MyLocationRecord _req : reqs) {
            db.deleteMyLocationRecord(_req);
        }
        db.close();
    }

    public static void clearFromDB(Context context)
    {
        DataSource db = new DataSource(context);
        db.open();
        List<MyLocationRecord> reqs = db.getMyLocationRecords();
        for(MyLocationRecord _req : reqs) {
            db.deleteMyLocationRecord(_req);
        }
        db.close();
    }

}
