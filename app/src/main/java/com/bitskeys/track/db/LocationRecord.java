package com.bitskeys.track.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;
import java.util.Vector;

/**
 * Created by Manish on 17-01-2016.
 */
public class LocationRecord extends DBObject {
    final static String FILENAME = "LocationRecord";

    private final static String DB_TABLE = "LocationRecord";

    private final static String COLUMN_USER_NAME = "user_name";
    private final static String COLUMN_GROUP_NAME = "group_name";
    private final static String COLUMN_DISPLAY_NAME = "disp_name";
    private final static String COLUMN_LATITUDE = "latitude";
    private final static String COLUMN_LONGITUDE = "longitude";
    private final static String COLUMN_RECORDED_TIME = "recorded_time";
    private final static String COLUMN_ADDRESS_LINE1 = "address_line1";
    private final static String COLUMN_ADDRESS_LINE2 = "address_line2";
    private final static String COLUMN_ADDRESS_LINE3 = "address_line3";

    private final static String[] ALL_COLUMNS = new String[]{
            COLUMN_ID,
            COLUMN_USER_NAME,
            COLUMN_GROUP_NAME,
            COLUMN_DISPLAY_NAME,
            COLUMN_LATITUDE,
            COLUMN_LONGITUDE,
            COLUMN_RECORDED_TIME,
            COLUMN_ADDRESS_LINE1,
            COLUMN_ADDRESS_LINE2,
            COLUMN_ADDRESS_LINE3
    };

    private final static String CREATE_TABLE =
            "CREATE TABLE " + DB_TABLE + " (" +
                    COLUMN_ID + " integer primary key autoincrement, " +
                    COLUMN_USER_NAME + " text, " +
                    COLUMN_GROUP_NAME + " text, " +
                    COLUMN_DISPLAY_NAME + " text, " +
                    COLUMN_LATITUDE + " integer not null, " +
                    COLUMN_LONGITUDE + " integer not null, " +
                    COLUMN_RECORDED_TIME + " text, " +
                    COLUMN_ADDRESS_LINE1 + " text, " +
                    COLUMN_ADDRESS_LINE2 + " text, " +
                    COLUMN_ADDRESS_LINE3 + " text " +
                    ");";
    String userName = null;
    String displayName = null;
    String groupName = null;
    long latitude = 0;
    long longitude = 0;
    String recordedTime = null;
    String addressLine1 = null;
    String addressLine2 = null;
    String addressLine3 = null;

    public static void onCreate(SQLiteDatabase db) {
        db.execSQL(CREATE_TABLE);
    }

    public static void onUpgrade(SQLiteDatabase db, int oldVersion,
                                 int newVersion) {
        if (oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
            onCreate(db);
        }
    }

    public static List<LocationRecord> loadFromDatabase(SQLiteDatabase db) {
        Vector<LocationRecord> v = new Vector<LocationRecord>();

        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, null,
                null, null, null, null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    public static LocationRecord loadFromDatabase(SQLiteDatabase db, long id) {
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_ID + " = " + id,
                null, null, null, null);
        cursor.moveToFirst();
        LocationRecord req = null;
        if(!cursor.isAfterLast())
            req = fromCursor(cursor);
        cursor.close();
        return req;
    }

    //userName All chat records
    public static List<LocationRecord>  loadFromDatabase(SQLiteDatabase db, String userName) {
        Vector<LocationRecord> v = new Vector<LocationRecord>();
        Cursor cursor = db.query(DB_TABLE,
                ALL_COLUMNS, COLUMN_USER_NAME +  " =?", new String[]{userName},
                null, null, null, null);
        cursor.moveToFirst();
        while(!cursor.isAfterLast()) {
            v.add(fromCursor(cursor));
            cursor.moveToNext();
        }
        cursor.close();
        return v;
    }

    public String getDisplayName()
    {
        return displayName;
    }
    public void setDisplayName(String displayName)
    {
        this.displayName = displayName;
    }

    private static LocationRecord fromCursor(Cursor cursor) {
        LocationRecord req = new LocationRecord();
        int i = 0;
        req.setId(cursor.getLong(i++));
        req.setUserName(cursor.getString(i++));
        req.setGroupName(cursor.getString(i++));
        req.setDisplayName(cursor.getString(i++));
        req.setLatitude(cursor.getLong(i++));
        req.setLongitude(cursor.getLong(i++));
        req.setRecordedTime(cursor.getString(i++));
        req.setAddressLine1(cursor.getString(i++));
        req.setAddressLine2(cursor.getString(i++));
        req.setAddressLine3(cursor.getString(i++));
        return req;
    }

    public final static String LOCATION_RECORD_ID = "locationRecord";

    @Override
    protected String getTableName() {
        return DB_TABLE;
    }

    @Override
    protected void putValues(ContentValues values) {
        values.put(COLUMN_USER_NAME, getUserName());
        values.put(COLUMN_GROUP_NAME, getGroupName());
        values.put(COLUMN_DISPLAY_NAME, getDisplayName());
        values.put(COLUMN_LATITUDE, getLatitude());
        values.put(COLUMN_LONGITUDE, getLongitude());
        values.put(COLUMN_RECORDED_TIME, getRecordedTime());
        values.put(COLUMN_ADDRESS_LINE1, getAddressLine1());
        values.put(COLUMN_ADDRESS_LINE2, getAddressLine2());
        values.put(COLUMN_ADDRESS_LINE3, getAddressLine3());
    }

    public String getUserName()
    {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getGroupName()
    {
        return groupName;
    }

    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }

    public long getLatitude()
    {
        return latitude;
    }

    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }


    public long getLongitude()
    {
        return longitude;
    }

    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }

    public String getRecordedTime() {
        return recordedTime;
    }

    public void setRecordedTime(String recordedTime) {
        this.recordedTime = recordedTime;
    }

    public String getAddressLine1() {
        return addressLine1;
    }

    public void setAddressLine1(String addressLine1) {
        this.addressLine1 = addressLine1;
    }

    public String getAddressLine2() {
        return addressLine2;
    }

    public void setAddressLine2(String addressLine2) {
        this.addressLine2 = addressLine2;
    }

    public String getAddressLine3() {
        return addressLine3;
    }

    public void setAddressLine3(String addressLine3) {
        this.addressLine3 = addressLine3;
    }

    @Override
    public String getTitleForMenu() {
        return getId() + "";
    }

    @Override
    public String getKeyForIntent() {
        return LOCATION_RECORD_ID;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = super.hashCode();
        return result;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (!(obj instanceof LocationRecord))
            return false;
        LocationRecord other = (LocationRecord) obj;

        if (userName == null) {
            if (other.userName!=null)
                return false;
        } else if (!userName.equals(other.userName)) {
            return false;
        }

        if (groupName == null) {
            if (other.groupName!=null)
                return false;
        } else if (!groupName.equals(other.groupName)) {
            return false;
        }

        if (displayName == null) {
            if (other.displayName!=null)
                return false;
        } else if (!displayName.equals(other.recordedTime)) {
            return false;
        }

        if (latitude != other.latitude) {
            return false;
        }


        if (longitude != other.longitude) {
            return false;
        }


        if (recordedTime == null) {
            if (other.recordedTime!=null)
                return false;
        } else if (!recordedTime.equals(other.recordedTime)) {
            return false;
        }

        return true;
    }

    public static void storeLocationRecord(Context context, String userName,String groupName, String displayName, long latitude, long longitude, String recordedTime, String addressLine1, String addressLine2, String addressLine3) {
        LocationRecord req = new LocationRecord();
        req.setUserName(userName);
        req.setGroupName(groupName);
        req.setDisplayName(displayName);
        req.setLatitude(latitude);
        req.setLongitude(longitude);
        req.setRecordedTime(recordedTime);

        req.setAddressLine1(addressLine1);
        req.setAddressLine2(addressLine2);
        req.setAddressLine3(addressLine3);

        DataSource db = new DataSource(context);

        try {
            db.open();

        /*
        boolean recordExists = false;
        List<LocationRecord> reqs = db.getLocationRecords(userName);
        for(LocationRecord _req : reqs) {
            if (_req.getUserName().equalsIgnoreCase(userName) == true)
            {
                recordExists = true;
                break;
            }
        }
        if (recordExists == false) {
        */

            db.persistLocationRecord(req);
            db.close();
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "exeption in db.open");
            e.printStackTrace();
            db.close();
        }
        //}


    }


    public static void clearFromDB(Context context, String userName)
    {
        DataSource db = new DataSource(context);
        try {
            db.open();
            List<LocationRecord> reqs = db.getLocationRecords(userName);
            for (LocationRecord _req : reqs) {
                db.deleteLocationRecord(_req);
            }
            db.close();
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "Error in location update");
            e.printStackTrace();
            db.close();
        }

    }

    public static void clearFromDB(Context context)
    {
        DataSource db = new DataSource(context);
        db.open();
        List<LocationRecord> reqs = db.getLocationRecords();
        for(LocationRecord _req : reqs) {
            db.deleteLocationRecord(_req);
        }
        db.close();
    }

}

