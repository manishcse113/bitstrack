package com.bitskeys.track.db;

import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.bitskeys.track.chatRoom.ChatRoomInfo;
import com.bitskeys.track.gen.TimeClock;

import org.jivesoftware.smack.chat.Chat;

import java.util.HashMap;
import java.util.List;
import java.util.Vector;

/**
 * Created by Neeraj on 22-02-2016.
 */


    public class RoomRecordNew extends DBObject
    {
        private static String FILENAME = "RoomRecordNew";
        private final static String DB_TABLE = "RoomRecordsNew";
        private final static String COLUMN_ROOM_NAME = "room_name";
        private final static String COLUMN_ROOM_JID = "room_jid";
        private final static String COLUMN_ROOM_MEMBER = "room_member";
        private final static String COLUMN_GROUP_NAME = "group_name";
        private final static String COLUMN_CREATION_TIMESTAMP = "creation_timestamp";
        private final static String COLUMN_PERMS = "perms";

        private final static String[] ALL_COLUMNS = new String[] {
                COLUMN_ID,
                COLUMN_ROOM_NAME,
                COLUMN_ROOM_JID,
                COLUMN_ROOM_MEMBER,
                COLUMN_GROUP_NAME,
                COLUMN_CREATION_TIMESTAMP,
                COLUMN_PERMS,
        };

        private final static String CREATE_TABLE =
                "CREATE TABLE " + DB_TABLE + " (" +
                        COLUMN_ID + " integer, " +
                        COLUMN_ROOM_NAME  + " text primary key, " +
                        COLUMN_ROOM_JID + " text, " +
                        COLUMN_ROOM_MEMBER + " text, " +
                        COLUMN_GROUP_NAME + " text " +
                        COLUMN_CREATION_TIMESTAMP + " integer not null, " +
                        COLUMN_PERMS + " integer not null " +
                        ");";

        public final static String ROOM_RECORD_ID = "roomRecord";


        public static void onCreate(SQLiteDatabase db) {
            try {
                db.execSQL(CREATE_TABLE);
            }catch (SQLException exc)
            {
                String msg = exc.getMessage();
                Log.d("SQLEXCEPTION:",msg);
            }

        }

        public static void onUpgrade(SQLiteDatabase db, int oldVersion,
                                     int newVersion) {
            if(oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
                onCreate(db);
            }
        }

        public RoomRecordNew() {
            //setCreationTimestamp(System.currentTimeMillis() / 1000);
        }

        public static HashMap<String,ChatRoomInfo> loadFromDatabase(SQLiteDatabase db)
        {
            HashMap<String,ChatRoomInfo> roomsMap = new HashMap<>();
            Cursor cursor = db.query(DB_TABLE,
                    ALL_COLUMNS, null,
                    null, null, null, null);
            cursor.moveToFirst();

            //COLUMN_ID = 0
            //COLUMN_ROOM_NAME=1,
            // COLUMN_ROOM_JID=2
            // COLUMN_ROOM_MEMBER=3
            // COLUMN_GROUP_NAME=4
            // COLUMN_CREATION_TIMESTAMP=5
            // COLUMN_PERMS=6

            while(!cursor.isAfterLast())
            {
                String roomName = cursor.getString(1);
                String roomJid = cursor.getString(2);
                String roomMember = cursor.getString(3);
                String groupName = cursor.getString(4);
                Integer perms = cursor.getInt(6);
                ChatRoomInfo roomInfo = roomsMap.get(roomName);
                if(roomInfo != null)
                {
                    roomInfo.addMemberInfo(roomMember,perms,groupName);
                }
                else
                {
                    roomInfo = new ChatRoomInfo(roomName,roomJid);
                    roomInfo.addMemberInfo(roomMember,perms,groupName);
                    roomsMap.put(roomMember,roomInfo);
                }

                cursor.moveToNext();
            }
            cursor.close();
            return roomsMap;
        }

//        public static RoomRecordNew loadFromDatabase(SQLiteDatabase db, long id) {
//            Cursor cursor = db.query(DB_TABLE,
//                    ALL_COLUMNS, COLUMN_ID + " = " + id,
//                    null, null, null, null);
//            cursor.moveToFirst();
//            RoomRecordNew req = null;
//            if(!cursor.isAfterLast())
//                req = fromCursor(cursor);
//            cursor.close();
//            return req;
//        }

        //userName All chat records
        public static ChatRoomInfo loadFromDatabase(SQLiteDatabase db, String roomName) {

            ChatRoomInfo roomInfo = null;
            Cursor cursor = db.query(DB_TABLE,
                    ALL_COLUMNS, COLUMN_ROOM_NAME +  " =?", new String[]{roomName},
                    null, null, null, null);


            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                String roomJid = cursor.getString(2);
                if(roomInfo == null)
                {
                    roomInfo = new ChatRoomInfo(roomName,roomJid);
                }

                String roomMember = cursor.getString(3);
                String groupName = cursor.getString(4);
                Integer perms = cursor.getInt(6);
                roomInfo.addMemberInfo(roomMember,perms,groupName);
                cursor.moveToNext();
            }
            cursor.close();
            return roomInfo;
        }

//        private static RoomRecordNew fromCursor(Cursor cursor) {
//            RoomRecordNew req = new RoomRecordNew();
//            int i = 0;
//            req.setId(cursor.getLong(i++));
//            req.setRoomName(cursor.getString(i++));
//            req.setRoomJid(cursor.getString(i++));
//            req.setInviterName(cursor.getString(i++));
//            req.setInviterJid(cursor.getString(i++));
//            req.setMembersCount(cursor.getLong(i++));
//            req.setMember1Name(cursor.getString(i++));
//            req.setMember2Name(cursor.getString(i++));
//            req.setMember3Name(cursor.getString(i++));
//            req.setMember4Name(cursor.getString(i++));
//            req.setMember5Name(cursor.getString(i++));
//            req.setMember6Name(cursor.getString(i++));
//            req.setMember7Name(cursor.getString(i++));
//            req.setMember8Name(cursor.getString(i++));
//            req.setMember9Name(cursor.getString(i++));
//            req.setMember10Name(cursor.getString(i++));
//            req.setGroupName(cursor.getString(i++));
//            return req;
//        }

//        public final static String ROOM_RECORD_ID = "roomRecord";

        @Override
        protected String getTableName() {
            return DB_TABLE;
        }

        @Override
        protected void putValues(ContentValues values) {
//            values.put(COLUMN_ROOM_NAME, getRoomName());
//            values.put(COLUMN_ROOM_JID, getRoomJid());
//            values.put(COLUMN_INVITER_NAME, getInviterName());
//            values.put(COLUMN_INVITER_JID, getInviterJid());
//            values.put(COLUMN_MEMBERS_COUNT, getMembersCount());
//            values.put(COLUMN_MEMBERS1, getMember1Name());
//            values.put(COLUMN_MEMBERS2, getMember2Name());
//            values.put(COLUMN_MEMBERS3, getMember3Name());
//            values.put(COLUMN_MEMBERS4, getMember4Name());
//            values.put(COLUMN_MEMBERS5, getMember5Name());
//            values.put(COLUMN_MEMBERS6, getMember6Name());
//            values.put(COLUMN_MEMBERS7, getMember7Name());
//            values.put(COLUMN_MEMBERS8, getMember8Name());
//            values.put(COLUMN_MEMBERS9, getMember9Name());
//            values.put(COLUMN_MEMBERS10, getMember10Name());
//            values.put(COLUMN_GROUP_NAME, getGroupName());
        }







        @Override
        public String getTitleForMenu() {
            return getId() + "";
        }

        @Override
        public String getKeyForIntent() {
            return ROOM_RECORD_ID;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            /* Comparison is invalid, so returning false */
            return false;
        }


        public static void storeUniqueNameRecordNew(Context context, String roomName,String roomJid,
                                                    String roomMember,String groupName, Integer perms
                                                    )
        {
            DataSource db = new DataSource(context);
            try {
                db.open();
                String Condition1 = COLUMN_ROOM_NAME + "=" + "'" + roomName + "'"+  " AND "  + COLUMN_ROOM_MEMBER + "=" + "'" + roomMember + "'";
                long timestamp = TimeClock.getEpocTime();
                String chkQuery = "Select * from " + DB_TABLE + " where " + COLUMN_ROOM_NAME + "=" + "'" + roomName + "'" + " AND " +
                                                                            COLUMN_ROOM_MEMBER + "=" + "'" + roomMember + "'" + ";";
                Cursor curs = db.db.rawQuery(chkQuery,null);
                if(curs.getCount() > 0)
                {
                    String delQuery = "DELETE from " + DB_TABLE + " where " + COLUMN_ROOM_NAME + "=" + "'" + roomName + "'" + " AND " +
                            COLUMN_ROOM_MEMBER + "=" + "'" + roomMember + "'" + ";";
                    db.db.rawQuery(delQuery,null);

                }
                String sqlQuery =
                        "insert into "+ DB_TABLE + " (" +
                                COLUMN_ROOM_NAME + "," +
                                COLUMN_ROOM_JID + "," +
                                COLUMN_GROUP_NAME + "," +
                                COLUMN_CREATION_TIMESTAMP + "," +
                                COLUMN_PERMS +
                                ") values " +
                                "(" +
                                "'" + roomName +  "'" + "," +
                                "'" + roomJid +  "'" + "," +
                                "'" + groupName + "'" + "," +
                                "'" + timestamp + "'" + "," +
                                "'" + perms + "'" +
                                ");";
//                db.db.execSQL(sqlQuery);
                db.db.rawQuery(sqlQuery,null);
                db.close();
            }
            catch(Exception exception)
            {
                exception.printStackTrace();
                Log.e(FILENAME, "Exception in opening db");
                db.close();
            }
        }

        public static void clearFromDB(Context context, String roomName)
        {
            DataSource db = new DataSource(context);
            db.open();
            db.db.execSQL("DELETE FROM " + DB_TABLE +
                    " WHERE " + COLUMN_ROOM_NAME + " = " + "'" + roomName + "'" +
                    ";");
            db.close();
        }

        public static void clearFromDB(Context context) {
            DataSource db = new DataSource(context);
            db.open();
            deleteRoom(db.db);
            db.close();
        }

        public static void deleteRoom(SQLiteDatabase db) {

            db.execSQL("DELETE FROM " + DB_TABLE +
                    ";");
        }
    }