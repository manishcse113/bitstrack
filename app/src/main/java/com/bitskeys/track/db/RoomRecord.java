package com.bitskeys.track.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import java.util.List;
import java.util.Vector;

/**
 * Created by Manish on 22-02-2016.
 */


    public class RoomRecord extends DBObject
    {
        private static String FILENAME = "RoomRecord";
        private final static String DB_TABLE = "RoomRecords";

        private final static String COLUMN_ROOM_NAME = "room_name";
        private final static String COLUMN_ROOM_JID = "room_jid";
        private final static String COLUMN_INVITER_NAME = "room_inviter_name";
        private final static String COLUMN_INVITER_JID = "room_inviter_jid";
        private final static String COLUMN_MEMBERS_COUNT = "room_member_count";
        private final static String COLUMN_MEMBERS1 = "room_member1";
        private final static String COLUMN_MEMBERS2 = "room_member2";
        private final static String COLUMN_MEMBERS3 = "room_member3";
        private final static String COLUMN_MEMBERS4 = "room_member4";
        private final static String COLUMN_MEMBERS5 = "room_member5";
        private final static String COLUMN_MEMBERS6 = "room_member6";
        private final static String COLUMN_MEMBERS7 = "room_member7";
        private final static String COLUMN_MEMBERS8 = "room_member8";
        private final static String COLUMN_MEMBERS9 = "room_member9";
        private final static String COLUMN_MEMBERS10 = "room_member10";
        private final static String COLUMN_GROUP_NAME = "group_name";

        private final static String[] ALL_COLUMNS = new String[] {
                COLUMN_ID,
                COLUMN_ROOM_NAME,
                COLUMN_ROOM_JID,
                COLUMN_INVITER_NAME,
                COLUMN_INVITER_JID,
                COLUMN_MEMBERS_COUNT,
                COLUMN_MEMBERS1,
                COLUMN_MEMBERS2,
                COLUMN_MEMBERS3,
                COLUMN_MEMBERS4,
                COLUMN_MEMBERS5,
                COLUMN_MEMBERS6,
                COLUMN_MEMBERS7,
                COLUMN_MEMBERS8,
                COLUMN_MEMBERS9,
                COLUMN_MEMBERS10,
                COLUMN_GROUP_NAME,
        };

        private final static String CREATE_TABLE =
                "CREATE TABLE " + DB_TABLE + " (" +
                        COLUMN_ID + " integer, " +
                        COLUMN_ROOM_NAME  + " text primary key, " +
                        COLUMN_ROOM_JID + " text, " +
                        COLUMN_INVITER_NAME + " text, " +
                        COLUMN_INVITER_JID + " text, " +
                        COLUMN_MEMBERS_COUNT + " integer not null, " +
                        COLUMN_MEMBERS1 + " text, " +
                        COLUMN_MEMBERS2 + " text, " +
                        COLUMN_MEMBERS3 + " text, " +
                        COLUMN_MEMBERS4 + " text, " +
                        COLUMN_MEMBERS5 + " text, " +
                        COLUMN_MEMBERS6 + " text, " +
                        COLUMN_MEMBERS7 + " text, " +
                        COLUMN_MEMBERS8 + " text, " +
                        COLUMN_MEMBERS9 + " text, " +
                        COLUMN_MEMBERS10 + " text, " +
                        COLUMN_GROUP_NAME + " text " +

					/*
			COLUMN_PHONE_NUMBER + " text, " +
			COLUMN_VALIDATION_CODE_1 + " text, " +
			COLUMN_VALIDATION_CODE_2 + " text, " +
			COLUMN_CREATION_TIMESTAMP + " integer not null, " +
			COLUMN_AUTH_PASSWORD + " text not null	*/
                        ");";

        String roomName = null;
        String roomJid = null;
        String inviterName = null;
        String inviterJid = null;
        long membersCount = 0;
        String member1Name  = null;
        String member2Name  = null;
        String member3Name  = null;
        String member4Name  = null;
        String member5Name  = null;
        String member6Name  = null;
        String member7Name  = null;
        String member8Name  = null;
        String member9Name  = null;
        String member10Name  = null;
        String groupName = null;

        public static void onCreate(SQLiteDatabase db) {
            db.execSQL(CREATE_TABLE);
        }

        public static void onUpgrade(SQLiteDatabase db, int oldVersion,
                                     int newVersion) {
            if(oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
                onCreate(db);
            }
        }


        public RoomRecord() {
            //setCreationTimestamp(System.currentTimeMillis() / 1000);
        }

        public static List<RoomRecord> loadFromDatabase(SQLiteDatabase db) {
            Vector<RoomRecord> v = new Vector<RoomRecord>();

            Cursor cursor = db.query(DB_TABLE,
                    ALL_COLUMNS, null,
                    null, null, null, null);
            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                v.add(fromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
            return v;
        }

        public static RoomRecord loadFromDatabase(SQLiteDatabase db, long id) {
            Cursor cursor = db.query(DB_TABLE,
                    ALL_COLUMNS, COLUMN_ID + " = " + id,
                    null, null, null, null);
            cursor.moveToFirst();
            RoomRecord req = null;
            if(!cursor.isAfterLast())
                req = fromCursor(cursor);
            cursor.close();
            return req;
        }

        //userName All chat records
        public static List<RoomRecord>  loadFromDatabase(SQLiteDatabase db, String roomName) {
            Vector<RoomRecord> v = new Vector<RoomRecord>();
            Cursor cursor = null;
            try {
                cursor = db.query(DB_TABLE,
                        ALL_COLUMNS, COLUMN_ROOM_NAME +  " =?", new String[]{roomName},
                        null, null, null, null);
            }catch (Exception ex)
            {
                String msg = ex.getMessage();
                Log.d("exception:",msg);
            }

            cursor.moveToFirst();
            while(!cursor.isAfterLast()) {
                v.add(fromCursor(cursor));
                cursor.moveToNext();
            }
            cursor.close();
            return v;
        }

        private static RoomRecord fromCursor(Cursor cursor) {
            RoomRecord req = new RoomRecord();
            int i = 0;
            req.setId(cursor.getLong(i++));
            req.setRoomName(cursor.getString(i++));
            req.setRoomJid(cursor.getString(i++));
            req.setInviterName(cursor.getString(i++));
            req.setInviterJid(cursor.getString(i++));
            req.setMembersCount(cursor.getLong(i++));
            req.setMember1Name(cursor.getString(i++));
            req.setMember2Name(cursor.getString(i++));
            req.setMember3Name(cursor.getString(i++));
            req.setMember4Name(cursor.getString(i++));
            req.setMember5Name(cursor.getString(i++));
            req.setMember6Name(cursor.getString(i++));
            req.setMember7Name(cursor.getString(i++));
            req.setMember8Name(cursor.getString(i++));
            req.setMember9Name(cursor.getString(i++));
            req.setMember10Name(cursor.getString(i++));
            req.setGroupName(cursor.getString(i++));
            return req;
        }

        public final static String ROOM_RECORD_ID = "roomRecord";

        @Override
        protected String getTableName() {
            return DB_TABLE;
        }

        @Override
        protected void putValues(ContentValues values) {
            values.put(COLUMN_ROOM_NAME, getRoomName());
            values.put(COLUMN_ROOM_JID, getRoomJid());
            values.put(COLUMN_INVITER_NAME, getInviterName());
            values.put(COLUMN_INVITER_JID, getInviterJid());
            values.put(COLUMN_MEMBERS_COUNT, getMembersCount());
            values.put(COLUMN_MEMBERS1, getMember1Name());
            values.put(COLUMN_MEMBERS2, getMember2Name());
            values.put(COLUMN_MEMBERS3, getMember3Name());
            values.put(COLUMN_MEMBERS4, getMember4Name());
            values.put(COLUMN_MEMBERS5, getMember5Name());
            values.put(COLUMN_MEMBERS6, getMember6Name());
            values.put(COLUMN_MEMBERS7, getMember7Name());
            values.put(COLUMN_MEMBERS8, getMember8Name());
            values.put(COLUMN_MEMBERS9, getMember9Name());
            values.put(COLUMN_MEMBERS10, getMember10Name());
            values.put(COLUMN_GROUP_NAME, getGroupName());
        }

        public String getRoomName()
        {
            return roomName;
        }
        public void setRoomName(String roomName) {
            this.roomName = roomName;
        }

        public String getRoomJid()
        {
            return roomJid;
        }
        public void setRoomJid(String roomJid) {
            this.roomJid = roomJid;
        }

        public String getInviterName()
        {
            return inviterName;
        }
        public void setInviterName(String inviterName) {
            this.inviterName = inviterName;
        }

        public String getInviterJid()
        {
            return inviterJid;
        }
        public void setInviterJid(String inviterJid) {
            this.inviterJid = inviterJid;
        }

        public long getMembersCount()
        {
            return membersCount;
        }
        public void setMembersCount(long membersCount) {
            this.membersCount = membersCount;
        }

        public String getMember1Name()
        {
            return member1Name;
        }
        public void setMember1Name(String member1Name) {
            this.member1Name = member1Name;
        }

        public String getMember2Name()
        {
            return member2Name;
        }
        public void setMember2Name(String member2Name) {
            this.member2Name = member2Name;
        }

        public String getMember3Name()
        {
            return member3Name;
        }
        public void setMember3Name(String member3Name) {
            this.member3Name = member3Name;
        }

        public String getMember4Name()
        {
            return member4Name;
        }
        public void setMember4Name(String member4Name) {
            this.member4Name = member4Name;
        }


        public String getMember5Name()
        {
            return member5Name;
        }
        public void setMember5Name(String member5Name) {
            this.member5Name = member5Name;
        }

        public String getMember6Name()
        {
            return member6Name;
        }
        public void setMember6Name(String member6Name) {
            this.member6Name = member6Name;
        }

        public String getMember7Name()
        {
            return member7Name;
        }
        public void setMember7Name(String member7Name) {
            this.member7Name = member7Name;
        }


        public String getMember8Name()
        {
            return member8Name;
        }
        public void setMember8Name(String member8Name) {
            this.member8Name = member8Name;
        }

        public String getMember9Name()
        {
            return member9Name;
        }
        public void setMember9Name(String member9Name) {
            this.member9Name = member9Name;
        }

        public String getMember10Name()
        {
            return member10Name;
        }
        public void setMember10Name(String member10Name) {
            this.member10Name = member10Name;
        }



        public String getGroupName()
        {
            return groupName;
        }
        public void setGroupName(String groupName) {
            this.groupName = groupName;
        }

        @Override
        public String getTitleForMenu() {
            return getId() + "";
        }

        @Override
        public String getKeyForIntent() {
            return ROOM_RECORD_ID;
        }

        @Override
        public int hashCode() {
            final int prime = 31;
            int result = super.hashCode();
            return result;
        }

        @Override
        public boolean equals(Object obj) {
            if (this == obj)
                return true;
            if (!super.equals(obj))
                return false;
            if (!(obj instanceof RoomRecord))
                return false;
            RoomRecord other = (RoomRecord) obj;

            if (roomName == null) {
                if (other.roomName!=null)
                    return false;
            } else if (!roomName.equals(other.roomName)) {
                return false;
            }

            if (groupName == null) {
                if (other.groupName!=null)
                    return false;
            } else if (!groupName.equals(other.groupName)) {
                return false;
            }
            return true;
        }

        public static void storeUniqueNameRecordNew(Context context, String roomName,String roomJid,
                                                    String inviterName, String inviterJid,
                                                    int membersCount,
                                                    String member1Name, String member2Name,
                                                    String member3Name, String member4Name,
                                                    String member5Name, String member6Name,
                                                    String member7Name, String member8Name,
                                                    String member9Name, String member10Name,
                                                    String groupName)
        {

            DataSource db = new DataSource(context);
            try {
                db.open();
                ContentValues data=new ContentValues();
                data.put(COLUMN_ID,0); //putting ID as 0 for now:
                data.put(COLUMN_ROOM_NAME,roomName);
                data.put(COLUMN_ROOM_JID,roomJid);
                data.put(COLUMN_INVITER_NAME,inviterName);
                data.put(COLUMN_INVITER_JID,inviterJid);
                data.put(COLUMN_MEMBERS_COUNT,membersCount);
                data.put(COLUMN_MEMBERS1,member1Name);
                data.put(COLUMN_MEMBERS2,member2Name);
                data.put(COLUMN_MEMBERS3,member3Name);
                data.put(COLUMN_MEMBERS4,member4Name);
                data.put(COLUMN_MEMBERS5,member5Name);
                data.put(COLUMN_MEMBERS6,member6Name);
                data.put(COLUMN_MEMBERS7,member7Name);
                data.put(COLUMN_MEMBERS8,member8Name);
                data.put(COLUMN_MEMBERS9,member9Name);
                data.put(COLUMN_MEMBERS10,member10Name);
                data.put(COLUMN_GROUP_NAME,groupName);
                String sqlQuery =
                        "insert or replace into "+DB_TABLE+ " (" +
                                COLUMN_ID + "," +
                                COLUMN_ROOM_NAME + "," +
                                COLUMN_ROOM_JID + "," +
                                COLUMN_INVITER_NAME + "," +
                                COLUMN_INVITER_JID + "," +
                                COLUMN_MEMBERS_COUNT + "," +
                                COLUMN_MEMBERS1 + "," +
                                COLUMN_MEMBERS2 + "," +
                                COLUMN_MEMBERS3 + "," +
                                COLUMN_MEMBERS4 + "," +
                                COLUMN_MEMBERS5 + "," +
                                COLUMN_MEMBERS6 + "," +
                                COLUMN_MEMBERS7 + "," +
                                COLUMN_MEMBERS8 + "," +
                                COLUMN_MEMBERS9 + "," +
                                COLUMN_MEMBERS10 + "," +
                                COLUMN_GROUP_NAME +
                                ") values " +
                                "(" +
                                "( select " + COLUMN_ID + " from " + DB_TABLE + " where " + COLUMN_ROOM_NAME + " = " + "'" + roomName + "'" + ")," +
                                "'" + roomName +  "'" + "," +
                                "'" + roomJid +  "'" + "," +
                                "'" + inviterName +  "'" + "," +
                                "'" + inviterJid +  "'" + "," +
                                "'" + membersCount +  "'" + "," +
                                "'" + member1Name +  "'" + "," +
                                "'" + member2Name +  "'" + "," +
                                "'" + member3Name +  "'" + "," +
                                "'" + member4Name +  "'" + "," +
                                "'" + member5Name +  "'" + "," +
                                "'" + member6Name +  "'" + "," +
                                "'" + member7Name +  "'" + "," +
                                "'" + member8Name +  "'" + "," +
                                "'" + member9Name +  "'" + "," +
                                "'" + member10Name +  "'" + "," +
                                "'" + 		groupName + "'" +
                                ");";
                db.db.execSQL(sqlQuery);
                db.close();
            }
            catch(Exception exception)
            {
                exception.printStackTrace();
                Log.e(FILENAME, "Exception in opeing db");
                db.close();
            }
        }

        public static void clearFromDB(Context context, String roomName)
        {
            DataSource db = new DataSource(context);
            db.open();
            db.db.execSQL("DELETE FROM " + DB_TABLE +
                    " WHERE " + COLUMN_ROOM_NAME + " = " + "'" + roomName + "'" +
                    ";");

            //List<RoomRecord> reqs = db.getRoomRecords(roomName);
            //for(RoomRecord _req : reqs) {
            //    db.deleteRoomRecord(_req);
            //}
            db.close();
        }

        public static void clearFromDB(Context context) {
            DataSource db = new DataSource(context);
            db.open();
            deleteRoom(db.db);
            db.close();
        }

        public static void deleteRoom(SQLiteDatabase db) {

            db.execSQL("DELETE FROM " + DB_TABLE +
                    ";");
        }
    }