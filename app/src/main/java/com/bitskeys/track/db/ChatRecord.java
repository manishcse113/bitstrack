package com.bitskeys.track.db;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.List;
import java.util.Vector;

public class ChatRecord extends DBObject {
	
	private final static String DB_TABLE = "ChatRecords";

	private final static String COLUMN_USER_NAME = "user_name";
	private final static String COLUMN_GROUP_NAME = "group_name";
	private final static String COLUMN_REMOTE_USER_NAME = "remote_user_name";
	private final static String COLUMN_CREATION_TIMESTAMP = "creation_timestamp";
	private final static String COLUMN_MESSAGE_DIRECTION = "message_direction";
	private final static String COLUMN_MIME_TYPE = "message_type"; // Text, image, video
	private final static String COLUMN_MESSAGE_TEXT = "message_text";
	private final static String COLUMN_MESSAGE_FILE_NAME = "file_name"; //File - image/video
	private final static String COLUMN_MESSAGE_FILE_PATH = "file_path"; //File - image/video
	private final static String COLUMN_MESSAGE_RECEIPT_ID = "message_receiptId"; //
	private final static String COLUMN_MESSAGE_RECEIPT_STATUS = "message_receipt_status"; //
	private final static String COLUMN_FILE_SIZE_EXPECTED = "filesize_expected";
	private final static String COLUMN_FILE_SIZE_DISK = "filesize_disk";
	/*
	private final static String COLUMN_PHONE_NUMBER = "phone_number";
	private final static String COLUMN_VALIDATION_CODE_1 = "v_code1";
	private final static String COLUMN_VALIDATION_CODE_2 = "v_code2";
	private final static String COLUMN_CREATION_TIMESTAMP = "creation_timestamp";
	private final static String COLUMN_AUTH_PASSWORD = "auth_password";
	*/

	private final static String[] ALL_COLUMNS = new String[] {
		COLUMN_ID,
			COLUMN_USER_NAME,
			COLUMN_GROUP_NAME,
			COLUMN_REMOTE_USER_NAME,
			COLUMN_CREATION_TIMESTAMP,
			COLUMN_MESSAGE_DIRECTION,
			COLUMN_MIME_TYPE,
			COLUMN_MESSAGE_TEXT,
			COLUMN_MESSAGE_FILE_NAME,
			COLUMN_MESSAGE_FILE_PATH,
			COLUMN_MESSAGE_RECEIPT_ID,
			COLUMN_MESSAGE_RECEIPT_STATUS,
			COLUMN_FILE_SIZE_EXPECTED,
			COLUMN_FILE_SIZE_DISK
		/*
		COLUMN_PHONE_NUMBER,
		COLUMN_VALIDATION_CODE_1,
		COLUMN_VALIDATION_CODE_2,
		COLUMN_CREATION_TIMESTAMP,
		COLUMN_AUTH_PASSWORD
		*/
	};
	
	private final static String CREATE_TABLE =
			"CREATE TABLE " + DB_TABLE + " (" +
			COLUMN_ID + " integer primary key autoincrement, " +
			COLUMN_USER_NAME  + " text, " +
			COLUMN_GROUP_NAME + " text, " +
			COLUMN_REMOTE_USER_NAME + " text, " +
			COLUMN_CREATION_TIMESTAMP + " integer not null, " +
			COLUMN_MESSAGE_DIRECTION + " integer not null, " +
			COLUMN_MIME_TYPE + " text, " +
			COLUMN_MESSAGE_TEXT + " text, " +
			COLUMN_MESSAGE_FILE_NAME + " text, " +
			COLUMN_MESSAGE_FILE_PATH + " text, " +
			COLUMN_MESSAGE_RECEIPT_ID + " text, " +
			COLUMN_MESSAGE_RECEIPT_STATUS + " text, " +
					COLUMN_FILE_SIZE_EXPECTED + " integer not null, "+
					COLUMN_FILE_SIZE_DISK + " integer not null "+
					/*
			COLUMN_PHONE_NUMBER + " text, " +
			COLUMN_VALIDATION_CODE_1 + " text, " +
			COLUMN_VALIDATION_CODE_2 + " text, " +
			COLUMN_CREATION_TIMESTAMP + " integer not null, " +
			COLUMN_AUTH_PASSWORD + " text not null	*/
			");";

	String userName = null;
	String groupName = null;
	String remoteUserName = null;
	long creationTimestamp;
	int messageDirection = 0;
	String mimeType = null;
	String messageText = null;
	String messageFileName = null;
	String messageFilePath = null;
	String receiptId = null;
	String receiptStatus = null;
	long fileSizeExpected = 0;
	long fileSizeDisk = 0;
	/*
	String phoneNumber = null;
	String validationCode1 = null;
	String validationCode2 = null;
	long creationTimestamp = 0;
	String authPassword = null;
	*/

	public static void onCreate(SQLiteDatabase db) {	
		db.execSQL(CREATE_TABLE);
	}
	
	public static void onUpgrade(SQLiteDatabase db, int oldVersion,
			int newVersion) {
		if(oldVersion < DatabaseHelper.DB_VERSION && newVersion >= DatabaseHelper.DB_VERSION) {
			onCreate(db);
		}
	}
	
	public ChatRecord() {
		//setCreationTimestamp(System.currentTimeMillis() / 1000);
	}
	
	public static List<ChatRecord> loadFromDatabase(SQLiteDatabase db) {
		Vector<ChatRecord> v = new Vector<ChatRecord>();
		
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, null,
				null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;
	}
	
	public static ChatRecord loadFromDatabase(SQLiteDatabase db, long id) {
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_ID + " = " + id,
				null, null, null, null);
		cursor.moveToFirst();
		ChatRecord req = null;
		if(!cursor.isAfterLast())
			 req = fromCursor(cursor);
		cursor.close();
		return req;
	}

	//userName All chat records
	public static List<ChatRecord>  loadFromDatabase(SQLiteDatabase db, String userName) {
		Vector<ChatRecord> v = new Vector<ChatRecord>();
		Cursor cursor = db.query(DB_TABLE,
				ALL_COLUMNS, COLUMN_USER_NAME +  " =?", new String[]{userName},
				null, null, null, null);
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;

	}

	//userName & remote user/Group chat records
	// third arugment is either remote_user_name or groupname
	public static List<ChatRecord>  loadFromDatabase(SQLiteDatabase db, String userName, String receiptStatus) {
		Vector<ChatRecord> v = new Vector<ChatRecord>();
		Cursor cursor;
		//if (!isGroup) {// if it is one user's history request
			cursor = db.query(DB_TABLE,
					ALL_COLUMNS, COLUMN_USER_NAME + " =?" + " and " + COLUMN_MESSAGE_RECEIPT_STATUS + " =?", new String[]{userName, receiptStatus},
					null, null, null, null);
		/*
		}
		else// if it is a group's history request
		{
			cursor = db.query(DB_TABLE,
					ALL_COLUMNS, COLUMN_USER_NAME + " =?" + " and " + COLUMN_GROUP_NAME + " =?",new String[]{userName, remoteUserGroupName},
					null, null, null, null);
		}
		*/
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;
	}

	//userName & remote user/Group chat records
	// third arugment is either remote_user_name or groupname
	public static List<ChatRecord>  loadFromDatabase(SQLiteDatabase db, String userName, String remoteUserGroupName, boolean isGroup) {
		Vector<ChatRecord> v = new Vector<ChatRecord>();
		Cursor cursor = null;
		if (!isGroup) {// if it is one user's history request
			try {
			cursor = db.query(DB_TABLE,
					ALL_COLUMNS, COLUMN_USER_NAME + " =?" + " and " + COLUMN_REMOTE_USER_NAME + " =?", new String[]{userName, remoteUserGroupName},
					null, null, null, null);}
			catch (Exception ex)
			{
				String str = ex.getMessage();
			}
		}
		else// if it is a group's history request
		{
			cursor = db.query(DB_TABLE,
					ALL_COLUMNS, COLUMN_USER_NAME + " =?" + " and " + COLUMN_GROUP_NAME + " =?",new String[]{userName, remoteUserGroupName},
					null, null, null, null);
		}
		if(cursor != null) {
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		}
		return v;
	}

	public static List<ChatRecord>  loadFromDatabase(SQLiteDatabase db, String userName, String remoteUserGroupName, boolean isGroup, Long StartTime) {
		Vector<ChatRecord> v = new Vector<ChatRecord>();
		Cursor cursor = null;

		if (!isGroup) {// if it is one user's history request
			try {
				cursor = db.query(DB_TABLE,
						ALL_COLUMNS, COLUMN_USER_NAME + " =?" + " and " + COLUMN_REMOTE_USER_NAME + " =?" + " and " + COLUMN_CREATION_TIMESTAMP + " >=?", new String[]{userName, remoteUserGroupName, StartTime.toString()},
						null, null, null, null);}
			catch (Exception ex)
			{
				String str = ex.getMessage();
			}
		}
		else// if it is a group's history request
		{
			cursor = db.query(DB_TABLE,
					ALL_COLUMNS, COLUMN_USER_NAME + " =?" + " and " + COLUMN_GROUP_NAME + " =?",new String[]{userName, remoteUserGroupName},
					null, null, null, null);
		}
		if(cursor != null) {
			cursor.moveToFirst();
			while(!cursor.isAfterLast()) {
				v.add(fromCursor(cursor));
				cursor.moveToNext();
			}
			cursor.close();
		}
		return v;
	}

	public static List<ChatRecord>  loadFromDatabaseUnAcked(SQLiteDatabase db, String userName, String remoteUserGroupName, boolean isGroup, String receiptStatus) {
		Vector<ChatRecord> v = new Vector<ChatRecord>();
		Cursor cursor;
		if (!isGroup) {// if it is one user's history request
			cursor = db.query(DB_TABLE,
					ALL_COLUMNS, COLUMN_USER_NAME + " =?" + " and " + COLUMN_REMOTE_USER_NAME + " =?" + " and " + COLUMN_MESSAGE_RECEIPT_STATUS + " =?", new String[]{userName, remoteUserGroupName, receiptStatus },
					null, null, null, null);
		}
		else// if it is a group's history request
		{
			cursor = db.query(DB_TABLE,
					ALL_COLUMNS, COLUMN_USER_NAME + " =?" + " and " + COLUMN_GROUP_NAME + " =?" + " and " + COLUMN_MESSAGE_RECEIPT_STATUS + " =?",new String[]{userName, remoteUserGroupName, receiptStatus},
					null, null, null, null);
		}
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;
	}

	public static List<ChatRecord>  loadFromDatabaseReceiptId(SQLiteDatabase db, String userName, String remoteUserGroupName, boolean isGroup, String receiptId) {
		Vector<ChatRecord> v = new Vector<ChatRecord>();
		Cursor cursor;
		if (!isGroup) {// if it is one user's history request
			cursor = db.query(DB_TABLE,
					ALL_COLUMNS, COLUMN_USER_NAME + " =?" + " and " + COLUMN_REMOTE_USER_NAME + " =?" + " and " + COLUMN_MESSAGE_RECEIPT_ID + " =?", new String[]{userName, remoteUserGroupName, receiptId },
					null, null, null, null);
		}
		else// if it is a group's history request
		{
			cursor = db.query(DB_TABLE,
					ALL_COLUMNS, COLUMN_USER_NAME + " =?" + " and " + COLUMN_GROUP_NAME + " =?" + " and " + COLUMN_MESSAGE_RECEIPT_ID + " =?",new String[]{userName, remoteUserGroupName, receiptId},
					null, null, null, null);
		}
		cursor.moveToFirst();
		while(!cursor.isAfterLast()) {
			v.add(fromCursor(cursor));
			cursor.moveToNext();
		}
		cursor.close();
		return v;
	}

	public static void LoadAndUpdateChatRecord(SQLiteDatabase db, String userName, String remoteUsername, boolean isGroup, String receiptId, String receiptStatus)
	{
		ContentValues values = new ContentValues();
		values.put(COLUMN_MESSAGE_RECEIPT_STATUS, receiptStatus);
		db.update(DB_TABLE, values, COLUMN_MESSAGE_RECEIPT_ID + " = ? ", new String[]{receiptId});
	}

	public static void LoadAndUpdateChatRecord(SQLiteDatabase db, String userName, String remoteUsername, boolean isGroup, String receiptId, String receiptStatus, Long timeStamp)
	{
		ContentValues values = new ContentValues();
		values.put(COLUMN_MESSAGE_RECEIPT_ID,receiptId);
		values.put(COLUMN_MESSAGE_RECEIPT_STATUS, receiptStatus);
		db.update(DB_TABLE, values, COLUMN_CREATION_TIMESTAMP + " = ? ", new String[]{ timeStamp.toString()});
	}

	private static ChatRecord fromCursor(Cursor cursor) {
		ChatRecord req = new ChatRecord();
		int i = 0;
		req.setId(cursor.getLong(i++));
		req.setUserName(cursor.getString(i++));
		req.setGroupName(cursor.getString(i++));
		req.setRemoteUserName(cursor.getString(i++));
		req.setCreationTimestamp(cursor.getLong(i++));
		req.setMessageDirection(cursor.getInt(i++));
		req.setMimeType(cursor.getString(i++));
		req.setMessageText(cursor.getString(i++));
		req.setMessageFileName(cursor.getString(i++));
		req.setMessageFilePath(cursor.getString(i++));
		req.setMessageReceiptId(cursor.getString(i++));
		req.setMessageReceiptStatus(cursor.getString(i++));
		req.setFileSizeExpected(cursor.getLong(i++));
		req.setFileSizeDisk(cursor.getLong(i++));

		//req.

		/*
		req.setId(cursor.getLong(i++));
		req.setPhoneNumber(cursor.getString(i++));
		req.setValidationCode1(cursor.getString(i++));
		req.setValidationCode1(cursor.getString(i++));
		req.setCreationTimestamp(cursor.getLong(i++));
		req.setAuthPassword(cursor.getString(i++));
		*/
		
		return req;
	}

	public final static String CHAT_RECORD_ID = "chatRecord";
	
	@Override
	protected String getTableName() {
		return DB_TABLE;
	}
	
	@Override
	protected void putValues(ContentValues values) {
		values.put(COLUMN_USER_NAME, getUserName());
		values.put(COLUMN_GROUP_NAME, getGroupName());
		values.put(COLUMN_REMOTE_USER_NAME, getRemoteUserName());
		values.put(COLUMN_CREATION_TIMESTAMP, getCreationTimestamp());
		values.put(COLUMN_MESSAGE_DIRECTION, getMessageDirection());
		values.put(COLUMN_MIME_TYPE, getMimeType());
		values.put(COLUMN_MESSAGE_TEXT, getMessageText());
		values.put(COLUMN_MESSAGE_FILE_NAME, getMessageFileName());
		values.put(COLUMN_MESSAGE_FILE_PATH, getMessageFilePath());
		values.put(COLUMN_MESSAGE_RECEIPT_ID, getMessageReceiptId());
		values.put(COLUMN_MESSAGE_RECEIPT_STATUS, getMessageReceiptStatus());
		values.put(COLUMN_FILE_SIZE_EXPECTED, getFileSizeExpected());
		values.put(COLUMN_FILE_SIZE_DISK, getFileSizeDisk());

		/*
		values.put(COLUMN_PHONE_NUMBER, getPhoneNumber());
		values.put(COLUMN_VALIDATION_CODE_1, getValidationCode1());
		values.put(COLUMN_VALIDATION_CODE_2, getValidationCode2());
		values.put(COLUMN_CREATION_TIMESTAMP, getCreationTimestamp());
		values.put(COLUMN_AUTH_PASSWORD, getAuthPassword());
		*/
	}

	public String getUserName()
	{
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getGroupName()
	{
		return groupName;
	}

	public void setGroupName(String groupName) {
		this.groupName = groupName;
	}

	public String getRemoteUserName()
	{
		return remoteUserName;
	}

	public void setRemoteUserName(String remoteUserName) {
		this.remoteUserName = remoteUserName;
	}

	public Long getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(Long creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public int getMessageDirection()
	{
		return messageDirection;
	}

	public void setMessageDirection(int messageDirection) {
		this.messageDirection = messageDirection;
	}

	public String getMimeType()
	{
		return mimeType;
	}

	public void setMimeType(String mimeType) {
		this.mimeType = mimeType;
	}

	public String getMessageText()
	{
		return messageText;
	}

	public void setMessageText(String messageText) {
		this.messageText = messageText;
	}

	public String getMessageFileName()
	{
		return messageFileName;
	}

	public void setMessageFileName(String messageFileName) {
		this.messageFileName = messageFileName;
	}

	public String getMessageFilePath()
	{
		return messageFilePath;
	}

	public void setMessageFilePath(String messageFilePath) {
		this.messageFilePath = messageFilePath;
	}

	public String getMessageReceiptId()
	{
		return receiptId;
	}

	public void setMessageReceiptId(String receiptId) {
		this.receiptId = receiptId;
	}

	public String getMessageReceiptStatus()
	{
		return receiptStatus;
	}

	public void setMessageReceiptStatus(String receiptStatus) {
		this.receiptStatus = receiptStatus;
	}

	public long getFileSizeExpected()
	{
		return fileSizeExpected;
	}

	public void setFileSizeExpected(long fileSizeExpected) {
		this.fileSizeExpected = fileSizeExpected;
	}

	public long getFileSizeDisk()
	{
		return fileSizeDisk;
	}

	public void setFileSizeDisk(long fileSizeDisk) {
		this.fileSizeDisk = fileSizeDisk;
	}

	/*
	public String getPhoneNumber() {
		return phoneNumber;
	}
	
	public void setPhoneNumber(String phoneNumber) {
		this.phoneNumber = phoneNumber;
	}

	public String getValidationCode1() {
		return validationCode1;
	}

	public void setValidationCode1(String validationCode1) {
		this.validationCode1 = validationCode1;
	}

	public String getValidationCode2() {
		return validationCode2;
	}

	public void setValidationCode2(String validationCode2) {
		this.validationCode2 = validationCode2;
	}

	public long getCreationTimestamp() {
		return creationTimestamp;
	}

	public void setCreationTimestamp(long creationTimestamp) {
		this.creationTimestamp = creationTimestamp;
	}

	public String getAuthPassword() {
		return authPassword;
	}

	public void setAuthPassword(String authPassword) {
		this.authPassword = authPassword;
	}
	*/

	@Override
	public String getTitleForMenu() {
		return getId() + "";
	}

	@Override
	public String getKeyForIntent() {
		return CHAT_RECORD_ID;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = super.hashCode();
		/*
		result = prime * result
				+ ((authPassword == null) ? 0 : authPassword.hashCode());
		result = prime * result
				+ (int) (creationTimestamp ^ (creationTimestamp >>> 32));
		result = prime * result
				+ ((phoneNumber == null) ? 0 : phoneNumber.hashCode());
		result = prime * result
				+ ((validationCode1 == null) ? 0 : validationCode1.hashCode());
		result = prime * result
				+ ((validationCode2 == null) ? 0 : validationCode2.hashCode());
		*/
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (!super.equals(obj))
			return false;
		if (!(obj instanceof ChatRecord))
			return false;
		ChatRecord other = (ChatRecord) obj;

		if (userName == null) {
			if (other.userName!=null)
				return false;
		} else if (!userName.equals(other.userName)) {
			return false;
		}

		if (groupName == null) {
			if (other.groupName!=null)
				return false;
		} else if (!groupName.equals(other.groupName)) {
			return false;
		}

		if (remoteUserName == null) {
			if (other.remoteUserName!=null)
				return false;
		} else if (!remoteUserName.equals(other.remoteUserName)) {
			return false;
		}

		if (creationTimestamp != other.creationTimestamp)
		{
			return false;
		}
		if (messageDirection != other.messageDirection) {
			return false;
		}

		if (mimeType == null) {
			if (other.mimeType!=null)
				return false;
		} else if (!mimeType.equals(other.mimeType)) {
			return false;
		}


		if (messageText == null) {
			if (other.messageText!=null)
				return false;
		} else if (!messageText.equals(other.messageText)) {
			return false;
		}

		if (messageFileName == null) {
			if (other.messageFileName!=null)
				return false;
		} else if (!messageFileName.equals(other.messageFileName)) {
			return false;
		}

		if (messageFilePath == null) {
			if (other.messageFilePath!=null)
				return false;
		} else if (!messageFilePath.equals(other.messageFilePath)) {
			return false;
		}

		if (receiptId == null) {
			if (other.receiptId!=null)
				return false;
		} else if (!receiptId.equals(other.receiptId)) {
			return false;
		}

		if (receiptStatus == null) {
			if (other.receiptStatus!=null)
				return false;
		} else if (!receiptStatus.equals(other.receiptStatus)) {
			return false;
		}

		if (fileSizeExpected != other.fileSizeExpected) {
			return false;
		}

		if (fileSizeDisk != other.fileSizeDisk) {
			return false;
		}
		/*
		if (authPassword == null) {
			if (other.authPassword != null)
				return false;
		} else if (!authPassword.equals(other.authPassword))
			return false;
		if (creationTimestamp != other.creationTimestamp)
			return false;
		if (phoneNumber == null) {
			if (other.phoneNumber != null)
				return false;
		} else if (!phoneNumber.equals(other.phoneNumber))
			return false;
		if (validationCode1 == null) {
			if (other.validationCode1 != null)
				return false;
		} else if (!validationCode1.equals(other.validationCode1))
			return false;
		if (validationCode2 == null) {
			if (other.validationCode2 != null)
				return false;
		} else if (!validationCode2.equals(other.validationCode2))
			return false;
		*/
		return true;
	}
	public static void storeMessage(Context context, String userName, String remoteUserName, int messageDirection, String groupName, String messageType, String messageText, String fileName, Long timeStamp, String receiptId, String receiptStatus, long fileSizeExpected, long fileSizeDisk) {
		ChatRecord req = new ChatRecord();
		req.setUserName(userName);
		req.setRemoteUserName(remoteUserName);
		req.setMessageDirection(messageDirection);
		req.setMimeType(messageType);
		req.setMessageText(messageText);
		req.setMessageFileName(fileName);
		req.setCreationTimestamp(timeStamp); //Store time lately:
		req.setMessageReceiptId(receiptId);
		req.setMessageReceiptStatus(receiptStatus);
		req.setFileSizeExpected(fileSizeExpected);
		req.setFileSizeDisk(fileSizeDisk);
		//filepath shall always be same
		if (fileName!=null)
		{
			//req.setMessageFilePath(FILEPATH); //Define Filepath
		}
		DataSource db = new DataSource(context);
		db.open();
		//ChatRecordCache e = new ChatRecordCache(req.getId(),req.getUserName(), req.getGroupName() , req.getRemoteUserName(),req.getCreationTimestamp(),
		//        req.getMessageDirection() ,req.getMessageType(), req.getMessageText(), req.getMessageFileName(), req.getMessageFilePath());
		//chatCacheList.add(e);


		db.persistChatRecord(req);
		db.close();
	}

	public static void clearFromDB(Context context, String userName)
	{
		DataSource db = new DataSource(context);
		synchronized (db)
		{
			db.open();
			List<ChatRecord> reqs = db.getChatRecords(userName);
			for (ChatRecord _req : reqs) {
				db.deleteChatRecord(_req);
			}
			db.close();
		}
	}

	public static void clearFromDB(Context context, String userName, String remoteUsername, boolean isGroup)
	{
		DataSource db = new DataSource(context);
		//synchronized (db)
		{
			db.open();
			List<ChatRecord> reqs = db.getChatRecords(userName, remoteUsername, isGroup);
			for (ChatRecord _req : reqs) {
				db.deleteChatRecord(_req);
			}
			db.close();
		}
	}

	public static void clearFromDB(Context context, String userName, String remoteUsername, boolean isGroup, String receiptId)
	{
		DataSource db = new DataSource(context);
		//synchronized (db)
		{
			db.open();
			List<ChatRecord> reqs = db.getChatRecords(userName, remoteUsername, isGroup, receiptId);
			for (ChatRecord _req : reqs) {
				db.deleteChatRecord(_req);
			}
			db.close();
		}
	}
}
