package com.bitskeys.track.db;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.logging.Logger;

//Manish
public class DatabaseHelper extends SQLiteOpenHelper {
	
	private Logger logger = Logger.getLogger(getClass().getName());
	
	private final static String DB_NAME = "BitChat";
	public final static int DB_VERSION = 8;
	public final static int APP_VERSION = 1;
	
	public DatabaseHelper(Context context) {
		super(context, DB_NAME, null, DB_VERSION);
	}

	@Override
	public void onCreate(SQLiteDatabase db) {
		ChatRecord.onCreate(db);
		MyRecord.onCreate(db);
		UserRecord.onCreate(db);
		LocationRecord.onCreate(db);
		ProfileImageRecord.onCreate(db);
		MyLocationRecord.onCreate(db);
		RoomRecord.onCreate(db);
		RoomRecordNew.onCreate(db);
		/*
		SIPIdentity.onCreate(db);
		PTTChannel.onCreate(db);
		ENUMSuffix.onCreate(db);
		
		//Manish: added for Ctalk, commented SIP5060
		CTalkRegisterRequest.onCreate(db);
		SIP5060ProvisioningRequest.onCreate(db);
		*/
	}

	@Override
	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
		logger.info("Upgrading DB from version " + oldVersion + " to version " + newVersion);

		switch(oldVersion)
		{
			case 6:
				MyLocationRecord.onUpgrade(db, oldVersion, newVersion);
			case 7:
				RoomRecord.onCreate(db);
			case 8:
				//Just change whatever has been changed in db from 7 to 8 here.
				//Do not change everything from hereon.
		}
	}
}
