package com.bitskeys.track.db;

/**
 * Created by GUR11219 on 11-08-2015.
 */
public class ChatRecordCache {


    private long mId = 0;
    private String mUserName = null;
    private String mGroupName = null;
    private String mRemoteUserName = null;
    private long mCreationTimestamp = 0;
    private  int mMessageDirection = 0;
    private String mMessageType = null;
    private String mMessageText = null;
    private String mMessageFileName = null;
    private String mMessageFilePath = null;

    public void ChatRecordCache()
    {
        mId = 0;
        mUserName = new String("");
        mGroupName = new String("");
        mRemoteUserName = new String("");
        mCreationTimestamp = 0;
        mMessageDirection = 0;
        mMessageType = new String("");
        mMessageText = new String("");
        mMessageFileName = new String("");
        mMessageFilePath = new String("");
    }

    public ChatRecordCache(long id, String userName, String groupName, String remoteUserName,long creationTimestamp, int messageDirection, String messageType, String messageText, String messageFileName, String messageFilePath )
    {
        mId = id;
        mUserName = userName;
        mGroupName = groupName;
        mRemoteUserName = remoteUserName;
        mCreationTimestamp = 0;
        mMessageDirection = messageDirection;
        mMessageType = messageType;
        mMessageText = messageText;
        mMessageFileName = messageFileName;
        mMessageFilePath = messageFilePath;
    }

    public long getId()
    {
        return mId;
    }
    public void setId(long id) {
        this.mId = id;
    }


    public String getUserName()
    {
        return mUserName;
    }
    public void setUserName(String userName) {
        this.mUserName = userName;
    }

    public String getGroupName()
    {
        return mGroupName;
    }
    public void setGroupName(String groupName) {
        this.mGroupName = groupName;
    }

    public String getRemoteUserName()
    {
        return mRemoteUserName;
    }
    public void setRemoteUserName(String remoteUserName) {
        this.mRemoteUserName = remoteUserName;
    }

    public long getCreationTimestamp() {
        return mCreationTimestamp;
    }
    public void setCreationTimestamp(long creationTimestamp) {
        this.mCreationTimestamp = creationTimestamp;
    }

    public int getMessageDirection()
    {
        return mMessageDirection;
    }
    public void setMessageDirection(int messageDirection) {
        this.mMessageDirection = messageDirection;
    }

    public String getMessageType()
    {
        return mMessageType;
    }
    public void setMessageType(String messageType) {
        this.mMessageType = messageType;
    }

    public String getMessageText()
    {
        return mMessageText;
    }
    public void setMessageText(String messageText) {
        this.mMessageText = messageText;
    }

    public String getMessageFileName()
    {
        return mMessageFileName;
    }
    public void setMessageFileName(String messageFileName) {
        this.mMessageFileName = messageFileName;
    }

    public String getMessageFilePath()
    {
        return mMessageFilePath;
    }
    public void setMessageFilePath(String messageFilePath) {
        this.mMessageFilePath = messageFilePath;
    }
}
