package com.bitskeys.track.chat;

import android.content.Context;
import android.os.Handler;
import android.util.Log;

import com.bitskeys.track.chatRoom.MultiChat;
import com.bitskeys.track.chatRoom.RoomListeners;
import com.bitskeys.track.chatRoom.RoomListenersCache;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.TimeClock;
import com.bitskeys.track.login.ChatConnection;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;

import java.util.Set;

/**
 * Created by Mr.Neeraj on 24-05-2016.
 */
public class MesgCmdProcessor {
    /*
      Format : BITS_CMD==Command==BITS_ARGS==ARG1==ARG2....

      where Command can be : MCA : MultiChat Add for adding a user to room where ARG1 is  room name
                             MCR : MultiChat Remove a user from room by administrator where ARG1 is room name.
     */

    public static String makeRoomInvitationCmdMessage(String room)
    {
        String cmd = "BITS_CMD==MCA==BITS_ARGS==" + room;
        return cmd;
    }

    public static String makeRoomRemovalCmdMessage(String from,String room)
    {
        String cmd = "BITS_CMD==MCR==BITS_ARGS==" + room;
        return cmd;
    }

    public static boolean sendRoomInvitationInMessage(Context appCtxt, String room, String remoteUserName)
    {
        String mesg = makeRoomInvitationCmdMessage(room);
        final long timeStamp = TimeClock.getEpocTime();
        ChatXMPPService.SendMessage(appCtxt,mesg, remoteUserName, ChatBubbleActivity.CHAT_MESSAGE_TYPE_CMD, timeStamp, false,false); //false means first time being sent
        return false;
    }

    public static MesgCmdInfo processCmdInMessage(String from,String message)
    {
        String command = null;
        String args[] = null;
        if(from == null || !message.contains("BITS_CMD"))
        {
            /* Not a command message */
            return null;
        }

        String tempa[] = message.split("BITS_CMD");
        if(tempa== null || tempa.length <2)
            return null;

        String tempb[] = tempa[1].split("BITS_ARGS");
        if(tempb.length == 1)
        {
            /* Only Command is present */
            String tempc[]= tempa[1].split("==");
            command = tempc[1];
        }
        else
        {
            String tempc[]= tempb[0].split("==");
            command = tempc[1];
            String tempArgs[] = tempb[1].split("==");
            if(tempArgs[0].equals(""))
            {
                if(tempArgs.length > 1)
                {
                    args = new String[tempArgs.length -1];
                    for(int i=0;i<args.length;i++)
                    {
                        args[i] = tempArgs[i+1];
                    }
                }

            }
            else
            {
                args = tempArgs;
            }

        }
        if(command.length()>0)
        {
            MesgCmdInfo cmdInfo =  new MesgCmdInfo(command,args);
            executeCommand(command,args,from);
            return cmdInfo;
        }



        return null;
    }

    public static void executeCommand(String cmd, final String[] args,String from)
    {
        if(cmd.equals("MCA"))
        {
            /* Invalid command string */
            if(args.length < 1)
                return;

            ChatConnection chatConnection = ChatConnection.getInstance();
            if(chatConnection != null){
                XMPPTCPConnection mConnection = chatConnection.getConnection();
                if(mConnection != null)
                {
                    MultiChat.joinTheChatRoomFromMesgCmd(mConnection,args[0]);
                    RoomListenersCache.initializeRoomListeners(args[0]);
//                  Following commented code worked.
//                    Set<String> rooms = MultiChat.getJoinedRooms(mConnection);
//                    Log.d("Total Rooms are ",String.valueOf(rooms.size()));
                }
            }

        }
        else if(cmd.equals("MCR"))
        {
            //this is request for removal a user from room
            //TODO
        }

    }
}
