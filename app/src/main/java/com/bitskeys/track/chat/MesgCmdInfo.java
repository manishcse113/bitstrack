package com.bitskeys.track.chat;

/**
 * Created by Mr.Neeraj on 25-05-2016.
 */
public class MesgCmdInfo {
    String CommandName;
    String args[];

    public MesgCmdInfo(String commandName, String[] args) {
        CommandName = commandName;
        this.args = args;
    }

    public String getCommandName() {
        return CommandName;
    }

    public void setCommandName(String commandName) {
        CommandName = commandName;
    }

    public String[] getArgs() {
        return args;
    }

    public void setArgs(String[] args) {
        this.args = args;
    }
}
