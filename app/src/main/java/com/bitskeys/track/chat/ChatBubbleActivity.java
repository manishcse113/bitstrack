package com.bitskeys.track.chat;

/**
 * Created by Manish on 28-09-2015.
 */

import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.database.Cursor;
import android.database.DataSetObserver;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.NotificationCompat;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnKeyListener;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.RestAPI.OpenfireRestAPI;
import com.bitskeys.track.chatRoom.RoomListenersCache;
import com.bitskeys.track.db.ChatRecord;
import com.bitskeys.track.db.DataSource;
import com.bitskeys.track.gen.BitMapWorkerRequest;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.FileSharing;
//import com.bitskeys.track.gen.FileTransferWrapper;
import com.bitskeys.track.gen.TimeClock;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.users.ChatUsersListActivity;
import com.bitskeys.track.users.UserListLayout;

import org.jivesoftware.smack.AbstractXMPPConnection;
import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.chat.Chat;
import org.jivesoftware.smack.chat.ChatManager;
import org.jivesoftware.smack.chat.ChatManagerListener;
import org.jivesoftware.smackx.receipts.DeliveryReceipt;
import org.jivesoftware.smackx.receipts.DeliveryReceiptRequest;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.net.URL;
import java.net.URLConnection;
import java.util.List;

//public class ChatBubbleActivity extends AppCompatActivity implements ChatArrayAdapter.ChatListCallback {

public class ChatBubbleActivity extends AppCompatActivity implements ChatArrayAdapter.ChatListCallback {
    //extends Activity {
    private static final String TAG = "ChatActivity";

    private static ChatArrayAdapter chatArrayAdapter;
    private ListView listView;
    private EditText chatText;
    private Button buttonSend;
    TextView mRemoteUserName = null;

    Intent intent;
    private boolean side = false;

    AbstractXMPPConnection mConnection = null;
    //ChatMessageListener myMessageListener = null;
    ChatManagerListener myChatListener = null;


    static String remoteUserName = null;
    static String remoteUserdisplayName = null;
    //static String historyTextStr;

    private final String FILENAME = "GBC: ChatBubble#";
    public static String CHAT_MESSAGE_TYPE_CMD = "Cmd";
    public static String CHAT_MESSAGE_TYPE_TEXT = "Text";
    public static String CHAT_MESSAGE_TYPE_IMAGE = "Image";
    public static String CHAT_MESSAGE_TYPE_STREAM = "Stream";
    public static int MESSAGEDIRECTIONRECEIVED = 0;
    public static int MESSAGEDIRECTIONSENT = 1;
    public static String MESSAGE_ACK_AWAITED = "ACKAWAITED";
    public static String MESSAGE_ACKED = "ACKED";
    public static String MESSAGE_RESEND_REQUIRED = "RESEND";

    public static String MESSAGE_READYTO_SEND = "READYTOSEND";
    public static String MESSAGE_FILE_UPLOADED = "UPLOADED";
    public static String MESSAGE_FILE_TODOWNLOAD = "TODOWNLOAD";

    public static String MESSAGE_FAILED_PERMANENT = "PERMANENT_FAILED";



    private String selectedImagePath;
    private static final int PICTURE_GALLERY = 1;
    private static final int FILE_SELECT_CODE = 2;

    public static boolean mDeliverythreadRunning = false;

    //public static List<ChatRecordCache> chatCacheList;
    private boolean mBoolLoadDB;

    static public boolean mIsFileSelectedForSharing = false;
    static public boolean mIsTextSelectedForSharing = false;
    static public int m_position;
    static public String m_fileNameWithPath = null;
    static public String m_textForSending = null;
    static public String m_mimeType;
    static public String m_receiptId;
    private boolean isRoom = false;
    private String roomName = null;
//    private String remoteUserName;

    private ActionBar actionBar;
    public void showActionBar() {
        actionBar.show();
    }
    /*
    public void hideActionBar() {
        actionBar.hide();
    }
    */
    @Override
    public void imageSelected(int position, String fileNameWithPath, String mimeType, String receiptId) {
        mIsFileSelectedForSharing = false;// This is set to true - while selecting sharing buttong
        this.m_position = position;
        this.m_fileNameWithPath = fileNameWithPath;
        this.m_mimeType = mimeType;
        this.m_receiptId = receiptId;
        //showActionBar();
    }

    @Override
    public void imageDeSelected(int position, String fileNameWithPath, String mimeType, String receiptId) {
        this.m_position = position;
        this.m_fileNameWithPath = fileNameWithPath;
        this.m_mimeType = mimeType;
        this.m_receiptId = receiptId;
        //hideActionBar();
    }

    @Override
    public void textSelected(int position, String text, String mimeType, String receiptId) {
        mIsTextSelectedForSharing = false;// This is set to true - while selecting sharing buttong
        this.m_position = position;
        this.m_textForSending = text;
        this.m_mimeType = mimeType;
        this.m_receiptId = receiptId;
        //showActionBar();
    }

    @Override
    public void textDeSelected(int position, String text, String mimeType, String receiptId) {
        this.m_position = position;
        this.m_textForSending = text;
        this.m_mimeType = mimeType;
        this.m_receiptId = receiptId;
        //hideActionBar();
    }

    @Override
    public void activateDefaultMenu() {
        //hideActionBar();
        setDefauleMenu(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            this.invalidateOptionsMenu();
        }
    }
    @Override
    public void activateSelectedMenu() {
        //hideActionBar();
        setDefauleMenu(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            this.invalidateOptionsMenu();
        }
    }



    public final static long DefaultMinMessagesToShow = 50;
    public final static long DefaultLoadMinDaysMessages = 7;
    public final static long Load1MonthDaysMessages = 31;
    public final static long Load3MonthDaysMessages = 91;
    public final static long Load6MonthDaysMessages = 181;
    public final static long Load1YearDaysMessages = 366;

    public final static String gLoad1MonthMessages = "Load Previous 1 months messages...";
    public final static String gLoad3MonthMessages = "Load Previous 3 months messages...";
    public final static String gLoad6MonthMessages = "Load Previous 6 months messages...";
    public final static String gLoad1YearMessages =  "Load Previous 1 year messages...";

    @Override
    public void loadPreviousMessages(String message)
    {
        chatArrayAdapter.clear();
        if (message.equals(gLoad1MonthMessages))
        {
            loadFromDB(ChatXMPPService.mUserName, remoteUserName, false, Load1MonthDaysMessages);
        }
        else if (message.equals(gLoad3MonthMessages))
        {
            loadFromDB(ChatXMPPService.mUserName, remoteUserName, false, Load3MonthDaysMessages);
        }
        else if (message.equals(gLoad6MonthMessages))
        {
            loadFromDB(ChatXMPPService.mUserName, remoteUserName, false, Load6MonthDaysMessages);
        }
        else if (message.equals(gLoad1YearMessages))
        {
            loadFromDB(ChatXMPPService.mUserName, remoteUserName, false, Load1YearDaysMessages);
        }
        return;
    }

    public boolean isRoom() {
        return isRoom;
    }

    public void setRoom(boolean room) {
        isRoom = room;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //Intent i = getIntent();
        Bundle extras = getIntent().getExtras();
        isRoom = extras.getBoolean(ChatUsersListActivity.IS_ROOM);
        if(!isRoom)
        {
            remoteUserName = extras.getString(ChatUsersListActivity.REMOTE_USER_NAME);

        }
        else
        {
            roomName = extras.getString(ChatUsersListActivity.REMOTE_USER_NAME);
            RoomListenersCache.initializeRoomListeners(roomName);
        }


        setContentView(R.layout.activity_chat);
        mBoolLoadDB = true;

        buttonSend = (Button) findViewById(R.id.buttonSend);

        listView = (ListView) findViewById(R.id.listView1);

        chatArrayAdapter = new ChatArrayAdapter(getApplicationContext(), R.layout.chatlistlayout2);
        listView.setAdapter(chatArrayAdapter);

        chatArrayAdapter.setCallback(this);


        chatText = (EditText) findViewById(R.id.chatText);
        chatText.setOnKeyListener(new OnKeyListener() {
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                if ((event.getAction() == KeyEvent.ACTION_DOWN) && (keyCode == KeyEvent.KEYCODE_ENTER)) {
                    return sendChatMessage();
                }
                return false;
            }
        });
        buttonSend.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                sendChatMessage();
            }
        });


//       Following code is not required as bBrowse button is removed from activity_chat.xml,
//        still keeping: Neeraj

//        ((Button) findViewById(R.id.bBrowse))
//                .setOnClickListener(new View.OnClickListener() {
//                    public void onClick(View arg0) {
//                        Intent intent;
//                        if (Build.VERSION.SDK_INT < 19) {
//                            intent = new Intent();
//                            intent.setAction(Intent.ACTION_GET_CONTENT);
//                            intent.setType("*/*");
//                            startActivityForResult(Intent.createChooser(intent, "Select file to upload "), PICTURE_GALLERY);
//                        } else {
//                            intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
//                            intent.addCategory(Intent.CATEGORY_OPENABLE);
//                            intent.setType("*/*");
//                            startActivityForResult(Intent.createChooser(intent, "Select file to upload "), PICTURE_GALLERY);
//                        }
//                        //Following works fine:
//                        //Intent photoPickerIntent = new Intent(Intent.ACTION_PICK, android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
//                        //startActivityForResult(photoPickerIntent, PICTURE_GALLERY);
//                    }
//                });
        ;
        listView.setTranscriptMode(AbsListView.TRANSCRIPT_MODE_ALWAYS_SCROLL);
        listView.setAdapter(chatArrayAdapter);

        //to scroll the list view to bottom on data change
        chatArrayAdapter.registerDataSetObserver(new DataSetObserver() {
            @Override
            public void onChanged() {
                super.onChanged();
                listView.setSelection(chatArrayAdapter.getCount() - 1);
            }
        });

        onHandleIntent(getIntent());
        doBindService();

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            //getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            String displayName = ChatXMPPService.getDisplayName(remoteUserName);

            /* This is display Name */
            getSupportActionBar().setTitle(displayName);
            //getSupportActionBar().setTitle(mRemoteUserName.getText().toString());

            //toolbar.setNavigationIcon(R.mipmap.ic_launcher);
            //toolbar.setNavigationIcon(R.mipmap.app_icon);

            //toolbar.setNavigationIcon(R.drawable.app_icon);
            //getSupportActionBar().setLogo(R.drawable.toolbar_app_icon);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
            //actionBar.hide();
            mDefaultMenu = true;
            showActionBar();
        }

        /*
        if (mDeliverythreadRunning == false)
        {
            mDeliverythreadRunning = true;
            monitoringReliableDeliveryThread();
        }
        */
    }

    void setDisplayNameOnScreen(String displayName)
    {
        String tempDisplayName = Utility.FormatStringToUserName(displayName);


        if(tempDisplayName != null)
        {
            if(mRemoteUserName != null)
            {
                mRemoteUserName.setText(tempDisplayName);
            }
            ActionBar aBar = getSupportActionBar();
            if(aBar != null)
            {
                aBar.setTitle(tempDisplayName);
            }
        }
    }
    private boolean sendChatMessage() {
        if (chatText.getText().toString() != null && chatText.getText().toString().length() >0) {
            chatText.setMovementMethod(new ScrollingMovementMethod());

            final long timeStamp = TimeClock.getEpocTime();
            Log.d(FILENAME, "setOnClickListener: Sending to" + remoteUserName);
            String receiptId = ChatXMPPService.SendMessage(getApplicationContext(), chatText.getText().toString(), remoteUserName, CHAT_MESSAGE_TYPE_TEXT, timeStamp, false,isRoom()); //false means first time being sent
            if (mBoolLoadDB) {
                //false here means that currently we are working only with usernames and not groups. Later it needs to be enhanced.
                loadFromDB(ChatXMPPService.mUserName, remoteUserName, false, DefaultLoadMinDaysMessages);
            }
            listView = (ListView) findViewById(R.id.listView1);
            //For now groups are false
            //Show message
            //If receiptId is null - then we have marked this as RESENT REQUIRED in sendMessage in db
            //if (receiptId == null), it is null - if I am offline
            //if peer is offline - then it shall be fine.
            chatArrayAdapter.add(new ChatMessage(MESSAGEDIRECTIONSENT, chatText.getText().toString(), CHAT_MESSAGE_TYPE_TEXT, null, timeStamp, receiptId, MESSAGE_ACK_AWAITED, 0, 0));
            chatText.setText("");
            listView.setScrollY(0);
        }
        return true;
    }

    /*
    public String SendMessage(Context context, String textToSend, String toUser, String messageType)
    {
        String deliveryReceiptId = null;
        try {
            Long timeStamp = TimeClock.getEpocTime();
            String receiptStatus = ChatBubbleActivity.MESSAGE_ACK_AWAITED;
            Chat chat = ChatManager.createChat(toUser + "@" + ChatXMPPService.DOMAIN);
            org.jivesoftware.smack.packet.Message message = new org.jivesoftware.smack.packet.Message();
            message.setBody(textToSend);
            deliveryReceiptId = DeliveryReceiptRequest.addTo(message);
            message.addExtension(new DeliveryReceipt(deliveryReceiptId));

            Log.d(FILENAME, "sendMessage: deliveryReceiptId for this message is: " + deliveryReceiptId);
            ChatRecord.storeMessage(context, ChatXMPPService.mUserName, toUser, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT, textToSend, null, timeStamp, deliveryReceiptId, receiptStatus, 0, 0);
            chat.sendMessage(message);

        }
        catch (SmackException.NotConnectedException e) {
            String string = e.getMessage();
            Log.e(FILENAME, "Exception" + string);
            e.printStackTrace();
        }
        return deliveryReceiptId;
    }
*/

    // Following two functions are for
    // For uploading image:
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
        super.onActivityResult(requestCode, resultCode, intent);
        String filePath = null;
        switch (requestCode) {
            case FILE_SELECT_CODE:
                if (resultCode == RESULT_OK) {
                    // Get the Uri of the selected file
                    Uri uri = intent.getData();
                    String mimeType = getContentResolver().getType(uri);
                    Log.d(FILENAME, "File Uri: " + uri.toString());
                    // Get the path
                    try {
                        filePath = Utility.getPath(this, uri);
                        Log.d(FILENAME, "File Path: " + filePath);
                    } catch (URISyntaxException e) {
                        Log.d(FILENAME, "Exception in getting path for uri " + uri);
                        e.printStackTrace();
                    }
                    // Get the file instance
                    // File file = new File(path);
                    // Initiate the upload
                    //filePath = cursor.getString(columnIndex);
                    selectedImagePath = filePath;
                    //cursor.close();

                    //Bitmap the_image = decodeFile(new File(filePath));
                    //sendFileAndUpdateView(filePath, mRemoteUserName.getText().toString(), mimeType);
                    sendFileAndUpdateView(filePath, remoteUserName, mimeType);

                }
                break;

            case PICTURE_GALLERY:
                if (resultCode == RESULT_OK && intent != null) {
                    Uri selectedImage = intent.getData();
                    final String mimeType = getContentResolver().getType(selectedImage);
                    final String[] filePathColumn = {MediaStore.MediaColumns.DATA, MediaStore.MediaColumns.DISPLAY_NAME};
                    Cursor cursor = getContentResolver().query(selectedImage, filePathColumn, null, null, null);
                    // some devices (OS versions return an URI of com.android instead of com.google.android
                    if (selectedImage.toString().startsWith("content://com.android.gallery3d.provider")) {
                        // use the com.google provider, not the com.android provider.
                        selectedImage = Uri.parse(selectedImage.toString().replace("com.android.gallery3d", "com.google.android.gallery3d"));
                    }
                    if (cursor != null) {
                        cursor.moveToFirst();
                        int columnIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DATA);
                        // if it is a picasa image on newer devices with OS 3.0 and up
                        if (selectedImage.toString().startsWith("content://com.google.android.gallery3d") ||
                                selectedImage.toString().startsWith("content://com.android.providers") ||
                                selectedImage.toString().startsWith("content://com.android.content") ||
                                selectedImage.toString().startsWith("content://com.android.externalstorage")
                                ) {
                            columnIndex = cursor.getColumnIndex(MediaStore.MediaColumns.DISPLAY_NAME);
                            if (columnIndex != -1) {
                                //progress_bar.setVisibility(View.VISIBLE);
                                final Uri uriurl = selectedImage;
                                // Do this in a background thread, since we are fetching a large image from the web
                                listView = (ListView) findViewById(R.id.listView1);

                               // new Thread(new Runnable() {
                               //     public void run()
                                    {
                                        File f = getBitmap("file_name", uriurl);
                                        //File f = getBitmap(uri.toString());
                                        if (f != null) {
                                            String localFilePath = f.getPath();
                                            sendFileAndUpdateView(localFilePath, remoteUserName, mimeType);
                                        }
                                        else
                                        {
                                            Log.d(FILENAME,"file is null");
                                        }
                                    }
                               // }).start();


                                //new LoadImageInBackground(ChatBubbleActivity.this, uriurl, this.getBaseContext(), remoteUserName, mimeType, chatArrayAdapter, listView).execute();
                            }
                        } else { // it is a regular local image file
                            filePath = cursor.getString(columnIndex);
                            if (filePath != null) {
                                selectedImagePath = filePath;
                                cursor.close();
                                //Bitmap the_image = decodeFile(new File(filePath));
                                //String textHistory = ChatXMPPService.mUserName + ": ";
                                //textHistory += selectedImagePath.toString();

                                //sendFileAndUpdateView(filePath, mRemoteUserName.getText().toString(), mimeType);
                                sendFileAndUpdateView(filePath, remoteUserName, mimeType);
                                /*
                                new Thread(new Runnable() {
                                    public void run() {
                                        FileTransferWrapper.SendFile(selectedImagePath, mRemoteUserName.getText().toString());
                                        // FileTransferWrapper.SendFile(selectedImagePath, mRemoteUserName.getText().toString() + "@" + ChatXMPPService.DOMAIN);
                                    }
                                }).start();

                                ChatRecord.storeMessage(this, ChatXMPPService.mUserName, mRemoteUserName.getText().toString(), MESSAGEDIRECTIONSENT, null, mimeType, textHistory, filePath);
                                chatArrayAdapter.add(new ChatMessage(MESSAGEDIRECTIONSENT, selectedImagePath, mimeType, filePath));
                                listView = (ListView) findViewById(R.id.listView1);
                                listView.setScrollY(0);
                                */
                            } else {
                                Log.d(FILENAME, "ChatBubbleActivity: Error: filepath is null");
                            }

                        }
                    }
                    // If it is a picasa image on devices running OS prior to 3.0
                    else if (selectedImage != null && selectedImage.toString().length() > 0) {
                        //progress_bar.setVisibility(View.VISIBLE);
                        final Uri uriurl = selectedImage;
                        // Do this in a background thread, since we are fetching a large image from the web
                        listView = (ListView) findViewById(R.id.listView1);

                      //  new Thread(new Runnable() {
                      //      public void run()
                            {
                                File f = getBitmap("file_name", uriurl);
                                //File f = getBitmap(uri.toString());
                                if (f != null) {
                                    String localFilePath = f.getPath();
                                    sendFileAndUpdateView(localFilePath, remoteUserName, mimeType);
                                }
                                else
                                {
                                    Log.d(FILENAME,"file is null");
                                }
                            }
                        //}).start();

                        //new LoadImageInBackground(ChatBubbleActivity.this, uriurl, this.getBaseContext(), remoteUserName, mimeType, chatArrayAdapter, listView).execute();
                    }
                }
                break;
        }
    }

    public void sendFileAndUpdateView(final String localFilePath, final String lremoteUserName, final String mimeType) {
        final long timeStamp = TimeClock.getEpocTime();
        final String receiptId = TimeClock.getEpocTimeLongToString(timeStamp);
        boolean isResending = false;
        //getfilesize;
        File f = new File(localFilePath);
        long fileSize = f.length();

        //localfilepath working as receiptId in case of files.
        //ChatRecord.storeMessage(this, ChatXMPPService.mUserName, remoteUserName, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, mimeType, localFilePath, localFilePath, timeStamp, localFilePath,MESSAGE_ACK_AWAITED  );
        //if (isResending == false)
        //{

        final String localFileName = f.getName();
        ChatRecord.storeMessage(this, ChatXMPPService.mUserName, lremoteUserName, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, mimeType, localFilePath, localFilePath, timeStamp, receiptId, ChatBubbleActivity.MESSAGE_READYTO_SEND, fileSize, fileSize);
        //}
        new Thread(new Runnable() {
            public void run() {
                //Last paramenter is resent request or not
                FileSharing.SendFile(getApplicationContext(), localFilePath, lremoteUserName, mimeType, timeStamp, false, receiptId);
                //ChatXMPPService.startActionSendMessage(getApplicationContext(), localFileName,lremoteUserName, CHAT_MESSAGE_TYPE_IMAGE);
            }
        }).start();
        //String textHistory =ChatXMPPService.mUserName + ": ";
        //textHistory += localFilePath;
        chatArrayAdapter.add(new ChatMessage(ChatBubbleActivity.MESSAGEDIRECTIONSENT, localFilePath, mimeType, localFilePath, timeStamp, receiptId, MESSAGE_ACK_AWAITED, fileSize, fileSize));
        listView = (ListView) findViewById(R.id.listView1);
        listView.setScrollY(0);
    }

    @Override
    protected void onDestroy() {
        Log.d(FILENAME, "onDestroy");
        //bManager.unregisterReceiver(bReceiver);
        doUnbindService();
        mBoolLoadDB = true;
        finishActivity(0);
        super.onDestroy();
    }

    @Override
    protected void onNewIntent(Intent intent) {
        onHandleIntent(intent);
    }

    //@Override
    protected void onHandleIntent(Intent intent) {
        Log.d(FILENAME, "onHandleIntent");
        if (intent != null) {
            final String action = intent.getAction();
            if (ChatUsersListActivity.ACTION_CHAT_START.equals(action)) { //View chat
                //doBindService();
                //myNameStr = ChatXMPPService.mUserName;
                remoteUserName = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                //final String remoteUserName = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                remoteUserdisplayName = ChatXMPPService.getDisplayName(remoteUserName);
                Log.d(FILENAME, "onHandleIntent: ACTION_CHAT_START# " + "remoteName" + remoteUserName);
                mRemoteUserName = (TextView) findViewById(R.id.chat_remoteName);
                setDisplayNameOnScreen(remoteUserdisplayName);

//                mRemoteUserName.setText(remoteUserdisplayName);
//                ActionBar aBar = getSupportActionBar();
//                if(aBar != null)
//                    setTitle(remoteUserdisplayName);

                //mRemoteUserName.setText(remoteUserName);
                processReceivedMessageIntent(remoteUserName, null); //If it is just start then we are just loading previous messages - no new message
            }
            else if (ChatXMPPService.ACTION_RECEIVEDMESSAGE.equals(action)) {//View Chat - after message received
                Log.d(FILENAME, "onHandleIntent: ACTION_RECEIVEDMESSAGE");
               // myNameStr = ChatXMPPService.mUserName;
                final String param1 = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                final String message = intent.getStringExtra(ChatXMPPService.MESSAGERECEIVED);
                remoteUserName = param1;
                remoteUserdisplayName = ChatXMPPService.getDisplayName(remoteUserName);
                mRemoteUserName = (TextView) findViewById(R.id.chat_remoteName);
                setDisplayNameOnScreen(remoteUserdisplayName);

//                mRemoteUserName.setText(remoteUserdisplayName);
//                ActionBar aBar = getSupportActionBar();
//                if(aBar != null)
//                    setTitle(remoteUserdisplayName);

                //mRemoteUserName.setText(param1);
                //message has been stored before raising the intent:
                processReceivedMessageIntent(remoteUserName, message);
            }

            if (ChatUsersListActivity.ACTION_CHAT_START_EXTERNAL_FILE_SHARE.equals(action)) { //External file sharing
                //doBindService();
                //myNameStr = ChatXMPPService.mUserName;
                remoteUserName = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                //final String remoteUserName = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                remoteUserdisplayName = ChatXMPPService.getDisplayName(remoteUserName);
                Log.d(FILENAME, "onHandleIntent: ACTION_CHAT_START FILE SHARE# " + "remoteName" + remoteUserName);

                setDisplayNameOnScreen(remoteUserdisplayName);

//                mRemoteUserName = (TextView) findViewById(R.id.chat_remoteName);
//                mRemoteUserName.setText(remoteUserdisplayName);
//                ActionBar aBar = getSupportActionBar();
//                if(aBar != null)
//                    setTitle(remoteUserdisplayName);

                final String fileUri = (String)intent.getStringExtra(ChatUsersListActivity.EXTERNAL_FILE);
                final String mimeType = (String)intent.getStringExtra(ChatUsersListActivity.MIME_TYPE);
                mIsFileSelectedForSharing = true;
                m_fileNameWithPath = fileUri;
                m_mimeType = mimeType;

                processSendFileIntent(remoteUserName); //If it is just start then we are just loading previous messages - no new message
            }
            if (ChatUsersListActivity.ACTION_CHAT_START_INTERNAL_FILE_SHARE.equals(action)) { //Internal file sharing
                //doBindService();
                //myNameStr = ChatXMPPService.mUserName;
                remoteUserName = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                //final String remoteUserName = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                remoteUserdisplayName = ChatXMPPService.getDisplayName(remoteUserName);
                Log.d(FILENAME, "onHandleIntent: ACTION_CHAT_START FILE SHARE# " + "remoteName" + remoteUserName);
                setDisplayNameOnScreen(remoteUserdisplayName);

//                mRemoteUserName = (TextView) findViewById(R.id.chat_remoteName);
//                mRemoteUserName.setText(remoteUserdisplayName);
//                ActionBar aBar = getSupportActionBar();
//                if(aBar != null)
//                    setTitle(remoteUserdisplayName);

                //mRemoteUserName.setText(remoteUserName);
                processSendFileIntent(remoteUserName); //If it is just start then we are just loading previous messages - no new message
            }
            if (ChatUsersListActivity.ACTION_CHAT_START_EXTERNAL_TEXT_SHARE.equals(action)) { //External or internal text sharing
                //doBindService();
               // myNameStr = ChatXMPPService.mUserName;
                remoteUserName = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                //final String remoteUserName = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                remoteUserdisplayName = ChatXMPPService.getDisplayName(remoteUserName);
                Log.d(FILENAME, "onHandleIntent: ACTION_CHAT_START TEXT SHARE# " + "remoteName" + remoteUserName);
                setDisplayNameOnScreen(remoteUserdisplayName);

//                mRemoteUserName = (TextView) findViewById(R.id.chat_remoteName);
//                mRemoteUserName.setText(remoteUserdisplayName);
//                ActionBar aBar = getSupportActionBar();
//                if(aBar != null)
//                    setTitle(remoteUserdisplayName);
                //mRemoteUserName.setText(remoteUserName);

                final String textToShare = (String)intent.getStringExtra(ChatUsersListActivity.EXTERNAL_TEXT);
                mIsTextSelectedForSharing = true;
                m_textForSending = textToShare;
                processSendMessageIntent(remoteUserName, m_textForSending); //If it is just start then we are just loading previous messages - no new message
            }
            if (ChatUsersListActivity.ACTION_CHAT_START_INTERNAL_TEXT_SHARE.equals(action)) { //External or internal text sharing
                //doBindService();
                //myNameStr = ChatXMPPService.mUserName;
                remoteUserName = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                //final String remoteUserName = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                remoteUserdisplayName = ChatXMPPService.getDisplayName(remoteUserName);
                Log.d(FILENAME, "onHandleIntent: ACTION_CHAT_START TEXT SHARE# " + "remoteName" + remoteUserName);
                setDisplayNameOnScreen(remoteUserdisplayName);

//                mRemoteUserName = (TextView) findViewById(R.id.chat_remoteName);
//                mRemoteUserName.setText(remoteUserdisplayName);
//                ActionBar aBar = getSupportActionBar();
//                if(aBar != null)
//                    setTitle(remoteUserdisplayName);

                //mRemoteUserName.setText(remoteUserName);
                processSendMessageIntent(remoteUserName, m_textForSending); //If it is just start then we are just loading previous messages - no new message
            }

            /* //Following is obsolete now and is never being called now:
            else if (ChatXMPPService.ACTION_RECEIVEDIMAGE.equals(action)) {
                Log.d(FILENAME, "onHandleIntent: ACTION_RECEIVEDIMAGE");
                myNameStr=ChatXMPPService.mUserName;
                final String param1 = intent.getStringExtra(ChatUsersListActivity.REMOTE_USER_NAME);
                mRemoteUserName = (TextView) findViewById(R.id.chat_remoteName);
                mRemoteUserName.setText(param1);
                remoteNameStr = param1;

                final String filePath = intent.getStringExtra(ChatXMPPService.MESSAGERECEIVED);
                chatText.setMovementMethod(new ScrollingMovementMethod());
                ChatRecord.storeMessage(this, ChatXMPPService.mUserName, mRemoteUserName.getText().toString(), MESSAGEDIRECTIONRECEIVED, null, CHAT_MESSAGE_TYPE_IMAGE, null, filePath);
                if ( mBoolLoadDB) {
                    //false here means that currently we are working only with usernames and not groups. Later it needs to be enhanced.
                    loadFromDB(ChatXMPPService.mUserName, remoteNameStr, false);
                }
                if (filePath!=null ) {
                    chatArrayAdapter.add(new ChatMessage(MESSAGEDIRECTIONRECEIVED, null, CHAT_MESSAGE_TYPE_IMAGE, filePath));
                    listView = (ListView) findViewById(R.id.listView1);
                    listView.setScrollY(0);
                }
            }
            */
        }
    }

    //Handling of received intent is different from received message:
    private void processReceivedMessageIntent(String remoteUserNameStr, String message) {
//        remoteUserName = remoteUserNameStr;
//        remoteUserdisplayName = ChatXMPPService.getDisplayName(remoteUserName);

        chatText.setMovementMethod(new ScrollingMovementMethod());
        loadFromDB(ChatXMPPService.mUserName, remoteUserNameStr, false, DefaultLoadMinDaysMessages);
        listView = (ListView) findViewById(R.id.listView1);
        listView.setScrollY(0);
    }

    //Handling of received intent is different from received message:
    private void processSendMessageIntent(String remoteUserNameStr, String message) {
        mIsTextSelectedForSharing = false;
        m_textForSending = null;
//        remoteUserName = remoteUserNameStr;
//        remoteUserdisplayName = ChatXMPPService.getDisplayName(remoteUserName);

        chatText.setMovementMethod(new ScrollingMovementMethod());
        loadFromDB(ChatXMPPService.mUserName, remoteUserNameStr, false, DefaultLoadMinDaysMessages);
        listView = (ListView) findViewById(R.id.listView1);
        listView.setScrollY(0);
        chatText.setText(message);
        sendChatMessage();


    }

    //Handling of received intent is different from received message:
    private void processSendFileIntent(String remoteUserNameStr) {
        remoteUserName = remoteUserNameStr;
        remoteUserdisplayName = ChatXMPPService.getDisplayName(remoteUserName);

        chatText.setMovementMethod(new ScrollingMovementMethod());
        loadFromDB(ChatXMPPService.mUserName, remoteUserName, false, DefaultLoadMinDaysMessages);

        listView = (ListView) findViewById(R.id.listView1);
        listView.setScrollY(0);
        if ( mIsFileSelectedForSharing == true) //IF we are here, it means fileSelected for sharing shall be true;
        {
            // Send file to new user and set this flag to false;
            mIsFileSelectedForSharing = false;
            sendFileAndUpdateView(m_fileNameWithPath, remoteUserName, m_mimeType);
            //sendFileAndUpdateView(m_fileNameWithPath, mRemoteUserName.getText().toString(), m_mimeType);
        }
    }

    public void prepareAndSendNotification(String lremoteUserName, String screenText, boolean isFile) {
        Context context = getApplicationContext();
        Intent resultIntent = new Intent(context, ChatBubbleActivity.class);
        resultIntent.setAction(ChatXMPPService.ACTION_RECEIVEDMESSAGE);
        resultIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        resultIntent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, lremoteUserName);
        resultIntent.putExtra(ChatXMPPService.MESSAGERECEIVED, screenText);

        TaskStackBuilder stackBuilder = TaskStackBuilder.create(context);
        // Adds the back stack
        stackBuilder.addParentStack(ChatBubbleActivity.class);
        // Adds the Intent to the top of the stack
        stackBuilder.addNextIntent(resultIntent);
        // Gets a PendingIntent containing the entire back stack
        PendingIntent resultPendingIntent =
                stackBuilder.getPendingIntent(0, PendingIntent.FLAG_UPDATE_CURRENT);
        NotificationCompat.Builder builder = new NotificationCompat.Builder(context);

        builder.setContentIntent(resultPendingIntent);
        NotificationManager mNotificationManager =
                (NotificationManager) context.getSystemService(Context.NOTIFICATION_SERVICE);
        if (isFile == true) {
            screenText = screenText.substring(screenText.lastIndexOf("/") + 1);
        }
        mNotificationManager.notify(10, builder.setContentTitle("BitChat@" + remoteUserName)
                .setContentText(screenText)
                .setSmallIcon(R.drawable.noti_icon)
                .setVibrate(new long[]{1000, 1000})
                .setLights(Color.RED, 3000, 3000)
                .setSound(Settings.System.DEFAULT_NOTIFICATION_URI)
                .setAutoCancel(true)
                .setTicker("BitChat")
                .setNumber(++ChatXMPPService.count)
                .build());
    }


    protected void loadFromDB(String userName, String lremoteUsername, boolean isGroup, long noOfDays) {
        DataSource db = new DataSource(this);
        db.open();
        //startDate:: to be passed here::
        //First attemtp to get messages of past 1 week::
        // if there messages of past 1 week are less than 50, then load messages of past 1 month
        // if messages of past 1 month is also less than 50 then load messages of past 3 month
        // if messages of past 3 month is also less than 50 then load messages of past 6 month
        // if messages of past 6 month is also less than 50 then load messages of past 12 month
        // Do not store messages older than 1 year:: purge policy:

        Long startDate = TimeClock.getPreviousStartDate(noOfDays); //get messages of noOfdays at minimum:
        List<ChatRecord> reqs = db.getChatRecords(userName, lremoteUsername, isGroup, startDate);
        long messages = 0;//
        while (messages <= DefaultMinMessagesToShow)
        {
            messages = reqs.size();
            if (messages > DefaultMinMessagesToShow)
            {
                //Add dummy message for loading previous messages
                if (noOfDays <= DefaultLoadMinDaysMessages) {
                    chatArrayAdapter.add(new ChatMessage(MESSAGEDIRECTIONSENT, gLoad1MonthMessages, CHAT_MESSAGE_TYPE_TEXT, null, startDate, null, null, 0, 0));
                }
                else if (noOfDays <= Load1MonthDaysMessages) {
                    chatArrayAdapter.add(new ChatMessage(MESSAGEDIRECTIONSENT, gLoad3MonthMessages, CHAT_MESSAGE_TYPE_TEXT, null, startDate, null, null, 0, 0));
                }
                else if (noOfDays <= Load3MonthDaysMessages) {
                    chatArrayAdapter.add(new ChatMessage(MESSAGEDIRECTIONSENT, gLoad6MonthMessages, CHAT_MESSAGE_TYPE_TEXT, null, startDate, null, null, 0, 0));
                }
                else if (noOfDays <= Load6MonthDaysMessages) {
                    chatArrayAdapter.add(new ChatMessage(MESSAGEDIRECTIONSENT, gLoad1YearMessages, CHAT_MESSAGE_TYPE_TEXT, null, startDate, null, null, 0, 0));
                }
                break;
            }
            else
            {
                if (noOfDays <= DefaultLoadMinDaysMessages)
                {
                    noOfDays = Load1MonthDaysMessages;
                    startDate = TimeClock.getPreviousStartDate(noOfDays); //past 1 month's date
                }
                else if (noOfDays <= Load1MonthDaysMessages)
                {
                    noOfDays = Load3MonthDaysMessages;
                    startDate = TimeClock.getPreviousStartDate(noOfDays); //past 3 month's date
                }
                else if (noOfDays <= Load3MonthDaysMessages)
                {
                    noOfDays = Load6MonthDaysMessages;
                    startDate = TimeClock.getPreviousStartDate(noOfDays); //past 6 month's date
                }
                else if (noOfDays <= Load6MonthDaysMessages)
                {
                    noOfDays = Load1YearDaysMessages;
                    startDate = TimeClock.getPreviousStartDate(noOfDays); //past 12 month's date
                }
                else if (noOfDays == Load1YearDaysMessages)
                {
                    //Messages older than 1 year are not shown for now:
                    break;
                }
                reqs = db.getChatRecords(userName, lremoteUsername, isGroup, startDate);
                messages = reqs.size();
            }
        }
        for (ChatRecord _req : reqs) {
            String mimeType = _req.getMimeType();
            String str = null;

            if (mimeType == null || mimeType.equals(CHAT_MESSAGE_TYPE_CMD))
            {
                continue;
            }
            if (mimeType.equals(CHAT_MESSAGE_TYPE_TEXT)) {
                chatArrayAdapter.add(new ChatMessage(_req.getMessageDirection(), _req.getMessageText(), CHAT_MESSAGE_TYPE_TEXT, null, _req.getCreationTimestamp(), _req.getMessageReceiptId(), _req.getMessageReceiptStatus(), _req.getFileSizeExpected(), _req.getFileSizeDisk()));
            } else
                chatArrayAdapter.add(new ChatMessage(_req.getMessageDirection(), _req.getMessageText(), mimeType, _req.getMessageFileName(), _req.getCreationTimestamp(), _req.getMessageReceiptId(), _req.getMessageReceiptStatus(), _req.getFileSizeExpected(), _req.getFileSizeDisk()));
        }
        db.close();
        mBoolLoadDB = false;
    }


    /*

    public static void updateChatMessage(String receiptId, String receiptStatus) {
        chatArrayAdapter.update(receiptId, receiptStatus);
    }

    */

    /**
     * Messenger used for communicating with service.
     */
    Messenger mService = null;
    boolean mServiceConnected = false;
    /**
     * Class for interacting with the main interface of the service.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    boolean mIsBound;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_MESSAGE) {
                Log.d(FILENAME, "IncomingHandler : MESSAGE_TYPE_RECEIVEDMESSAGE ");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            else if(msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYREPORT)
            {// It comes here from monitoring thread for sending resent files required - implemented in filetransferWrapper
                Log.d(FILENAME, "IncomingHandler : MESSAGE_TYPE_MESSAGEDELIVERYREPORT ");
                Bundle b = msg.getData();
                CharSequence text = "MESSAGEDELIVERYRECEIPT";
                if (b != null) {
                    String remoteUserName = (String)b.getSerializable(ChatUsersListActivity.REMOTE_USER_NAME);
                    //update view - if this is current user, otherwise no need to update anywhere, it is already updated in the db.
                    if(remoteUserName.equals(remoteUserName)) {
                        //int messageDirection = (int) b.getSerializable(ChatXMPPService.MESSAGEDIRECTION);
                        //String messageSent = (String) b.getSerializable(ChatXMPPService.MESSAGESENT);
                        //String mimeType = (String) b.getSerializable(ChatXMPPService.CHAT_MESSAGETYPE);
                        //String fileSent = (String) b.getSerializable(ChatXMPPService.FILESENT);
                        //Long timeStamp = (Long) b.getSerializable(ChatXMPPService.TIMESTAMP);
                        String receiptId = (String) b.getSerializable(ChatXMPPService.RECEIPTID);
                        String receiptStatus = (String) b.getSerializable(ChatXMPPService.RECEIPTSTATUS);
                        //long fileSizeExpected = (long) b.getSerializable(ChatXMPPService.FILESIZEEXPECTED);
                        //long fileSizeDisk = (long) b.getSerializable(ChatXMPPService.FILESIZEDISK);
                        //chatArrayAdapter.update(new ChatMessage(messageDirection, messageSent, mimeType, fileSent, timeStamp, receiptId, receiptStatus, fileSizeExpected, fileSizeDisk));
                        chatArrayAdapter.update(new ChatMessage(receiptId, receiptStatus));
                        chatArrayAdapter.notifyDataSetInvalidated();
                    }
                }

                else
                {
                        text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            else if(msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYID)
            {// It comes here from monitoring thread for sending resent files required - implemented in filetransferWrapper
                Log.d(FILENAME, "IncomingHandler : INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYID ");
                Bundle b = msg.getData();
                CharSequence text = "MESSAGEDELIVERYRECEIPT";
                if (b != null) {
                    String remoteUserName = (String)b.getSerializable(ChatUsersListActivity.REMOTE_USER_NAME);
                    //update view - if this is current user, otherwise no need to update anywhere, it is already updated in the db.
                    if(remoteUserName.equals(remoteUserName)) {
                        String receiptId = (String) b.getSerializable(ChatXMPPService.RECEIPTID);
                        Long timeStamp = (Long) b.getSerializable(ChatXMPPService.TIMESTAMP);
                        //String receiptStatus = (String) b.getSerializable(ChatXMPPService.RECEIPTSTATUS);
                        //long fileSizeExpected = (long) b.getSerializable(ChatXMPPService.FILESIZEEXPECTED);
                        //long fileSizeDisk = (long) b.getSerializable(ChatXMPPService.FILESIZEDISK);
                        //chatArrayAdapter.update(new ChatMessage(messageDirection, messageSent, mimeType, fileSent, timeStamp, receiptId, receiptStatus, fileSizeExpected, fileSizeDisk));
                        chatArrayAdapter.update(new ChatMessage(receiptId, timeStamp), timeStamp);
                        //chatArrayAdapter.notifyDataSetInvalidated();
                    }
                }
                else
                {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            else
            {
                super.handleMessage(msg);
            }
        }
    }

    public void processBundleMsg(Bundle extras) {
        Log.d(FILENAME, "processBundleMsg");
        if (extras != null) {
            String remoteUserName = (String) extras.getSerializable(ChatUsersListActivity.REMOTE_USER_NAME);
            String mimeType = (String) extras.getSerializable(ChatXMPPService.CHAT_MESSAGETYPE);
            String screenText = null;
            String fileNameWithPath = null;
            long fileSizeExpected = 0;
            long fileSizeDisk = 0;

            if (mimeType.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT)) {
                screenText = (String) extras.getSerializable(ChatXMPPService.MESSAGERECEIVED);
            } else {
                fileNameWithPath = (String) extras.getSerializable(ChatXMPPService.MESSAGERECEIVED);
                fileSizeExpected = (long) extras.getSerializable(ChatXMPPService.FILESIZEEXPECTED);
                fileSizeDisk = (long) extras.getSerializable(ChatXMPPService.FILESIZEDISK);
                screenText = remoteUserName + ":" + fileNameWithPath;
            }
            processReceivedMessage(remoteUserName, mimeType, screenText, fileNameWithPath, fileSizeExpected, fileSizeDisk);
        }
    }

    private void processReceivedMessage(String remoteUserNameStr, String mimeType, String message, String fileNamewithPath, long fileSizeExpected, long fileSizeDisk) {

        //String timeStamp = TimeClock.getDateTime();
        final long timeStamp = TimeClock.getEpocTime();
        final String receiptId = TimeClock.getEpocTimeLongToString(timeStamp);

        //First Store the message if not null, if this is not in focus - raise a notificaiton
        if (message != null || fileNamewithPath != null) {
            ChatRecord.storeMessage(this, ChatXMPPService.mUserName, remoteUserNameStr, MESSAGEDIRECTIONRECEIVED, null, mimeType, message, fileNamewithPath, timeStamp, null, ChatBubbleActivity.MESSAGE_ACKED, fileSizeExpected, fileSizeDisk);
            //Show Notification as new message is received and is to be shown to user

            if (hasWindowFocus() == false || !remoteUserNameStr.equals(remoteUserName))
                if (message != null && fileNamewithPath == null) {
                    prepareAndSendNotification(remoteUserNameStr, message, false);
                } else if (fileNamewithPath != null) {
                    prepareAndSendNotification(remoteUserNameStr, fileNamewithPath, true);
                }
        }
        // If this is same user - then load the message and show it:
        if (remoteUserNameStr.equals(remoteUserName)) {
            chatText.setMovementMethod(new ScrollingMovementMethod());
            listView = (ListView) findViewById(R.id.listView1);
            if (mBoolLoadDB) {
                //false here means that currently we are working only with usernames and not groups. Later it needs to be enhanced.
                loadFromDB(ChatXMPPService.mUserName, remoteUserName, false, DefaultLoadMinDaysMessages);
                listView.setScrollY(0);
            }
            if (message != null || fileNamewithPath != null) {
                chatArrayAdapter.add(new ChatMessage(MESSAGEDIRECTIONRECEIVED, message, mimeType, fileNamewithPath, timeStamp, receiptId, MESSAGE_ACKED, fileSizeExpected, fileSizeDisk));
                listView.setScrollY(0);
            }
        }
    }

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            mServiceConnected = true;
            // Register our messenger also on Service side:
            Message msg = Message.obtain(null,
                    ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_CHAT);
            msg.replyTo = mMessenger;
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                // We always have to trap RemoteException
                // (DeadObjectException
                // is thrown if the target Handler no longer exists)
                e.printStackTrace();
            }
        }

        /**
         * Connection dropped.
         */
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(FILENAME, "Disconnected from service.");
            mService = null;
            mServiceConnected = false;
        }
    };

    /**
     * Sends message with text stored in bundle extra data ("data" key).
     *
     * @param code to send
     */
    void sendToService(int code) {
        if (mServiceConnected) {
            Message msg = null;
            if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_CHAT) {
                Log.d(FILENAME, "sendToService: MESSAGE_TYPE_UNREGISTER");
                //Manish: Send from here, whatever to send to service from activity:
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_CHAT);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            } else if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_CHAT_QUERY) {
                Log.d(FILENAME, "sendToService: MESSAGE_TYPE_CHAT_QUERY");
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_CHAT_QUERY);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            }
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                // We always have to trap RemoteException
                // (DeadObjectException
                // is thrown if the target Handler no longer exists)
                e.printStackTrace();
            }
        } else {
            Log.d(FILENAME, "Cannot send - not connected to service.");
        }
    }

    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        Log.d(FILENAME, "binding initiated");
        mIsBound = getApplicationContext().bindService(new Intent(getApplicationContext(), ChatXMPPService.class), mConn, Context.BIND_AUTO_CREATE);
        Log.d(FILENAME, "binding done");
        //mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            Log.d(FILENAME, "doUnbindService");
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_CHAT);
            }
            // Detach our existing connection.
            //unbindService(mConn);
            mIsBound = false;
        }
    }


    private Menu mMenu;
    private MenuItem mMIBackButton;
    private MenuItem mMIRemoteUser;
    private MenuItem mMIPhoneCall;
    private MenuItem mMIAttachment;
    private MenuItem mMIView;
    private MenuItem mMIShare;
    private MenuItem mMIDelete;
    private MenuItem mMIShareOther;
    //private MenuItem mMISettings;
    private MenuItem mMIContactUs;
    private MenuItem mClearUserHistory;

    public static boolean mDefaultMenu = true;

    public static void setDefauleMenu(boolean flag)
    {
        mDefaultMenu = flag;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_chat, menu);
        mMenu = menu;
        //mDefaultMenu = true;
        mMIBackButton = menu.findItem(R.id.action_backbutton);
        mMIRemoteUser = menu.findItem(R.id.action_remote_user);
       // mMyProfileButton = menu.findItem(R.id.action_my_profile);
        if(mMIRemoteUser != null)
        {
            Bitmap userPhoto = null;
            Bitmap workingbm = Utility.getSavedBitmapForUser(remoteUserName, this);

            //userPhoto = UserListLayout.getAvatarBitmapFromDB(OpenfireRestAPI.getUserName(),this);
            if(workingbm != null)
            {
                mMIRemoteUser.setIcon(new BitmapDrawable(getResources(), workingbm));
            }
            else
            {
                userPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.avatar).copy(Bitmap.Config.ARGB_8888, true);;
                mMIRemoteUser.setIcon(new BitmapDrawable(getResources(), userPhoto));
            }
        }
        mMIPhoneCall =  menu.findItem(R.id.phonecall);
        mMIAttachment = menu.findItem(R.id.action_attach);
        mMIView = menu.findItem(R.id.action_fileview);
        mMIShare = menu.findItem(R.id.action_share);
        mMIDelete = menu.findItem(R.id.action_delete);
        mMIShareOther = menu.findItem(R.id.action_share_other);
        //mMISettings = menu.findItem(R.id.action_settings);
        mMIContactUs = menu.findItem(R.id.action_contactus);
        mClearUserHistory = menu.findItem(R.id.action_clean_history);
        //activateDefaultMenu();
        //menu.add()
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (mDefaultMenu)
        {
            mMIBackButton.setVisible(true);
            mMIRemoteUser.setVisible(true);
            if(mMIRemoteUser != null)
            {
                Bitmap userPhoto = null;
                Bitmap workingbm = Utility.getSavedBitmapForUser(remoteUserName, this);

                //userPhoto = UserListLayout.getAvatarBitmapFromDB(OpenfireRestAPI.getUserName(),this);
                if(workingbm != null)
                {
                    mMIRemoteUser.setIcon(new BitmapDrawable(getResources(), workingbm));
                }
                else
                {
                    userPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.avatar).copy(Bitmap.Config.ARGB_8888, true);;
                    mMIRemoteUser.setIcon(new BitmapDrawable(getResources(), userPhoto));
                }
            }
            mMIPhoneCall.setVisible(true);
            mMIAttachment.setVisible(true);
            mMIView.setVisible(false);
            mMIShare.setVisible(false);
            mMIDelete.setVisible(false);
            mMIShareOther.setVisible(false);
            //mMISettings.setVisible(true);
            mMIContactUs.setVisible(true);
            mClearUserHistory.setVisible(true);
        }
        else
        {
            mMIBackButton.setVisible(true);
            mMIRemoteUser.setVisible(false);
            mMIPhoneCall.setVisible(true);
            mMIAttachment.setVisible(false);
            mMIView.setVisible(true);
            mMIShare.setVisible(true);
            mMIDelete.setVisible(true);
            mMIShareOther.setVisible(true);
            //mMISettings.setVisible(true);
            mMIContactUs.setVisible(true);
            mClearUserHistory.setVisible(true);
        }
        return true;
    }

        @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mIsFileSelectedForSharing = false;
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_backbutton:
                // User chose the "Settings" item, show the app settings UI...
                onBackPressed();
                //hideActionBar();
                activateDefaultMenu();
                break;

            case R.id.action_remote_user:
                // User chose the "Settings" item, show the app settings UI...
                //onBackPressed();
                //hideActionBar();
                mMIRemoteUser.setTitle(remoteUserdisplayName + "NSW");
                //mMIRemoteUser.setTitle(mRemoteUserName.getText());
                activateDefaultMenu();
                break;

            case R.id.phonecall:
                // User chose the "Settings" item, show the app settings UI...
                //onBackPressed();
                //hideActionBar();
                userCallSelected(remoteUserName.toString());
                //mMIRemoteUser.setTitle(mRemoteUserName.getText());
                activateDefaultMenu();
                break;

            case R.id.action_attach:
                // User chose the "Settings" item, show the app settings UI...
                if (Build.VERSION.SDK_INT < 19) {
                    intent = new Intent();
                    intent.setAction(Intent.ACTION_GET_CONTENT);
                    intent.setType("*/*");
                    startActivityForResult(Intent.createChooser(intent, "Select file to upload "), PICTURE_GALLERY);
                } else {
                    intent = new Intent(Intent.ACTION_OPEN_DOCUMENT);
                    intent.addCategory(Intent.CATEGORY_OPENABLE);
                    intent.setType("*/*");
                    startActivityForResult(Intent.createChooser(intent, "Select file to upload "), PICTURE_GALLERY);
                }
                activateDefaultMenu();
                break;

            case R.id.action_fileview:
                //hideActionBar();
                if (ChatXMPPService.isViewIntentSafe == true && !m_mimeType.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT)) {
                    intent = new Intent(Intent.ACTION_VIEW);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    File file = new File(m_fileNameWithPath);
                    //how to get index of this?
                    //Uri uri = Uri.parse("content://com.bitskeys.track/" + chatMessageObj.filePath);
                    Uri data = Uri.fromFile(file);
                    intent.setDataAndType(data, m_mimeType);
                    try
                    {
                        startActivity(intent);
                    }
                    catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(this, "No app found to view your file.", Toast.LENGTH_SHORT).show();
                    }
                }
                else
                {
                    //TBD: nothing implemented for viewing of text for now...
                }
                activateDefaultMenu();
                break;
            case R.id.action_share:
                if (!m_mimeType.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT)) {
                    mIsFileSelectedForSharing = true;
                    //Set Flag - file selected for sharing with other user:
                    //Clear Flag
                    // - as user clicks anywhere on screen,
                    // - as user selects other user
                    // - file parameters already stored here - so can be passed as a message to user screen::
                    // remaining handling shall be done if user is selected. Otherwise flag needs to be cleared in userscreen - if user is not selected:
                }
                else
                {
                    mIsTextSelectedForSharing = true;
                }
                onBackPressed();
                activateDefaultMenu();
                //hideActionBar();
                break;
            case R.id.action_share_other:
                if (!m_mimeType.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT)) {
                    // User chose the "share_other" action,
                    Intent i = new Intent(Intent.ACTION_SEND);
                    i.setFlags(Intent.FLAG_GRANT_READ_URI_PERMISSION);
                    //i.setType("audio/mp3");
                    //File file = new File(m_fileNameWithPath);
                    //Uri data = Uri.fromFile(file);
                    File recordingFile = new File(m_fileNameWithPath);
                    Uri fileUri = Uri.fromFile(recordingFile);
                    i.setDataAndType(fileUri, m_mimeType);
                    i.putExtra(Intent.EXTRA_STREAM, fileUri);
                    try {
                        startActivity(Intent.createChooser(i, "Share " + recordingFile.getName()));
                    } catch (android.content.ActivityNotFoundException ex) {
                        Toast.makeText(this, "There are no app installed to share your file.", Toast.LENGTH_SHORT).show();
                    }
                }
                else //Share Text with other APPS:
                {
                    Intent sendIntent = new Intent();
                    sendIntent.setAction(Intent.ACTION_SEND);
                    sendIntent.putExtra(Intent.EXTRA_TEXT, m_textForSending);
                    sendIntent.setType("text/plain");
                    startActivity(sendIntent);
                }
                activateDefaultMenu();
                //hideActionBar();
                break;
            case R.id.action_delete:
                // User chose the "Favorite" action, mark the current item
                // as a favorite...
                if (m_receiptId == null)
                {
                    Log.d(FILENAME, "Seemingly crash issue is fixed here");
                    chatArrayAdapter.delete(m_position);
                    // Delete from ChatMessageList
                    //hideActionBar();
                    activateDefaultMenu();
                    break;
                }
                ChatRecord.clearFromDB(this, ChatXMPPService.mUserName.toString(), remoteUserName.toString(), false, m_receiptId);
                chatArrayAdapter.delete(m_position);
                // Delete from ChatMessageList
                //hideActionBar();
                activateDefaultMenu();
                break;
            case R.id.action_clean_history:
                //hideActionBar();
                ChatRecord.clearFromDB(this, ChatXMPPService.mUserName.toString(), remoteUserName.toString(), false);
                chatArrayAdapter.clear();
                activateDefaultMenu();
                break;
            case R.id.home:
                onBackPressed();
                activateDefaultMenu();
                break;
            /*
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                //hideActionBar();
                activateDefaultMenu();
                return true;
                */
            case R.id.action_contactus:
                // User chose the "Settings" item, show the app settings UI...
                //hideActionBar();
                sendEmail();
                activateDefaultMenu();

                break;
            default://Back button - implemented:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                activateDefaultMenu();
                //hideActionBar();
                return super.onOptionsItemSelected(item);
        }
            return true;
    }

    public void userCallSelected(String selectedUserName)
    {
        Toast.makeText(getApplicationContext(), "Calling user "+ ChatXMPPService.getDisplayName(selectedUserName), Toast.LENGTH_LONG).show();
        //setSelectedUserName(selectedUserName);
        //setSelectedUserDisplayName(ChatXMPPService.getDisplayName(selectedUserName));
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(selectedUserName.trim())));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
        finish();
        return;
    }

    protected void sendEmail() {

        Log.i("Send email", "");
        Toast.makeText(this, "Select Email", Toast.LENGTH_SHORT).show();
        String[] TO = {"contactus@bitskeytechnologies.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Bitskey - contact - 9810414413");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Hi Support, \n Please contact us back -" + ChatXMPPService.mUserName);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.d(FILENAME, "Finished sending email...");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }


    public ChatArrayAdapter getChatArrayAdapter()
    {
        return chatArrayAdapter;
    }


    public static final int progress_bar_type = 0;

    // File url to download
    private static String file_url = "http://ec2-54-201-247-62.us-west-2.compute.amazonaws.com:8888/images/IMG_20150620_182044.jpg";
    private ProgressDialog pDialog;
    /**
     * Showing Dialog
     * */
    @Override
    protected Dialog onCreateDialog(int id) {
        switch (id) {
            case progress_bar_type: // we set this to 0
                pDialog = new ProgressDialog(this);
                pDialog.setMessage("Downloading file. Please wait...");
                pDialog.setIndeterminate(false);
                pDialog.setMax(100);
                pDialog.setProgressStyle(ProgressDialog.STYLE_HORIZONTAL);
                pDialog.setCancelable(true);
                pDialog.show();
                return pDialog;
            default:
                return null;
        }
    }
    /**
     * Background Async Task to download file
     * */


    class DownloadFileFromURL extends AsyncTask<String, String, String> {
        // button to show progress dialog
        Button btnShowProgress;
        // Progress Dialog
        String outputFileName;
        //ImageView my_image;
        // Progress dialog type (0 - for Horizontal progress bar)
        /**
         * Before starting background thread
         * Show Progress Bar Dialog
         * */
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            //showDialog(progress_bar_type);
        }
        /**
         * Downloading file in background thread
         * */
        @Override
        protected String doInBackground(String... f_url) {
            int count;
            try {
                URL url = new URL(f_url[0]);
                URLConnection conection = url.openConnection();
                conection.connect();
                // this will be useful so that you can show a tipical 0-100% progress bar
                int lenghtOfFile = conection.getContentLength();
                // download the file
                InputStream input = new BufferedInputStream(url.openStream(), 8192);

                //String outputFileName = f_url.substring(0, f_url.indexOf("/"));
                String outputFileName = file_url.substring(file_url.indexOf("/images/")); //part1
                outputFileName = outputFileName.substring(8); //part2 - index of first char in filenam
                //outputFileName = outputFileName.substring(outputFileName.indexOf("/")); //part3
                //outputFileName = outputFileName.substring(outputFileName.indexOf("/")); //part4

                // Output stream

                File mf = Environment.getExternalStorageDirectory();
                String root = mf.toString();
                root = root + "/bitchat";
                File myDir = new File(root);
                myDir.mkdirs();
                outputFileName = root + "/" + outputFileName;
                final File file = new File(outputFileName);

                OutputStream output = new FileOutputStream( outputFileName);
                byte data[] = new byte[1024];
                long total = 0;
                while ((count = input.read(data)) != -1) {
                    total += count;
                    // publishing the progress....
                    // After this onProgressUpdate will be called
                    publishProgress(""+(int)((total*100)/lenghtOfFile));
                    // writing data to file
                    output.write(data, 0, count);
                }
                // flushing output
                output.flush();
                // closing streams
                output.close();
                input.close();
            }
            catch (Exception e)
            {
                Log.e("Error: ", e.getMessage());
            }
            return null;
        }

        /**
         * Updating progress bar
         * */
        protected void onProgressUpdate(String... progress)
        {
            // setting progress percentage
            pDialog.setProgress(Integer.parseInt(progress[0]));
        }

        /**
         * After completing background task
         * Dismiss the progress dialog
         * **/
        @Override
        protected void onPostExecute(String file_url) {
            // dismiss the dialog after the file was downloaded
            dismissDialog(progress_bar_type);
            // Displaying downloaded image into image view
            // Reading image path from sdcard
            String imagePath = outputFileName;//Environment.getExternalStorageDirectory().toString() + "/downloadedfile.jpg";
            // setting downloaded into image view
            //my_image.setImageDrawable(Drawable.createFromPath(imagePath));
        }
    }

    private File getBitmap(String tag, Uri url)
    {
        File cacheDir;
        // if the device has an SD card
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),".OCFL311");
        } else {
            // it does not have an SD card
            //cacheDir=ActivityPicture.this.getCacheDir();
            cacheDir= getCacheDir();
        }
        if(!cacheDir.exists())
            cacheDir.mkdirs();

        //Get FileName
        String fileName = null;
        if (url.getScheme().equals("file")) {
            fileName = url.getLastPathSegment();
        } else {
            Cursor cursor = null;
            try {
                cursor = getContentResolver().query(url, new String[]{
                        MediaStore.Images.ImageColumns.DISPLAY_NAME
                }, null, null, null);

                if (cursor != null && cursor.moveToFirst()) {
                    fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME));
                    Log.d(FILENAME, "name is " + fileName);
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        File f;
        try {
            Bitmap bitmap=null;
            InputStream is = null;
            if (url.toString().startsWith("content://com.google.android.gallery3d") ||
                    url.toString().startsWith("content://com.android.providers") ||
                    url.toString().startsWith("content://com.android.content") ||
                    url.toString().startsWith("content://com.android.externalstorage")

                    ) {
                is = getContentResolver().openInputStream(url);
            } else {
                is=new URL(url.toString()).openStream();
            }
            if (fileName!=null) {
                f = new File(cacheDir, fileName);
            }
            else
                f = new File(cacheDir, tag);
            OutputStream os = new FileOutputStream(f);
            //Utils.CopyStream(is, os);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) != -1) {
                os.write(buffer, 0, len);
            }
            //f.
            //return null;
            //is.decodeStream
            return f;
        } catch (Exception ex) {
            Log.d(FILENAME, "Exception: " + ex.getMessage());
            // something went wrong
            ex.printStackTrace();
            return null;
        }
    }

}