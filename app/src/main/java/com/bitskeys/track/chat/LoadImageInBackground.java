package com.bitskeys.track.chat;

import android.app.ProgressDialog;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.provider.MediaStore;
import android.util.Log;
import android.widget.ListView;

import com.bitskeys.track.db.ChatRecord;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.FileSharing;
//import com.bitskeys.track.gen.FileTransferWrapper;
import com.bitskeys.track.gen.TimeClock;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URL;

/**
 * Created by Manish on 22-09-2015.
 */

class LoadImageInBackground extends AsyncTask<Void,Void,Void> {
    private ProgressDialog progress;
    private final String FILENAME = "GBC LoadImageBG#";
    private String remoteUserName;
    private String password;
    private Context context;
    private Uri uri;
    private String localFilePath;
    private String mimeType;
    private ChatArrayAdapter chatArrayAdapter;
    private ListView listView;

    /*
    // Initialize MemoryCache
    MemoryCache memoryCache = new MemoryCache();
    FileCache fileCache;
    //Create Map (collection) to store image and image url in key value pair
    private Map<TextView, String> imageViews = Collections.synchronizedMap(
            new WeakHashMap<TextView, String>());
    ExecutorService executorService;
    //handler to display images in UI thread
    Handler handler = new Handler();
    */

    LoadImageInBackground(ChatBubbleActivity activity, Uri uri, Context context, String remoteUserName, String mimeType, ChatArrayAdapter chatArrayAdapter, ListView listView)
    {
        Log.d(FILENAME, "LoadImageInBackground");
        progress = new ProgressDialog(activity);
        this.uri = uri;
        //userName = activity.mUserName;
        //password = activity.mPassword;
        this.context = context;
        this.remoteUserName = remoteUserName;
        this.mimeType = mimeType;
        this.chatArrayAdapter = chatArrayAdapter;
        this.listView = listView;
    }

    @Override
    protected Void doInBackground(Void...arg){
        boolean done = false;
        Log.d(FILENAME, "doInBackground");
        publishProgress();
        // do your processing here like sending data or downloading etc.
        File f = getBitmap("file_name", uri);
        //File f = getBitmap(uri.toString());
        if (f != null) {
            localFilePath = f.getPath();
        }
        else {
            Log.d(FILENAME,"file is null");
        }
        done=true;
        Log.d(FILENAME, "doInBackground exit");
        return null;
    }
    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
        Log.d(FILENAME, "onPreExecute");
        //progress = new ProgressDialog();
        progress.setMessage("Loading image in ...");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        progress.show();
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        Log.d(FILENAME, "onProgressUpdate");
        progress.show();
    }
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        Log.d(FILENAME, "onPostExecute Loading Image");
        if (progress!=null && progress.isShowing()) {
            progress.dismiss();
        }
        progress = null;
        sendFileAndUpdateView(localFilePath);
    }

    public void sendFileAndUpdateView(final String localFilePath)
    {
        this.localFilePath = localFilePath;
        //final String timeStamp = TimeClock.getDateTime();
        final long timeStamp = TimeClock.getEpocTime();
        final String receiptId = TimeClock.getEpocTimeLongToString(timeStamp);

        boolean isResending = false;

        File f = new File(localFilePath);
        long fileSize = f.length();

        //localfilepath working as receiptId in case of files.
        //ChatRecord.storeMessage(this, ChatXMPPService.mUserName, remoteUserName, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, mimeType, localFilePath, localFilePath, timeStamp, localFilePath,MESSAGE_ACK_AWAITED  );

        //if (isResending == false)
        //{
        ChatRecord.storeMessage(context, ChatXMPPService.mUserName, remoteUserName, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, mimeType, localFilePath, localFilePath, timeStamp, receiptId, ChatBubbleActivity.MESSAGE_ACK_AWAITED, fileSize, fileSize);
        //}

        final String localFileName = f.getName();
        new Thread(new Runnable() {
            public void run() {
                FileSharing.SendFile(context, localFilePath, remoteUserName, mimeType, timeStamp, false, receiptId);

                //DownloadFileFromURL dwFile = new DownloadFileFromURL();
                //dwFile.execute(file_url);
                ChatXMPPService.startActionSendMessage(context, localFileName,remoteUserName, ChatBubbleActivity.CHAT_MESSAGE_TYPE_IMAGE);

                //FileTransferWrapper.SendFile(context, localFilePath, remoteUserName, mimeType, timeStamp, false, receiptId);
                //FileTransferWrapper.SendFile(selectedImagePath, remoteUserName + "@" + ChatXMPPService.DOMAIN);
            }
        }).start();
        //String textHistory =ChatXMPPService.mUserName + ": ";
        //textHistory += localFilePath.toString();
                //ChatRecord.storeMessage(context, ChatXMPPService.mUserName, remoteUserName, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, mimeType, textHistory, localFilePath, timeStamp);
        chatArrayAdapter.add(new ChatMessage(ChatBubbleActivity.MESSAGEDIRECTIONSENT, localFilePath, mimeType, localFilePath, timeStamp, receiptId, ChatBubbleActivity.MESSAGE_ACK_AWAITED, fileSize, fileSize));
        //listView = (ListView) findViewById(R.id.listView1);
        listView.setScrollY(0);
    }


    private File getBitmap(String tag, Uri url)
    {
        File cacheDir;
        // if the device has an SD card
        if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)) {
            cacheDir=new File(android.os.Environment.getExternalStorageDirectory(),".OCFL311");
        } else {
            // it does not have an SD card
            //cacheDir=ActivityPicture.this.getCacheDir();
            cacheDir=context.getCacheDir();
        }
        if(!cacheDir.exists())
            cacheDir.mkdirs();

        //Get FileName
        String fileName = null;
        if (url.getScheme().equals("file")) {
            fileName = url.getLastPathSegment();
        } else {
            Cursor cursor = null;
            try {
                cursor = context.getContentResolver().query(url, new String[]{
                        MediaStore.Images.ImageColumns.DISPLAY_NAME
                }, null, null, null);

                if (cursor != null && cursor.moveToFirst()) {
                    fileName = cursor.getString(cursor.getColumnIndex(MediaStore.Images.ImageColumns.DISPLAY_NAME));
                    Log.d(FILENAME, "name is " + fileName);
                }
            } finally {
                if (cursor != null) {
                    cursor.close();
                }
            }
        }
        File f;
        try {
            Bitmap bitmap=null;
            InputStream is = null;
            if (url.toString().startsWith("content://com.google.android.gallery3d") ||
                    url.toString().startsWith("content://com.android.providers") ||
                            url.toString().startsWith("content://com.android.content") ||
                                    url.toString().startsWith("content://com.android.externalstorage")

                    ) {
                is=context.getContentResolver().openInputStream(url);
            } else {
                is=new URL(url.toString()).openStream();
            }
            if (fileName!=null) {
                f = new File(cacheDir, fileName);
            }
            else
                f = new File(cacheDir, tag);
            OutputStream os = new FileOutputStream(f);
            //Utils.CopyStream(is, os);
            byte[] buffer = new byte[1024];
            int len;
            while ((len = is.read(buffer)) != -1) {
                os.write(buffer, 0, len);
            }
            //f.
            //return null;
            //is.decodeStream
            return f;
        } catch (Exception ex) {
            Log.d(FILENAME, "Exception: " + ex.getMessage());
            // something went wrong
            ex.printStackTrace();
            return null;
        }
    }


    /*
    private Bitmap getBitmap(String url)
    {
        File f=fileCache.getFile(url);

        //from SD cache
        //CHECK : if trying to decode file which not exist in cache return null
        Bitmap b = decodeFile(f);
        if(b!=null)
            return b;

        // Download image file from web
        try {

            Bitmap bitmap=null;
            URL imageUrl = new URL(url);
            HttpURLConnection conn = (HttpURLConnection)imageUrl.openConnection();
            conn.setConnectTimeout(30000);
            conn.setReadTimeout(30000);
            conn.setInstanceFollowRedirects(true);
            InputStream is=conn.getInputStream();

            // Constructs a new FileOutputStream that writes to file
            // if file not exist then it will create file
            OutputStream os = new FileOutputStream(f);

            // See Utils class CopyStream method
            // It will each pixel from input stream and
            // write pixels to output stream (file)
            Utility.CopyStream(is, os);

            os.close();
            conn.disconnect();

            //Now file created and going to resize file with defined height
            // Decodes image and scales it to reduce memory consumption
            bitmap = decodeFile(f);

            return bitmap;

        } catch (Throwable ex){
            ex.printStackTrace();
            if(ex instanceof OutOfMemoryError)
                memoryCache.clear();
            return null;
        }
    }

    //Decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f){

        try {

            //Decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1, null, o);
            stream1.close();

            //Find the correct scale value. It should be the power of 2.

            // Set width/height of recreated image
            final int REQUIRED_SIZE=85;

            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2 < REQUIRED_SIZE || height_tmp/2 < REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            //decode with current scale values
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            FileInputStream stream2=new FileInputStream(f);
            Bitmap bitmap= BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;

        } catch (FileNotFoundException e) {
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }

    */

    /*
    public void clearCache() {
        //Clear cache directory downloaded images and stored data in maps
        memoryCache.clear();
        fileCache.clear();
    }

    //Used to display bitmap in the UI thread
    class BitmapDisplayer implements Runnable
    {
        Bitmap bitmap;
        PhotoToLoad photoToLoad;
        public BitmapDisplayer(Bitmap b, PhotoToLoad p){bitmap=b;photoToLoad=p;}
        public void run()
        {
            if(imageViewReused(photoToLoad))
                return;

            // Show bitmap on UI
            if(bitmap!=null)
                photoToLoad.imageView.setImageBitmap(bitmap);
            else
                photoToLoad.imageView.setImageResource(stub_id);
        }
    }

    boolean imageViewReused(PhotoToLoad photoToLoad){

        String tag=imageViews.get(photoToLoad.imageView);
        //Check url is already exist in imageViews MAP
        if(tag==null || !tag.equals(photoToLoad.url))
            return true;
        return false;
    }

    // default image show in list (Before online image download)
    final int stub_id= R.drawable.stub;

    public void DisplayImage(String url, ImageView imageView)
    {
        //Store image and url in Map
        imageViews.put(imageView, url);

        //Check image is stored in MemoryCache Map or not (see MemoryCache.java)
        Bitmap bitmap = memoryCache.get(url);

        if(bitmap!=null){
            // if image is stored in MemoryCache Map then
            // Show image in listview row
            imageView.setImageBitmap(bitmap);
        }
        else
        {
            //queue Photo to download from url
            queuePhoto(url, imageView);

            //Before downloading image show default image
            imageView.setImageResource(stub_id);
        }
    }

    private void queuePhoto(String url, ImageView imageView)
    {
        // Store image and url in PhotoToLoad object
        PhotoToLoad p = new PhotoToLoad(url, imageView);

        // pass PhotoToLoad object to PhotosLoader runnable class
        // and submit PhotosLoader runnable to executers to run runnable
        // Submits a PhotosLoader runnable task for execution

        executorService.submit(new PhotosLoader(p));
    }

    //Task for the queue
    private class PhotoToLoad
    {
        public String url;
        public ImageView imageView;
        public PhotoToLoad(String u, ImageView i){
            url=u;
            imageView=i;
        }
    }


    class PhotosLoader implements Runnable {
        PhotoToLoad photoToLoad;

        PhotosLoader(PhotoToLoad photoToLoad){
            this.photoToLoad=photoToLoad;
        }

        @Override
        public void run() {
            try{
                //Check if image already downloaded
                if(imageViewReused(photoToLoad))
                    return;
                // download image from web url
                Bitmap bmp = getBitmap(photoToLoad.url);

                // set image data in Memory Cache
                memoryCache.put(photoToLoad.url, bmp);

                if(imageViewReused(photoToLoad))
                    return;

                // Get bitmap to display
                BitmapDisplayer bd=new BitmapDisplayer(bmp, photoToLoad);

                // Causes the Runnable bd (BitmapDisplayer) to be added to the message queue.
                // The runnable will be run on the thread to which this handler is attached.
                // BitmapDisplayer run method will call
                handler.post(bd);

            }catch(Throwable th){
                th.printStackTrace();
            }
        }
    }
*/


/*

    protected void storeMessage(String userName, String remoteUserName, int messageDirection, String groupName, String mimeType, String messageText, String fileName) {
        ChatRecord req = new ChatRecord();
        req.setUserName(userName);
        req.setRemoteUserName(remoteUserName);
        req.setMessageDirection(messageDirection);
        req.setMimeType(mimeType);
        req.setMessageText(messageText);
        req.setMessageFileName(fileName);
        req.setMimeType(mimeType);
        //req.setCreationTimestamp(android.text.format.Time.()); //Store time lately:
        //filepath shall always be same
        if (fileName!=null)
        {
            //req.setMessageFilePath(FILEPATH); //Define Filepath
        }
        DataSource db = new DataSource(context);
        db.open();
        //ChatRecordCache e = new ChatRecordCache(req.getId(),req.getUserName(), req.getGroupName() , req.getRemoteUserName(),req.getCreationTimestamp(),
        //        req.getMessageDirection() ,req.getMessageType(), req.getMessageText(), req.getMessageFileName(), req.getMessageFilePath());
        //chatCacheList.add(e);
        db.persistChatRecord(req);
        db.close();
    }

*/
}