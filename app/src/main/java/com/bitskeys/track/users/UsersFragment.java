package com.bitskeys.track.users;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.ContextMenu;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.Toast;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.chat.ChatBubbleActivity;
import com.bitskeys.track.db.ChatRecord;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.TimeClock;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.map.UserLocation;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
//import com.bitskeys.track.chat2.MainActivity;

// Shows all the users activity.
public class UsersFragment extends ListFragment implements AdapterView.OnItemClickListener, UserListLayout.UserListCallback,View.OnClickListener {
    private final String FILENAME = "GBC UsersFragment#";

    UserFragCallBack mCallback;

    // Container Activity must implement this interface
    public interface OnLineSelectedListener {
        public void onListItemSelected(int position);
    }

    public interface UserFragCallBack{
        public void deleteUserActionFragment();
    }

    @Override
    public void onClick(View v) {
        if(mCallback != null) {
            mCallback.deleteUserActionFragment();
        }

    }

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long id) {
        if (getActivity()!=null) {
            Toast.makeText(getActivity(), "class" + i, Toast.LENGTH_SHORT).show();
        }
    }

    //extends AppCompatActivity {
    //private BroadcastReceiver bReceiver;
    //private LocalBroadcastManager bManager;

    //MyReceiver myReceiver;

    //UserMaps mUsersMap;
    UserListLayout simpleAdpt = null;
    public static boolean mIsUserSelectedForSharing = false;
    public static String mSelectedUserName = null;
    public static int mSelectedPosition = 0;

    @Override
    public void userSelected(int position, String userName) {
        Log.d(FILENAME, "userSelected");
        mIsUserSelectedForSharing = true;// This is set to true - while selecting sharing buttong
        mSelectedUserName = userName;
        mSelectedPosition = position;
    }

    @Override
    public void userDeSelected(int position, String userName) {
        Log.d(FILENAME, "userDeSelected");
        mIsUserSelectedForSharing = false;// This is set to true - while selecting sharing buttong
        mSelectedUserName = userName;
        mSelectedPosition = position;
        //hideActionBar();
    }

    @Override
    public void activateDefaultMenu() {
        Log.d(FILENAME, "activateDefaultMenu");
        //hideActionBar();
        setDefauleMenu(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if (getActivity() != null)
            {
                getActivity().invalidateOptionsMenu();
            }
        }
    }

    @Override
    public void activateSelectedMenu() {
        Log.d(FILENAME, "activateSelectedMenu");
        //hideActionBar();
        setDefauleMenu(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            if(getActivity()!=null) {
                getActivity().invalidateOptionsMenu();
            }
        }
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        Log.d(FILENAME, "onCreate");
        super.onCreate(savedInstanceState);
        //setHasOptionsMenu(true);
    }


    private static View view;
    private ListView lv = null;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(FILENAME, "onCreateView");

        if (container == null) {
            return null;
        }
        view = (ListView) inflater.inflate(R.layout.fragment_chatusers_list, container, false);
        // We get the ListView component from the layout
        lv = (ListView) view.findViewById(R.id.list);

        //registerForContextMenu(lv);
        return view;
    }

    @Override
    public void onViewCreated(View view, Bundle savedInstanceState) {
        Log.d(FILENAME, "onViewCreated");
        // TODO Auto-generated method stub
        // ListView lv = ChatUsersListActivity.fragmentManager.findFragmentById(R.id.userList);
        //Fragment userFragment   = (Fragment)(ChatUsersListActivity.fragmentManager.findFragmentById(R.id.userList));
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        Log.d(FILENAME, "onActivityCreated");
        super.onActivityCreated(savedInstanceState);

        // Data loading etc
        registerForContextMenu(getListView());
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(FILENAME, "onAttach");
        super.onAttach(activity);
        try {
            mCallback = (UserFragCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }


    @Override
    public void onDetach() {
        Log.d(FILENAME, "onDetach");
        super.onDetach();

            /*
        try {
            Field childFragmentManager = Fragment.class
                    .getDeclaredField("mChildFragmentManager");
            childFragmentManager.setAccessible(true);
            childFragmentManager.set(this, null);

        } catch (NoSuchFieldException e) {
            throw new RuntimeException(e);
        } catch (IllegalAccessException e) {
            throw new RuntimeException(e);
        }
        */
    }

    List<UserLocation> users;
    public void updateLocationsList(List<UserLocation> usersMapLocations, Context context)
    {
        Log.d(FILENAME, "updateUsersList");
        if (usersMapLocations != null) {
            users = usersMapLocations;
        }

        //This can be optimized: as this is duplicate code from custom map fragment:
        long time = System.currentTimeMillis();
        String epochtime = Long.toString(time);
        Date todayDate = new Date(Long.parseLong(epochtime));
        List tempUserObjList= new ArrayList();
        for (UserLocation user : users) {
            //users structure contains information as long (after multiplying by 100), so we are converting it again into double by deviding by 100
            //
            String userName = user.getUserName();
            if (userName.equals(ChatXMPPService.mUserName)) {
                continue;
            }
            LatLng ll = new LatLng(((double) (user.getLatitude())) / ChatXMPPService.DOUBLETOLONG, ((double) (user.getLongitude())) / ChatXMPPService.DOUBLETOLONG);
            String recordedTime = user.getRecordedTime();
            Long timeL = Long.parseLong(recordedTime);

            //Date date = new Date(timeL);
            if (TimeClock.isItToday(timeL)) {
                recordedTime = TimeClock.getTimeStrFromEpochTime(timeL);
//                Date date = new Date(timeL*1000);
//                SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy");
//                recordedTime = dateFormat.format(date);


            } else {
                //recordedTime = date.toString();
                //SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm"); // Set your date format
                //String currentData = sdf.format(d);
//                Date date = new Date(timeL*1000);
//                SimpleDateFormat dateFormat = new SimpleDateFormat("EEE, MMM d, ''yy");
                recordedTime = TimeClock.getEpocTimeLongToString(timeL);
//                recordedTime = dateFormat.format(date);
            }
            //String lastSeenInfo = "Last Seen@" + recordedTime + " " + Double.toString((double)(((double)(user.getLatitude()))/ChatXMPPService.DOUBLETOLONG)) + " " + Double.toString((double)((double)(user.getLongitude()))/ ChatXMPPService.DOUBLETOLONG);
            String lastSeenInfo = "Seen@" + recordedTime + " " + user.getAddressLine3()+ " " + user.getAddressLine2();
            UserObject userObj = new UserObject(userName,ChatXMPPService.getDisplayName(userName), 0, 0, lastSeenInfo , false, false);
            userObj.setLatLng(ll);
            Bitmap bitmap = Utility.getSavedBitmapForUser(userName, context);
            if(bitmap != null)
                userObj.setMyPhoto(bitmap);
            tempUserObjList.add(userObj);
            //MarkerOptions mMarker = new MarkerOptions().position(ll).title(user.getDisplayName() + " " + recordedTime + " " + ((double)(user.getLatitude()))/ChatXMPPService.DOUBLETOLONG + " " + ((double)(user.getLongitude()))/ ChatXMPPService.DOUBLETOLONG);
        }

        if (simpleAdpt != null)
        {
            simpleAdpt.updateLocationsList(tempUserObjList);
            simpleAdpt.notifyDataSetInvalidated();
        }

    }

    //CreateUserObjectList
    public List createUserObjectList(Map<String, String> map,Context context)
    {
        List userObjList= new ArrayList();
        //Map<String, String> map = listobj.mUserPresenceMap;
        int i = 0;
        String userName;
        String presence;
        Integer presenceId;

        synchronized (map)
        {
            try {
                for (Map.Entry<String, String> entry : map.entrySet()) {
                    //userNameListTemp[i] = entry.getKey();
                    userName = entry.getKey();
                    presence = entry.getValue();
                    if (presence.equals(ChatXMPPService.OFFLINE)) {
                        presenceId = R.drawable.ic_3offline_icon;
                    } else if (presence.equals(ChatXMPPService.ONLINE)) {
                        presenceId = R.drawable.ic_1online_icon;
                    } else {
                        presenceId = R.drawable.ic_2away;
                    }
                    //Neeraj Changed "...No location" to ""
                    UserObject userObj = new UserObject(userName, ChatXMPPService.getDisplayName(userName), ChatXMPPService.getAvatarId(userName), presenceId, "", false, false);
                    Bitmap bitmap = Utility.getSavedBitmapForUser(userName, context);
                    if (bitmap != null)
                        userObj.setMyPhoto(bitmap);
                    userObjList.add(userObj);
                    i++;
                }
            } catch (Exception e) {
                Log.d(FILENAME, "exception in createuserlist" + e.getMessage());
                e.printStackTrace();
            }
        }
        if (users!=null) {
            updateLocationsList(users,context);
        }
        Log.d(FILENAME, "New/Update Users count## " + i);
        return userObjList;
    }

    public void updateFragment(Context context, int vg, Map<String, String>  mapobj, boolean amIOffline) {
        Log.d(FILENAME, "updateFragment");
        if (view != null)
        {
                // We get the ListView component from the layout
                //ListView lv = (ListView) view.findViewById(R.id.listView);
                //Manish: commented for map
                //userNameList = ulist;
                //mUsersMap = listobj;

                // create UserObjectList
                List userObjList = createUserObjectList(mapobj,context);
                if (simpleAdpt == null ) {
                    if (lv !=null) {
                        Log.d(FILENAME, "Creating new UserList");

                        simpleAdpt = new UserListLayout(context, vg, userObjList);
                        setListAdapter(simpleAdpt);
                        simpleAdpt.setCallback(this);
                        simpleAdpt.setAmIOffline(amIOffline);
                        simpleAdpt.setUserListActivityCallback((ChatUsersListActivity) (getActivity()));
                    }
                    else
                    {
                        Log.d(FILENAME, "Invalid case");
                    }
                } else {
                    Log.d(FILENAME, "calling updateUsersList");
                    //update userlist and update it:
                    simpleAdpt.updateUsersList(userObjList);
                    simpleAdpt.setAmIOffline(amIOffline);
                    simpleAdpt.notifyDataSetInvalidated();
                    //simpleAdpt.notifyDataSetChanged();
                }
        }
    }

    public void updateUsers(Context context, int vg, Map<String, String>  mapobj, boolean amIOffline) {
        Log.d(FILENAME, "updateUsers");
        if (view != null)
        {
                // We get the ListView component from the layout
                //lv = (ListView) view.findViewById(R.id.listView);
                //userNameList = ulist;
                //mUsersMap = listobj;
                // create UserObjectList
                List userObjList = createUserObjectList(mapobj,context);
                if (simpleAdpt == null) {
                    if (lv !=null) {
                        Log.d(FILENAME, "Creating new UserList");
                        simpleAdpt = new UserListLayout(context, vg, userObjList);
                        setListAdapter(simpleAdpt);
                        simpleAdpt.setCallback(this);
                        simpleAdpt.setAmIOffline(amIOffline);
                        simpleAdpt.setUserListActivityCallback((ChatUsersListActivity) (getActivity()));
                    }
                    else
                    {
                        Log.d(FILENAME, "Invalid case");
                    }
                } else {
                    Log.d(FILENAME, "calling updateUsersList");
                    simpleAdpt.updateUsersList(userObjList);
                    //simpleAdpt.add(listobj);
                    //simpleAdpt.notifyDataSetChanged();
                    simpleAdpt.setAmIOffline(amIOffline);
                    simpleAdpt.notifyDataSetInvalidated();
                }

        }
    }

    public void addUsers(Context context, int vg, Map<String, String>  mapobj, boolean amIOffline) {
        Log.d(FILENAME, "addUsers");
        if (view != null)
        {
                // We get the ListView component from the layout
                //ListView lv = (ListView) view.findViewById(R.id.listView);
                //userNameList = ulist;
                //mUsersMap = listobj;
                List userObjList = createUserObjectList(mapobj,context);
                // Very if these users are already present or not..
                // if already present then -- those users shall be removed from the list

                if (simpleAdpt == null) {
                    if (lv !=null) {

                        //simpleAdpt = new UserListLayout(context, vg, id, userNameList, mUsersMap, allUsersOffline);
                        Log.d(FILENAME, "Creating new UserList");
                        simpleAdpt = new UserListLayout(context, vg, userObjList);
                        setListAdapter(simpleAdpt);
                        simpleAdpt.setCallback(this);
                        simpleAdpt.setAmIOffline(amIOffline);
                        simpleAdpt.setUserListActivityCallback((ChatUsersListActivity) (getActivity()));
                    }
                    else
                    {
                        Log.d(FILENAME, "Invalid case");
                    }
                } else {
                    Log.d(FILENAME, "calling addUsersList");
                    //simpleAdpt.removeUsersList(userObjList);
                    simpleAdpt.addUsersList(userObjList);
                    simpleAdpt.setAmIOffline(amIOffline);
                    simpleAdpt.notifyDataSetInvalidated();
                }
        }
    }

    public void updateFragmentOffline(boolean amIOffline) {
        Log.d(FILENAME, "updateFragmentOffline");
        if (view != null) {
            // create UserObjectList
            //List userObjList = createUserObjectList(listobj);
            // We get the ListView component from the layout
            //simpleAdpt.updateUsersList(userObjList);
            if (simpleAdpt!=null)
            {
                simpleAdpt.setAmIOffline(amIOffline);
                simpleAdpt.notifyDataSetInvalidated();
            }
        }
    }


    public void addChatRoom(Context context, int vg, String chatRoom) {
        Log.d(FILENAME, "addChatRoom");
        if (view != null)
        {
            List userObjList= new ArrayList();
            UserObject userObj = new UserObject(chatRoom,chatRoom, 0, 0, "ChatRoomAdded" , false, true);
            userObjList.add(userObj);

            //List userObjList = createUserObjectList(mapobj,context);
            // Very if these users are already present or not..
            // if already present then -- those users shall be removed from the list
            if (simpleAdpt == null) {
                if (lv !=null) {

                    //simpleAdpt = new UserListLayout(context, vg, id, userNameList, mUsersMap, allUsersOffline);
                    Log.d(FILENAME, "Creating new UserList");
                    simpleAdpt = new UserListLayout(context, vg, userObjList);
                    setListAdapter(simpleAdpt);
                    simpleAdpt.setCallback(this);
                    simpleAdpt.setUserListActivityCallback((ChatUsersListActivity) (getActivity()));
                }
                else
                {
                    Log.d(FILENAME, "Invalid case");
                }
            } else {
                Log.d(FILENAME, "calling addChatRoom");
                //simpleAdpt.removeUsersList(userObjList);
                simpleAdpt.addUsersList(userObjList);
                simpleAdpt.notifyDataSetInvalidated();
            }
        }
    }

    public void removeChatRoom(Context context, int vg, String chatRoom) {
        Log.d(FILENAME, "removeChatRoom");
        if (view != null)
        {
            //List userObjList= new ArrayList();
            UserObject userObj = new UserObject(chatRoom,chatRoom, 0, 0, "ChatRoomRemoved" , false, true);
            //userObjList.add(userObj);

            //List userObjList = createUserObjectList(mapobj,context);
            // Very if these users are already present or not..
            // if already present then -- those users shall be removed from the list
            if (simpleAdpt == null) {
                    Log.d(FILENAME, "Invalid case");
            } else {
                Log.d(FILENAME, "calling deleteChatRoom");
                //simpleAdpt.removeUsersList(userObjList);
                simpleAdpt.delete(userObj);
                simpleAdpt.notifyDataSetInvalidated();
            }
        }
    }

    public void addChatRoomList(Context context, int vg, Map<String, String>  roomList) {
        Log.d(FILENAME, "addChatRoomList");
        if (view != null)
        {
            List userObjList= new ArrayList();
            String roomName;
            String jidName;

            for (Map.Entry<String, String> entry : roomList.entrySet()) {
                //userNameListTemp[i] = entry.getKey();
                roomName = entry.getKey();
                //jidName = entry.getValue();
                UserObject userObj = new UserObject(roomName, roomName, 0, 0, "ChatRoomAdded", false, true);
                userObjList.add(userObj);
            }

            //List userObjList = createUserObjectList(mapobj,context);
            // Very if these users are already present or not..
            // if already present then -- those users shall be removed from the list
            if (simpleAdpt == null) {
                if (lv !=null) {

                    //simpleAdpt = new UserListLayout(context, vg, id, userNameList, mUsersMap, allUsersOffline);
                    Log.d(FILENAME, "Creating new UserList");
                    simpleAdpt = new UserListLayout(context, vg, userObjList);
                    setListAdapter(simpleAdpt);
                    simpleAdpt.setCallback(this);
                    simpleAdpt.setUserListActivityCallback((ChatUsersListActivity) (getActivity()));
                }
                else
                {
                    Log.d(FILENAME, "Invalid case");
                }
            } else {
                Log.d(FILENAME, "calling addChatRoom");
                //simpleAdpt.removeUsersList(userObjList);
                simpleAdpt.addUsersList(userObjList);
                simpleAdpt.notifyDataSetInvalidated();
            }
        }
    }


    /*
    // React to user clicks on item
    public void setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView p, View view, int position,long id)
            {

            // We know the View is a TextView so we can cast it
            //TextView clickedView = (TextView) view;
            //Toast.makeText(MainActivity.this, "Item with id ["+id+"] - Position ["+position+"] - Planet ["+clickedView.getText()+"]", Toast.LENGTH_SHORT).show();
            }
        });
        */
    /*
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        super.onListItemClick(l, v, position, id);
        Log.d(FILENAME, "onListItemClick called");
        Toast t = Toast.makeText(getActivity(), "Message",
                Toast.LENGTH_SHORT);
        t.show();
    }
    */

    static String selectedUserDisplayName = null;

    // We want to create a context Menu when the user long click on an item
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        //super.onCreateContextMenu(menu, v, menuInfo);


        Log.d(FILENAME, "onCreateContextMenu");
        AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
        // We know that each row in the adapter is a Map
        //HashMap map = (HashMap)
        mSelectedUserName = (String)((UserObject)(simpleAdpt.userObjList.get(aInfo.position))).userName;
        selectedUserDisplayName = ChatXMPPService.getDisplayName(mSelectedUserName); //(String)((UserObject)(simpleAdpt.userObjList.get(aInfo.position))).userName;
        ChatUsersListActivity.setSelectedUserDisplayName(selectedUserDisplayName);
        menu.setHeaderTitle("Information for " + selectedUserDisplayName);

        Log.d(FILENAME, "onCreateContextMenu " + selectedUserDisplayName);
        menu.add(aInfo.position, 1, 1, selectedUserDisplayName);
        menu.add(aInfo.position, 2, 2, "TrackView");
        menu.add(aInfo.position, 3, 3, "Chat");
        menu.add(aInfo.position, 4, 4, "Phone Call");
        //menu.add(aInfo.position, 4, 4, "Add to Contacts");
        menu.add(aInfo.position, 5, 5, "Clear history All");
        //menu.add(aInfo.position, 6, 6, "Clear history before 3 months");
        //menu.add(aInfo.position, 7, 7, "Clear history before 1 month");
        //menu.add(aInfo.position, 8, 8, "Clear history before 1 week");
        //menu.add(aInfo.position, 9, 9, "Clear history last week");
        //menu.add(aInfo.position, 10, 10, "Clear history today");
        //menu.add(aInfo.position, 11, 11, "Back");

    }

    /*
    // This method is called when user selects an Item in the Context menu
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d(FILENAME, "onContextItemSelected");
        int itemId = item.getItemId();
        int selectedposition = item.getGroupId();
        //String selectedUserName =     simpleAdpt.getItem(selectedposition);

        switch (itemId) {
            case 1: //user Detailed information
                Toast.makeText(getActivity(), "Item id [" + itemId + "]" + selectedUserDisplayName, Toast.LENGTH_SHORT).show();
                break;
            case 2: // Block/Unblock User
                break;
            case 3: // Chat with user
                UserListLayout.startActionChat(getActivity(), ChatXMPPService.mUserName.toString(), selectedUserName);
                break;
            case 4: // Add to contact list
                break;
            case 5: //Clear history All
                //item.setTitleCondensed()
                ChatRecord.clearFromDB(getActivity(), ChatXMPPService.mUserName.toString(), selectedUserName, false);
                break;
            case 6: //Clear history before 3 month
                break;
            case 7: //Clear history before 1 month
                break;
            case 8: //Clear history before 1 week
                break;
            case 9: //Clear history of last 1 week
                break;
            case 10: //Clear history today
                break;
            case 11: //back
                break;
            default:
                return super.onContextItemSelected(item);
        }
        //item.setTitleCondensed(ChatXMPPService.mUserName);
        //Log.d(FILENAME, "StartingChat param1" + ChatXMPPService.mUserName + "param2 " + item.getTitle().toString());
        // Implements our logic

        startActionChat(getActivity(), item.getTitleCondensed().toString(), item.getTitle().toString());
        return true;
    }
*/

    // TODO: Customize helper method\\\\\\\\\
    void startActionChat(Context context, String param1, String param2) {
        Log.d(FILENAME, "startActionChat");
        //void startActionChat(String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        Intent intent = new Intent(getActivity(), ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        startActivity(intent);
    }

    private Menu mMenu;
    private MenuItem mMyProfileButton;
    private MenuItem mTrackUserButton;
    //private MenuItem mSearchUserButton;
    //private MenuItem mStartMyDayButton;
    //private MenuItem mStopMyDayButton;

    private MenuItem mMIBackButton;
    private MenuItem mChat;
    private MenuItem mClearUserHistory;
    private MenuItem mClearAllUsersHistory;
    private MenuItem mMISettings;

    public static boolean mDefaultMenu = true;

    public static void setDefauleMenu(boolean flag) {

        mDefaultMenu = flag;
    }

    public boolean onCreateOptionsMenu(Menu menu) {
        Log.d(FILENAME, "onCreateOptionsMenu");
        MenuInflater inflater = getActivity().getMenuInflater();
        inflater.inflate(R.menu.menu_users, menu);
        //activateDefaultMenu();
        //menu.add()
        return true;
    }

    public void onPrepareOptionsMenu(Menu menu) {
        Log.d(FILENAME, "onPrepareOptionsMenu");
        super.onPrepareOptionsMenu(menu);
        mMenu = menu;
        //mDefaultMenu = true;

        mMyProfileButton = menu.findItem(R.id.action_my_profile);
       // mSearchUserButton = menu.findItem(R.id.action_search_user);
       // mStartMyDayButton = menu.findItem(R.id.action_start_my_day);
       // mStopMyDayButton = menu.findItem(R.id.action_stop_my_day);


        mMIBackButton = menu.findItem(R.id.action_backbutton);
        //mChat = menu.findItem(R.id.action_chat);
        mClearUserHistory = menu.findItem(R.id.action_clean_history);
        mClearAllUsersHistory = menu.findItem(R.id.action_clean_all_users_history);
        mMISettings = menu.findItem(R.id.action_settings);
        if (mDefaultMenu) {
            if(mMyProfileButton != null)
                mMyProfileButton.setVisible(true);
            //mSearchUserButton.setVisible(true);
            //mStartMyDayButton.setVisible(true);
            //mStopMyDayButton.setVisible(true);

            mMIBackButton.setVisible(true);
            mMISettings.setVisible(true);
            mClearAllUsersHistory.setVisible(true);

            mTrackUserButton.setVisible(false);
            //mChat.setVisible(false);
            mClearUserHistory.setVisible(false);
        } else {
            if(mMyProfileButton != null)
                mMyProfileButton.setVisible(true);
            mTrackUserButton.setVisible(true);
            //mSearchUserButton.setVisible(false);
            //mStartMyDayButton.setVisible(true);
            //mStopMyDayButton.setVisible(true);
            mMIBackButton.setVisible(true);
            //mChat.setVisible(true);
            mMISettings.setVisible(true);
            mClearUserHistory.setVisible(true);
            mClearAllUsersHistory.setVisible(false);


        }
        return;
    }

    public boolean onOptionsItemSelected(MenuItem item) {
        Log.d(FILENAME, "onOptionsItemSelected");
        // mIsFileSelectedForSharing = false;
        Intent intent;
        switch (item.getItemId()) {
            case R.id.action_my_profile:
                activateDefaultMenu();
                return true;
            /*
            case R.id.action_search_user:
                activateDefaultMenu();
                return true;
            case R.id.action_start_my_day:
                activateDefaultMenu();
                return true;
            case R.id.action_stop_my_day:
                activateDefaultMenu();
                return true;
                */
            case R.id.action_backbutton:
                // User chose the "Settings" item, show the app settings UI...
                if(getActivity()!=null) {
                    getActivity().onBackPressed();
                }
                //hideActionBar();
                activateDefaultMenu();
                return true;
            case R.id.action_clean_all_users_history:
                //hideActionBar();
                ChatRecord.clearFromDB(getActivity(), ChatXMPPService.mUserName.toString());
                activateDefaultMenu();
                return true;
            case R.id.home:
                if(getActivity()!=null) {
                    getActivity().onBackPressed();
                }
                activateDefaultMenu();
                return true;
            case R.id.action_settings:
                // User chose the "Settings" item, show the app settings UI...
                //hideActionBar();
                activateDefaultMenu();
                return true;
            default://Back button - implemented:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                activateDefaultMenu();
                //hideActionBar();
                return super.onOptionsItemSelected(item);
        }
    }
}