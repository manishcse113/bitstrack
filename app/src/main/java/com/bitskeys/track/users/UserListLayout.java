package com.bitskeys.track.users;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.chat.ChatBubbleActivity;
import com.bitskeys.track.profile.RemoteUserProfileActivity;
import com.bitskeys.track.db.DataSource;
import com.bitskeys.track.db.ProfileImageRecord;
import com.bitskeys.track.gen.BitMapWorkerRequest;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.Utility;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

/**
 * Created by Manish on 31-07-2015.
 */

public class UserListLayout extends ArrayAdapter {
    int groupid;

    //String[] userNamelist;
    //List userNamelist = new ArrayList();
    //List userDisplayNamelist = new ArrayList();
    //List avatarList = new ArrayList();
    //List imageList = new ArrayList();
    //List avatarcontextList = new ArrayList();

    public static List userObjList = new ArrayList();
    //public static List userObjRoomsList = new ArrayList();
    Context context;

    //List userObjList = new ArrayList();
    //String[] userDisplayNamelist;
    //Integer[] avatarList;
    //Integer[] imageList;
    //String [] avatarcontextList;
    //int Listsize = 0;
    int mapSize = 0;
    private final String FILENAME = "GBC UserListLaout#";
    private UserListCallback callback;
    private UserListActivityCallback callbackActivity;
    boolean amIOffline = false;

    public void setAmIOffline(boolean flag) {
        amIOffline = flag;
        /*
        for (Integer image : imageList)
        {
            image = R.drawable.ic_offline_icon;
        }
        */
        return;
    }


    Map<String, String> map;
    Map<String, String> treeMap;

    public UserListLayout(Context context, int textViewResourceId) {
        super(context, textViewResourceId);
    }



    /*
    public UserListLayout(Context context, int vg, int id, String[] ulist, UserMaps listobj, boolean allUsersOffline){
        super(context,vg, ulist );
        this.context=context;
        groupid=vg;
        updateUsers(ulist, listobj, allUsersOffline);
    }
    */

    public UserListLayout(Context context, int vg, List userObjList) {
        super(context, vg, userObjList);
        this.context = context;
        groupid = vg;
        this.userObjList.clear();
        this.userObjList.addAll(userObjList);
        sortUserObjList();
    }


    /*

    public void updateUsers(String[] ulist, UserMaps listobj, boolean allUsersOffline)
    {
        map = listobj.mUserPresenceMap;
        treeMap = new TreeMap<String, String>(map);
        int i = 0;
        for (Map.Entry<String, String> entry : treeMap.entrySet()) {
            userNamelist.add(entry.getKey());
            userDisplayNamelist.add(ChatXMPPService.mUsersListMap.getDisplayName(entry.getKey()));
            //userDisplayNamelist[i]= ChatXMPPService.mUsersListMap.mUserNameToDisplayNameMap.get(userNamelist[i]);
            avatarList.add (R.drawable.avatar);
            avatarcontextList.add("...");

            if (allUsersOffline == true || entry.getValue().equals("offline"))
            {
                imageList.add(R.drawable.ic_offline_icon);
            }
            else if (entry.getValue().equals("online"))
            {
                imageList.add(R.drawable.ic_online_icon);
            }
            else
            {
                imageList.add(R.drawable.ic_away);
            }
            i++;
        }
    }
    */

    /*

    public UserListLayout(Context context, int vg, int id, String[] ulist, UserMaps listobj, boolean allUsersOffline){
        super(context,vg, ulist );
        this.context=context;
        groupid=vg;
        updateUsers(ulist, listobj, allUsersOffline);
    }
    */

    public void sortUserObjList() {
        //first sort based on displayname
        Collections.sort(userObjList, new Comparator<UserObject>() {
            @Override
            public int compare(final UserObject object1, final UserObject object2) {
                return object1.userDisplayName.compareTo(object2.userDisplayName);
            }
        });

        //now sort on imageId
        Collections.sort(userObjList, new Comparator<UserObject>() {
            @Override
            public int compare(final UserObject object1, final UserObject object2) {
                return object1.image.compareTo(object2.image);
            }
        });
        //userObjList.t
    }

    //@Override
    public void add(UserObject object) {
        userObjList.add(object);
        super.add(object);
    }

    public void update(UserObject object) {
        //search message based on unique id: receiptId and replace the contents.
        boolean updated = false;
        for (Object userObject : userObjList) {
            if (((UserObject) (userObject)).userName.equals(((UserObject) (object)).userName)) {
                //cop from object to the existing message: Ideally only receiptstatus shall change.
                ((UserObject) (userObject)).image = ((UserObject) (object)).image;
                //((UserObject)(userObject)).lastSeenInfo = ((UserObject)(object)).lastSeenInfo;
                updated = true;
                break;
            }
        }
        if (!updated) {
            add(object);
        }
        sortUserObjList();
        //super.notify();
    }

    public void addUsersList(List objectList) {
        updateUsersList(objectList);

        /*
        //search message based on unique id: receiptId and replace the contents.
        for (Object userObject : objectList)
        {
            add((UserObject) (userObject));
        }

        sortUserObjList();
        */
        //super.notify();
    }

    public void updateUsersList(List objectList) {
        //search message based on unique id: receiptId and replace the contents.
        for (Object userObject : objectList) {
            update((UserObject) (userObject));
        }
        sortUserObjList();
        //super.notify();
    }

    public void updateLocations(UserObject object) {
        //search message based on unique id: receiptId and replace the contents.
        boolean updated = false;
        for (Object userObject : userObjList) {
            if (((UserObject) (userObject)).userName.equals(((UserObject) (object)).userName)) {
                //cop from object to the existing message: Ideally only receiptstatus shall change.
                //((UserObject)(userObject)).image = ((UserObject)(object)).image;
                ((UserObject) (userObject)).lastSeenInfo = ((UserObject) (object)).lastSeenInfo;
                ((UserObject) (userObject)).ltlng = ((UserObject) (object)).ltlng;
                updated = true;
                break;
            }
        }
        //super.notify();
    }


    public void updateLocationsList(List objectList) {
        //search message based on unique id: receiptId and replace the contents.
        for (Object userObject : objectList) {
            updateLocations((UserObject) (userObject));
        }
        //super.notify();
    }

    public void clear() {
        userObjList.clear();
        super.clear();
    }

    public void delete(UserObject object) {
        for (Object userObject : userObjList) {
            if (((UserObject) (userObject)).userName.equals(((UserObject) (object)).userName)) {
                userObjList.remove(userObject);
                //userObjList.
                super.remove(object);
                break;
            }
        }
    }

    /*
    public void removeUsersList(List objectList) {
        userObjList.removeAll(objectList);
        sortUserObjList();
    }
    */

    //@Override
    public void delete(int position) {
        int index = position;
        userObjList.remove(index);
        super.remove(index);
    }


    public int getCount() {
        return this.userObjList.size();
    }

    public UserObject getItem(int index) {
        UserObject userObject = (UserObject) userObjList.get(index);
        return userObject;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // View itemView = inflater.inflate(groupid, parent, false);
        View fragment_list_view = inflater.inflate(R.layout.fragment_chatusers_list, null);
        //Log.d(FILENAME, "getView position#"+ position + " view#"+convertView);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(R.layout.userlistlayout, (ViewGroup) fragment_list_view, false);
            populateUserListLayout(inflater, row, position);

        } else {
            //You get here in case of scroll
            populateUserListLayout(inflater, row, position);

        }
        //((ViewGroup) fragment_list_view).addView(row);
        return row;
    }


    public static Bitmap getAvatarBitmapFromDB(String userName, Context context) {
        if (userName == null || userName.length() == 0)
            return null;

        Bitmap bitmap = null;
        byte[] imageBytes = null;
        DataSource db = new DataSource(context);
        try {
            db.open();
            List<ProfileImageRecord> list = db.getProfileImageRecords(userName);
            for (ProfileImageRecord _req : list) {
                imageBytes = _req.getImageBytes();
                break;
            }
            db.close();
        } catch (Exception ex) {
            db.close();
        }

        if (imageBytes != null && imageBytes.length > 0) {
            bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        }
        return bitmap;
    }

    public View populateUserListLayout(LayoutInflater inflater, final View user_view, final int position) {
        //Log.d(FILENAME, "inflateUserListLayout");
        try {
            ImageView avatarView = (ImageView) user_view.findViewById(R.id.avatar);
            boolean isAvatarApplied = false;

            TextView textuserName = (TextView) user_view.findViewById(R.id.username);
            TextView textStatus = (TextView)user_view.findViewById(R.id.status);
            textStatus.setText("");

            String userDisplayName = textuserName.getText().toString();
            String remoteUserName = ChatXMPPService.getUserName(userDisplayName);

            UserObject uObj = (UserObject)(userObjList.get(position));
            final boolean isItARoom = uObj.isItARoom;

            avatarView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                    Intent myIntent = new Intent(getContext(), RemoteUserProfileActivity.class);
                    TextView textuserName = (TextView) user_view.findViewById(R.id.username);

                    String userDisplayName = textuserName.getText().toString();
                    String userName = ChatXMPPService.getUserName(userDisplayName);
                    // Convert the first Character to uppercase.
                    String formatDisplayName = Utility.FormatStringToUserName(userDisplayName);

                    //startRemoteProfileActivity(context, userName, userDisplayName);
                    callbackActivity.onClickForUserAction(userName,userDisplayName, isItARoom);
                }
            });

            //Bitmap avatarBitmap = getAvatarBitmapFromDB(remoteUserName, context);

            //Bitmap avatarBitmap = Utility.getSavedBitmapForUser(remoteUserName, context);
            Bitmap avatarBitmap = uObj.getMyPhoto();
            if (avatarBitmap != null) {
                avatarView.setImageBitmap(null);
                avatarView.setImageBitmap(avatarBitmap);
            }
            else
            {
                avatarView.setImageResource(R.drawable.avatar);
                BitMapWorkerRequest.AddAndStartBitMapWorker(context.getResources(),
                        remoteUserName,
                        null,
                        avatarView.getWidth(),
                        avatarView.getHeight(),
                        null,
                        null,
                        null,
                        null,
                        null,
                        R.id.avatar, user_view.getContext(),null);
            }

//           else {

//            }


//            String jid = userNamelist[position] + "@" + ChatXMPPService.DOMAINJID_BARE;
//            AvatarInfo aInfo = AvatarContainer.get(jid);
//
//            if(aInfo != null)
//            {
//                byte[] imageByte = aInfo.getImageBytes();
//                if(imageByte != null && imageByte.length >0)
//                {
//                    Bitmap bmp = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
//                    isAvatarApplied = true;
//                }
//            }
//            else
//            {
//                ProfileInfo pInfo = ChatXMPPService.LoadProfileInfoFromServer(jid);
//                if(pInfo != null)
//                {
//                    byte[] imageByte = pInfo.getImageBytes();
//                    if(imageByte != null && imageByte.length>0)
//                    {
//                        aInfo =new AvatarInfo(jid,imageByte);
//                    }
//
//                    /* Populate Image if maximum limit not reached */
//                    if( AvatarContainer.add(jid,aInfo))
//                    {
//
//                        if(imageByte != null)
//                        {
//                            Bitmap bmp = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
//                            isAvatarApplied = true;
//                        }
//
//                    }
//                }
//            }
//            if(!isAvatarApplied)
//            {
//                avatarView.setImageDrawable(context.getResources().getDrawable(((UserObject)(userObjList.get(position))).avatar));
//            }
//
//            ProfileInfo pInfo = ChatXMPPService.LoadProfileInfoFromServer(jid);
//            if(pInfo != null)
//            {
//                byte[] imageByte = pInfo.getImageBytes();
//                if(imageByte != null)
//                {
//                    Bitmap bmp = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length);
//                    avatarView.setImageBitmap(bmp);
//                }
//                else
//                {
//                    avatarView.setImageDrawable(context.getResources().getDrawable(avatarList[position]));
//                }
//            } else {
//                avatarView.setImageDrawable(context.getResources().getDrawable(avatarList[position]));
//            }


            textuserName = (TextView) user_view.findViewById(R.id.username);
            userDisplayName = ((UserObject) (userObjList.get(position))).userDisplayName; //(String)userDisplayNamelist.get(position);
            // Convert the first Character to uppercase.

            String formatDisplayName = Utility.FormatStringToUserName(userDisplayName);

            if (userDisplayName != null && userDisplayName.length() > 0) {
//                textuserName.setText(userDisplayName);
                textuserName.setText(formatDisplayName);
            } else {
                textuserName.setText(((UserObject) (userObjList.get(position))).userName);//(String)userNamelist.get(position));
            }
            //user_viewsetOnItemClickListener(new View.OnItemClickListener() {
            textuserName.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    Log.d(FILENAME, "onLongClick");
                    ((Activity) context).openContextMenu(v);
                    return true;
                }
            });

            textuserName.setOnClickListener(new View.OnClickListener() {
                                                public void onClick(View arg0) {
                                                    Log.d(FILENAME, "onClickListener");
                                                    String displayName = ((TextView) (arg0)).getText().toString();
                                                    String lremoteUserName = ChatXMPPService.getUserName(displayName);

                                                    if (ChatUsersListActivity.mIsFileSelectedForSharing == true) //Case: where intent received from external source for file sharing.
                                                    {
                                                        //startActionChatExternalFileShare(context, ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                                                        startActionChatExternalFileShare(context, ChatXMPPService.mUserName.toString(), lremoteUserName);
                                                        ChatUsersListActivity.mIsFileSelectedForSharing = false;

                                                    } else if (ChatUsersListActivity.mIsTextSelectedForSharing)//Case: where intent received from external Text source for file sharing.
                                                    {
                                                        //startActionChatExternalTextShare(context, ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                                                        startActionChatExternalTextShare(context, ChatXMPPService.mUserName.toString(), lremoteUserName);
                                                        ChatUsersListActivity.mIsTextSelectedForSharing = false;

                                                    } else if (ChatBubbleActivity.mIsFileSelectedForSharing == true) //Case: where file being shared from user1 to user2
                                                    {
                                                        //startActionChatInternalFileShare(context, ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                                                        startActionChatInternalFileShare(context, ChatXMPPService.mUserName.toString(), lremoteUserName);

                                                    } else if (ChatBubbleActivity.mIsTextSelectedForSharing == true) //Case: where file being shared from user1 to user2
                                                    {
                                                        //startActionChatInternalTextShare(context, ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                                                        startActionChatInternalTextShare(context, ChatXMPPService.mUserName.toString(), lremoteUserName);

                                                    } else { //normal display of chat interface
                                                        //String lremoteUserName =
                                                        //startActionChat(context, ChatXMPPService.mUserName.toString(), ((TextView) (arg0)).getText().toString());
                                                        //startActionChat(context, ChatXMPPService.mUserName.toString(), lremoteUserName);
                                                        if (((UserObject) (userObjList.get(position))).isSelected == false) {
                                                            ((UserObject) (userObjList.get(position))).isSelected = true;
                                                            //user_view.setBackgroundColor(Color.LTGRAY);
                                                            //callbackActivity.userActivitySelected(lremoteUserName);
                                                            callbackActivity.onClickForUserAction(lremoteUserName, displayName, isItARoom);
                                                        }
                                                        else if (((UserObject) (userObjList.get(position))).isSelected == true) {
                                                            ((UserObject) (userObjList.get(position))).isSelected = false;
                                                            //user_view.setBackgroundColor(Color.WHITE);
                                                            callbackActivity.userActivityDeSelected(lremoteUserName);
                                                            //callbackActivity.onClickForUserAction(lremoteUserName, userDisplayName);
                                                        }
                                                    }
                                                    return;
                                                }

                                            }
            );

            /*
            //This is temp code here: Later this code will be removed from here::
            //if addressLine1 == ""; then calcuate address otherwise not
            if(((UserObject) (userObjList.get(position))).lastSeenInfo.charAt(0)=='L'
                    ||
            (((UserObject) (userObjList.get(position))).ltlng== null))

            {
                //do nothing it has already been converted in readable address
            }
            else
            {
                //convert and show the address:

                LatLng ltlng = ((UserObject) (userObjList.get(position))).ltlng;
                String addressLine1 = "";
                String addressLine2 = "";
                String addressLine3 = "";
                ArrayList<String> addressFragments = ChatXMPPService.getAddress(context, (long) (ltlng.latitude) * ChatXMPPService.DOUBLETOLONG, (long) (ltlng.longitude) * ChatXMPPService.DOUBLETOLONG);
                if (addressFragments != null) {
                    for (int k = 0; k < addressFragments.size(); k++) {
                        if (k == 0)
                            addressLine1 = addressFragments.get(k);
                        else if (k == 1)
                            addressLine2 = addressFragments.get(k);
                        else if (k == 2)
                            addressLine3 = addressFragments.get(k);
                        else
                            break;
                    }
                }
                String lastSeenInfo = "Last@" + addressLine3 + ((UserObject) (userObjList.get(position))).lastSeenInfo;
                ((UserObject) (userObjList.get(position))).lastSeenInfo = lastSeenInfo;
            }
            */

            TextView textuserInfo = (TextView) user_view.findViewById(R.id.userinfo);

            if (isItARoom == false)
            {
                textuserInfo.setText(((UserObject) (userObjList.get(position))).lastSeenInfo);
            }
            textuserInfo.setSelected(true);
            textuserInfo.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Log.d(FILENAME, "onClick");
                    callbackActivity.userMapSelected(position, ((UserObject) (userObjList.get(position))).userName);//(String)(userNamelist.get(position)));
                    //callbackActivity.onClickForUserAction(((UserObject) (userObjList.get(position))).userName, ((UserObject) (userObjList.get(position))).userDisplayName, isItARoom);
                }
            });


            /*
            ImageView trackUserMap = (ImageView) user_view.findViewById(R.id.trackonMap);
            trackUserMap.setImageDrawable(context.getResources().getDrawable(R.drawable.map_icon_blue_3d));
            //TextView textuserName = (TextView) user_view.findViewById(R.id.username);
            trackUserMap.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Log.d(FILENAME, "onClick");
                    callbackActivity.userMapSelected(position, ((UserObject)(userObjList.get(position))).userName);//(String)(userNamelist.get(position)));
                }
            });
            */

            if (isItARoom == false) {
                //Phone call intent - from application
                ImageView phonecall = (ImageView) user_view.findViewById(R.id.phoneCall);
                //phonecall.setImageDrawable(context.getResources().getDrawable(R.drawable.ic_action_call));
                //TextView textuserName = (TextView) user_view.findViewById(R.id.username);
                phonecall.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        Log.d(FILENAME, "onClick");
                        callbackActivity.userCallSelected(((UserObject) (userObjList.get(position))).userName);//(String)(userNamelist.get(position)));
                    }
                });
            }

            if (isItARoom == false)
            {
                if (amIOffline || (((UserObject) (userObjList.get(position))).image == R.drawable.ic_3offline_icon)) {
                    textuserName.setTextColor(Color.DKGRAY);
                } else if (((UserObject) (userObjList.get(position))).image == R.drawable.ic_2away) {
                    //textuserName.setTextColor(Color.CYAN);
                    textStatus.setText("(away)");
                    textStatus.setTextColor(Color.YELLOW);
                } else {
                    //textuserName.setTextColor(Color.GREEN);
                    textStatus.setText("(online)");
                    textStatus.setTextColor(Color.GREEN);
                }
            }
            else
            {
                textStatus.setText("(room)");
                textStatus.setTextColor(Color.GREEN);
            }

            // textuserName.bringToFront();

            //Phone call intent - from application
            //ImageView imageView = (ImageView) user_view.findViewById(R.id.icon);
            //imageView.setImageDrawable(context.getResources().getDrawable(imageList[position]));
            //ImageView onlineStatus = (ImageView) user_view.findViewById(R.id.icon);
            /*
            onlineStatus.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Log.d(FILENAME, " onlineStatus onClick");
                    TextView textuserName = (TextView) user_view.findViewById(R.id.username);
                    textuserName.performClick();
                    Toast.makeText(getContext(), "Clicked ", Toast.LENGTH_SHORT).show();
                    return;
                }
            });
            */




            ImageView chatContext = (ImageView) user_view.findViewById(R.id.chat);
             //Commented following clear db, as it will be moved to context menu:
            //chatContext.setText(avatarcontextList[position]);
            chatContext.setOnClickListener(new View.OnClickListener() {
                    public void onClick(View arg0) {
                        startActionChat(context, ChatXMPPService.mUserName, (((UserObject) (userObjList.get(position))).userName),isItARoom);
                        //Toast.makeText(getContext(), "Clicked can clear db ", Toast.LENGTH_SHORT).show();
                        //ChatRecord.clearFromDB(context, ChatXMPPService.mUserName.toString(), userNamelist[position], false);
                        //return;
                    }
                }
            );

        }catch (Exception e)
        {

            Log.d(FILENAME, " Exception " + e.getMessage());
        }
        return user_view;
    }


    // TODO: Customize helper method\\\\\\\\\
    public static void startActionChat(Context context, String param1, String param2,Boolean isRoom) {
//        Log.d(FILENAME, "onLongClick");
        //Intent intent = new Intent(getContext(), MainActivity.class);
        Intent intent = new Intent(context, ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        intent.putExtra(ChatUsersListActivity.IS_ROOM,isRoom);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);//It was crashing without it - as it was getting called from onClick
        context.startActivity(intent);
    }

    public static boolean startRemoteProfileActivity(Context ctxt, String userName, String displayName) {
        if (userName == null || userName.length() == 0)
            return false;
        Intent intent = new Intent(ctxt, RemoteUserProfileActivity.class);
        Bundle b = new Bundle();
        b.putString("USERNAME", userName);
        b.putString("DISPLAYNAME", displayName);
        intent.putExtras(b);
        ctxt.startActivity(intent);
        return true;
    }

    // TODO: Customize helper method\\\\\\\\\
    public static void startActionChatExternalFileShare(Context context, String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        //Intent intent = new Intent(getContext(), MainActivity.class);
        Intent intent = new Intent(context, ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START_EXTERNAL_FILE_SHARE);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        intent.putExtra(ChatUsersListActivity.EXTERNAL_FILE, ChatUsersListActivity.mfileNameWithPath);
        intent.putExtra(ChatUsersListActivity.MIME_TYPE, ChatUsersListActivity.mMimeType);
        context.startActivity(intent);
    }

    // TODO: Customize helper method\\\\\\\\\
    public static void startActionChatInternalFileShare(Context context, String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        //Intent intent = new Intent(getContext(), MainActivity.class);
        Intent intent = new Intent(context, ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START_INTERNAL_FILE_SHARE);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        context.startActivity(intent);
    }


    // TODO: Customize helper method\\\\\\\\\
    public static void startActionChatExternalTextShare(Context context, String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        //Intent intent = new Intent(getContext(), MainActivity.class);
        Intent intent = new Intent(context, ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START_EXTERNAL_TEXT_SHARE);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        intent.putExtra(ChatUsersListActivity.EXTERNAL_TEXT, ChatUsersListActivity.mTextToSend);
        context.startActivity(intent);
    }

    public static void startActionChatInternalTextShare(Context context, String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        //Intent intent = new Intent(getContext(), MainActivity.class);
        Intent intent = new Intent(context, ChatBubbleActivity.class);
        intent.setAction(ChatUsersListActivity.ACTION_CHAT_START_INTERNAL_TEXT_SHARE);
        intent.putExtra(ChatUsersListActivity.MY_NAME, param1);
        intent.putExtra(ChatUsersListActivity.REMOTE_USER_NAME, param2);
        context.startActivity(intent);
    }

    public void setCallback(UserListCallback callback) {
        Log.d(FILENAME, "setCallback");
        this.callback = callback;
    }

    public interface UserListCallback {
        public void userSelected(int position, String selectedUserName);

        public void userDeSelected(int position, String selectedUserName);

        public void activateDefaultMenu();

        public void activateSelectedMenu();
    }

    public void setUserListActivityCallback(UserListActivityCallback callback) {
        Log.d(FILENAME, "setUserListActivityCallback");
        this.callbackActivity = callback;
    }

    public interface UserListActivityCallback {
        public void userMapSelected(int position, String selectedUserName);

        public void userCallSelected(String selectedUserName);

        public void userActivitySelected(String selectedUserName);
        public void userActivityDeSelected(String selectedUserName);
        public void onClickForUserAction(String userName,String displayName, boolean isItRoom);
    }



}
