package com.bitskeys.track.users;

import android.graphics.Bitmap;

import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Manish on 23-01-2016.
 */
public class UserObject extends Object{

    public String userName;
    public String userDisplayName;
    public Integer avatar;
    public Integer image;
    String avatarcontext;
    String lastSeenInfo;
    LatLng ltlng = null;
    public boolean isSelected = false;
    Bitmap myPhoto = null;
    boolean toAddtoGroup = false;
    boolean isItARoom = false;
    public UserObject(String userName, String userDisplayName, Integer avatar, Integer image, String lastSeenInfo , boolean isSelected, boolean isItaRoom)
    {
        super();
        this.userName = userName;
        this.userDisplayName = userDisplayName;
        this.avatar = avatar;
        this.image = image;
        this.avatarcontext = avatarcontext;
        this.lastSeenInfo = lastSeenInfo;
        this.isSelected = isSelected;
        toAddtoGroup = false;
        this.isItARoom = isItaRoom;
    }
    public void setMyPhoto(Bitmap input)
    {
        myPhoto = input;
    }
    public Bitmap getMyPhoto()
    {
        return myPhoto;
    }

    public void setLastSeenInfo(String lastSeenInfo)
    {
        this.lastSeenInfo = lastSeenInfo;
    }
    public void setLatLng(LatLng latlng)
    {
        this.ltlng = latlng;
    }
    public void setAddtogroup(boolean flag)
    {
        toAddtoGroup = flag;
    }

    public boolean isAddedToGroup()
    {
        return toAddtoGroup;
    }

    public void setSelected(boolean isSelected)
    {
        this.isSelected = isSelected;
    }
}
