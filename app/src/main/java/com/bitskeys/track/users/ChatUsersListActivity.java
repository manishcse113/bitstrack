package com.bitskeys.track.users;

import android.content.BroadcastReceiver;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.os.Vibrator;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
//import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.RestAPI.OpenfireRestAPI;
import com.bitskeys.track.chat.ChatBubbleActivity;
import com.bitskeys.track.chat.MesgCmdProcessor;
import com.bitskeys.track.chatRoom.ChatRoomInfo;
import com.bitskeys.track.chatRoom.MultiChat;
import com.bitskeys.track.chatRoom.CreateRoomFragment;
import com.bitskeys.track.chatRoom.CreateRoomFragment1;
import com.bitskeys.track.db.ChatRecord;
import com.bitskeys.track.db.DataSource;
import com.bitskeys.track.db.LocationRecord;
import com.bitskeys.track.db.RoomRecord;
import com.bitskeys.track.db.RoomRecordNew;
import com.bitskeys.track.fragments.TrackingSettings;
import com.bitskeys.track.fragments.UserActionFragment;
import com.bitskeys.track.gen.BitMapWorkerRequest;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.login.ChatConnection;
import com.bitskeys.track.map.CustomMapFragment;
import com.bitskeys.track.map.MyLocationManager;
import com.bitskeys.track.map.TrackUserFragment;
import com.bitskeys.track.map.UserLocation;
import com.bitskeys.track.map.UserLocationsList;
import com.bitskeys.track.profile.FragmentRemoteGroupProfile;
import com.bitskeys.track.settings.MainSettingsActivity;
//import com.bitskeys.track.profile.RemoteGroupProfileActivity;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.Affiliate;
import org.jivesoftware.smackx.muc.MultiUserChat;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

//import android.support.v7.app.ActionBar;
//import com.bitskeys.track.chat2.MainActivity;

// Shows all the users activity.
//public class ChatUsersListActivity extends AppCompatActivity implements UserListLayout.UserListCallback {

public class ChatUsersListActivity
        extends AppCompatActivity
        //extends android.support.v4.app.FragmentActivity
        implements UserListLayout.UserListActivityCallback,
        UserActionFragment.UserActionCallBack,
        UsersFragment.UserFragCallBack,
        CreateRoomFragment1.CreateRoomFrag1CallBack,
        CreateRoomFragment.CreateRoomFragCallBack,
        FragmentRemoteGroupProfile.GroupProfileEditCallBack,
        TrackingSettings.TrackingSettingsCallback
{
    @Override
    public void trackingPrefcancelled()
    {
        onNavigateToUsersListFromMap();
    }

    @Override
    public void trackingPrefOk(boolean canGetLocation)
    {
        onClickCheckin(canGetLocation);
        onNavigateToUsersListFromMap();
    }

    @Override
    public void userMapSelected(int position, String selectedUserName)
    {
        Toast.makeText(getApplicationContext(), "Loading today's track for "+ ChatXMPPService.getDisplayName(selectedUserName), Toast.LENGTH_LONG).show();
        setSelectedUserName(selectedUserName);
        setSelectedUserDisplayName(ChatXMPPService.getDisplayName(selectedUserName));
        MyLocationManager.trackUser(this, selectedUserName, selectedUserName);//send dummy mobile number for now
        onNavigateToTrackUserMap();
        Vibrate();
        return;
    }

    @Override
    public void userActivitySelected(String selectedUserName)
    {
        setSelectedUserName(selectedUserName);
        setSelectedUserDisplayName(ChatXMPPService.getDisplayName(selectedUserName));
        activateSelectedMenu();
        Vibrate();
        //MyLocationManager.trackUser(this, selectedUserName, selectedUserName);//send dummy mobile number for now
        //onNavigateToTrackUserMap();
        return;
    }

    public void Vibrate()
    {
        Vibrator vibe = (Vibrator) getSystemService(Context.VIBRATOR_SERVICE);
        vibe.vibrate(100);
    }

    @Override
    public void userActivityDeSelected(String selectedUserName)
    {
        setSelectedUserName(selectedUserName);
        setSelectedUserDisplayName(ChatXMPPService.getDisplayName(selectedUserName));
        activateDefaultMenu();
        Vibrate();
        //MyLocationManager.trackUser(this, selectedUserName, selectedUserName);//send dummy mobile number for now
        //onNavigateToTrackUserMap();
        return;
    }

    @Override
    public void userCallSelected(String selectedUserName)
    {
        Toast.makeText(getApplicationContext(), "Calling user "+ ChatXMPPService.getDisplayName(selectedUserName), Toast.LENGTH_LONG).show();
        setSelectedUserName(selectedUserName);
        setSelectedUserDisplayName(ChatXMPPService.getDisplayName(selectedUserName));
        Intent callIntent = new Intent(Intent.ACTION_DIAL);
        callIntent.setData(Uri.parse("tel:" + Uri.encode(selectedUserName.trim())));
        callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        startActivity(callIntent);
        Vibrate();
        return;
    }
    //private ViewPager mViewPager;
    //private TabsAdapter mTabsAdapter;

    //extends AppCompatActivity {
    private BroadcastReceiver bReceiver;
    private LocalBroadcastManager bManager;

    //MyReceiver myReceiver;
    private final String FILENAME = "GBC UsersListActivity#";
    UserListLayout simpleAdpt = null;
    UserMaps mUsersMap;
    String[] userNameList;
    Integer[] imageList;
    public static final String ACTION_CHAT_SEND = "com.bitskeys.track.action.CHATSEND";
    public static final String ACTION_CHAT_RECEIVE = "com.bitskeys.track.action.CHATRECEIVE";
    public static final String ACTION_CHAT_START = "com.bitskeys.track.action.CHATSTART";
    public static final String ACTION_CHAT_START_EXTERNAL_FILE_SHARE = "com.bitskeys.track.action.CHATSTARTEXTERNALFILESHARE";
    public static final String ACTION_CHAT_START_INTERNAL_FILE_SHARE = "com.bitskeys.track.action.CHATSTARTINTERNALFILESHARE";
    public static final String ACTION_CHAT_START_EXTERNAL_TEXT_SHARE = "com.bitskeys.track.action.CHATSTARTEXTERNALTEXTSHARE";
    public static final String ACTION_CHAT_START_INTERNAL_TEXT_SHARE = "com.bitskeys.track.action.CHATSTARTINTERNALTEXTSHARE";
    public static final String REMOTE_USER_NAME = "com.bitskeys.track.extra.REMOTE_USER_NAME";
    public static final String MY_NAME = "com.bitskeys.track.extra.MY_NAME";
    public static final String IS_ROOM = "IS_ROOM";
    public static final String EXTERNAL_FILE = "com.bitskeys.track.extra.IS_EXT_FILE";
    public static final String EXTERNAL_TEXT = "com.bitskeys.track.extra.IS_EXT_TEXT";
    public static final String MIME_TYPE = "com.bitskeys.track.extra.MIME_TYPE";
    private boolean mBoolLoadDB;
    private ActionBar actionBar;
    public static boolean mIsUserSelectedForSharing = false;
    public static String mSelectedUserName = null;
    public static int mSelectedPosition = 0;

    public void showActionBar() {
        actionBar.show();
    }

    public void hideActionBar() {
        actionBar.hide();
    }

    /*
    @Override
    public void onResume()
    {
        IntentFilter filterSend = new IntentFilter();
        filterSend.addAction(ChatXMPPService.ACTION_USERLIST);
        bManager = LocalBroadcastManager.getInstance(getApplicationContext());
        bManager.registerReceiver(bReceiver, filterSend);
    }

    @Override
    public void onPause()
    {
       // IntentFilter filterSend = new IntentFilter();
        //filterSend.addAction(ChatXMPPService.ACTION_USERLIST);
        bManager = LocalBroadcastManager.getInstance(getApplicationContext());
        bManager.unregisterReceiver(bReceiver);
    }
    */

    @Override
    protected void onStart() {
        // TODO Auto-generated method stub

        //Register BroadcastReceiver
        //to receive event from our service
        /*
        myReceiver = new MyReceiver();
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction(ChatXMPPService.ACTION_USERLIST);
        registerReceiver(myReceiver, intentFilter);
        */
        super.onStart();
        Log.d(FILENAME, "onStart");
    }

    @Override
    protected void onStop() {
        // TODO Auto-generated method stub
        //unregisterReceiver(myReceiver);
        super.onStop();
        Log.d(FILENAME, "onStop");
        //following commented: as it seems crashing here.
        /*
        if (mServiceConnected) {
            //unbindService(mConn);
            doUnbindService();
            // Manish: Service need not be stopped - as it is being used by other activites.
            //stopService(new Intent(this, ChatXMPPService.class));
            mServiceConnected = false;
        }
        */
    }

    public static FragmentManager fragmentManager;
    public static FragmentTransaction transaction = null;
    public UsersFragment mUserFragment;
    public CustomMapFragment mMapFragment;
    public TrackUserFragment mTrackUserFragment;
    public UserActionFragment mUserActionFragment;
    public CreateRoomFragment mCreateRoomFragment;
    public CreateRoomFragment1 mCreateRoomFragment1;
    public FragmentRemoteGroupProfile mViewGroupProfileFragment;

    public TrackingSettings mTrackingSettings;

    public void deleteUserActionFragment()
    {
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();
        mUserActionFragment = (UserActionFragment) getSupportFragmentManager().findFragmentByTag(TAG_USER_ACTION);
        if(mUserActionFragment != null)
        {
            //mUserActionFragment.setV
            transaction.setTransition(transaction.TRANSIT_FRAGMENT_CLOSE);
            transaction.remove(mUserActionFragment);
        }
        transaction.commit();

    }

    //Callback for shown userAction
    @Override
    public void onClickForUserAction(String userName,String displayName, boolean isItARoom)
    {
        Vibrate();
        fragmentManager = getSupportFragmentManager();
        FragmentTransaction transaction = fragmentManager.beginTransaction();

        if (isItARoom == false) {
            mUserActionFragment = (UserActionFragment) getSupportFragmentManager().findFragmentByTag(TAG_USER_ACTION);
            if (mUserActionFragment != null) {
                //mUserActionFragment.setV
                transaction.setTransition(transaction.TRANSIT_FRAGMENT_CLOSE);
                transaction.remove(mUserActionFragment);
            }
            mUserActionFragment = new UserActionFragment();
            Bundle bundle = new Bundle();
            bundle.putString("USER_NAME", userName);
            bundle.putString("DISPLAY_NAME", displayName);
            bundle.putBoolean("ISITAROOM", isItARoom);
            mUserActionFragment.setArguments(bundle);
            transaction.setTransition(transaction.TRANSIT_FRAGMENT_OPEN);
            transaction.add(R.id.user_action_frame, mUserActionFragment, TAG_USER_ACTION);
            transaction.commit();
        }
        else
        {
            onNavigateToGroupView();
            ((FragmentRemoteGroupProfile)mViewGroupProfileFragment).onProfileView(userName);
        }
    }

    boolean shouldShowUsersList = true;
    String TAG_MAP ="TAG_MAP_FRAGMENT";
    String TAG_USER_MAP ="TAG_USER_MAP_FRAGMENT";
    String TAG_USERSLIST ="TAG_USERSLIST_FRAGMENT";
    String TAG_CREATEROOMFRAGMENT ="TAG_CREATEROOMFRAGMENT";
    String TAG_CREATEROOMFRAGMENT1 ="TAG_CREATEROOMFRAGMENT1";
    String TAG_USER_ACTION ="TAG_USER_ACTION_FRAGMENT";
    String TAG_USER_GROUP_VIEW_PROFILE = "TAG_USER_GROUP_VIEW_PROFILE";

    String TAG_TRACKING_SETTINGS = "TAG_TRACKING_SETTINGS";

    static boolean mMyDayStarted = false;

    public static boolean isMyDayStarted()
    {
        return mMyDayStarted;
    }

    public void onResume() {
        super.onResume();
        Log.d(FILENAME, "onResume");
    }

    public void initFragment(int id, Fragment fragment, String tag, boolean toShow, boolean toAddBackStack)
    {

        transaction = fragmentManager.beginTransaction();
        transaction.add(id, fragment, tag);
        if (toShow == false)
        {
            transaction.hide(fragment);
        }
        else
        {
            transaction.show(fragment);
        }
        if(toAddBackStack)
        {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chatusers_list);

        Log.d(FILENAME, "onCreate");

        //map
        // initialising the object of the FragmentManager. Here I'm passing getSupportFragmentManager(). You can pass getFragmentManager() if you are coding for Android 3.0 or above.
        fragmentManager = getSupportFragmentManager();

        mUserFragment = new UsersFragment();
        initFragment(R.id.userList, mUserFragment, TAG_USERSLIST, true, false );

        mMapFragment = new CustomMapFragment();
        initFragment(R.id.map, mMapFragment, TAG_MAP, false, false);

        mTrackUserFragment = new TrackUserFragment();
        initFragment(R.id.trackUsermap, mTrackUserFragment, TAG_USER_MAP, false, false);
        //FragmentTransaction transaction = fragmentManager.beginTransaction();

        mCreateRoomFragment = new CreateRoomFragment();
        initFragment(R.id.createRoom, mCreateRoomFragment, TAG_CREATEROOMFRAGMENT, false, false);

        mCreateRoomFragment1 = new CreateRoomFragment1();
        initFragment(R.id.createRoom1, mCreateRoomFragment1, TAG_CREATEROOMFRAGMENT1, false, false);

        mViewGroupProfileFragment= new FragmentRemoteGroupProfile();
        initFragment(R.id.groupView, mViewGroupProfileFragment, TAG_USER_GROUP_VIEW_PROFILE, false, false);

        mTrackingSettings = new TrackingSettings();
        initFragment(R.id.trackingSettings, mTrackingSettings, TAG_TRACKING_SETTINGS, false, false);
        mTrackingSettings.setCallback(this);
/*
        transaction = fragmentManager.beginTransaction();
        transaction.add(R.id.groupView, mViewGroupProfileFragment, TAG_USER_GROUP_VIEW_PROFILE);
        transaction.hide(mViewGroupProfileFragment);
        //transaction.addToBackStack(null);
        transaction.commit();
*/

        ImageButton contact_button = (ImageButton)findViewById(R.id.contacts_img_button);
        contact_button.setSelected(false);
        contact_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                deleteUserActionFragment();
                onNavigateToUsersListFromMap();
                Log.d(FILENAME, "onClick");
            }
        });

        ImageButton location_button = (ImageButton)findViewById(R.id.location_img_button);
        location_button.setSelected(false);
        location_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View arg0) {
                deleteUserActionFragment();
                onNavigateToMap();
                Toast.makeText(getApplicationContext(), "Loading maps...", Toast.LENGTH_LONG).show();
                Log.d(FILENAME, "onClick");
                mMapFragment.refresh();
            }
        });


        final Context context = this;
        mDefaultMenu = true;
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        if (toolbar != null) {
            setSupportActionBar(toolbar);
            actionBar = getSupportActionBar();
            //getSupportActionBar().setDisplayShowTitleEnabled(true);
            //toolbar.setNavigationIcon(R.mipmap.app_icon);

            //getSupportActionBar().setLogo(R.drawable.toolbar_app_icon);
            getSupportActionBar().setDisplayUseLogoEnabled(true);
            getSupportActionBar().setTitle("BitChat");
            //actionBar.setDisplayHomeAsUpEnabled(true);
            //actionBar.setTitle(getTitle());

            //actionBar.setLogo(R.drawable.ic_launcher);
            //actionBar.setDisplayUseLogoEnabled(true);
            //actionBar.hide();
            mDefaultMenu = true;
            showActionBar();
            activateDefaultMenu();
        }
        sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_USERLIST);
        doBindService();
        handleIntent(getIntent());
    }

    @Override
    protected void onNewIntent(Intent intent) {

        Log.d(FILENAME, "onNewIntent");
        handleIntent(intent);
    }

    @Override
    protected void onDestroy() {
        Log.d(FILENAME, "onDestroy");
        //unregisterReceiver(bReceiver);
        sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST);
        // bManager.unregisterReceiver(bReceiver);
        super.onDestroy();
    }


    public void handleIntent(Intent intent) {
        Log.d(FILENAME, "handleIntent");

        String action = intent.getAction();
        String type = intent.getType();

        if (Intent.ACTION_SEND.equals(action) && type != null) { //If external sharing events received:
            //Process - only if logged in

            //if (simpleAdpt != null) {
            if (ChatConnection.getStatus() == ChatConnection.Status.FINISHED) {
                mMimeType = type;
                if ("text/plain".equals(type)) {
                    handleSendText(intent); // Handle text being sent
                } else if (type.startsWith("image/")) {
                    handleSendImage(intent); // Handle single image being sent
                } else if (type.startsWith("application/pdf")) {
                    handleSendImage(intent); // Handle single image being sent
                } else if (type.startsWith("video/")) {
                    handleSendImage(intent); // Handle single image being sent
                }
            } else { //login first
                Toast.makeText(this, "Login required", Toast.LENGTH_SHORT).show();
            }
        } else if (ChatXMPPService.ACTION_USERLIST.equals(action))// intent received from login activity
        {
            Bundle extras = intent.getExtras();
            if (extras != null) {
                processBundleMsg(extras, ChatXMPPService.ACTION_USERLIST);
            }
        } else {
            //It comes here - on clicking notification event: in which case it is probably not bind to service:
            //doBindService();
            sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_USERLIST);
        }
    }

    static public boolean mIsFileSelectedForSharing = false;
    static public String mfileNameWithPath = null;
    Uri imageUri = null;
    static public boolean mIsTextSelectedForSharing = false;
    static public String mTextToSend = null;
    static public String mMimeType = null;
    //Process intents from externs apps for sharing files

    //Process intents from externs apps for sharing files
    void handleSendText(Intent intent) {
        Log.d(FILENAME, "handleSendText");

        mTextToSend = intent.getStringExtra(Intent.EXTRA_TEXT);
        if (mTextToSend != null) {
            mIsTextSelectedForSharing = true;

            // Update UI to reflect text being shared
            //Save this information to pass to the user being selected...
        }
    }

    void handleSendImage(Intent intent) {
        Log.d(FILENAME, "handleSendImage");
        imageUri = (Uri) intent.getParcelableExtra(Intent.EXTRA_STREAM);
        if (imageUri != null) {
            mIsFileSelectedForSharing = true;
            mfileNameWithPath = imageUri.getPath();
            // Update UI to reflect image being shared
            //Save this information to pass to the user being selected...
        }
    }

    //UserMaps mUsersMapTemp;
    HashMap<String, String> mUsersPresenceMap;
    HashMap<String, String> mUsersPresenceTempMmap;
    HashMap<String, String> mRoomListMmap;

    String[] userNameListTemp;
    Integer[] imageListTemp;

    public void processBundleMsg(Bundle extras, String action) {
        Log.d(FILENAME, "processBundleMsg");

        boolean amIOffline = false;
        XMPPTCPConnection xmppTcpConnection = ChatConnection.getInstance().getConnection();
        if (xmppTcpConnection!= null && xmppTcpConnection.isConnected())
        {
            Log.d(FILENAME, "online");
        }
        else
        {
            Log.d(FILENAME, "connecting");
            amIOffline = true;
        }

        if (extras != null)
        {
            if (action.equals(ChatXMPPService.ACTION_USERLIST)) {
                Log.d(FILENAME, "ACTION_USERLIST");
                mUsersPresenceMap = (HashMap<String, String>) extras.getSerializable("userPresenceMap");
                // do something with the customer
                if (mUsersPresenceMap == null) {
                    Log.d(FILENAME, "mUsersMap is empty");
                    return;
                }

                mUserFragment.updateFragment(this, R.layout.userlistlayout, mUsersPresenceMap, amIOffline);
            }
            else if (action.equals(ChatXMPPService.ACTION_USERLIST_USERS_ADDED))
            {
                Log.d(FILENAME, "ACTION_USERLIST_USERS_ADDED");
                mUsersPresenceTempMmap = (HashMap<String, String>)  extras.getSerializable("userPresenceMap");
                // do something with the customer
                if (mUsersPresenceTempMmap == null) {
                    Log.d(FILENAME, "mUsersMap is empty");
                    return;
                }
                mUserFragment.addUsers(this, R.layout.userlistlayout, mUsersPresenceTempMmap, amIOffline);
            }
            else if (action.equals(ChatXMPPService.ACTION_USERLIST_USERS_UPDATED))
            {
                Log.d(FILENAME, "ACTION_USERLIST_USERS_UPDATED");
                mUsersPresenceTempMmap = (HashMap<String, String>)  extras.getSerializable("userPresenceMap");
                // do something with the customer
                if (mUsersPresenceTempMmap == null) {
                    Log.d(FILENAME, "mUsersMap is empty");
                    return;
                }
                mUserFragment.updateUsers(this, R.layout.userlistlayout, mUsersPresenceTempMmap, amIOffline);
            }
            else if (action.equals(ChatXMPPService.ACTION_USERLIST_USERS_PRESENCE))
            {
                Log.d(FILENAME, "ACTION_USERLIST_USERS_PRESENCE");
                //mUsersPresenceTempMmap
                UserPresenceChanged userPresenceChanged= (UserPresenceChanged)  extras.getSerializable("userPresenceChanged");
                // do something with the customer
                if (mUsersPresenceTempMmap == null) {
                    mUsersPresenceTempMmap = new HashMap<String, String>();
                    //Log.d(FILENAME, "mUsersMap is empty");
                    //return;
                }

                mUsersPresenceTempMmap.put(userPresenceChanged.userName, userPresenceChanged.presence);
                mUserFragment.updateUsers(this, R.layout.userlistlayout, mUsersPresenceTempMmap, amIOffline);
            }
            else if (action.equals(ChatXMPPService.ACTION_CHATROOM_ADDED))
            {
                Log.d(FILENAME, "ACTION_CHATROOM_ADDED");
                //mUsersPresenceTempMmap
                String roomName= (String)  extras.getSerializable("roomName");
                // do something with the customer
                if (roomName != null) {
                    mUserFragment.addChatRoom(this, R.layout.userlistlayout, roomName);
                }
            }
            else if (action.equals(ChatXMPPService.ACTION_CHATROOM_REMOVED))
            {
                Log.d(FILENAME, "ACTION_CHATROOM_ADDED");
                //mUsersPresenceTempMmap
                String roomName= (String)  extras.getSerializable("roomName");
                // do something with the customer
                if (roomName != null) {
                    mUserFragment.removeChatRoom(this, R.layout.userlistlayout, roomName);
                }
            }
            else if (action.equals(ChatXMPPService.ACTION_CHATROOMLIST_ADDED))
            {
                Log.d(FILENAME, "ACTION_CHATROOMLIST_ADDED");
                mRoomListMmap = (HashMap<String, String>)  extras.getSerializable("roomList");
                // do something with the customer
                if (mRoomListMmap == null) {
                    Log.d(FILENAME, "mRoomListMmap is empty");
                    return;
                }
                mUserFragment.addChatRoomList(this, R.layout.userlistlayout, mRoomListMmap);
                //mUserFragment.updateUsers(this, R.layout.userlistlayout, mUsersPresenceTempMmap, amIOffline);
            }
            else if (action.equals(ChatXMPPService.ACTION_TRACKING_PREFERENCE))
            {
                Log.d(FILENAME, "ACTION_TRACKING_PREFERENCE");
                //mUsersPresenceTempMmap
                myTrackingAllowed = (boolean)  extras.getSerializable("trackingPreference");
                activateDefaultMenu();

            }

        }
    }


    // We want to create a context Menu when the user long click on an item
    public static String selectedUserDisplayName = null;
    public static void setSelectedUserDisplayName(String displayName)
    {
        if (displayName!=null) {
            selectedUserDisplayName = displayName;
            //setSelectedUserName(ChatXMPPService.getUserName(displayName));
        }
        else
        {
            selectedUserDisplayName = selectedUserName;
        }
    }

    public static String getSelectedUserDisplayName()
    {
        return selectedUserDisplayName;
    }


    public static String selectedUserName = null;
    public static void setSelectedUserName(String username)
    {
        selectedUserName = username;
    }

    public static String getSelectedUserName()
    {
        return selectedUserName;
    }


    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        Log.d(FILENAME, "onCreateContextMenu");
        AdapterView.AdapterContextMenuInfo aInfo = (AdapterView.AdapterContextMenuInfo) menuInfo;
        // We know that each row in the adapter is a Map
        //HashMap map = (HashMap)
        //selectedUserName =     simpleAdpt.getItem(aInfo.position);
        mSelectedUserName = (String)((UserObject)(simpleAdpt.userObjList.get(aInfo.position))).userName;
        selectedUserDisplayName = ChatXMPPService.getDisplayName(mSelectedUserName); //(String)((UserObject)(simpleAdpt.userObjList.get(aInfo.position))).userName;
        //selectedUserDisplayName =    (String)( simpleAdpt.userNamelist.get(aInfo.position));
        menu.setHeaderTitle("Information for " + selectedUserDisplayName);

        Log.d(FILENAME, "onCreateContextMenu " + selectedUserDisplayName);
        menu.add(aInfo.position, 1, 1, selectedUserDisplayName);
       // menu.add(aInfo.position, 2, 2, "Block/Unblock");
        menu.add(aInfo.position, 3, 3, "Chat");
        //menu.add(aInfo.position, 4, 4, "Add to Contacts");
        menu.add(aInfo.position, 5, 5, "Clear history All");
        //menu.add(aInfo.position, 6, 6, "Clear history before 3 months");
        //menu.add(aInfo.position, 7, 7, "Clear history before 1 month");
        //menu.add(aInfo.position, 8, 8, "Clear history before 1 week");
        //menu.add(aInfo.position, 9, 9, "Clear history last week");
        //menu.add(aInfo.position, 10, 10, "Clear history today");
        //menu.add(aInfo.position, 11, 11, "Back");
    }

    // This method is called when user selects an Item in the Context menu
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Log.d(FILENAME, "onContextItemSelected");
        int itemId = item.getItemId();
        int selectedposition = item.getGroupId();
        // selectedUserName =     .userNamelist[0];

        switch(itemId)
        {
            case 1: //user Detailed information
                Toast.makeText(this, selectedUserDisplayName , Toast.LENGTH_SHORT).show();
                break;
            case 2: // TrackView
                //Launch TrackFragment for this user:
                Toast.makeText(this, "Navigating to user, press back to dismiss" + selectedUserDisplayName , Toast.LENGTH_SHORT).show();
                MyLocationManager.trackUser(this, ChatUsersListActivity.getSelectedUserName(), ChatUsersListActivity.getSelectedUserName());//send dummy mobile number for now
                onNavigateToTrackUserMap();

                break;
            case 3: // Chat with user
                //Neeraj : Todo, we need to do it proper for room vs user case, making it default to user by passing false.
                UserListLayout.startActionChat(this, ChatXMPPService.mUserName.toString(), selectedUserName,false);
                break;
            case 4: // Phone call
                userCallSelected(selectedUserName);
                break;
            case 5: //Clear history All
                //item.setTitleCondensed()
                ChatRecord.clearFromDB(this, ChatXMPPService.mUserName.toString(), selectedUserName, false);
                break;
            case 6: //Clear history before 3 month
                break;
            case 7: //Clear history before 1 month
                break;
            case 8: //Clear history before 1 week
                 break;
            case 9: //Clear history of last 1 week
                break;
            case 10: //Clear history today
                break;
            case 11: //back
                break;
            default:
                return super.onContextItemSelected(item);
        }

        //item.setTitleCondensed(ChatXMPPService.mUserName);
        //Log.d(FILENAME, "StartingChat param1" + ChatXMPPService.mUserName + "param2 " + item.getTitle().toString());
        // Implements our logic

        //startActionChat(this.getBaseContext(), item.getTitleCondensed().toString(), item.getTitle().toString());
        return true;
    }



    // TODO: Customize helper method\\\\\\\\\
    public void startActionChat(Context context, String param1, String param2) {
        Log.d(FILENAME, "startActionChat");
        //void startActionChat(String param1, String param2) {
        //Log.d(FILENAME, "startActionChat");
        Intent intent = new Intent(getApplicationContext(), ChatBubbleActivity.class);
        intent.setAction(ACTION_CHAT_START);
        intent.putExtra(MY_NAME, param1);
        intent.putExtra(REMOTE_USER_NAME, param2);
        startActivity(intent);
    }


    /**
     * Messenger used for communicating with service.
     */
    Messenger mService = null;
    boolean mServiceConnected = false;

    //public boolean mIsFileSharingSelected = false;
    //public String mfileSharingName = null;
    /**
     * Class for interacting with the main interface of the service.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    boolean mIsBound;

    class IncomingHandler extends Handler {

        @Override
        public void handleMessage(Message msg) {
            //Also display connection status

            if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST) {
                Log.d(FILENAME, "IncomingHandler : MESSAGE_TYPE_USERLIST ");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b, ChatXMPPService.ACTION_USERLIST);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_USERS_ADDED) {
                Log.d(FILENAME, "IncomingHandler : MESSAGE_TYPE_USERLIST_USERS_ADDED ");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b, ChatXMPPService.ACTION_USERLIST_USERS_ADDED);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_USERS_UPDATED) {
                Log.d(FILENAME, "IncomingHandler : MESSAGE_TYPE_USERLIST_USERS_UPDATED ");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b, ChatXMPPService.ACTION_USERLIST_USERS_UPDATED);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_USERS_PRESENCE_CHANGED) {
                Log.d(FILENAME, "IncomingHandler : MESSAGE_TYPE_USERLIST_USERS_PRESENCE_CHANGED ");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b, ChatXMPPService.ACTION_USERLIST_USERS_PRESENCE);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            else if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERS_MAP_LOCATIONS) {
                Log.d(FILENAME, "IncomingHandler : INTER_TASK_MESSAGE_TYPE_USERS_MAP_LOCATIONS ");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processMapRelatedUpdates(b);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            else if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERS_MAP_LOCATIONS_FAILED) {
                Log.d(FILENAME, "IncomingHandler : INTER_TASK_MESSAGE_TYPE_USERS_MAP_LOCATIONS_FAILED ");
                processMapRelatedUpdates(null);
                CharSequence text = null;
                text = "Service responded with empty message";
                Log.d(FILENAME, "Response: " + text);
            }
            else if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USER_MAP_LOCATIONS) {
                Log.d(FILENAME, "IncomingHandler : INTER_TASK_MESSAGE_TYPE_USER_MAP_LOCATIONS ");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processUserMapRelatedUpdates(b);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            else if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USER_MAP_LOCATIONS_FAILED) {
                Log.d(FILENAME, "IncomingHandler : INTER_TASK_MESSAGE_TYPE_USER_MAP_LOCATIONS_FAILED ");
                Bundle b = msg.getData();
                CharSequence text = null;
                processUserMapRelatedUpdates(b);
                /*
                if (b != null) {

                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                */
                Log.d(FILENAME, "Response: " + text);
            }
            else if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOM_ADDED) {
                Log.d(FILENAME, "IncomingHandler : INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOM_ADDED ");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b, ChatXMPPService.ACTION_CHATROOM_ADDED);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            else if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOM_REMOVED) {
                Log.d(FILENAME, "IncomingHandler : INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOM_REMOVED ");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b, ChatXMPPService.ACTION_CHATROOM_REMOVED);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            else if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOMLIST_ADDED) {
                Log.d(FILENAME, "IncomingHandler : INTER_TASK_MESSAGE_TYPE_USERLIST_CHATROOMLIST_ADDED ");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b, ChatXMPPService.ACTION_CHATROOMLIST_ADDED);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }
            else if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_TRACKING_PREFERENCE)
            {
                Log.d(FILENAME, "IncomingHandler : INTER_TASK_MESSAGE_TYPE_USERLIST_TRACKING_PREFERENCE");
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b, ChatXMPPService.ACTION_TRACKING_PREFERENCE);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d(FILENAME, "Response: " + text);
            }


            /*
            else if (msg.what == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST)
            { //Share message from other user
                Log.d(FILENAME,"IncomingHandler : MESSAGE_TYPE_SHARE_FILE" );
                Bundle b = msg.getData();
                //Log.d(FILENAME, "processBundleMsg");
                if (b != null) {
                    mfileSharingName = (String) b.getSerializable("filenamewithPath");
                    mIsFileSharingSelected = true;
                }

            }
            */
            else {
                super.handleMessage(msg);
            }
        }
    }



    //if information is not retrieved from server - then try to load the information from db.
    public void processMapRelatedUpdates(Bundle extras) {
        Log.d(FILENAME, "processMapRelatedUpdates");
        if (extras != null) {
            UserLocationsList userLocationsList = (UserLocationsList) extras.getSerializable("usersMapLocations");
            // do something with the customer
            if (userLocationsList == null) {
                // Try loading from db
                userLocationsList = new UserLocationsList();
                userLocationsList.usersMapLocations = prepareUsersLocationListFromDb();
                if (userLocationsList.usersMapLocations == null) {
                    Log.d(FILENAME, "usersMapLocations is empty");
                    return;
                }
            }

            mMapFragment.updateUsersList(userLocationsList.usersMapLocations);
            mUserFragment.updateLocationsList(userLocationsList.usersMapLocations,this);
        }
        else
        {
            UserLocationsList userLocationsList = new UserLocationsList();
            userLocationsList.usersMapLocations = prepareUsersLocationListFromDb();
            if (userLocationsList.usersMapLocations == null) {
                mMapFragment.updateUsersList(null);
            }
            else
            {
                mMapFragment.updateUsersList(userLocationsList.usersMapLocations);
                mUserFragment.updateLocationsList(userLocationsList.usersMapLocations,this);
            }
        }
    }

    public void processUserMapRelatedUpdates(Bundle extras) { //This is called in case of single user track
        Log.d(FILENAME, "processUserMapRelatedUpdates");
        if (extras != null) {
            UserLocationsList userLocationsList = (UserLocationsList) extras.getSerializable("userMapLocations");
            // do something with the customer
            if (userLocationsList == null) {
                // Try loading from db
                userLocationsList = new UserLocationsList();
                userLocationsList.usersMapLocations = prepareUserLocationListFromDb(selectedUserName);
                if (userLocationsList.usersMapLocations == null) {
                    mTrackUserFragment.updateUsersList(null);
                    Log.d(FILENAME, "usersMapLocations is empty");
                    return;
                }
            }
            mTrackUserFragment.updateUsersList(userLocationsList.usersMapLocations);
        }
        else
        {
            UserLocationsList userLocationsList = new UserLocationsList();
            userLocationsList.usersMapLocations = prepareUserLocationListFromDb(selectedUserName);
            if (userLocationsList.usersMapLocations == null)
            {
                mTrackUserFragment.updateUsersList(null);
            }
            else
            {
                mTrackUserFragment.updateUsersList(userLocationsList.usersMapLocations);
            }
        }
    }


    private List<UserLocation> prepareUsersLocationListFromDb()
    {
        DataSource db = new DataSource(this);
        db.open();

        List<LocationRecord> reqs = db.getLocationRecords();
        List<UserLocation> userslocations  = new ArrayList<>();
        for(LocationRecord _req : reqs) {
            userslocations.add(new UserLocation(_req.getUserName(), _req.getGroupName(), _req.getDisplayName(), _req.getLatitude(), _req.getLongitude(), _req.getRecordedTime(), _req.getAddressLine1(), _req.getAddressLine2(), _req.getAddressLine3()));
        }
        db.close();
        return userslocations;
    }

    private List<UserLocation> prepareUserLocationListFromDb(String userName)
    {
        DataSource db = new DataSource(this);
        db.open();
        List<UserLocation> userslocations = null;
        try {
            List<LocationRecord> reqs = db.getLocationRecords(userName);
            userslocations = new ArrayList<>();
            //Here expecting only one user record at the most:
            for (LocationRecord _req : reqs) {
                userslocations.add(new UserLocation(_req.getUserName(), _req.getGroupName(), _req.getDisplayName(), _req.getLatitude(), _req.getLongitude(), _req.getRecordedTime(), _req.getAddressLine1(), _req.getAddressLine2(), _req.getAddressLine3()));
            }
        }
        catch(Exception e)
        {
            Log.e(FILENAME,"Exception in db " +e.getMessage());
            e.printStackTrace();
        }
        db.close();
        return userslocations;
    }

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            mServiceConnected = true;
            // Register our messenger also on Service side:
            Message msg = Message.obtain(null,
                    ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_USERLIST);
            msg.replyTo = mMessenger;
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "RemoteException - try again" + e.getMessage());
                // We always have to trap RemoteException
                // (DeadObjectException
                // is thrown if the target Handler no longer exists)
                e.printStackTrace();
            }
        }

        /**
         * Connection dropped.
         */
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(FILENAME, "Disconnected from service.");
            mService = null;
            mServiceConnected = false;
        }


    };

    /**
     * Sends message with text stored in bundle extra data ("data" key).
     *
     * @param code to send
     */
    void sendToService(int code) {
        if (mServiceConnected) {

            Message msg = null;
            if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST) {
                Log.d(FILENAME, "sendToService: MESSAGE_TYPE_UNREGISTER");
                //Manish: Send from here, whatever to send to service from activity:
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            } else if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_QUERY) {
                Log.d(FILENAME, "sendToService: MESSAGE_TYPE_USERLIST_QUERY");
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_USERLIST_QUERY);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            }
            else if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_STOP_TRACKING) {
                Log.d(FILENAME, "sendToService: INTER_TASK_MESSAGE_TYPE_STOP_TRACKING");
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_STOP_TRACKING);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            }
            else if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_START_TRACKING) {
                Log.d(FILENAME, "sendToService: INTER_TASK_MESSAGE_TYPE_START_TRACKING");
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_START_TRACKING);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            }

            else if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_CHATROOMCALLBACK) {
                Log.d(FILENAME, "sendToService: INTER_TASK_MESSAGE_TYPE_REGISTER_CHATROOMCALLBACK");
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_CHATROOMCALLBACK);
                Bundle b = new Bundle();
                b.putInt("data", code);
                b.putString("roomName", mRoomName);
                msg.setData(b);
            }

            try {
                mService.send(msg);
            } catch (RemoteException e) {
                Log.e(FILENAME, "RemoteException - try again" + e.getMessage());
                // We always have to trap RemoteException
                // (DeadObjectException
                // is thrown if the target Handler no longer exists)
                e.printStackTrace();
            }
        } else {
            Log.d(FILENAME, "Cannot send - not connected to service.");
        }
    }


    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        Log.d(FILENAME, "binding initiated");
        mIsBound = getApplicationContext().bindService(new Intent(getApplicationContext(), ChatXMPPService.class), mConn, Context.BIND_AUTO_CREATE);
        Log.d(FILENAME, "binding done");
        //mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            Log.d(FILENAME, "doUnbindService");
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_USERLIST);
            }
            // Detach our existing connection.
            unbindService(mConn);
            mIsBound = false;
        }
    }




    private Menu mMenu;

    private MenuItem mMyProfileButton;
    private MenuItem mChecinButton;
    private MenuItem mTrackUserButton;
    private MenuItem mChatButton;
    private MenuItem mEmailButton;
    private MenuItem mPhoneButton;
    private MenuItem mRefreshButton;
    //private MenuItem mSearchUserButton;
    //private MenuItem mStartMyDayButton;
    //private MenuItem mStopMyDayButton;
    private MenuItem mClearUserHistory;
    private MenuItem mClearAllUsersHistory;
    //private MenuItem mMISettings;
    private MenuItem mContactButton;

    private MenuItem mCreateRoom;
    private MenuItem mDeleteRoom;
    private MenuItem mIsAnnouncement;

    //private MenuItem mMIBackButton;
    //private MenuItem mChat;
    //private MenuItem mClearUserHistory;

    public static boolean mDefaultMenu = true;
    public static void setDefauleMenu(boolean flag)
    {
        mDefaultMenu = flag;
    }

/*

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        mUserFragment.onCreateOptionsMenu(menu);
        return true;
    }


    @Override
    public boolean onPrepareOptionsMenu(Menu menu) {
        super.onPrepareOptionsMenu(menu);
        mUserFragment.onPrepareOptionsMenu(menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        mUserFragment.onOptionsItemSelected(item);
        return true;
    }

*/


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu_users, menu);
        //inflater.inflate(R.id.my_toolbar, menu);
        mMenu = menu;
        //mDefaultMenu = true;

        mMyProfileButton = menu.findItem(R.id.action_my_profile);
        if(mMyProfileButton != null)
        {
            Bitmap myPhoto = null;
            myPhoto = UserListLayout.getAvatarBitmapFromDB(OpenfireRestAPI.getUserName(),this);
            if(myPhoto != null)
                mMyProfileButton.setIcon(new BitmapDrawable(getResources(), myPhoto));
        }
        mChecinButton = menu.findItem(R.id.action_checkIn);
        mTrackUserButton = menu.findItem(R.id.action_trackme);
        mChatButton = menu.findItem(R.id.action_chat);
        mEmailButton = menu.findItem(R.id.action_email);
        mPhoneButton = menu.findItem(R.id.phonecall);
        mRefreshButton = menu.findItem(R.id.action_refresh);
        //mSearchUserButton = menu.findItem(R.id.action_search_user);
        //mStartMyDayButton = menu.findItem(R.id.action_start_my_day);
        //mStopMyDayButton = menu.findItem(R.id.action_stop_my_day);
        mClearUserHistory = menu.findItem(R.id.action_clean_history);
        mClearAllUsersHistory = menu.findItem(R.id.action_clean_all_users_history);
        //mMISettings = menu.findItem(R.id.action_settings);
        mContactButton = menu.findItem(R.id.action_contactus);

        mCreateRoom = menu.findItem(R.id.action_createRoom);
        mDeleteRoom = menu.findItem(R.id.action_deleteRoom);
        mIsAnnouncement = menu.findItem(R.id.announcement);
        //activateDefaultMenu();
        //mMenu.add(mMIBackButton);
        return true;
    }

    @Override
    public boolean onPrepareOptionsMenu (Menu menu) {
        super.onPrepareOptionsMenu(menu);
        if (mDefaultMenu)
        {
           // prepareProfilePhoto();
            mMyProfileButton = menu.findItem(R.id.action_my_profile);
            if(mMyProfileButton != null)
            {
                Bitmap myPhoto = null;
                myPhoto = UserListLayout.getAvatarBitmapFromDB(OpenfireRestAPI.getUserName(),this);
                if(myPhoto != null)
                {
                    mMyProfileButton.setIcon(new BitmapDrawable(getResources(), myPhoto));
                }
                else
                {
                    myPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.avatar).copy(Bitmap.Config.ARGB_8888, true);;
                    mMyProfileButton.setIcon(new BitmapDrawable(getResources(), myPhoto));
                }
            }
            if (mIsAnnouncement != null)
            {
                if (ChatXMPPService.isAnnouncementOn(this))
                {
                    mIsAnnouncement.setIcon(R.drawable.speakeron);
                }
                else
                {
                    mIsAnnouncement.setIcon(R.drawable.speakeroff);
                }
            }
            mMyProfileButton.setVisible(true);
            if (myTrackingAllowed == true)
            {
                mChecinButton.setIcon(R.drawable.map_icon_pink_3d);
            }
            else
            {

                mChecinButton.setIcon(R.drawable.map_icon_pink_3d_not_allowed);
            }
            mChecinButton.setVisible(true);
            mTrackUserButton.setVisible(true);
            mChatButton.setVisible(false);
            mEmailButton.setVisible(false);
            mPhoneButton.setVisible(false);
            mRefreshButton.setVisible(true);
            //mSearchUserButton.setVisible(true);
            //mStartMyDayButton.setVisible(true);
            //mStopMyDayButton.setVisible(true);
            mClearUserHistory.setVisible(false);
            mClearAllUsersHistory.setVisible(true);
            //mMISettings.setVisible(true);
            mContactButton.setVisible(true);
            mCreateRoom.setVisible(true);
            mDeleteRoom.setVisible(true);

        }
        else
        {
            //prepareProfilePhoto();
            mMyProfileButton = menu.findItem(R.id.action_my_profile);
            if(mMyProfileButton != null)
            {
                Bitmap userPhoto = null;
                Bitmap workingbm = Utility.getSavedBitmapForUser(selectedUserName, this);

                //userPhoto = UserListLayout.getAvatarBitmapFromDB(OpenfireRestAPI.getUserName(),this);
                if(workingbm != null)
                {
                    mMyProfileButton.setIcon(new BitmapDrawable(getResources(), workingbm));
                }
                else
                {
                    userPhoto = BitmapFactory.decodeResource(getResources(), R.drawable.avatar).copy(Bitmap.Config.ARGB_8888, true);;
                    mMyProfileButton.setIcon(new BitmapDrawable(getResources(), userPhoto));
                }
            }
            if (mIsAnnouncement != null)
            {
                if (ChatXMPPService.isAnnouncementOn(this))
                {
                    mIsAnnouncement.setIcon(R.drawable.speakeron);
                }
                else
                {
                    mIsAnnouncement.setIcon(R.drawable.speakeroff);
                }
            }
            mMyProfileButton.setVisible(true);
            if (myTrackingAllowed == true)
            {
                mChecinButton.setIcon(R.drawable.map_icon_pink_3d);
            }
            else
            {

                mChecinButton.setIcon(R.drawable.map_icon_pink_3d_not_allowed);
            }
            mChecinButton.setVisible(false);
            mTrackUserButton.setVisible(true);
            mChatButton.setVisible(true);
            mEmailButton.setVisible(true);
            mPhoneButton.setVisible(true);
            mRefreshButton.setVisible(true);
            //mSearchUserButton.setVisible(false);
            //mStartMyDayButton.setVisible(false);
            //mStopMyDayButton.setVisible(false);
            mClearUserHistory.setVisible(true);
            mClearAllUsersHistory.setVisible(false);
            //mMISettings.setVisible(true);
            mContactButton.setVisible(true);
            mCreateRoom.setVisible(false);
            mDeleteRoom.setVisible(false);
        }
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // mIsFileSelectedForSharing = false;
        Intent intent;
        switch (item.getItemId()) {

            case R.id.action_my_profile:
                Vibrate();
                launchPostRegFragment();
                //Toast.makeText(this, "Neeraj - Implement Profile Fragment", Toast.LENGTH_SHORT).show();
                //activateDefaultMenu();
                return true;
            case R.id.action_checkIn:
                onNavigateToTrackingSettingsFragment();
               // onClickCheckin();
                activateDefaultMenu();
                return true;
            case R.id.action_trackme:
                if (mChecinButton.isVisible()) {
                    onClickTrackMe(ChatXMPPService.mUserName); //either this or other user's name need to be passed here.
                }
                else
                {
                    onClickTrackMe(selectedUserName);
                }
                activateDefaultMenu();
                return true;
            case R.id.action_chat:
                Vibrate();
                startActionChat(this, ChatXMPPService.mUserName.toString(), selectedUserName);
                activateDefaultMenu();
                return true;
            case R.id.action_email:
                Vibrate();
                Toast.makeText(this, "To be Implemented", Toast.LENGTH_SHORT).show();
                activateDefaultMenu();
                return true;
            case R.id.phonecall:
                Vibrate();
                // User chose the "Settings" item, show the app settings UI...
                //onBackPressed();
                //hideActionBar();
                userCallSelected(selectedUserName);
                //mMIRemoteUser.setTitle(mRemoteUserName.getText());
                activateDefaultMenu();
                return true;

            case R.id.action_refresh:
                onClickRefresh();
                activateDefaultMenu();
                return true;
            /*
            case R.id.action_search_user:
                Vibrate();
                Toast.makeText(this, "To be Implemented", Toast.LENGTH_SHORT).show();
                activateDefaultMenu();
                return true;

            case R.id.action_start_my_day:
                Vibrate();
                Toast.makeText(this, "To be Implemented", Toast.LENGTH_SHORT).show();
                activateDefaultMenu();
                return true;
            case R.id.action_stop_my_day:
                Vibrate();
                Toast.makeText(this, "To be Implemented", Toast.LENGTH_SHORT).show();
                activateDefaultMenu();
                return true;
            */
            case R.id.action_clean_history:
                Vibrate();
                //hideActionBar();
                //ChatRecord.clearFromDB(this, ChatXMPPService.mUserName.toString());
                ChatRecord.clearFromDB(this, ChatXMPPService.mUserName.toString(), selectedUserName, false);
                activateDefaultMenu();
                return true;
            /*
            case R.id.action_backbutton:
                // User chose the "Settings" item, show the app settings UI...
                onBackPressed();
                //hideActionBar();
                activateDefaultMenu();
                return true;
                */
            case R.id.action_clean_all_users_history:
                Vibrate();
                //hideActionBar();
                ChatRecord.clearFromDB(this, ChatXMPPService.mUserName.toString());
                activateDefaultMenu();
                return true;
            case R.id.home:
                Vibrate();
                onBackPressed();
                activateDefaultMenu();
                return true;
            /*
            case R.id.action_settings:
                Vibrate();
                // User chose the "Settings" item, show the app settings UI...
                //hideActionBar();
                Toast.makeText(this, "To be Implemented", Toast.LENGTH_SHORT).show();
                activateDefaultMenu();
                return true;
                */
            case R.id.action_contactus:
                // User chose the "Settings" item, show the app settings UI...
                //hideActionBar();
                Vibrate();
                sendEmail();
                activateDefaultMenu();
                return true;

            case R.id.action_settings:
                Intent settingIntent = new Intent(this, MainSettingsActivity.class);
                Bundle b = new Bundle();
                b.putString("USERNAME", ChatXMPPService.mUserName);
                b.putString("DISPLAYNAME", ChatXMPPService.mDisplayName);
                settingIntent.putExtras(b);
                this.startActivity(settingIntent);
                return true;
            case R.id.action_createRoom:
                Vibrate();
                onNavigateToCreateUserFragment1();
                if (mCreateRoomFragment1!= null) {
                    mCreateRoomFragment1.onCreateChatRoom(this);
                }
                return true;

            case R.id.action_deleteRoom:
                //tbd
                Vibrate();
                onNavigateToCreateUserFragment1();
                if (mCreateRoomFragment1!= null) {
                    mCreateRoomFragment1.onDeleteChatRoom(this);
                }
                return true;
            case R.id.announcement:
                Vibrate();
                if (mIsAnnouncement != null)
                {
                    if (ChatXMPPService.isAnnouncementOn(this))
                    {
                        mIsAnnouncement.setIcon(R.drawable.speakeroff);
                        ChatXMPPService.setAnnouncement(this, false);

                    }
                    else
                    {
                        mIsAnnouncement.setIcon(R.drawable.speakeron);
                        ChatXMPPService.setAnnouncement(this, true);
                    }
                }
            default://Back button - implemented:
                // If we got here, the user's action was not recognized.
                // Invoke the superclass to handle it.
                activateDefaultMenu();
                //hideActionBar();
                return super.onOptionsItemSelected(item);
        }
    }

    public void launchPostRegFragment()
    {
        /*
        android.app.FragmentManager managerF = getFragmentManager();
        try {
            managerF.popBackStack(null, managerF.POP_BACK_STACK_INCLUSIVE);
        }catch(Exception ex)
        {
            Log.d("ERROR","Ignoring exception as suggested on stackoverflow :)");
        }
        android.app.FragmentTransaction transaction = managerF.beginTransaction();
        ProfileCreateFragment profCreateFrag = new ProfileCreateFragment();
        transaction.replace(R.id.base_fragment_layout, profCreateFrag);
        transaction.addToBackStack(null);
        //transaction.add(R.id.base_fragment_layout, profCreateFrag, "ProfileCreateFragment");
        transaction.commit();
        //disableProgressBar();
        */
    }


    //Methods of callback from CreateRoomFragment1
    @Override
    public void cancelCreateChatRoom()
    {
        Vibrate();
        onNavigateToUsersListFromMap();
    }

    @Override
    public void createChatRoom(String roomName)
    {
        onNavigateToCreateUserFragment();
        //hideFragment(TAG_CREATEROOMFRAGMENT1);
        //showFragment(TAG_CREATEROOMFRAGMENT);
        if (mCreateRoomFragment!= null) {
            mCreateRoomFragment.updateFragment(this, roomName, mUsersPresenceMap);
        }
        return;
    }

    public void deleteChatRoom(String roomName)
    {
        //hideFragment(TAG_CREATEROOMFRAGMENT1);
        XMPPTCPConnection connection = ChatConnection.getInstance().getConnection();
        if( connection == null) {
            return;
        }
        if (roomName == null)
        {
            return;
        }
        RoomRecord.clearFromDB(getBaseContext(), roomName);
        mRoomName = roomName;
        if( MultiChat.destroyChatRoom(connection, roomName))//, owners, ChatXMPPService.mDisplayName) == 0)
        {
            Toast.makeText(this, "ChatRoom " + roomName + " deleted success ", Toast.LENGTH_SHORT).show();
            mUserFragment.removeChatRoom(this, R.layout.userlistlayout, roomName);
        }
        else
        {
            Toast.makeText(this, "ChatRoom " + roomName + " deletion failed ", Toast.LENGTH_SHORT).show();
        }
        onNavigateToUsersListFromMap();
        return;
    }


    String mRoomName = null;
    //Methods of callbacks from CreateRoomFragment1
    @Override
    public void submitChatRoom(String roomName, List userObjList)
    {
        Vibrate();
        List<String> owners = new ArrayList<String>();

        for (Object userObject : userObjList) {
            owners.add(((UserObject)(userObject)).userName);
                //selectedUserObjList.add(userObject);
        }
        XMPPTCPConnection connection = ChatConnection.getInstance().getConnection();
        if( connection == null)
        {
            return;
        }
        mRoomName = roomName;
        if( MultiChat.createChatRoom(connection, roomName, owners, ChatXMPPService.mDisplayName) == 0)
        {
            String roomJid = ChatXMPPService.DOMAINJID_BARE; //roomFullName.substring(roomFullName.indexOf("@"));
            String inviterName = ChatXMPPService.mUserName;// inviter.substring(0, inviter.indexOf("@"));
            String inviterJid = ChatXMPPService.DOMAINJID_BARE;

            Toast.makeText(this, "ChatRoom " + roomName + " created success ", Toast.LENGTH_SHORT).show();
            for(int i=0;i<owners.size();i++)
            {
                MesgCmdProcessor.sendRoomInvitationInMessage(getApplicationContext(),roomName,owners.get(i));
                MultiUserChat muc = MultiChat.FormMultiUserChat(connection,roomName);
                if(muc != null)
                {
                    try
                    {
                        muc.join(ChatXMPPService.mDisplayName);
                        RoomRecordNew.storeUniqueNameRecordNew(getApplicationContext(),
                                roomName,
                                roomJid,
                                owners.get(i),
                                ChatXMPPService.GROUPNAME,
                                ChatRoomInfo.ROOM_MEMEBER
                                );

                        DataSource db = new DataSource(getApplicationContext());
                        ChatRoomInfo roomInfo = db.getChatRoomInfo(roomName);
                    }
                    catch (Exception ex)
                    {
                        String message = ex.getMessage();
                        Log.d("room join Exception",message);

                    }

                }
            }



            //String roomFullName = room.getRoom();
            //roomName = roomFullName.substring(0, roomFullName.indexOf("@"));

            RoomRecord.storeUniqueNameRecordNew(getBaseContext(), roomName, roomJid, inviterName, inviterJid,
                    1, null, null, null, null, null, null, null, null, null, null, ChatXMPPService.GROUPNAME);
            if (roomName != null) {
                mUserFragment.addChatRoom(this, R.layout.userlistlayout, roomName);
            }

            sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_CHATROOMCALLBACK);
        }
        else
        {
            Toast.makeText(this, "ChatRoom " + roomName + " creation failed ", Toast.LENGTH_SHORT).show();
        }
        //hideFragment(TAG_CREATEROOMFRAGMENT);
        onNavigateToUsersListFromMap();
        return;
    }

    @Override
    public void cancelChatRoomFrag()
    {
        Vibrate();
        onNavigateToUsersListFromMap();
        return;
    }

    //Following is callBack - as user clicks on editProfile in RoomProfile
    @Override
    public void onGroupProfileEdit(String roomName)
    {
        //hideFragment(TAG_USER_GROUP_VIEW_PROFILE);
        onNavigateToCreateUserFragment();
        //showFragment(TAG_CREATEROOMFRAGMENT);
        if (mCreateRoomFragment!= null) {
            mCreateRoomFragment.editGroupFragment(this, roomName, mUsersPresenceMap);
        }
        //hideMenu();
        return;
    }

    //Following is callBack - as user revised the list of users in room
    @Override
    public void submitEditedChatRoom(String roomName, List<UserObject> editedList)
    {
        Log.d(FILENAME, "submitEditedChatRoom");

        Vibrate();
        XMPPTCPConnection connection = ChatConnection.getInstance().getConnection();
        if( connection == null)
        {
            Log.d(FILENAME, " Connection is null, group could not be edited");
            return;
        }
        mRoomName = roomName;

        List<Affiliate> usersInRoomList = MultiChat.getMembersOfChatRoom(connection,roomName);

        //if (usersInRoomList!=null)
        //{
        //Get Two lists
        //1> Get new addition lists
        List<String> newAdditions = new ArrayList<String>();
        for (UserObject userObj : editedList)
        {
            String userName = userObj.userName;
            boolean newAddition = true;
            if(usersInRoomList!=null)
            {
                for (Affiliate userAffiliation : usersInRoomList) {
                    if (userAffiliation.getNick().equals(userName)) {
                        newAddition = false;
                        break;
                    }
                }
            }
            if (newAddition == true)
            {
                //Add to new additions list
                newAdditions.add(userName);
            }
        }
        //Invite new additions
        MultiChat.inviteUsersWithAffiliation(connection, roomName, newAdditions, MultiChat.AFFILIATION_TYPE.AFFILIATION_MEMBER);

        //1> Check for all deletions:
        //setList of selected users in room in userObjList
        if(usersInRoomList!=null)
        {
            for (Affiliate userAffiliation : usersInRoomList) {
                boolean userRemoved = true;
                String userName = userAffiliation.getNick();
                for (UserObject userObj : editedList) {
                    if (userObj.userName.equals(userName)) {
                        userRemoved = false;
                        break;
                    }
                }
                if (userRemoved == true) {
                    //Kick user out of room
                    MultiChat.kickUserFromTheChatRoom(connection, roomName, userName);
                }
            }
        }
        //hideFragment(TAG_CREATEROOMFRAGMENT);
        onNavigateToUsersListFromMap();
        return;
    }


    private void prepareProfilePhoto()
    {
        ImageView userView = (ImageView)findViewById(R.id.action_my_profile);
        String displayName = OpenfireRestAPI.getDisplayName();
        if(displayName == null)
        {
            if(ChatXMPPService.mUserName == null)
                ChatXMPPService.mUserName = OpenfireRestAPI.getUserName();
            displayName = ChatXMPPService.mUserName;
        }
        if(OpenfireRestAPI.getUserName() != null)
        {
            if(userView != null)
            {
                BitMapWorkerRequest.AddAndStartBitMapWorkerOnlyDb(getResources(),
                        OpenfireRestAPI.getUserName(),
                        userView,
                        userView.getWidth(),
                        userView.getHeight(),
                        null,
                        null,
                        null,
                        null,
                        null,
                        R.id.action_my_profile,
                        this);
            }
        }
    }


    //@Override
    public void activateDefaultMenu() {
        //hideActionBar();
        setDefauleMenu(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            invalidateOptionsMenu();
        }
    }
    //@Override
    public void activateSelectedMenu() {
        //hideActionBar();
        setDefauleMenu(false);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            invalidateOptionsMenu();
        }
    }

    public void hideMenu() {
        hideActionBar();
        //setDefauleMenu(false);
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            invalidateOptionsMenu();
        }*/
    }

    public void showMenu() {
        showActionBar();
        //setDefauleMenu(false);
        /*
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
            invalidateOptionsMenu();
        }*/
    }

    protected void sendEmail() {

        Log.i("Send email", "");
        Toast.makeText(this, "Select Email", Toast.LENGTH_SHORT).show();
        String[] TO = {"contactus@bitskeytechnologies.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Bitskey - contact - 9810414413");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Hi Support, \n Please contact us back -" + ChatXMPPService.mUserName);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.d(FILENAME, "Finished sending email...");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }


    protected void sendEmail(String userName) {

        Log.i("Send email", "");
        Toast.makeText(this, "Select Email", Toast.LENGTH_SHORT).show();
        String[] TO = {"contactus@bitskeytechnologies.com"};
        String[] CC = {""};
        Intent emailIntent = new Intent(Intent.ACTION_SEND);

        emailIntent.setData(Uri.parse("mailto:"));
        emailIntent.setType("text/plain");
        emailIntent.putExtra(Intent.EXTRA_EMAIL, TO);
        emailIntent.putExtra(Intent.EXTRA_CC, CC);
        emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Bitskey - contact - 9810414413");
        emailIntent.putExtra(Intent.EXTRA_TEXT, "Hi Support, \n Please contact us back -" + ChatXMPPService.mUserName);

        try {
            startActivity(Intent.createChooser(emailIntent, "Send mail..."));
            finish();
            Log.d(FILENAME, "Finished sending email...");
        }
        catch (android.content.ActivityNotFoundException ex) {
            Toast.makeText(this, "There is no email client installed.", Toast.LENGTH_SHORT).show();
        }
    }

    public static boolean myTrackingAllowed = true;
    public void onClickCheckin(boolean canGetLocation)
    {
        myTrackingAllowed = canGetLocation;
        if (myTrackingAllowed == true)
        {
            Toast.makeText(getApplicationContext(), "My Tracking Enabled...", Toast.LENGTH_LONG).show();
            //myTrackingAllowed = false;
            //Icon change handled in activating menu
            if (mService != null) {
                sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_START_TRACKING);
            }
            mChecinButton.setIcon(R.drawable.map_icon_pink_3d);
        }
        else
        {
            Toast.makeText(getApplicationContext(), "My Tracking Disabled...", Toast.LENGTH_LONG).show();
            //myTrackingAllowed = true;
            //Icon change handled in activating menu
            if (mService != null) {
                sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_STOP_TRACKING);
            }
            mChecinButton.setIcon(R.drawable.map_icon_pink_3d_not_allowed);
        }
        //onNavigateToMap();
        //mMapFragment.refresh();
    }


    public void onClickTrackMe(String userName)
    {
        if (userName.equals(ChatXMPPService.mUserName))
        {
            Toast.makeText(getApplicationContext(), "Checking in current location - will load track", Toast.LENGTH_LONG).show();
            MyLocationManager.obtainMyLocation(this); //checkin my location first
            //send to chatxmpp service as well
        }
        userMapSelected(0, userName);
    }


    public void onClickRefresh()
    {
        boolean amIOffline = false;
        //Also display connection status
        XMPPTCPConnection xmppTcpConnection = ChatConnection.getInstance().getConnection();
        Toast.makeText(getApplicationContext(), "Refreshing ", Toast.LENGTH_LONG).show();
        if (xmppTcpConnection!= null && xmppTcpConnection.isConnected())
        {
        }
        else
        {
            //show offline status
            amIOffline = true;
            mUserFragment.updateFragmentOffline(amIOffline);
        }
        if (mMapFragment.isVisible()) {
            Toast.makeText(getApplicationContext(), "Refreshing Current locations...", Toast.LENGTH_LONG).show();
            mMapFragment.refresh();
        }
        else if (mTrackUserFragment.isVisible()) {
            Toast.makeText(getApplicationContext(), "Refreshing locations of " + selectedUserDisplayName, Toast.LENGTH_LONG).show();
            mTrackUserFragment.refresh();
            onClickForUserAction(selectedUserName, selectedUserDisplayName, false);
        }
    }

    @Override
    public void onBackPressed()
    {
        onNavigateToUsersListFromMap();
        super.onBackPressed();
    }

    public void hideFragment (String tag)
    {
        Log.d(FILENAME, "hideFragment " + tag);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if((ft==null) || (fragment == null))
        {
            return;
        }
        ft.hide(fragment);
        //ft.addToBackStack(null);
        try
        {
            ft.commit();
            //TBD; It can crash here due to known implementation in android. Proper solution not found - so handled exception instead.
        }
        catch(IllegalStateException exception)
        {
            Log.e(FILENAME, "IllegalStateException - try again" + exception.getMessage());
            exception.printStackTrace();
            return;
        }
    }

    public void showFragment (String tag)
    {
        Log.d(FILENAME, "showFragment " + tag);
        FragmentTransaction ft = getSupportFragmentManager().beginTransaction();
        Fragment fragment = getSupportFragmentManager().findFragmentByTag(tag);
        if((ft==null) || (fragment == null))
        {
            return;
        }
        ft.show(fragment);
        //ft.addToBackStack(null);
        try
        {
            ft.commit();
        }
        catch(IllegalStateException exception)
        {
            Log.e(FILENAME, "IllegalStateException - try again" + exception.getMessage());
            exception.printStackTrace();
            return;
        }
    }

    public void onNavigateToGroupView() {
        Vibrate();
        showMenu();
        hideFragment(TAG_USERSLIST);
        hideFragment(TAG_USER_MAP);
        hideFragment(TAG_CREATEROOMFRAGMENT);
        hideFragment(TAG_CREATEROOMFRAGMENT1);
        hideFragment(TAG_MAP);
        showFragment(TAG_USER_GROUP_VIEW_PROFILE);
        hideFragment(TAG_TRACKING_SETTINGS);
    }

    public void onNavigateToMap() {
        Vibrate();
        showMenu();
        hideFragment(TAG_USERSLIST);
        hideFragment(TAG_USER_MAP);
        hideFragment(TAG_CREATEROOMFRAGMENT);
        hideFragment(TAG_CREATEROOMFRAGMENT1);
        hideFragment(TAG_USER_GROUP_VIEW_PROFILE);
        showFragment(TAG_MAP);
        hideFragment(TAG_TRACKING_SETTINGS);
    }

    public void onNavigateToTrackUserMap() {
        showMenu();
        Vibrate();
        hideFragment(TAG_USERSLIST);
        showFragment(TAG_USER_MAP);
        hideFragment(TAG_CREATEROOMFRAGMENT);
        hideFragment(TAG_CREATEROOMFRAGMENT1);
        hideFragment(TAG_MAP);
        hideFragment(TAG_USER_GROUP_VIEW_PROFILE);
        hideFragment(TAG_TRACKING_SETTINGS);
        onClickForUserAction(selectedUserName, selectedUserDisplayName, false);
    }

    public void onNavigateToUsersListFromMap() {
        showMenu();
        Vibrate();
        showFragment(TAG_USERSLIST);
        hideFragment(TAG_USER_MAP);
        hideFragment(TAG_CREATEROOMFRAGMENT);
        hideFragment(TAG_CREATEROOMFRAGMENT1);
        hideFragment(TAG_MAP);
        hideFragment(TAG_USER_GROUP_VIEW_PROFILE);
        hideFragment(TAG_TRACKING_SETTINGS);
    }

    public void onNavigateToCreateUserFragment() {
        showMenu();
        Vibrate();
        hideFragment(TAG_USERSLIST);
        hideFragment(TAG_USER_MAP);
        hideFragment(TAG_CREATEROOMFRAGMENT1);
        showFragment(TAG_CREATEROOMFRAGMENT);
        hideFragment(TAG_MAP);
        hideFragment(TAG_USER_GROUP_VIEW_PROFILE);
        hideFragment(TAG_TRACKING_SETTINGS);
    }

    public void onNavigateToCreateUserFragment1() {
        showMenu();
        Vibrate();
        hideFragment(TAG_USERSLIST);
        hideFragment(TAG_USER_MAP);
        hideFragment(TAG_CREATEROOMFRAGMENT);
        showFragment(TAG_CREATEROOMFRAGMENT1);
        hideFragment(TAG_MAP);
        hideFragment(TAG_USER_GROUP_VIEW_PROFILE);
        hideFragment(TAG_TRACKING_SETTINGS);
    }

    public void onNavigateToTrackingSettingsFragment() {
        showMenu();
        Vibrate();
        hideFragment(TAG_USERSLIST);
        hideFragment(TAG_USER_MAP);
        hideFragment(TAG_CREATEROOMFRAGMENT);
        hideFragment(TAG_CREATEROOMFRAGMENT1);
        hideFragment(TAG_MAP);
        hideFragment(TAG_USER_GROUP_VIEW_PROFILE);
        showFragment(TAG_TRACKING_SETTINGS);
    }

    /*
    public void onNavigateToUsersListFromUserMap() {
        showMenu();
        Vibrate();
        hideFragment(TAG_USERSLIST);
        showFragment(TAG_USER_MAP);
        hideFragment(TAG_CREATEROOMFRAGMENT);
        hideFragment(TAG_CREATEROOMFRAGMENT1);
        hideFragment(TAG_MAP);
        hideFragment(TAG_USER_GROUP_VIEW_PROFILE);
    }
    */
}