package com.bitskeys.track.users;

import java.io.Serializable;

/**
 * Created by Manish on 27-01-2016.
 */
public class UserPresenceChanged implements Serializable {
    public String userName;
    public String presence;
    public void UserPresenceChanged(){}
    public void setInfo(String userName, String presence)
    {
        this.userName = userName;
        this.presence = presence;
    }

}
