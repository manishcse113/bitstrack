package com.bitskeys.track.map;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.location.Address;
import android.location.Criteria;
import android.location.Geocoder;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import com.bitskeys.track.db.DataSource;
import com.bitskeys.track.db.LocationRecord;
import com.bitskeys.track.db.MyLocationRecord;
import com.bitskeys.track.db.SharedPreference;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.TimeClock;
import com.bitskeys.track.login.ConnectivityReceiver;
import com.google.android.gms.maps.model.LatLng;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.JsonHttpResponseHandler;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Locale;

/**
 * Created by Manish on 6/11/2016.
 */
public class LocationListenerImpl implements android.location.LocationListener
{
    public static String FILENAME = "GBC LocationListenerImpl";
    //Location related functions::
    public static LocationManager mLocationManager = null;
    public static android.location.Location mLocation;
    public static LatLng myPosition;
    public static boolean mIsGPSEnabled;
    public static boolean mIsNetworkEnabled;

    public static long mMyOldLat = 0;
    public static long mMyOldLong = 0;
    public static long myLastRecordedTime = 0;
    public static long myCurrentTime = 0;
    public static long minLatLongChange = 1;
    public static final long GPSTURNOFFTIME = 60000;

    public static boolean mCanGetLocation = true;
    public static int mTrackingInterval = 120000;
    //static long MIN_TIME_BW_UPDATES = 180000; //10000 = 10 seconds//12000 =2 minutes, 7 minutes  //900000 == 15 minute updates://30 minute = 30 * 60 * 1000 = 1800000, time is in miliseconds
    static long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; //10 meters : //20 mitures, //200 meters;
    private static int instanceCount =0;
    private static LocationListenerImpl myInstance = null;
    private static SenorEventListenerImpl sensorListnerInstance = null;

    public Context context = null;
    private LocationListenerImpl()
    {
        sensorListnerInstance = SenorEventListenerImpl.getInstance();
    }


    public ServiceCallback mCallback = null;
    public void RegisterCallback(ServiceCallback callback)
    {
        mCallback = callback;
    }
    public void InitContext(Context cntxt)
    {
        context = cntxt;
        sensorListnerInstance.InitContext(context);
        sensorListnerInstance.initSensorListner(this);
    }

    public static LocationListenerImpl getInstance()
    {
        if (instanceCount==0)
        {
            instanceCount++;
            myInstance = new LocationListenerImpl();
        }
        return myInstance;
    }

    public void initFromPreferences( )
    {
        int startDay = SharedPreference.getInstance().getStartTrackingDay();
        int endDay = SharedPreference.getInstance().getLastTrackingDay();
        int startTime = SharedPreference.getInstance().getStartTrackingTime();
        int endTime = SharedPreference.getInstance().getLastTrackingTime();

        mCanGetLocation = SharedPreference.getInstance().getTrackingOn();
        mTrackingInterval = SharedPreference.getInstance().getTrackingInterval();
        showLocationEnableDialog = SharedPreference.getInstance().getLocationDialogPref();
        TimeClock.setTrackingPreference(mCanGetLocation, startDay, endDay, startTime, endTime);
    }

/*
    @Override
    public void onCreate() {
        super.onCreate();
    }

    @Override
    public void onDestroy() {
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        if (intent != null) {
        }
    }
*/
    public static void setTrackingInterval( int interval)
    {
        mTrackingInterval = interval;
    }
    public static LatLng getMyLocation() {
        return myPosition;
    }

    //new android.location.LocationListener() {
    @Override
    public void onLocationChanged(android.location.Location location) {
        //Log.d(FILENAME, "onLocationChanged Location changed");
        mLocation = location;
        myPosition = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
        //User Manager to upload locations information to server
        //Upload locations after multiplying by 100: as db stores them as long instead of double
        long latitydeL = (long) (mLocation.getLatitude() * ChatXMPPService.DOUBLETOLONG);
        long longitudeL = (long) (mLocation.getLongitude() * ChatXMPPService.DOUBLETOLONG);

        if (mCanGetLocation) {
            if (isLocationChanged(latitydeL, longitudeL))
            {
                captureLoc(context, ChatXMPPService.GROUPNAME, ChatXMPPService.mUserName, ChatXMPPService.mUserName, latitydeL, longitudeL);
            }
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
        //Log.d(FILENAME, "onProviderDisabled disabled");
        // getting GPS status
        mIsGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

        //Log.d(FILENAME, "isGPSEnabled Enabled ? = " + mIsGPSEnabled);
        // getting network status
        mIsNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        //Log.d(FILENAME, "isNetworkEnabled Enabled ? = " + mIsNetworkEnabled);
    }

    @Override
    public void onProviderEnabled(String provider) {
        //Log.d(FILENAME, "onProviderDisabled enabled");
        // getting GPS status
        mIsGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        //Log.d(FILENAME, "isGPSEnabled Enabled ? = " + mIsGPSEnabled);
        // getting network status
        mIsNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        //Log.d(FILENAME, "isNetworkEnabled Enabled ? = " + mIsNetworkEnabled);
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle bundle) {
        //   Log.d(FILENAME, "onStatusChanged provider");
    }

    //Check if location is changed or not
    //If location is same for some time, then unregister from GPS to save battery
    public boolean isLocationChanged(long latitude, long longitude)
    {
        myCurrentTime = System.currentTimeMillis();
        if (Math.abs(latitude - mMyOldLat)<=2 && Math.abs(longitude - mMyOldLong) <= 2)
        {
            //if it is more than threshold time location is same - remove gps listener to save battery
            // It will be started again as there is a change in acceleration
            if ( myCurrentTime >= myLastRecordedTime + GPSTURNOFFTIME)
            {
                if (mLocationManager!=null)
                {
                    unRegisterLocationUpdates();
                }
                return false;
            }
            //Since there is no difference - so no need to store duplicate location
            return false;
        }
        mMyOldLat = latitude;
        mMyOldLong = longitude;
        myLastRecordedTime = myCurrentTime;
        return true;
    }

    public long obtainLocationifNeeded(long sleepTime)
    {
        if (sleepTime >= mTrackingInterval*1000)
        {
            if (mCanGetLocation == true)
            {
                if (TimeClock.isTrackingTime())
                {
                    obtainMyLocation(context);
                }
            }
            sleepTime = 0;
        }
        return sleepTime;
    }

    public Location obtainMyLocation(Context context) {
        //Log.d(FILENAME, "ObtainMyLocation");
        //mLocationManager = (LocationManager) getSystemService(LOCATION_SERVICE);
        if (mLocationManager != null) {
            //  Log.d(FILENAME, "mLocationManager !=null");
            Criteria criteria = new Criteria();
            String provider = mLocationManager.getBestProvider(criteria, true);
            mLocation = mLocationManager.getLastKnownLocation(provider);
            if (mLocation != null) {
                //Log.d(FILENAME, "Bingo my location retrieved");
                myPosition = new LatLng(mLocation.getLatitude(), mLocation.getLongitude());
                long latitydeL = (long) (mLocation.getLatitude() * ChatXMPPService.DOUBLETOLONG);
                long longitudeL = (long) (mLocation.getLongitude() * ChatXMPPService.DOUBLETOLONG);
                //TBD: remove my phone number from here..
                //userName and Mobile number are same::
                if (isLocationChanged(latitydeL, longitudeL))
                {
                    captureLoc(context, ChatXMPPService.GROUPNAME, ChatXMPPService.mUserName, ChatXMPPService.mUserName, latitydeL, longitudeL);
                }
            }
        }
        return mLocation;
    }

    public void registerGPSIfNeeded()
    {
        if (mCanGetLocation ==true &&
                mIsGPSEnabled== false &&
                mIsNetworkEnabled==false) {
            mLocation = registerForLocationUpdates();
        }
    }

    // To send my locations to server
    public android.location.Location registerForLocationUpdates() {
        //Log.d(FILENAME, "StartMyLocationUpdates");
        if (mCanGetLocation == false)
        {
            return mLocation;
        }
        if (mLocationManager==null)
        {
            mLocationManager = ChatXMPPService.getLocationManager();
        }
        // getting GPS status
        mIsGPSEnabled = mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        //Log.d(FILENAME, "isGPSEnabled Enabled ? = " + mIsGPSEnabled);
        // getting network status
        mIsNetworkEnabled = mLocationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        //Log.d(FILENAME, "isNetworkEnabled Enabled ? = " + mIsNetworkEnabled);
        if (!mIsGPSEnabled && !mIsNetworkEnabled) {
            // no network provider is enabled
            if (!mIsGPSEnabled)
            {
                if (showLocationEnableDialog)
                {
                    mCallback.startLocationEnableDialog();
                    showLocationEnableDialog = false;
                }
            }
        }
        else
        {
            //retrieve provider..
            Criteria criteria = new Criteria();
            final String provider = mLocationManager.getBestProvider(criteria, true);

                mLocationManager.requestLocationUpdates(
                        provider,
                        mTrackingInterval,
                        MIN_DISTANCE_CHANGE_FOR_UPDATES,
                        this
                );
            //obtainMyLocation(this);
        }
        return mLocation;
    }

    public void unRegisterLocationUpdates()
    {
        if (mLocationManager!=null)
        {
            mLocationManager.removeUpdates(this);
            mLocationManager = null;
            mIsGPSEnabled = false;
            mIsNetworkEnabled = false;
        }
        //Now since we are unregistering from location updates - so register with accelerometer
        sensorListnerInstance.detectAndRegisterForSignificantMotion();
    }

    public static boolean showLocationEnableDialog = true;
    public void StopMyLocationUpdates(Context cntxt) {
        //retrieve provider..
        //Criteria criteria = new Criteria();
        //String provider =
        //mLocationManager.removeUpdates();
        mCanGetLocation = false;
        showLocationEnableDialog = false;
        unRegisterLocationUpdates();
    }

    public void AllowMyLocationUpdates(Context cntxt) {
        //retrieve provider..
        //Criteria criteria = new Criteria();
        //String provider =
        //mLocationManager.removeUpdates();
        mCanGetLocation = true;
        showLocationEnableDialog = true;
        registerForLocationUpdates();
    }


    //Location JSON related functionality here::
    public static final String BASE_URI = ChatXMPPService.DOMAIN + ":8080/mapservice/map";
    public static final String PATH_TRACK = "/locateUsersWithAddress/";
    public static final String USER_TRACK = "/trackUserWithAddress/"; //trackUserAddress
    public static final String PATH_CAPTURE = "/captureWithAddress/"; //captureAddress
    public static final String PATH_SAVELOGS = "/save/";
    public static final String PATH_LOCATEUSERS = "/locateUsers";
    //private static final String FILENAME = "GBC LocationClient";
    public static final String BASE_IMAGE_URI = ChatXMPPService.DOMAIN + ":8888/images/";

    public static void trackUsers(Context context, String groupName) {
        Log.d(FILENAME, "trackUsers group");
        JSONObject json = new JSONObject();
        try {
            json.put("groupName", groupName);
        } catch (JSONException e) {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return;
        }
        String jsonString = json.toString();
        String jsonPath = "http://" + BASE_URI + PATH_TRACK + groupName;
        invokeJsonRequestTrack(context, jsonPath, jsonString);
        return;
    }

    public static String getTrackUserJsonRequestString(String userName, String groupName, String mobile) {
        JSONObject json = new JSONObject();
        try {
            json.put("groupName", groupName);
            json.put("userName", userName);
            json.put("mobile", mobile);
        } catch (JSONException e) {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return null;
        }
        String jsonString = json.toString();
        return jsonString;
    }

    public static void trackUser(Context context, String groupName, String userName, String mobile) {
        Log.d(FILENAME, "trackUsers");
        String jsonString = getTrackUserJsonRequestString(userName, groupName, mobile);
        String jsonPath = "http://" + BASE_URI + USER_TRACK + userName;//"+919810414413";//userName; //hard coded for now
        invokeJsonRequestTrackUser(context, jsonPath, jsonString);
        return;
    }

    public static void trackUsers(Context context, String groupName, long latitude, long longitude, long radius) {
        Log.d(FILENAME, "trackUsers");
        JSONObject json = new JSONObject();
        try {
            json.put("groupName", groupName);
            json.put("latitude", latitude);
            json.put("longitude", longitude);
            json.put("radius", radius);

        } catch (JSONException e) {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return;
        }
        String jsonString = json.toString();
        String jsonPath = "http://" + BASE_URI + PATH_TRACK + groupName;
        invokeJsonRequestTrack(context, jsonPath, jsonString);
        return;
    }

    public static List jsonMyLocationString = new ArrayList();
    public static List jsonMyDBLocationString = new ArrayList();

    //Need to read from db the unsent records of of last time..
    public void readAndAddToMylocationString() {
        Log.d(FILENAME, "readAndAddToMylocationString");
        DataSource db = new DataSource(context);
        db.open();
        List<MyLocationRecord> reqs = db.getMyLocationRecords();
        //To enhance user experience:
        //  First get only 20 records and send to user list activity
        //Then update all users and final list will be sent again from the function calling this function
        int i = 0;
        for (MyLocationRecord _req : reqs) {
            JSONObject json = new JSONObject();
            try {
                json.put("mobileNumber", _req.getUserName());
                json.put("userName", _req.getUserName());
                json.put("latitude", _req.getLatitude());
                json.put("longitude", _req.getLongitude());
                json.put("groupName", _req.getGroupName());
                json.put("recordedTime", _req.getRecordedTime());
            } catch (JSONException e) {
                Log.e(FILENAME, e.getMessage());
                e.printStackTrace();
                return;
            }
            String jsonString = json.toString();
            synchronized (jsonMyDBLocationString)
            {
                jsonMyDBLocationString.add(jsonString);
            }
        }
        db.close();
    }


    public static void captureLoc(Context context, String groupName, String userName, String mobile, long latitude, long longitude) {
        Log.d(FILENAME, "captureLoc");
        //long differeneLat = Math.abs(latitude - mMyOldLat);
        //long differnceLong = Math.abs(longitude - mMyOldLong);
        String epochtime;
        JSONObject json = new JSONObject();
        String addressLine1 = "";
        String addressLine2 = "";
        String addressLine3 = "";//Double.toString((double)(latitude)/DOUBLETOLONG) + " " + Double.toString((double)(longitude)/DOUBLETOLONG);
        try {
            ArrayList<String> addressFragments = getAddress(context, latitude, longitude);
            if (addressFragments != null) {

                for (int k = 0; k < addressFragments.size(); k++) {
                    if (k == 0)
                        addressLine1 = addressFragments.get(k);
                    else if (k == 1)
                        addressLine2 = addressFragments.get(k);
                    else if (k == 2)
                        addressLine3 = addressFragments.get(k);
                    else
                        break;
                }
            }
        } catch (Exception e) {
            Log.e(FILENAME, "Exception in getting address");
            e.printStackTrace();
        }

        try {
            json.put("mobileNumber", mobile);
            json.put("userName", userName);
            json.put("latitude", latitude);
            json.put("longitude", longitude);
            json.put("groupName", groupName);
            //json.put("recordedTime", new Date());
            //myLastRecordedTime = myCurrentTime = System.currentTimeMillis();
            epochtime = Long.toString(myLastRecordedTime);
            json.put("recordedTime", epochtime);

            json.put("addressLine1",addressLine1);
            json.put("addressLine2",addressLine2);
            json.put("addressLine3",addressLine3);

        } catch (JSONException e) {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return;
        }
        String jsonString = json.toString();
        synchronized (jsonMyLocationString)
        {
            jsonMyLocationString.add(jsonString);
        }
        String jsonPath = "http://" + BASE_URI + PATH_CAPTURE;

        //Following works - if user doesn't kill application.
        // However- if application was killed, then it is important to send all the locations which were not sent earlier.
        if (ConnectivityReceiver.isConnected(context)) {

            //if there is anything in db - send this first:
            synchronized ((jsonMyDBLocationString))
            {
                if (jsonMyDBLocationString.size() > 0) {
                    for (Object jsonMessage : jsonMyDBLocationString) {
                        //For every jsonString send it to server.
                        invokeJsonRequestCapture(context, jsonPath, (String) jsonMessage);
                        try {
                            JSONObject obj = new JSONObject(jsonMessage.toString());
                            MyLocationRecord.clearFromDB(context, obj.get("recordedTime").toString());//Ideally it shall be done on receiving success
                        } catch (Exception e) {
                            Log.d(FILENAME, "Exception in getting json Object back");
                            e.printStackTrace();
                        }
                    }
                    jsonMyDBLocationString.clear();
                }
            }

            //First
            synchronized ((jsonMyLocationString))
            {
                for (Object jsonMessage : jsonMyLocationString) {
                    //For every jsonString send it to server.
                    invokeJsonRequestCapture(context, jsonPath, (String) jsonMessage);
                    try {
                        JSONObject obj = new JSONObject(jsonMessage.toString());
                        MyLocationRecord.clearFromDB(context, obj.get("recordedTime").toString());//Ideally it shall be done on receiving success
                    } catch (Exception e) {
                        Log.d(FILENAME, "Exception in getting json Object back");
                        e.printStackTrace();
                    }

                    //jsonMyLocationString.(jsonMessage);
                }
                jsonMyLocationString.clear();
            }
        } else {
            //Store my locations in db as well - for later sending to server:
            String displayName = ChatXMPPService.getDisplayName(userName);
            MyLocationRecord.storeMyLocationRecord(context, userName, ChatXMPPService.GROUPNAME, displayName, latitude, longitude, epochtime);
        }
        return;
    }


    public static void invokeJsonRequestCapture(Context context, String jsonPath, String jsonString) {
        //Log.d(FILENAME, "invokeJsonRequestCapture");
        AsyncHttpClient client = new AsyncHttpClient();
        client.removeHeader("Content-Type");
        client.addHeader("Content-Type", "application/json;charset=UTF-8");
        client.addHeader("Accept", "application/json");
        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
            client.post(context, jsonPath, se, "application/json", new
                AsyncHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, org.apache.http.Header[] headers,
                                          byte[] responseBody) {
                        //Log.d(FILENAME, "http onSuccess: " + responseBody);
                        //try {
                        // JSON Object
                        HashMap<String, String> hmap = new HashMap<String, String>();
                        for (org.apache.http.Header h : headers) {
                            hmap.put(h.getName(), h.getValue());
                            if (h.getName().equals("Content-Length")) {
                                if (h.getValue().equals("0")) {
                                    Log.d(FILENAME, "successfully response");
                                }
                            }
                        }
                    }

                    // When the response returned by REST has Http response code other than '200'
                    @Override
                    public void onFailure(int statusCode, org.apache.http.Header[] headers,
                                          byte[] responseBody, Throwable
                                                  error) {
                        // Hide Progress Dialog
                        //prgDialog.hide();
                        Log.d(FILENAME, "http onFailure");
                        // When Http response code is '404'
                        if (statusCode == 404) {
                            Log.d(FILENAME, "Failed:404" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code is '404'
                        else if (statusCode == 409) {
                            Log.d(FILENAME, "Failed:409" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Log.d(FILENAME, "Failed:500" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code other than 404, 500
                        else {
                            Log.d(FILENAME, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]");
                        }
                    }
                }
            );
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        catch (Exception e)
        {
            e.printStackTrace();
            Log.d(FILENAME, e.getMessage());
        }
        return;
    }

    public static UserLocationsList usersMapLocations = new UserLocationsList();
    public static UserLocationsList userMapLocations = new UserLocationsList();
    public static void invokeJsonRequestTrack(Context context, String jsonPath, String jsonString) {
        Log.d(FILENAME, "invokeJsonRequestTrack");
        AsyncHttpClient client = new AsyncHttpClient();
        //Applicable in case a list is returned
        client.removeHeader("Content-Type");
        client.addHeader("Content-Type", "application/json;charset=UTF-8");
        client.addHeader("Accept", "application/json");
        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        final Context context2 = context;
        client.get(context, jsonPath, null,
                // client.get(context, jsonPath, null, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, org.apache.http.Header[] headers,
                                          org.json.JSONArray responseArray) {
                        Log.d(FILENAME, "http onSuccess: ");// + responseArray);
                        HashMap<String, String> hmap = new HashMap<String, String>();
                        for (org.apache.http.Header h : headers) {
                            hmap.put(h.getName(), h.getValue());
                            if (h.getName().equals("Content-Length")) {
                                if (h.getValue().equals("0")) {
                                    Log.d(FILENAME, "successfully response");
                                }
                            }
                        }
                        //MapInfoProcessInBackground mapInfoProcess = new MapInfoProcessInBackground(context2, usersMapLocations, responseArray);
                        //mapInfoProcess.execute();

                        try {
                            usersMapLocations.usersMapLocations.clear();
                            for (int i = 0; i < responseArray.length(); i++) {
                                //JSONObject objects = responseArray.getJSONArray(i);
                                JSONObject object = responseArray.getJSONObject(i);
                                UserLocation loc = new UserLocation();
                                String groupName = object.getString("groupName");
                                loc.setGroupName(groupName);

                                loc.setMobileNumber(object.getString("mobileNumber"));
                                loc.setUserName(object.getString("userName"));

                                //Workaround handling for crash here...
                                String userName = object.getString("userName");
                                String displayName = null;
                                if (userName != null) {
                                    displayName = ChatXMPPService.getDisplayName(userName);
                                    if (displayName == null) {
                                        continue;
                                    }
                                }

                                loc.setDisplayName(displayName);
                                long latitude = object.getLong("latitude");

                                long longitude = object.getLong("longitude");

                                //Hack to see locations when doubletolong was 1000
                                if (latitude < 100000)
                                {
                                    latitude = latitude*10;
                                }
                                if (longitude < 100000)
                                {
                                    longitude = longitude*10;
                                }

                                loc.setLatitude(latitude);
                                loc.setLongitude(longitude);

                                String recordedTime = object.getString("recordedTime");
                                loc.setRecordedTime(recordedTime);
                                //loc.setAddress(addressLine1, addressLine2, addressLine3);
                                String addressLine1 = "";
                                String addressLine2 = "...No Address";
                                String addressLine3 = Double.toString((double) (latitude) / ChatXMPPService.DOUBLETOLONG) + " " + Double.toString((double) (longitude) / ChatXMPPService.DOUBLETOLONG);
                                addressLine1 = object.getString("addressLine1");
                                addressLine2 = object.getString("addressLine2");
                                addressLine3 = object.getString("addressLine3");
                                loc.setAddress(addressLine1, addressLine2, addressLine3);
                                LocationRecord.clearFromDB(context2, userName);
                                LocationRecord.storeLocationRecord(context2, userName, groupName, displayName, latitude, longitude, recordedTime, addressLine1, addressLine2, addressLine3);
                                usersMapLocations.usersMapLocations.add(loc);
                                UserLocation tmp = null;
                                if ((tmp = usersMapLocations.userLocationHashMap.get(object.getString("userName"))) != null) {
                                    usersMapLocations.userLocationHashMap.remove(tmp);
                                }
                                usersMapLocations.userLocationHashMap.put(object.getString("userName"), loc);

                                //SendNewLocationResults to useractivity--> mapfragment
                            }
                            ChatXMPPService.SendtoUserListActivity(ChatXMPPService.ACTION_USERS_MAP_UPDATES);

                        } catch (JSONException e) {
                            Log.d(FILENAME, e.getMessage());
                        }

                    }

                    // When the response returned by REST has Http response code other than '200'
                    @Override
                    public void onFailure(int statusCode, org.apache.http.Header[] headers,
                                          byte[] responseBody, Throwable error) {
                        Log.d(FILENAME, "http onFailure");
                        // When Http response code is '404'
                        if (statusCode == 404) {
                            Log.d(FILENAME, "Failed:404" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code is '404'
                        else if (statusCode == 409) {
                            Log.d(FILENAME, "Failed:409" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Log.d(FILENAME, "Failed:500" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code other than 404, 500
                        else {
                            Log.d(FILENAME, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]");
                        }

                        ChatXMPPService.SendtoUserListActivity(ChatXMPPService.ACTION_USERS_MAP_UPDATES_FAILED);
                    }
                }
        );
        return;
    }

    public static void invokeJsonRequestTrackUser(final Context context, String jsonPath, String jsonString) {
        AsyncHttpClient client = new AsyncHttpClient();
        //Applicable in case a list is returned
        client.removeHeader("Content-Type");
        client.addHeader("Content-Type", "application/json;charset=UTF-8");
        client.addHeader("Accept", "application/json");
        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        userMapLocations.usersMapLocations.clear();
        client.get(context, jsonPath, null,
                // client.get(context, jsonPath, null, "application/json",
                new JsonHttpResponseHandler() {
                    @Override
                    public void onSuccess(int statusCode, org.apache.http.Header[] headers,
                                          org.json.JSONArray responseArray) {
                        Log.d(FILENAME, "http onSuccess: ");// + responseArray);
                        HashMap<String, String> hmap = new HashMap<String, String>();
                        for (org.apache.http.Header h : headers) {
                            hmap.put(h.getName(), h.getValue());
                            if (h.getName().equals("Content-Length")) {
                                if (h.getValue().equals("0")) {
                                    Log.d(FILENAME, "successfully response");
                                }
                            }
                        }
                        //Move this code to asynctasak:

                        try {
                            userMapLocations.usersMapLocations.clear();
                            for (int i = 0; i < responseArray.length(); i++) {
                                //JSONObject objects = responseArray.getJSONArray(i);
                                JSONObject object = responseArray.getJSONObject(i);
                                UserLocation loc = new UserLocation();
                                loc.setGroupName(object.getString("groupName"));
                                loc.setMobileNumber(object.getString("mobileNumber"));
                                loc.setUserName(object.getString("userName"));
                                loc.setDisplayName(ChatXMPPService.getDisplayName(object.getString("userName")));

                                long latitude = object.getLong("latitude");
                                long longitude = object.getLong("longitude");

                                //Hack to see locations when doubletolong was 1000
                                if (latitude < 100000)
                                {
                                    latitude = latitude*10;
                                }
                                if (longitude < 100000)
                                {
                                    longitude = longitude*10;
                                }

                                loc.setLatitude(latitude);
                                loc.setLongitude(longitude);
                                loc.setRecordedTime(object.getString("recordedTime"));
                                String addressLine1 = "";
                                String addressLine2 = "...No Address";
                                String addressLine3 = Double.toString((double) (latitude) / ChatXMPPService.DOUBLETOLONG) + " " + Double.toString((double) (longitude) / ChatXMPPService.DOUBLETOLONG);
                                addressLine1 = object.getString("addressLine1");
                                addressLine2 = object.getString("addressLine2");
                                addressLine3 = object.getString("addressLine3");
                                loc.setAddress(addressLine1, addressLine2, addressLine3);
                                userMapLocations.usersMapLocations.add(loc);
                                //SendNewLocationResults to useractivity--> mapfragment
                            }
                            ChatXMPPService.SendtoUserListActivity(ChatXMPPService.ACTION_USER_MAP_UPDATES);
                        } catch (JSONException e) {
                            Log.d(FILENAME, e.getMessage());
                        }
                    }

                    // When the response returned by REST has Http response code other than '200'
                    @Override
                    public void onFailure(int statusCode, org.apache.http.Header[] headers,
                                          byte[] responseBody, Throwable error) {
                        Log.d(FILENAME, "http onFailure");
                        // When Http response code is '404'
                        if (statusCode == 404) {
                            Log.d(FILENAME, "Failed:404" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code is '404'
                        else if (statusCode == 409) {
                            Log.d(FILENAME, "Failed:409" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code is '500'
                        else if (statusCode == 500) {
                            Log.d(FILENAME, "Failed:500" + error.getMessage() + ":" + responseBody.toString());
                        }
                        // When Http response code other than 404, 500
                        else {
                            Log.d(FILENAME, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]");
                        }
                        ChatXMPPService.SendtoUserListActivity(ChatXMPPService.ACTION_USER_MAP_UPDATES_FAILED);// --> send fail message for refreshing the same:
                    }
                }
        );
        return;
    }

    public static Geocoder geocoder = null;
    public static ArrayList<String> getAddress(Context context, long latitudeL, long longitudeL) {
        ArrayList<String> addressFragments = null;
        List<Address> addresses = null;
        if (geocoder == null) {
            geocoder = new Geocoder(context, Locale.getDefault());
        }
        double latitude = ((double) latitudeL) / ChatXMPPService.DOUBLETOLONG;
        double longitude = ((double) longitudeL) / ChatXMPPService.DOUBLETOLONG;
        try {
            addresses = geocoder.getFromLocation(
                    latitude,
                    longitude,
                    // In this sample, get just a single address.
                    1);
        } catch (IOException ioException) {
            // Catch network or other I/O problems.
            //String errorMessage = getString("Service not available");
            Log.e(FILENAME, "Service not available", ioException);
            return null;
        } catch (IllegalArgumentException illegalArgumentException) {
            // Catch invalid latitude or longitude values.
            // errorMessage = getString(R.string.invalid_lat_long_used);
            Log.e(FILENAME, "Invalid lat lng used" + ". " +
                    "Latitude = " + latitude +
                    ", Longitude = " +
                    longitude, illegalArgumentException);
            return null;
        }

        // Handle case where no address was found.
        if (addresses == null || addresses.size() == 0) {
            Log.e(FILENAME, "No address found" + latitude + " " + longitude);
        } else {
            Address address = addresses.get(0);
            addressFragments = new ArrayList<String>();

            // Fetch the address lines using getAddressLine,
            // join them, and send them to the thread.
            for (int i = 0; i < address.getMaxAddressLineIndex(); i++) {
                addressFragments.add(address.getAddressLine(i));
            }
            Log.i(FILENAME, "Address found");
        }
        return addressFragments;
    }
}

