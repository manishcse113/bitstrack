package com.bitskeys.track.map;

import android.content.Context;
import android.hardware.Sensor;
import android.hardware.SensorEvent;
import android.hardware.SensorEventListener;
import android.hardware.SensorManager;
import android.hardware.TriggerEventListener;
import android.util.Log;

import com.bitskeys.track.gen.ChatXMPPService;

/**
 * Created by Manish on 6/11/2016.
 */
public class SenorEventListenerImpl implements SensorEventListener {

    public final String FILENAME = "GBC SenorEventListener";
    private static SensorManager mSensorManager;
    private static Sensor mSensor;
    private TriggerEventListener mTriggerEventListener;

    public Context context = null;

    LocationListenerImpl locationListenerInstance = null;
    private static int instanceCount =0;
    private static SenorEventListenerImpl myInstance = null;
    private SenorEventListenerImpl()
    {

    }

    public void InitContext(Context cntxt)
    {
        context = cntxt;
    }
    public void initSensorListner(LocationListenerImpl locationListenerInstance)
    {
        this.locationListenerInstance = locationListenerInstance;
    }

    public static SenorEventListenerImpl getInstance()
    {
        if (instanceCount==0)
        {
            instanceCount++;
            myInstance = new SenorEventListenerImpl();
        }
        return myInstance;
    }

    public void detectAndRegisterForSignificantMotion()
    {
        Log.d(FILENAME, "detectAndRegisterForSignificantMotion");
        mSensorManager = ChatXMPPService.getSensorManager();
        if (mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) != null) {
            mSensor = mSensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER);
            mSensorManager.registerListener(this, mSensor , SensorManager.SENSOR_DELAY_NORMAL);
        }

        else
        {
            Log.d(FILENAME, "sensor not found - locations will not be accurate");
            // Failure! No LINER ACCLEROMETER.
        }
    }

    public void unRegisterAccelerometer()
    {
        if (mSensorManager!=null)
        {
            mSensorManager.unregisterListener(this);
        }
    }

    private long lastUpdate = 0;
    private float last_x, last_y, last_z;
    private float x, y, z;
    private float speed;
    private long diffTime;
    private static final int SHAKE_THRESHOLD = 1;
    private static final int MIN_TIME_BW_UPDATES = 10000;//10 seconds

    //SensorEventListner interface
    @Override
    public final void onAccuracyChanged(Sensor sensor, int accuracy) {
        // Do something here if sensor accuracy changes.
    }

    @Override
    public void onSensorChanged(SensorEvent sensorEvent)
    {
        Sensor mySensor = sensorEvent.sensor;
        if (mySensor.getType() == Sensor.TYPE_ACCELEROMETER)
        {

            long curTime = System.currentTimeMillis();
            if ((curTime - lastUpdate) > MIN_TIME_BW_UPDATES)
            {
                x = sensorEvent.values[0];
                y = sensorEvent.values[1];
                z = sensorEvent.values[2];
                diffTime = (curTime - lastUpdate);
                lastUpdate = curTime;
                speed = Math.abs(x + y + z - last_x - last_y - last_z)/ diffTime * 10000;
                if (speed > SHAKE_THRESHOLD)
                {
                    unRegisterAccelerometer();
                    locationListenerInstance.registerForLocationUpdates();
                    locationListenerInstance.obtainMyLocation(context);
                    //Log.d(FILENAME, "Trying to autodetect the time for location dump");
                    //Now since GPS is on - we can unregister from accelrometer

                }
                last_x = x;
                last_y = y;
                last_z = z;
            }
        }
    }
}
