package com.bitskeys.track.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.RestAPI.OpenfireRestAPI;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.TimeClock;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.login.ChatConnection;
import com.bitskeys.track.users.ChatUsersListActivity;
import com.bitskeys.track.users.UserListLayout;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.Polyline;
import com.google.android.gms.maps.model.PolylineOptions;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Manish on 07-01-2016.
 */
public class TrackUserFragment extends com.google.android.gms.maps.SupportMapFragment {
    //private LatLng myPosition;
    private static String FILENAME = "GBC TrackUserFragment";

    private static LatLng lastPosition;
    GoogleMap mGoogleMap;
    List mMarkers;
    String mSelectedUserName;
    boolean mapLoaded = false;

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        mGoogleMap = getMap();
        Log.d(FILENAME, "onActivityCreated");
        builder = new LatLngBounds.Builder();
        mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                //set the flag for same:
                mapLoaded = true;
                //mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 30));
            }
        });
        //resetCamera();
    }

    /* Request updates at startup */
    @Override
    public void onResume() {
        super.onResume();
        Log.d(FILENAME, "onResume");
        refresh();
    }

    @Override
    public void onPause() {
        Log.d(FILENAME, "onPause");
        super.onPause();
    }

    List<UserLocation> users;//These are locations of same user

    public void sortUserObjList()
    {
        //first sort based on displayname
        Collections.sort(users, new Comparator<UserLocation>() {
            @Override
            public int compare(final UserLocation object1, final UserLocation object2) {
                return object1.getRecordedTime().compareTo(object2.getRecordedTime());
            }
        });
    }

    public void refresh() {
        Log.d(FILENAME, "refresh");
        mSelectedUserName = ChatUsersListActivity.getSelectedUserName();
        if (mSelectedUserName != null) {
            MyLocationManager.trackUser(getActivity(), mSelectedUserName, mSelectedUserName);//send dummy mobile number for now
        }
        //refreshMapLocations(); This will get called from updateUsersList in turn, so not calling from here.
    }

    public void updateUsersList(List<UserLocation> usersMapLocations) {
        Log.d(FILENAME, "updateUsersList");
        if (mGoogleMap != null) {
            mGoogleMap.clear();
        } else {
            return;
        }
        users = usersMapLocations;
        XMPPTCPConnection xmpptcpConnection = ChatConnection.getInstance().getConnection();
        if ((xmpptcpConnection != null) && (xmpptcpConnection.isConnected() == false)) {
            if (getActivity()!=null) {
                Toast.makeText(getActivity().getApplicationContext(), "Connection Failed...", Toast.LENGTH_LONG).show();
            }
        }
        refreshMapLocations();//This will automatically update userslist.
    }


    public void refreshMapLocations() {
        Log.d(FILENAME, "refreshMapLocations");
        refreshUsersLocations(users);
        //refreshMyLocation();
    }

    public void refreshUsersLocations(List<UserLocation> usersLocations) {
        Log.d(FILENAME, "refreshUsersLocations");
        users = usersLocations;
        if (users == null || users.size() == 0) {
            if (getActivity()!=null) {
                Toast.makeText(getActivity().getApplicationContext(), "User is not Trackable Or Check Connection", Toast.LENGTH_LONG).show();
            }
        } else {
            addMarkersToMap();

        }
    }
    LatLngBounds.Builder builder;
    LatLngBounds bounds;
    private void addMarkersToMap() {
        Log.d(FILENAME, "addMarkersToMap");
        mMarkers = new ArrayList();
        PolylineOptions rectOptions = new PolylineOptions();
        //MarkerOptions marker;
        //for (int i = 0; i < users.size(); i++) {


        builder = new LatLngBounds.Builder();
        //for (Marker marker : markers) {
       // }
        lastPosition = null;
        sortUserObjList();
        int i = 1;
        int totalPositions = users.size();
        //Also -plot my current location::
        addMyCurrentPosition();

        for (UserLocation user : users) {
            LatLng ll = new LatLng(((double) (user.getLatitude())) / ChatXMPPService.DOUBLETOLONG, ((double) (user.getLongitude())) / ChatXMPPService.DOUBLETOLONG);
            //BitmapDescriptor bitmapMarker;
            //bitmapMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);

            String recordedTime = user.getRecordedTime();
            Long timeL = Long.parseLong(recordedTime);
            //Date date = new Date(timeL);
            if (TimeClock.isItToday(timeL)) {
                recordedTime = TimeClock.getTimeStrFromEpochTime(timeL);
            } else {
                recordedTime = TimeClock.getEpocTimeLongToString(timeL);
            }

            String address= " ";
            if (user.getAddressLine3() != null)
            {
                address += user.getAddressLine3();
            }
            if (user.getAddressLine2()!=null)
            {
                address += " " + user.getAddressLine2();
            }

            //String address = " " + user.getAddressLine3()+ " " + user.getAddressLine2();

            //MarkerOptions mMarker = new MarkerOptions().position(ll).title(recordedTime + " " + ((double) (user.getLatitude())) / ChatXMPPService.DOUBLETOLONG + " " + ((double) (user.getLongitude())) / ChatXMPPService.DOUBLETOLONG);
            // Changing marker icon
            //mMarker.icon(bitmapMarker);
            //mMarkers.add(mGoogleMap.addMarker(mMarker));
            try
            {
                if (i<totalPositions)
                {
                    addCustomMarker(ll, recordedTime, i, address, user.getUserName(), R.drawable.map_icon_pink_3d_small, 0);
                }
                else
                {
                    //Current position - highlight differently
                    addCustomMarker(ll, recordedTime, i, address, user.getUserName(), R.drawable.map_icon_green_3d_small, 1);
                }
            }
            catch (Exception e)
            {
                Log.e(FILENAME, "addCustomMarker exception "+e.getMessage());
                e.printStackTrace();
            }
            i++;

            rectOptions.add(ll);
            rectOptions.color(Color.RED);
            //strokeColor(Color.RED);
            lastPosition = ll;
        }

        if (lastPosition != null) {
            Polyline polyline = mGoogleMap.addPolyline(rectOptions);
            if (i>1)
            {
                bounds = builder.build();
                if (mapLoaded == true) {
                    int padding = 50; // offset from edges of the map in pixels
                    CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                    //resetCamera();
                    mGoogleMap.moveCamera(cu);
                    mGoogleMap.animateCamera(cu);
                } else {
                    mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lastPosition, 15));
                    mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(12), 2000, null);

                }
            }

        }
        else
        {
            Toast.makeText(getActivity().getApplicationContext(), "No Locations for user", Toast.LENGTH_LONG).show();
        }
    }

    void addMyCurrentPosition()
    {
        String addressLine1 = "";
        String addressLine2 = "";
        String addressLine3 = "";//Double.toString((double)(latitude)/DOUBLETOLONG) + " " + Double.toString((double)(longitude)/DOUBLETOLONG);
        String address = "";
        try {
            ArrayList<String> addressFragments = LocationListenerImpl.getAddress(getActivity(), LocationListenerImpl.mMyOldLat, LocationListenerImpl.mMyOldLong);
            if (addressFragments != null) {

                for (int k = 0; k < addressFragments.size(); k++) {
                    if (k == 0)
                        addressLine1 = addressFragments.get(k);
                    else if (k == 1)
                        addressLine2 = addressFragments.get(k);
                    else if (k == 2)
                        addressLine3 = addressFragments.get(k);
                    else
                        break;
                }
            }
            if (addressLine3 != null)
            {
                address += addressLine3;
            }
            if (addressLine2!=null)
            {
                address += " " + addressLine2;
            }

        } catch (Exception e) {
            Log.e(FILENAME, "Exception in getting address");
            e.printStackTrace();
        }
        Long recordedTimeL = LocationListenerImpl.myLastRecordedTime;
        String recordedTime = null;
        if (TimeClock.isItToday(recordedTimeL)) {
            recordedTime = TimeClock.getTimeStrFromEpochTime(recordedTimeL);
        } else {
            recordedTime = TimeClock.getEpocTimeLongToString(recordedTimeL);
        }
        //LatLng ll = new LatLng(ChatXMPPService.mMyOldLat, ChatXMPPService.mMyOldLong);
        if (LocationListenerImpl.myPosition!=null) {
            addCustomMarker(LocationListenerImpl.myPosition, recordedTime, 0, address, ChatXMPPService.mUserName, R.drawable.map_icon_blue_3d_small, 1);
        }
    }


    void addCustomMarker(LatLng location, String recordedTime, int i, String address, String userName, int resourceId, float iconId)
    {
        String text = Integer.toString(i);
        BitmapDescriptor bitmapMarker;


        MarkerOptions mMarker = new MarkerOptions().position(location).title("(" + i + ") " + recordedTime + address);

        if (iconId == 0)
        {
            bitmapMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_ORANGE);
            mMarker.icon(bitmapMarker);//, text);
        }
        if (iconId == 1)
        {
            mMarker.icon(BitmapDescriptorFactory.fromBitmap(writeTextOnDrawable(userName, resourceId, text)));
        }

        mGoogleMap.addMarker(mMarker);
        builder.include(mMarker.getPosition());
        //new MarkerOptions().position(location).
          //      icon(BitmapDescriptorFactory.fromBitmap(writeTextOnDrawable(R.drawable.map_icon_pink_3d_small, text)).
            //                    title(recordedTime + " " + ((double) (user.getLatitude())) / ChatXMPPService.DOUBLETOLONG + " " + ((double) (user.getLongitude())) / ChatXMPPService.DOUBLETOLONG)
              //  ));
    }

    private Bitmap writeTextOnDrawable(String userName, int drawableId, String text) {

        if (getActivity() == null)
        {
            return null;
        }
        //Bitmap bm = uObj.getMyPhoto();

        Bitmap workingbm;
        Bitmap bm;
        if ( userName.equals(ChatXMPPService.mUserName))
        {
            workingbm = UserListLayout.getAvatarBitmapFromDB(OpenfireRestAPI.getUserName(), getActivity());

        }
        else
        {
            workingbm = Utility.getSavedBitmapForUser(userName, getActivity());
        }
        //bm;// = Utility.getSavedBitmapForUser(userName, getActivity());
        if (workingbm != null) {
            //avatarView.setImageBitmap(null);
            //avatarView.setImageBitmap(bm);
            Matrix matrix = new Matrix();
            matrix.postScale((float)0.3, (float)0.3);
            Bitmap resizedBitmap = Bitmap.createBitmap(workingbm, 0, 0, workingbm.getWidth(), workingbm.getHeight(), matrix, false);

            bm = resizedBitmap.copy(Bitmap.Config.ARGB_8888, true);
        }
        else
        {
            bm = BitmapFactory.decodeResource(getActivity().getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);;

            /*
            //         .copy(Bitmap.Config.ARGB_8888, true);
            avatarView.setImageResource(R.drawable.avatar);
            BitMapWorkerRequest.AddAndStartBitMapWorker(getResources(),
                    userName,
                    null,
                    avatarView.getWidth(),
                    avatarView.getHeight(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    R.id.avatar, getActivity(), null);
                    */
        }


       // Bitmap bm = BitmapFactory.decodeResource(getActivity().getResources(), drawableId)
       //         .copy(Bitmap.Config.ARGB_8888, true);

        Typeface tf = Typeface.create("Helvetica", Typeface.BOLD);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(Color.BLUE);
        paint.setTypeface(tf);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(convertToPixels(getActivity(), 11));

        Rect textRect = new Rect();
        paint.getTextBounds(text, 0, text.length(), textRect);

        Canvas canvas = new Canvas(bm);

        //If the text is bigger than the canvas , reduce the font size
        if(textRect.width() >= (canvas.getWidth() - 4))     //the padding on either sides is considered as 4, so as to appropriately fit in the text
            paint.setTextSize(convertToPixels(getActivity(), 7));        //Scaling needs to be used for different dpi's

        //Calculate the positions
        int xPos = (canvas.getWidth() / 2) - 2;     //-2 is for regulating the x position offset

        //"- ((paint.descent() + paint.ascent()) / 2)" is the distance from the baseline to the center.
        int yPos = (int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2)) ;

        canvas.drawText(text, xPos, yPos, paint);

        return  bm;
    }

    public static int convertToPixels(Context context, int nDP)
    {
        final float conversionScale = context.getResources().getDisplayMetrics().density;

        return (int) ((nDP * conversionScale) + 0.5f) ;

    }

    public void resetCamera() {

        //builder = new LatLngBounds.Builder();
        // Set boundaries ...

        CameraUpdate cu;
        try{
            bounds = builder.build();
            cu = CameraUpdateFactory.newLatLngBounds(bounds,10);
            // This line will cause the exception first times
            // when map is still not "inflated"
            mGoogleMap.moveCamera(cu);
            mGoogleMap.animateCamera(cu);
            System.out.println("Set with padding");
        } catch(IllegalStateException e) {
            e.printStackTrace();
            cu = CameraUpdateFactory.newLatLngBounds(bounds,400,400,0);
            mGoogleMap.moveCamera(cu);
            mGoogleMap.animateCamera(cu);
            System.out.println("Set with wh");
        }

        //do the rest...
    }

}
