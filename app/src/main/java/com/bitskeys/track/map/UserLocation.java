package com.bitskeys.track.map;

import com.bitskeys.track.gen.ChatXMPPService;

public class UserLocation {

    private String groupName;
    private static Integer id = 0;
    private String mobileNumber;
    private String userName;
    private String displayName;
    private long latitude; //Store latitude after multipling by 100
    private long longitude; //Store longitude after multipling by 100
    private String recordedTime;
    private String addressLine1;
    private String addressLine2;
    private String addressLine3;

    public UserLocation()
    {

    }

    public String getDisplayName() {
        return displayName;
    }

    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    public UserLocation( String groupName, String userName, String mobile, long latitude, long longitude, String recordedTime, String addressLine1, String addressLine2, String addressLine3)
    {
        setGroupName(ChatXMPPService.GROUPNAME);
        setId(id++);
        setUserName(userName);
        setMobileNumber(mobile);
        setLatitude(latitude);
        setLongitude(longitude);
        setDisplayName(ChatXMPPService.getDisplayName(userName));
        setRecordedTime(recordedTime);
        if (addressLine1 == null )
        {
            addressLine1= "";
        }
        if (addressLine2 == null )
        {
            addressLine2 = "";
        }
        if (addressLine3 == null )
        {
            addressLine3 = "";
        }
        setAddress(addressLine1, addressLine2,addressLine3);

        //Time now = new Time();
        //now.setToNow();
        //setRecordedTime(());
    }

    public Integer getId() {
        return id;
    }
    public void setId(Integer id) {
        this.id = id;
    }
    public String getMobileNumber() {
        return mobileNumber;
    }
    public void setMobileNumber(String mobileNumber) {
        this.mobileNumber = mobileNumber;
    }
    public void setGroupName(String groupName) {
        this.groupName = groupName;
    }
    public String geGroupName() {
        return groupName;
    }
    public String getUserName() {
        return userName;
    }
    public void setUserName(String userName) {
        this.userName = userName;
    }
    public long getLatitude() {
        return latitude;
    }
    public void setLatitude(long latitude) {
        this.latitude = latitude;
    }
    public long getLongitude() {
        return longitude;
    }
    public void setLongitude(long longitude) {
        this.longitude = longitude;
    }
    public String getRecordedTime() {
        return recordedTime;
    }
    public void setRecordedTime(String recordedTime) {
        this.recordedTime = recordedTime;
    }

    public String getAddressLine1()
    {
        if (addressLine1==null)
        {
            addressLine1 = " ";
        }
        else if (addressLine1.equalsIgnoreCase("null"))
        {
            addressLine1 = null;
        }
        return addressLine1;
    }
    public String getAddressLine2()
    {
        if (addressLine2==null)
        {
            addressLine2 = " ";
        }
        else if (addressLine2.equalsIgnoreCase("null"))
        {
            addressLine2 = null;
        }
        return addressLine2;
    }
    public String getAddressLine3()
    {
        if (addressLine3==null)
        {
            addressLine3 = " ";
        }
        else if (addressLine3.equalsIgnoreCase("null"))
        {
            addressLine3 = null;
        }
        return addressLine3;
    }
    public void setAddress(String addressLine1, String addressLine2, String addressLine3)
    {
        this.addressLine1 = addressLine1;
        this.addressLine2 = addressLine2;
        this.addressLine3 = addressLine3;
    }

}

