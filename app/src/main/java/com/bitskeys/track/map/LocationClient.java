package com.bitskeys.track.map;

//import android.location.Location;

/**
 *
 * @author amarnath
 *
 */

public class LocationClient {

    //static final String MAPURL = "http://ec2-54-201-247-62.us-west-2.compute.amazonaws.com:8080/mapservice/";
    //static final String MAPURL = ChatXMPPService.DOMAIN + ":8080/mapservice/";

    //public static final String BASE_URI = "http://localhost:8080/mapservice/map";

    /*
    public static final String BASE_URI = ChatXMPPService.DOMAIN + ":8080/mapservice/map";
    public static final String PATH_TRACK = "/track";
    public static final String PATH_CAPTURE = "/capture";
    public static final String PATH_LOCATEUSERS = "/locateUsers";
    //private static final String FILENAME = "GBC LocationClient";

    public static void trackUsers(String groupName)
    {
        JSONObject json = new JSONObject();
        try {
            json.put("groupName", groupName);
        } catch (JSONException e) {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return;
        }
        String jsonString = json.toString();
        Context context = this;
        String jsonPath = "http://" + BASE_URI + PATH_TRACK;
        invokeJsonRequestTrack(context, jsonPath, jsonString);
        return;
    }

    public static void trackUser(String groupName, String userName, String mobile)
    {
        JSONObject json = new JSONObject();
        try {
            json.put("groupName", groupName);
            json.put("userName", userName);
            json.put("mobile", mobile);
        } catch (JSONException e) {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return;
        }
        String jsonString = json.toString();
        Context context = this;
        String jsonPath = "http://" + BASE_URI + PATH_TRACK;
        invokeJsonRequestTrack(context, jsonPath, jsonString);
        return;
    }

    public static void trackUsers(String groupName, long latitude, long longitude, long radius) {
        JSONObject json = new JSONObject();
        try {
            json.put("groupName", groupName);
            //json.put("mobile", mobile);
            //json.put("userName", userName);
            json.put("latitude", latitude);
            json.put("longitude", longitude);
            json.put("radius", radius);

        } catch (JSONException e) {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return;
        }
        String jsonString = json.toString();
        Context context = this;
        String jsonPath = "http://" + BASE_URI + PATH_TRACK;
        invokeJsonRequestTrack(context, jsonPath, jsonString);
        return;
    }

    public static void captureLoc(String groupName, String userName, String mobile, long latitude, long longitude) {
        JSONObject json = new JSONObject();
        try {
            json.put("groupName", groupName);
            json.put("mobile", mobile);
            json.put("userName", userName);
            json.put("latitude", latitude);
            json.put("longitude", longitude);
            json.put("Date", new Date());

        } catch (JSONException e) {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return;
        }
        String jsonString = json.toString();
        Context context = this;
        String jsonPath = "http://" + BASE_URI + PATH_CAPTURE;
        invokeJsonRequestCapture(context, jsonPath, jsonString);
        return;
    }



    public static void invokeJsonRequestCapture(Context context, String jsonPath, String jsonString)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        client.removeHeader("Content-Type");
        client.addHeader("Content-Type", "application/json;charset=UTF-8");
        client.addHeader("Accept", "application/json");
        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        client.post(context,jsonPath,se,"application/json",new
        AsyncHttpResponseHandler() {
            @Override
            public void onSuccess ( int statusCode, org.apache.http.Header[] headers,
            byte[] responseBody){
                Log.d(FILENAME, "http onSuccess: " + responseBody);
                try {
                    // JSON Object
                    HashMap<String, String> hmap = new HashMap<String, String>();
                    for (org.apache.http.Header h : headers) {
                        hmap.put(h.getName(), h.getValue());
                        if (h.getName().equals("Content-Length")) {
                            if (h.getValue().equals("0")) {
                                Log.d(FILENAME, "successfully response");
                            }
                        }
                    }
                    JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        Log.d(FILENAME, "successfully status");
                    }
                    // Else display error message
                    else {
                        Log.d(FILENAME, "Error in capturing Captured");
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    e.printStackTrace();
                    Log.d(FILENAME, "Error in capturing Captured");
                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure ( int statusCode, org.apache.http.Header[] headers,
            byte[] responseBody, Throwable
            error){
                // Hide Progress Dialog
                //prgDialog.hide();
                Log.d(FILENAME, "http onFailure");
                // When Http response code is '404'
                if (statusCode == 404) {
                    Log.d(FILENAME, "Failed:404" + error.getMessage() + ":" + responseBody.toString());
                }
                // When Http response code is '404'
                else if (statusCode == 409) {
                    Log.d(FILENAME, "Failed:409" + error.getMessage() + ":" + responseBody.toString());
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Log.d(FILENAME, "Failed:500" + error.getMessage() + ":" + responseBody.toString());
                }
                // When Http response code other than 404, 500
                else {
                    Log.d(FILENAME, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]");
                }
            }
        }
        );
        return;
    }

    public static void invokeJsonRequestTrack(Context context, String jsonPath, String jsonString)
    {
        AsyncHttpClient client = new AsyncHttpClient();
        //Applicable in case a list is returned
        final List<UserLocation> path = new ArrayList();

        client.removeHeader("Content-Type");
        client.addHeader("Content-Type", "application/json;charset=UTF-8");
        client.addHeader("Accept", "application/json");
        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }

        client.post(context,jsonPath,se,"application/json",new
            AsyncHttpResponseHandler() {
                @Override
                public void onSuccess ( int statusCode, org.apache.http.Header[] headers,
                                        org.json.JSONArray responseArray)
                {
                    Log.d(FILENAME, "http onSuccess: " + responseArray);
                    HashMap<String, String> hmap = new HashMap<String, String>();
                    for (org.apache.http.Header h : headers) {
                        hmap.put(h.getName(), h.getValue());
                        if (h.getName().equals("Content-Length")) {
                            if (h.getValue().equals("0")) {
                                Log.d(FILENAME, "successfully response");
                            }
                        }
                    }
                    try {
                        for(int i = 0; i < responseArray.length(); i++) {
                            //JSONObject objects = responseArray.getJSONArray(i);
                            JSONObject object = responseArray.getJSONObject(i);
                            UserLocation loc = new UserLocation();
                            loc.setGroupName(object.getString("groupName"));
                            loc.setMobileNumber(object.getString("mobile"));
                            loc.setUserName(object.getString("userName"));
                            loc.setLatitude(object.getLong("latitude"));
                            loc.setLongitude(object.getLong("longitude"));
                            path.add(loc);
                            //loc.setRecordedTime(object.get("Date"));
                        }
                    }
                    catch (JSONException e)
                    {
                        Log.d(FILENAME, e.getMessage());
                    }
                }
                // When the response returned by REST has Http response code other than '200'
                @Override
                public void onFailure ( int statusCode, org.apache.http.Header[] headers,
                                        byte[] responseBody, Throwable  error)
                {
                    Log.d(FILENAME, "http onFailure");
                    // When Http response code is '404'
                    if (statusCode == 404) {
                        Log.d(FILENAME, "Failed:404" + error.getMessage() + ":" + responseBody.toString());
                    }
                    // When Http response code is '404'
                    else if (statusCode == 409) {
                        Log.d(FILENAME, "Failed:409" + error.getMessage() + ":" + responseBody.toString());
                    }
                    // When Http response code is '500'
                    else if (statusCode == 500) {
                        Log.d(FILENAME, "Failed:500" + error.getMessage() + ":" + responseBody.toString());
                    }
                    // When Http response code other than 404, 500
                    else {
                        Log.d(FILENAME, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]");
                    }
                }
            }
        );
        return;
    }
    */
}
