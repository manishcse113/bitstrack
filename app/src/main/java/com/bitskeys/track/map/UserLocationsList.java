package com.bitskeys.track.map;

import com.bitskeys.track.map.UserLocation;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

/**
 * Created by Manish on 27-12-2015.
 */
public class UserLocationsList implements Serializable {
    public static List<UserLocation> usersMapLocations = new ArrayList();
    public static HashMap<String,UserLocation> userLocationHashMap= new HashMap<String,UserLocation>();
}
