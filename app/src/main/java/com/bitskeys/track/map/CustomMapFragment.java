package com.bitskeys.track.map;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Typeface;
import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.FileSharing;
import com.bitskeys.track.gen.TimeClock;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.login.ChatConnection;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by Manish on 19-12-2015.
 */
public class CustomMapFragment extends com.google.android.gms.maps.SupportMapFragment
{
    //private LatLng myPosition;
    private static String FILENAME = "GBC CustomMapFragment";

    private static LatLng myPosition;
    private static LatLng lastUserPosition;
    GoogleMap mGoogleMap;
    List mMarkers;
    boolean mapLoaded = false;
    /*
    private static View view;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (container == null) {
            return null;
        }

        return inflater.inflate(R.layout.fragment_map, container, false);
        // We get the ListView component from the layout
        //ListView lv = (ListView) view.findViewById(R.id.listView);
        //Button buttonRefresh = (Button) getActivity().findViewById(R.id.buttonRefresh);
        //registerForContextMenu(lv);
        //return view;
    }
    */

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);

        mGoogleMap = getMap();
        Log.d(FILENAME, "onActivityCreated");
        //builder = new LatLngBounds.Builder();
        //resetCamera();
        if (mGoogleMap == null)
        {
            return;
        }
        mGoogleMap.setOnMapLoadedCallback(new GoogleMap.OnMapLoadedCallback() {
            @Override
            public void onMapLoaded() {
                //mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngBounds(bounds, 30));
                mapLoaded = true;
            }
        });
    }

    /* Request updates at startup */
    @Override
    public void onResume() {
        super.onResume();
        Log.d(FILENAME, "onResume");
        refresh();
    }

    /* Remove the locationlistener updates when Activity is paused */
    @Override
    public void onPause() {
        super.onPause();
        Log.d(FILENAME, "onPause");
    }
    List<UserLocation> users;

    //This list is updated from by ChatUsersListActivity as it receives message from
    // chatxmppservice - on receiving success of post request.
    public void updateUsersList(List<UserLocation> usersMapLocations)
    {
        Log.d(FILENAME, "updateUsersList");
        if (usersMapLocations!= null) {
            users = usersMapLocations;
        }
        else
        {
            if (getActivity()!=null) {
                //use the old dialgogs and print Toast
                Toast.makeText(getActivity().getApplicationContext(), "New information not retrieved...", Toast.LENGTH_LONG).show();
            }
        }

        XMPPTCPConnection xmppTcpConnection = ChatConnection.getInstance().getConnection();
        if (xmppTcpConnection!= null && xmppTcpConnection.isConnected() == false)
        {
            if(getActivity()!=null) {
                Toast.makeText(getActivity().getApplicationContext(), "Connection Failed...", Toast.LENGTH_LONG).show();
            }
        }
        refreshMapLocations();//This will automatically update userslist.
    }

    public void refresh()
    {
        Log.d(FILENAME, "refresh");
        MyLocationManager.obtainMyLocation(getActivity());
        MyLocationManager.getAllUsersLocations(getActivity(), ChatXMPPService.GROUPNAME);
        //refreshMapLocations(); This will get called from updateUsersList in turn, so not calling from here.
    }

    public void refreshMapLocations()
    {
        Log.d(FILENAME, "refreshMapLocations");
        if (mGoogleMap == null)
        {
            return;
        }
        mGoogleMap.clear();
        refreshUsersLocations(users);
        refreshMyLocation();
    }

    public void refreshUsersLocations(List<UserLocation> usersLocations) {
        Log.d(FILENAME, "refreshUsersLocations");
        users = usersLocations;
        if (users!=null) {
            addMarkersToMap();
        }
    }

    LatLngBounds bounds;
    public void refreshMyLocation()
    {
        if(mGoogleMap ==null)
            return;
        Log.d(FILENAME, "refreshMyLocation");
        // Enabling MyLocation Layer of google map
        mGoogleMap.setMyLocationEnabled(true);
        myPosition = MyLocationManager.getMyLocation();

        if (myPosition!=null) {
            // Use only same precision value as being used for all users:

                    //MyLocationManager.uploadLocation(getActivity(), ChatXMPPService.GROUPNAME, ChatXMPPService.mUserName, "9810414413",(long)((myPosition.latitude)*ChatXMPPService.DOUBLETOLONG),(long)((myPosition.longitude)*ChatXMPPService.DOUBLETOLONG) );
            //mGoogleMap.addMarker(new MarkerOptions().position(myPosition).title("Default Location"));

            String addressLine1 = "";
            String addressLine2 = "";
            String addressLine3 = myPosition.latitude + " " + myPosition.longitude;
            ArrayList<String>  addressFragments = LocationListenerImpl.getAddress(getActivity(), (long)(myPosition.latitude * ChatXMPPService.DOUBLETOLONG), (long)(myPosition.longitude*ChatXMPPService.DOUBLETOLONG));
            if (addressFragments != null)
            {
                for (int k = 0; k < addressFragments.size(); k++) {
                    if (k == 0)
                        addressLine1 = addressFragments.get(k);
                    else if (k == 1)
                        addressLine2 = addressFragments.get(k);
                    else if (k == 2)
                        addressLine3 = addressFragments.get(k);
                    else
                        break;
                }
            }


            MarkerOptions myMarker = new MarkerOptions().position(myPosition).title("You are HERE" + " " + addressLine3 + " " + addressLine2);
            mGoogleMap.addMarker(myMarker);
            builder.include(myMarker.getPosition());
            bounds = builder.build();
            //resetCamera();
            if (mapLoaded == true) {
                int padding = 50; // offset from edges of the map in pixels
                CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
                mGoogleMap.moveCamera(cu);
                mGoogleMap.animateCamera(cu);
            }
            else {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(myPosition, 15));
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(11), 2000, null);
            }

        }
        else if ( lastUserPosition != null) {
                mGoogleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(lastUserPosition, 15));
                mGoogleMap.animateCamera(CameraUpdateFactory.zoomTo(7), 2000, null);// If focus is on other users- we shall show zoom out view:
        }
        else
        {
            if (getActivity()!=null) {
                Toast.makeText(getActivity().getApplicationContext(), "No user trackable, Check Connection", Toast.LENGTH_LONG).show();
            }
        }
    }

    //Simulate crash

    String test = null;
    LatLngBounds.Builder builder;
    private void addMarkersToMap() {
        Log.d(FILENAME, "addMarkersToMap");
        mMarkers = new ArrayList();
        //MarkerOptions marker;
        //for (int i = 0; i < users.size(); i++) {

        lastUserPosition = null;
        long time = System.currentTimeMillis();
        String epochtime = Long.toString(time);
        Date todayDate = new Date(Long.parseLong(epochtime));
        builder = new LatLngBounds.Builder();

        int i =0;
        for(UserLocation user : users) {
            //users structure contains information as long (after multiplying by 100), so we are converting it again into double by deviding by 100
            //
            i++;
            if (user.getUserName().equals(ChatXMPPService.mUserName))
            {
                continue;
            }
            LatLng ll = new LatLng(((double)(user.getLatitude()))/ChatXMPPService.DOUBLETOLONG, ((double)(user.getLongitude()))/ChatXMPPService.DOUBLETOLONG);
            BitmapDescriptor bitmapMarker;
            String recordedTime = user.getRecordedTime();
            Long timeL = Long.parseLong(recordedTime);
            Date date = new Date(timeL);
            if (TimeClock.isItToday(timeL))
            {
                recordedTime = TimeClock.getTimeStrFromEpochTime(timeL);
                //bitmapMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_GREEN);
                lastUserPosition = ll;
            }
            else
            {
                recordedTime = TimeClock.getEpocTimeLongToString(timeL);
                        //bitmapMarker = BitmapDescriptorFactory.defaultMarker(BitmapDescriptorFactory.HUE_YELLOW);
            }

            String address = user.getAddressLine3()+ " " + user.getAddressLine2();
            try
            {
                addCustomMarker(ll, recordedTime, i, address, user.getUserName());
            }
            catch(Exception e)
            {
                Log.e(FILENAME, "error in addcustomMarker " +e.getMessage() );
                e.printStackTrace();
            }
            //MarkerOptions mMarker = new MarkerOptions().position(ll).title(user.getDisplayName() + " " + recordedTime + " " + ((double)(user.getLatitude()))/ChatXMPPService.DOUBLETOLONG + " " + ((double)(user.getLongitude()))/ ChatXMPPService.DOUBLETOLONG);
            //MarkerOptions mMarker = new MarkerOptions().position(ll).title(user.getDisplayName() + " " + recordedTime + " " + user.getAddressLine3()+ " " + user.getAddressLine2());
            // Changing marker icon
            //mMarker.icon(bitmapMarker);
            //if(mGoogleMap != null)
            //{
            //    mMarkers.add(mGoogleMap.addMarker(mMarker));
            //}
            //builder.include(mMarker.getPosition());
            if (lastUserPosition == null) //focus on any user initially and then settle on green one
            {
                lastUserPosition = ll;
            }

            //int length = test.length(); //crash
            //Log.d(FILENAME,"test carash " + length);

        }
    }

    double roundTwoDecimals(double d) {
        Log.d(FILENAME, "roundTwoDecimals");
        double dd = ((double)((long)( d * ChatXMPPService.DOUBLETOLONG)))/ChatXMPPService.DOUBLETOLONG;
        return dd;
        //DecimalFormat twoDForm = new DecimalFormat("#.##");
        //return Double.valueOf(twoDForm.format(d));
    }


    void addCustomMarker(LatLng location, String recordedTime, int i, String address, String userName)
    {
        String text = Integer.toString(i);

        MarkerOptions mMarker = new MarkerOptions().position(location).title("(" + i + ") " + recordedTime + address);
        mMarker.icon(BitmapDescriptorFactory.fromBitmap(writeTextOnDrawable(userName, R.drawable.map_icon_pink_3d_small, text)));
        if (mGoogleMap!=null && mMarker != null)
        {
            mGoogleMap.addMarker(mMarker);
        }
        if(mMarker != null)
            builder.include(mMarker.getPosition());
        //new MarkerOptions().position(location).
        //      icon(BitmapDescriptorFactory.fromBitmap(writeTextOnDrawable(R.drawable.map_icon_pink_3d_small, text)).
        //                    title(recordedTime + " " + ((double) (user.getLatitude())) / ChatXMPPService.DOUBLETOLONG + " " + ((double) (user.getLongitude())) / ChatXMPPService.DOUBLETOLONG)
        //  ));
    }

    private Bitmap writeTextOnDrawable(String userName, int drawableId, String text) {

        if (getActivity() == null)
        {
            return null;
        }
        //Bitmap bm = uObj.getMyPhoto();
        Bitmap workingbm = Utility.getSavedBitmapForUser(userName, getActivity());
        Bitmap bm;// = Utility.getSavedBitmapForUser(userName, getActivity());

        if (workingbm != null) {
            //avatarView.setImageBitmap(null);
            //avatarView.setImageBitmap(bm);
            Matrix matrix = new Matrix();
            matrix.postScale((float)0.3, (float)0.3);
            Bitmap resizedBitmap = Bitmap.createBitmap(workingbm, 0, 0, workingbm.getWidth(), workingbm.getHeight(), matrix, false);

            bm = resizedBitmap.copy(Bitmap.Config.ARGB_8888, true);
        }
        else
        {
            bm = BitmapFactory.decodeResource(getActivity().getResources(), drawableId).copy(Bitmap.Config.ARGB_8888, true);;

            /*
            //         .copy(Bitmap.Config.ARGB_8888, true);
            avatarView.setImageResource(R.drawable.avatar);
            BitMapWorkerRequest.AddAndStartBitMapWorker(getResources(),
                    userName,
                    null,
                    avatarView.getWidth(),
                    avatarView.getHeight(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    R.id.avatar, getActivity(), null);
                    */
        }


        // Bitmap bm = BitmapFactory.decodeResource(getActivity().getResources(), drawableId)
        //         .copy(Bitmap.Config.ARGB_8888, true);

        Typeface tf = Typeface.create("Helvetica", Typeface.BOLD);

        Paint paint = new Paint();
        paint.setStyle(Paint.Style.FILL_AND_STROKE);
        paint.setColor(Color.BLUE);
        paint.setTypeface(tf);
        paint.setTextAlign(Paint.Align.CENTER);
        paint.setTextSize(convertToPixels(getActivity(), 11));

        Rect textRect = new Rect();
        paint.getTextBounds(text, 0, text.length(), textRect);

        Canvas canvas = new Canvas(bm);

        //If the text is bigger than the canvas , reduce the font size
        if(textRect.width() >= (canvas.getWidth() - 4))     //the padding on either sides is considered as 4, so as to appropriately fit in the text
            paint.setTextSize(convertToPixels(getActivity(), 7));        //Scaling needs to be used for different dpi's

        //Calculate the positions
        int xPos = (canvas.getWidth() / 2) - 2;     //-2 is for regulating the x position offset

        //"- ((paint.descent() + paint.ascent()) / 2)" is the distance from the baseline to the center.
        int yPos = (int) ((canvas.getHeight() / 2) - ((paint.descent() + paint.ascent()) / 2)) ;

        canvas.drawText(text, xPos, yPos, paint);

        return  bm;
    }

    public static int convertToPixels(Context context, int nDP)
    {
        final float conversionScale = context.getResources().getDisplayMetrics().density;

        return (int) ((nDP * conversionScale) + 0.5f) ;

    }

    public void resetCamera() {

        //builder = new LatLngBounds.Builder();
        // Set boundaries ...

        CameraUpdate cu;
        try{
            bounds = builder.build();
            cu = CameraUpdateFactory.newLatLngBounds(bounds,10);
            // This line will cause the exception first times
            // when map is still not "inflated"
            mGoogleMap.moveCamera(cu);
            mGoogleMap.animateCamera(cu);
            System.out.println("Set with padding");
        } catch(IllegalStateException e) {
            e.printStackTrace();
            cu = CameraUpdateFactory.newLatLngBounds(bounds,400,400,0);
            mGoogleMap.moveCamera(cu);
            mGoogleMap.animateCamera(cu);
            System.out.println("Set with wh");
        }

        //do the rest...
    }
}