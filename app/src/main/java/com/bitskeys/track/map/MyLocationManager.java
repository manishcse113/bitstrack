package com.bitskeys.track.map;

import android.content.Context;
import android.location.Location;
import android.location.LocationManager;
import android.util.Log;

import com.bitskeys.track.gen.ChatXMPPService;
import com.google.android.gms.maps.model.LatLng;

/**
 * Created by Manish on 23-12-2015.
 */
public class MyLocationManager  {

    //LocationManager mLocationManage;
    android.location.Location mLocation;
    //private static LatLng myPosition = new LatLng(28.43, 77.05);
    boolean mIsGPSEnabled;
    boolean mIsNetworkEnabled;
    boolean mCanGetLocation;
    long MIN_TIME_BW_UPDATES = 30000; //300000; //5 minute, time is in miliseconds
    float MIN_DISTANCE_CHANGE_FOR_UPDATES = 50; //meters;

    private static String FILENAME = "GBC MyLocationManager";
    MyLocationManager()
    {
        Log.d(FILENAME, "MyLocationManager");
        //super("MyLocationManager Service");
        return;
    }

    public static LatLng getMyLocation()
    {
        Log.d(FILENAME, "getMyLocation");
        return LocationListenerImpl.getMyLocation();
    }


    public static Location obtainMyLocation(Context context)
    {
        Log.d(FILENAME, "obtainMyLocation");
        return LocationListenerImpl.getInstance().obtainMyLocation(context);
    }


    public static void trackUser(Context context, String userName, String mobileNum)
    {
        Log.d(FILENAME, "TrackUser group");
        //List<UserLocation> path =
        //userName and mobile are same for the user
        LocationListenerImpl.trackUser(context, ChatXMPPService.GROUPNAME, userName, userName);
    }

    public static void getAllUsersLocations(Context context,String groupName)
    {
        Log.d(FILENAME, "GetAllUsersLocations group");
        /*
        List<UserLocation> users = LocationClient.trackUsers(groupName);
        for(UserLocation user : users) {
            Log.d(FILENAME, " GrouNameName " + groupName);
        }
        return users;
        */
        LocationListenerImpl.trackUsers(context, groupName);
    }

    public static void GetAllUsersLocations(Context context, long latitude, long longitude, long radius)
    {
        Log.d(FILENAME, "GetAllUsersLocations");
        //Covert latitude and longidute to long from double
        LocationListenerImpl.trackUsers(context, ChatXMPPService.GROUPNAME, latitude, longitude, radius);
        /*
        List<UserLocation> users = LocationClient.trackUsers(ChatXMPPService.GROUPNAME, latitude, longitude, radius);
        for(UserLocation user : users) {
            Log.d(FILENAME, "Id " + user.getId() + " Name " + user.getUserName() + " Mobile " + user.getMobileNumber() + " lat " + user.getLatitude() + " longitude " + user.getLongitude());
        }
        return users;
        */
    }

    /*
    public static void uploadLocation(Context context, String groupName, String username, String mobile, long latitude, long longitude)
    {
        Log.d(FILENAME, "uploadLocation");
        //userName and Mobile are same in our case
        ChatXMPPService.captureLoc(context, groupName, username, username, latitude, longitude);
        return;
    }
    */
}
