package com.bitskeys.track.map;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.bitskeys.track.db.LocationRecord;
import com.bitskeys.track.gen.ChatXMPPService;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Manish on 04-02-2016.
 */
public class MapInfoProcessInBackground  extends AsyncTask<Void,Void,Void> {


    private static String FILENAME = "GBC MapInfoProcess";
    //private MapProcessCallback mMapProgCallback;
    private UserLocationsList usersMapLocations;
    private org.json.JSONArray responseArray;
    private Context context;

    public interface MapProcessCallback
    {
        //public void onUsersMapSuccess();
        //public void LoginFailure(int statusCode, String errMessage);
    }

    public MapInfoProcessInBackground(Context contxt, UserLocationsList usersMapLocations, org.json.JSONArray responseArray)
    {
        Log.d(FILENAME, "LoginBackground");

        //mMapProgCallback = mCallback;
        this.responseArray = responseArray;
        this.usersMapLocations = usersMapLocations;
        this.context = context;
    }

    @Override
    protected Void doInBackground(Void... arg)
    {

        try {
            usersMapLocations.usersMapLocations.clear();
            for (int i = 0; i < responseArray.length(); i++) {
                //JSONObject objects = responseArray.getJSONArray(i);
                JSONObject object = responseArray.getJSONObject(i);
                UserLocation loc = new UserLocation();
                String groupName = object.getString("groupName");
                loc.setGroupName(groupName);

                loc.setMobileNumber(object.getString("mobileNumber"));
                loc.setUserName(object.getString("userName"));

                //Workaround handling for crash here...
                String userName = object.getString("userName");
                String displayName = null;
                if (userName != null) {
                    displayName = ChatXMPPService.getDisplayName(userName);
                    if (displayName == null) {
                        continue;
                    }
                }

                loc.setDisplayName(displayName);
                long latitude = object.getLong("latitude");
                loc.setLatitude(latitude);
                long longitude = object.getLong("longitude");
                loc.setLongitude(longitude);

                String recordedTime = object.getString("recordedTime");
                loc.setRecordedTime(recordedTime);

                String addressLine1 = "";
                String addressLine2 = "";
                String addressLine3 = Double.toString((double) (latitude) / ChatXMPPService.DOUBLETOLONG) + " " + Double.toString((double) (longitude) / ChatXMPPService.DOUBLETOLONG);
                ArrayList<String> addressFragments = LocationListenerImpl.getAddress(context, latitude, longitude);
                if (addressFragments != null) {

                    for (int k = 0; k < addressFragments.size(); k++) {
                        if (k == 0)
                            addressLine1 = addressFragments.get(k);
                        else if (k == 1)
                            addressLine2 = addressFragments.get(k);
                        else if (k == 2)
                            addressLine3 = addressFragments.get(k);
                        else
                            break;
                    }
                }
                loc.setAddress(addressLine1, addressLine2, addressLine3);

                //loc.setAddress(object.getString("addressLine1"), object.getString("addressLine2"), object.getString("addressLine3"));
                LocationRecord.clearFromDB(context, userName);
                LocationRecord.storeLocationRecord(context, userName, groupName, displayName, latitude, longitude, recordedTime, addressLine1, addressLine2, addressLine3);
                usersMapLocations.usersMapLocations.add(loc);
                UserLocation tmp = null;
                if ((tmp = usersMapLocations.userLocationHashMap.get(object.getString("userName"))) != null) {
                    usersMapLocations.userLocationHashMap.remove(tmp);
                }
                usersMapLocations.userLocationHashMap.put(object.getString("userName"), loc);

                //SendNewLocationResults to useractivity--> mapfragment
            }

        } catch (JSONException e) {
            Log.d(FILENAME, e.getMessage());
        }

        return null;
    }

    @Override
    protected void onPreExecute() {
        super.onPreExecute();
        Log.d(FILENAME, "onPreExecute");
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void result) {

        ChatXMPPService.onUsersMapSuccess();
        /*
        if(mMapProgCallback != null)
        {
            mMapProgCallback.onUsersMapSuccess();
        }
        */
    }
}