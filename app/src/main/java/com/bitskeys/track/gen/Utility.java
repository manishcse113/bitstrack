package com.bitskeys.track.gen;

/**
 * Created by GUR11219 on 02-07-2015.
 */

import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Matrix;
import android.location.Address;
import android.location.Geocoder;
import android.net.Uri;
import android.os.Environment;
import android.text.TextUtils;
import android.util.Log;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Class which has Utility methods
 */
public class Utility {
    //Email Pattern
    private static final String EMAIL_PATTERN =
            "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@"
                    + "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";
    private static Pattern pattern;
    private static Matcher matcher;

    private final String FILENAME = "GBC: Utility#";

    /**
     * Validate Email with regular expression
     *
     * @param email
     * @return true for Valid Email and false for Invalid Email
     */
    public static boolean validate(String email) {
        pattern = Pattern.compile(EMAIL_PATTERN);
        matcher = pattern.matcher(email);
        return matcher.matches();

    }

    /**
     * Checks for Null String object
     *
     * @param txt
     * @return true for not null and false for null String object
     */
    public static boolean isNotNull(String txt) {
        return txt != null && txt.trim().length() > 0 ? true : false;
    }

    public static String getPath(Context context, Uri uri) throws URISyntaxException {
        if ("content".equalsIgnoreCase(uri.getScheme())) {
            String[] projection = { "_data" };
            Cursor cursor = null;

            try {
                cursor = context.getContentResolver().query(uri, projection, null, null, null);
                int column_index = cursor.getColumnIndexOrThrow("_data");
                if (cursor.moveToFirst()) {
                    return cursor.getString(column_index);
                }
            } catch (Exception e) {
                // Eat it
            }
        }
        else if ("file".equalsIgnoreCase(uri.getScheme())) {
            return uri.getPath();
        }

        return null;
    }

    public static void CopyStream(InputStream is, OutputStream os)
    {
        final int buffer_size=1024;
        try
        {
            byte[] bytes=new byte[buffer_size];
            for(;;)
            {
                //Read byte from input stream
                int count=is.read(bytes, 0, buffer_size);
                if(count==-1)
                    break;
                //Write byte from output stream
                os.write(bytes, 0, count);
            }
        }
        catch(Exception ex){}
    }

    public static String FormatStringToUserName(String source)
    {
        if(source == null || source.length() ==0 )
            return null;


        String temp = source;
        if(temp != null)
        {
            StringBuffer res = new StringBuffer();
            String[] strArr = temp.split(" ");
            boolean isSpacePending = false;
            for (String str : strArr) {
                if(isSpacePending)
                {
                    res.append(" ");
                    isSpacePending = false;
                }
                char[] stringArray = str.trim().toCharArray();
                if(stringArray != null)
                {
                    stringArray[0] = Character.toUpperCase(stringArray[0]);
                    str = new String(stringArray);
                    isSpacePending = true;
                    res.append(str);
                }

            }
            temp = res.toString();
        }
        return temp;
    }

    public static Bitmap codec(Bitmap src, Bitmap.CompressFormat format,int quality)
    {
        ByteArrayOutputStream os = new ByteArrayOutputStream();
        src.compress(format, quality, os);

        byte[] array = os.toByteArray();
        return BitmapFactory.decodeByteArray(array, 0, array.length);
    }

    public static int calculateInSampleSize(
            BitmapFactory.Options options, int reqWidth, int reqHeight) {
        // Raw height and width of image
        final int height = options.outHeight;
        final int width = options.outWidth;
        int inSampleSize = 1;

        if (height > reqHeight || width > reqWidth) {

            final int halfHeight = height / 2;
            final int halfWidth = width / 2;

            // Calculate the largest inSampleSize value that is a power of 2 and keeps both
            // height and width larger than the requested height and width.
            while ((halfHeight / inSampleSize) > reqHeight
                    && (halfWidth / inSampleSize) > reqWidth) {
                inSampleSize *= 2;
            }
        }

        return inSampleSize;
    }

    public static Bitmap decodeSampledBitmapFromResource(int reqWidth, int reqHeight,byte[] imageByte) {

        Bitmap resultBmp = null;
        try {
            // First decode with inJustDecodeBounds=true to check dimensions
            final BitmapFactory.Options options = new BitmapFactory.Options();
            options.inJustDecodeBounds = true;

            BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length, options);

            // Calculate inSampleSize
            options.inSampleSize = calculateInSampleSize(options, reqWidth, reqHeight);

            // Decode bitmap with inSampleSize set
            options.inJustDecodeBounds = false;
            resultBmp = BitmapFactory.decodeByteArray(imageByte, 0, imageByte.length, options);
        } catch ( Exception ex)
        {
            String msg = ex.getMessage();
            Log.d("DBG",msg);
        }

        return resultBmp;

    }

    public static Bitmap getResizedBitmap(Bitmap bm, int newHeight, int newWidth)
    {
        int width = bm.getWidth();
        int height = bm.getHeight();
        float scaleWidth = ((float) newWidth) / width;
        float scaleHeight = ((float) newHeight) / height;
        // create a matrix for the manipulation
        Matrix matrix = new Matrix();
        // resize the bit map
        matrix.postScale(scaleWidth, scaleHeight);
        // recreate the new Bitmap
        Bitmap resizedBitmap = Bitmap.createBitmap(bm, 0, 0, width, height, matrix, false);
        return resizedBitmap;
    }

    public static boolean saveBitmapFileOnDisk(Bitmap pictureBitmap,Context context,String userName)
    {
        boolean result = false;
        try {

            File mf = StorageUtils.getIndividualCacheDirectory(context,"profile");
            OutputStream fOut = null;
            String root = mf.toString();
            File myDir = new File(root);
            myDir.mkdirs();
            final File file = new File(root + "/" + userName + ".jpeg");
            String testPath = file.getAbsolutePath();
            fOut = new FileOutputStream(file);
            pictureBitmap.compress(Bitmap.CompressFormat.JPEG, 85, fOut);
            fOut.flush();
            fOut.close();
            result = true;
            //MediaStore.Images.Media.insertImage(getContent(), file.getAbsolutePath(), file.getName(), file.getName());

        }
        catch (Exception ex)
        {

        }

        return result;
    }

    public static Bitmap getSavedBitmapForUser(String userName,Context context)
    {
        Bitmap bitmap = null;
        try
        {
            File root = StorageUtils.getIndividualCacheDirectory(context,"profile");
            String filePath= root + "/" +userName + ".jpeg";
            File myFile = new File(filePath);
            if(myFile.exists())
            {
                bitmap = BitmapFactory.decodeFile(filePath);
            }

        }catch (Exception ex)
        {

        }

        return bitmap;
    }

    public static String getSavedImageURIForUser(String userName,Context context)
    {
        Bitmap bitmap = null;
        String path = null;
        try
        {
            File root = StorageUtils.getIndividualCacheDirectory(context,"profile");
            path = root.getAbsolutePath() + "/" + userName + ".jpeg";
            bitmap = BitmapFactory.decodeFile(root + "/" +userName + ".jpeg");
        }catch (Exception ex)
        {

        }

        return path;
    }


    String getDeviceId() {

        /*
        WifiManager m_wm = (WifiManager)getSystemService(Context.WIFI_SERVICE);
        String m_wlanMacAdd = m_wm.getConnectionInfo().getMacAddress();
        */
        return null;
    }


}