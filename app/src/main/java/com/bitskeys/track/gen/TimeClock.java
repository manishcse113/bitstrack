package com.bitskeys.track.gen;

/**
 * Created by Manish on 11-10-2015.
 */

import com.bitskeys.track.map.LocationListenerImpl;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Clock utility.
 */
public class TimeClock {
    private String dateTime;

    public static long getEpocTime()
    {
        long time = System.currentTimeMillis();
        return time;
    }

    public static int getHourofDay()
    {
        //long hour = getHour();
        //return hour;

        Calendar calendar = Calendar.getInstance();
        int hour = calendar.get(Calendar.HOUR_OF_DAY);
        return hour;
    }

    public static int getMinute()
    {


        Calendar calendar = Calendar.getInstance();
        int minute = calendar.get(Calendar.MINUTE);
        return minute;
    }

    public static int getSecond()
    {
        Calendar calendar = Calendar.getInstance();
        int second = calendar.get(Calendar.SECOND);
        return second;
    }

    public static int getWeekDay()
    {
        Calendar calendar = Calendar.getInstance();
        int day = calendar.get(Calendar.DAY_OF_WEEK);
        return day;
    }

    public static int getDayOfMonth()
    {
        Calendar calendar = Calendar.getInstance();
        int date = calendar.get(Calendar.DAY_OF_MONTH);
        return date;
    }

    public static int getMonth()
    {
        Calendar calendar = Calendar.getInstance();
        int month = calendar.get(Calendar.MONTH);
        return month+1;
    }

    public static int getyear()
    {
        Calendar calendar = Calendar.getInstance();
        int year = calendar.get(Calendar.YEAR);

        return year - 2000;
    }

    public static String get_date_month_year()
    {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String formattedDate = df.format(c.getTime());
        return formattedDate;

        /*

        Date date = new Date(getEpocTime());
        SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-YYYY"); // Set your date format
        //SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); // Set your date format
        String epochtimeStr = sdf.format(date);
        return epochtimeStr;
        */
    }
    public static String get_hr_min_sec()
    {
        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());

        SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
        String formattedDate = df.format(c.getTime());
        return formattedDate;

        /*
        Date date = new Date(getEpocTime());
        SimpleDateFormat sdf = new SimpleDateFormat("HH:mm:ss"); // Set your date format
        String epochtimeStr = sdf.format(date);
        return epochtimeStr;
        */
    }


    //These can go to settings::
    //public static mCanGetLocation
    public static int mStartTrackTime = 9;
    public static int mEndTrackTime = 18;
    public static int mStartTrackDay = 2;
    public static int mEndTrackDay = 6;

    public static void setTrackingPreference(boolean canGetLocation, int startTrackDay, int endTrackDay, int startTrackTime, int endTrackTime)
    {
        LocationListenerImpl.mCanGetLocation = canGetLocation;
        mStartTrackTime = startTrackTime;
        mEndTrackTime = endTrackTime;
        mStartTrackDay = startTrackDay;
        mEndTrackDay = endTrackDay;
    }

    public static boolean isTrackingTime()
    {
        int hour = getHourofDay();
        int day = getWeekDay();
        boolean isTrackingTime = false;
        if (day >= mStartTrackDay && day <= mEndTrackDay)
        {
            if (hour >= mStartTrackTime && hour <= mEndTrackTime)
            {
                isTrackingTime = true;
            }
        }
        return isTrackingTime;
    }

    public static long getPreviousStartDate(long days)
    {
        long currentEpocTime = getEpocTime();
        long numberOfmilisecondsForDays = days*86400000;     //days * 1day time = days * 1000*60*60*24 = days * 36*24*100000 = 86200000
        long returnStartDate =  currentEpocTime - numberOfmilisecondsForDays;
        return returnStartDate;
    }

    public static String getEpocTimeLongToString(long time) {
        //String epochtimeStr = Long.toString(time);
        //String epochtimeStr = Long.toString(time);

        Date date = new Date(time);
        SimpleDateFormat sdf = new SimpleDateFormat("MM-dd HH:mm:ss"); // Set your date format
        String epochtimeStr = sdf.format(date);
        return epochtimeStr;

        //Date date = new Date(Long.parseLong(epochtimeStr));
        //epochtimeStr = date.toString();
        //return epochtimeStr;
    }

    public static String getTimeStrFromEpochTime(Long epochTimeL)
    {
        String sTime;
        sTime = DateFormat.getTimeInstance().format(new Date(epochTimeL));
        return sTime;
    }
    public static boolean isItToday(Long epochTimeL)
    {
        Long past1dayTime = getPreviousStartDate(1);
        if (past1dayTime < epochTimeL)
        {
            return true;
        }
        else
        {
            return false;
        }
    }

//       Date date = new Date(Long.parseLong(epochtimeStr));
//        epochtimeStr = sTime.toString();
//            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy  HH:mm:ss");
//            String currentDateandTime = sdf.format(new Date(Long.parseLong(epochtimeStr)));
        //String sTime = DateFormat.getTimeInstance().format(new Date(Long.parseLong(epochtimeStr)));
       //return sTime;
   //}

    /*
    public static Long getEpocTimeStringToLong(String timStr)
    {
        Long epochtime = String.toLong(timStr);
        return epochtime;
    }*/

    /*
    public static String getDateTime() {

//        Time now = new Time();
//        now.setToNow();
//        String sTime = now.format("%d-%m-%y  %T");

        String sTime = DateFormat.getDateTimeInstance().format(new Date());

        return sTime;
    }
    */

    public void setDateTime(String dateTime) {
        this.dateTime = dateTime;
    }
    /**
     * Get current time in human-readable form.
     * @return current time as a string.
     */
//    public static String getNow() {
//        Time now = new Time();
//        now.setToNow();
//        String sTime = now.format("%Y_%m_%d %T");
//        return sTime;
//    }
    /**
     * Get current time in human-readable form without spaces and special characters.
     * The returned value may be used to compose a file name.
     * @return current time as a string.
     */
//    public static String getTimeStamp() {
//        Time now = new Time();
//        now.setToNow();
//        String sTime = now.format("%d-%m-%y-%H-%M-%S");
//        return sTime;
//    }

}
