package com.bitskeys.track.gen;

/**
 * Created by Preeti on 01-09-2015.
 */
public class CountryInfo {
    String CountryName;
    String CountryCode;
    String isoCodeTwoDigits;
    String isoCodeThreeDigits;

    public CountryInfo(String countryName, String countryCode, String isoCodeTwoDigits, String isoCodeThreeDigits) {
        CountryName = countryName;
        CountryCode = countryCode;
        this.isoCodeTwoDigits = isoCodeTwoDigits;
        this.isoCodeThreeDigits = isoCodeThreeDigits;
    }

    public String getCountryName() {
        return CountryName;
    }

    public void setCountryName(String countryName) {
        CountryName = countryName;
    }

    public String getCountryCode() {
        return CountryCode;
    }

    public void setCountryCode(String countryCode) {
        CountryCode = countryCode;
    }

    public String getIsoCodeTwoDigits() {
        return isoCodeTwoDigits;
    }

    public void setIsoCodeTwoDigits(String isoCodeTwoDigits) {
        this.isoCodeTwoDigits = isoCodeTwoDigits;
    }

    public String getIsoCodeThreeDigits() {
        return isoCodeThreeDigits;
    }

    public void setIsoCodeThreeDigits(String isoCodeThreeDigits) {
        this.isoCodeThreeDigits = isoCodeThreeDigits;
    }
}
