package com.bitskeys.track.gen;

import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

//import com.bitskeys.track.R;
import com.bitskeys.track.users.UserObject;

import org.w3c.dom.Text;

import java.lang.ref.WeakReference;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by swarnkarn on 1/24/2016.
 */
public class BitMapWorkerRequest {
    private static ArrayList<BitMapWorkerRequest> pendingRequests= new ArrayList<BitMapWorkerRequest>();

    private WeakReference<ImageView> imageViewReference;
    private WeakReference<Context> appContextReference;
    private WeakReference<TextView> firstNameTvReference = null;
    private WeakReference<TextView>lastNameTvReference = null;
    private WeakReference<TextView> emailIdTvReference = null;
    private WeakReference<TextView> orgNameTvReference = null;
    private WeakReference<ProgressBar> progessBarReference = null;
    String firstName = null;/* this will be setup by inside of bitmapworker */
    String lastName = null;/* this will be setup by inside of bitmapworker */
    String orgName = null;/* this will be setup by inside of bitmapworker */
    String emailId = null;/* this will be setup by inside of bitmapworker */
    int maxTry = 1;//one by default, this must be set while constructing BitMapWorkerRequest
    boolean tryOnlyFromDB=false;
    int userListIndex = -1;
    UserObject userObject = null;

    public UserObject getUserObject() {
        return userObject;
    }

    public void setUserObject(UserObject userObject) {
        this.userObject = userObject;
    }

    public boolean isTryOnlyFromDB() {
        return tryOnlyFromDB;
    }

    public void setTryOnlyFromDB(boolean tryOnlyFromDB) {
        this.tryOnlyFromDB = tryOnlyFromDB;
    }

    public int getMaxTry() {
        return maxTry;
    }

    public void setMaxTry(int maxTry) {
        this.maxTry = maxTry;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOrgName() {
        return orgName;
    }

    public void setOrgName(String orgName) {
        this.orgName = orgName;
    }

    public String getEmailId() {
        return emailId;
    }

    public void setEmailId(String emailId) {
        this.emailId = emailId;
    }

    private static Object BitMapWorkerCountLock = new Object();
    Resources resources;
    String remoteUserName = null;
    int bitmapLength = 100;
    int bitmapHeight = 100;
    static int maxBitWorkerCount = 100;
    static int bitMapWorkerCount =0;
    int resourceId = -1;

    public int getUserListIndex() {
        return userListIndex;
    }

    public void setUserListIndex(int userListIndex) {
        this.userListIndex = userListIndex;
    }

    public String getRemoteUserName() {
        return remoteUserName;
    }

    public void setRemoteUserName(String remoteUserName) {
        this.remoteUserName = remoteUserName;
    }

    public Resources getResources() {
        return resources;
    }

    public void setResources(Resources resources) {
        this.resources = resources;
    }

    public int getBitmapLength() {
        return bitmapLength;
    }

    public void setBitmapLength(int bitmapLength) {
        this.bitmapLength = bitmapLength;
    }

    public int getBitmapHeight() {
        return bitmapHeight;
    }

    public void setBitmapHeight(int bitmapHeight) {
        this.bitmapHeight = bitmapHeight;
    }

    public static int getMaxBitWorkerCount() {
        return maxBitWorkerCount;
    }

    public static void setMaxBitWorkerCount(int maxBitWorkerCount) {
        BitMapWorkerRequest.maxBitWorkerCount = maxBitWorkerCount;
    }

    public static int getBitMapWorkerCount() {
        return bitMapWorkerCount;
    }

    public static void setBitMapWorkerCount(int bitMapWorkerCount) {
        BitMapWorkerRequest.bitMapWorkerCount = bitMapWorkerCount;
    }

    public int getResourceId() {
        return resourceId;
    }

    public void setResourceId(int resourceId) {
        this.resourceId = resourceId;
    }


    public WeakReference<TextView> getFirstNameTvReference() {
        return firstNameTvReference;
    }

    public void setFirstNameTvReference(WeakReference<TextView> firstNameTvReference) {
        this.firstNameTvReference = firstNameTvReference;
    }

    public WeakReference<TextView> getLastNameTvReference() {
        return lastNameTvReference;
    }

    public void setLastNameTvReference(WeakReference<TextView> lastNameTvReference) {
        this.lastNameTvReference = lastNameTvReference;
    }

    public WeakReference<TextView> getEmailIdTvReference() {
        return emailIdTvReference;
    }

    public void setEmailIdTvReference(WeakReference<TextView> emailIdTvReference) {
        this.emailIdTvReference = emailIdTvReference;
    }

    public WeakReference<TextView> getOrgNameTvReference() {
        return orgNameTvReference;
    }

    public void setOrgNameTvReference(WeakReference<TextView> orgNameTvReference) {
        this.orgNameTvReference = orgNameTvReference;
    }

    public WeakReference<ProgressBar> getProgessBarReference() {
        return progessBarReference;
    }

    public void setProgessBarReference(WeakReference<ProgressBar> progessBarReference) {
        this.progessBarReference = progessBarReference;
    }

    public WeakReference<ImageView> getImageViewReference() {
        return imageViewReference;
    }

    public void setImageViewReference(WeakReference<ImageView> imageViewReference) {
        this.imageViewReference = imageViewReference;
    }

    public Context getAppContext()
    {
        Context ctxt = appContextReference.get();
        return ctxt;
    }

    public WeakReference<Context> getAppContextReference() {
        return appContextReference;
    }

    public void setAppContextReference(WeakReference<Context> appContextReference) {
        this.appContextReference = appContextReference;
    }


    BitMapWorkerRequest(String userName,
                        Resources res,
                        ImageView imageView,
                        int length,
                        int height,
                        int resId,
                        TextView firstNameTv,
                        TextView lastNameTv,
                        TextView emailIdTv,
                        TextView orgNameTv,
                        ProgressBar progressBar,
                        Context ctxt
    )
    {
        try {
            remoteUserName = userName;
            resources = res;
            resourceId = resId;
            bitmapLength = length;
            bitmapHeight = height;
            if(imageView != null) {
                imageViewReference = new WeakReference<ImageView>(imageView);
            }
            if(ctxt != null){
                appContextReference = new WeakReference<Context>(ctxt);
            }
            if(firstNameTv != null)
                firstNameTvReference = new WeakReference<TextView>(firstNameTv);
            if(lastNameTv != null)
                lastNameTvReference = new WeakReference<TextView>(lastNameTv);
            if(emailIdTv != null)
                emailIdTvReference = new WeakReference<TextView>(emailIdTv);
            if(orgNameTv != null)
                orgNameTvReference = new WeakReference<TextView>(orgNameTv);
            if(progressBar != null)
                progessBarReference = new WeakReference<ProgressBar>(progressBar);
        }
        catch (Exception ex)
        {
            String msg = ex.getMessage();
            Log.d("",msg);
        }
    }


    public static int AddAndStartBitMapWorkerWithUserObjListIndex(Resources res,
                                              String userName,
                                              ImageView imageView,
                                              int length,
                                              int height,
                                              TextView firstNameTv,
                                              TextView lastNameTv,
                                              TextView emailIdTv,
                                              TextView orgNameTv,
                                              ProgressBar progressBar,
                                              int resId,// index to be set in userObjList
                                              int index,
                                              Context ctxt)
    {
        synchronized (BitMapWorkerCountLock) {
        /* Check if request is valid or not */
            if (res == null || imageView == null || userName == null || userName.length() == 0)
                return -1;

            if (length <= 0){
                length = 100;
            }

            if (height <= 0)
                height = 100;

            BitMapWorkerRequest req = new BitMapWorkerRequest(userName,res,imageView,length,height,resId,firstNameTv,lastNameTv,
                    emailIdTv,orgNameTv,progressBar,ctxt);
            req.setUserListIndex(index);
            //BitMapWorkerRequest req = new  BitMapWorkerRequest(userName,res,imageView,resId,length,height);
            if(bitMapWorkerCount > maxBitWorkerCount) {
                pendingRequests.add(req);
            }
            else
            {
                bitMapWorkerCount++;
                //BitmapWorkerTask workerTask = new BitmapWorkerTask(imageView, res,length,height,userName);
                BitmapWorkerTask workerTask = new BitmapWorkerTask(req);
                workerTask.execute(resId);
            }

        }
        return 0;
    }
    public static int AddAndStartBitMapWorker(Resources res,
                                              String userName,
                                              ImageView imageView,
                                              int length,
                                              int height,
                                              TextView firstNameTv,
                                              TextView lastNameTv,
                                              TextView emailIdTv,
                                              TextView orgNameTv,
                                              ProgressBar progressBar,
                                              int resId,
                                              Context ctxt,
                                              UserObject uObj)
    {

        synchronized (BitMapWorkerCountLock) {
        /* Check if request is valid or not */

            if (res == null || userName == null || userName.length() == 0)
                return -1;


            if (length <= 0){
                length = 100;
            }

            if (height <= 0)
                height = 100;

            BitMapWorkerRequest req = new BitMapWorkerRequest(userName,res,imageView,length,height,resId,firstNameTv,lastNameTv,
                                                            emailIdTv,orgNameTv,progressBar,ctxt);

            if(uObj != null)
            {
                req.setUserObject(uObj);
            }
            //BitMapWorkerRequest req = new  BitMapWorkerRequest(userName,res,imageView,resId,length,height);
            if(bitMapWorkerCount > maxBitWorkerCount) {
                pendingRequests.add(req);
            }
            else
            {
                bitMapWorkerCount++;
                //BitmapWorkerTask workerTask = new BitmapWorkerTask(imageView, res,length,height,userName);
                BitmapWorkerTask workerTask = new BitmapWorkerTask(req);
                workerTask.execute(resId);
            }

        }
        return 0;
    }

    public static int AddAndStartBitMapWorkerOnlyDb(Resources res,
                                              String userName,
                                              ImageView imageView,
                                              int length,
                                              int height,
                                              TextView firstNameTv,
                                              TextView lastNameTv,
                                              TextView emailIdTv,
                                              TextView orgNameTv,
                                              ProgressBar progressBar,
                                              int resId,
                                              Context ctxt)
    {
        synchronized (BitMapWorkerCountLock) {
        /* Check if request is valid or not */
            if (res == null || imageView == null || userName == null || userName.length() == 0)
                return -1;

            if (length <= 0){
                length = 100;
            }

            if (height <= 0)
                height = 100;

            BitMapWorkerRequest req = new BitMapWorkerRequest(userName,res,imageView,length,height,resId,firstNameTv,lastNameTv,
                    emailIdTv,orgNameTv,progressBar,ctxt);
            req.setTryOnlyFromDB(true);

            bitMapWorkerCount++;
            //BitmapWorkerTask workerTask = new BitmapWorkerTask(imageView, res,length,height,userName);
            BitmapWorkerTask workerTask = new BitmapWorkerTask(req);
            workerTask.execute(resId);
        }
        return 0;
    }


    public static void decrementBitWorkerCount()
    {

            if(bitMapWorkerCount >0)
                bitMapWorkerCount--;

    }



}
