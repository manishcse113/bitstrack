package com.bitskeys.track.gen;

import android.content.Context;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.util.Log;

import com.bitskeys.track.chat.ChatBubbleActivity;
import com.bitskeys.track.db.ChatRecord;
import com.bitskeys.track.db.DataSource;
import com.bitskeys.track.login.ChatConnection;
import com.bitskeys.track.login.ConnectivityReceiver;
import com.bitskeys.track.users.ChatUsersListActivity;
import com.loopj.android.http.AsyncHttpClient;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.json.JSONObject;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.net.HttpURLConnection;
import java.net.URL;
import java.net.URLConnection;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manish on 4/8/2016.
 */
public class FileSharing {

    private static final String FILENAME = "GBC FileSharing";

    //private List<FileTransferListener> listeners = new ArrayList<FileTransferListener>();
    private static Context mContext;
    public static String BOUNDARY = "eriksboundry--";
    public static String TWOHYPENS = "--";
    public static String LINEEND = "\r\n";
    public static int MAXBUFFERSIZE = 1024 * 1024;

    public static String MEDIAPORT = "8888";
    public static String MEDIAUPLOADURL = "http://" + ChatXMPPService.DOMAINJID_BARE + ":" + MEDIAPORT;
    public static String MEDIARESOURCE = "/images/";

    int bytesRead, bytesAvailable, bufferSize;
    byte[] buffer;

    public static boolean uploadFile(String uploadUrl, String filename, String contentType) throws IOException {
        FileInputStream fileInputStream = new FileInputStream(new File(filename));
        URL url = new URL(uploadUrl);
        HttpURLConnection connection = (HttpURLConnection) url.openConnection();

        // Allow Inputs &amp; Outputs.
        connection.setDoInput(true);
        connection.setDoOutput(true);
        connection.setUseCaches(false);

        // Set HTTP method to POST.
        connection.setRequestMethod("POST");
        connection.setRequestProperty("Connection", "Keep-Alive");
        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + BOUNDARY);

        DataOutputStream outputStream = new DataOutputStream(connection.getOutputStream());
        outputStream.writeBytes(TWOHYPENS + BOUNDARY + LINEEND);
        outputStream.writeBytes("Content-Disposition: form-data; name=\"img\";filename=\"" + filename + "\"" + LINEEND);
        //outputStream.writeByte(System.getProperty("line.separator").getBytes());
        //contentType = gif
        outputStream.writeBytes("Content-type: " + contentType + LINEEND); // or whatever format you are sending.
        outputStream.writeBytes("Content-Transfer-Encoding: binary" + LINEEND);
        outputStream.writeBytes(LINEEND);

        int bytesAvailable = fileInputStream.available();
        int bufferSize = Math.min(bytesAvailable, MAXBUFFERSIZE);
        byte buffer[] = new byte[bufferSize];

        // Read file
        long bytesRead = fileInputStream.read(buffer, 0, bufferSize);

        while (bytesRead > 0) {
            outputStream.write(buffer, 0, bufferSize);
            bytesAvailable = fileInputStream.available();
            bufferSize = Math.min(bytesAvailable, MAXBUFFERSIZE);
            bytesRead = fileInputStream.read(buffer, 0, bufferSize);
        }
        outputStream.writeBytes(LINEEND);
        outputStream.writeBytes(TWOHYPENS + BOUNDARY + TWOHYPENS + LINEEND);

        fileInputStream.close();
        outputStream.flush();
        outputStream.close();

        // Responses from the server (code and message)
        int serverResponseCode = connection.getResponseCode();
        String serverResponseMessage = connection.getResponseMessage();

        System.out.println(serverResponseMessage);
        System.out.println(serverResponseCode);
        return true;
    }

    public static boolean SendFile(Context context, String filenameWithPath, String userAddress, String mimeType, Long timeStamp, boolean isResending, String receiptId)
    {
        XMPPTCPConnection connection = ChatConnection.getInstance().getConnection();
        mContext = context;
        boolean permanentFailure = false;
        if (filenameWithPath == null) {
            Log.d(FILENAME, "SendFile: Error: filename is null");
            permanentFailure = true;
        }
        else {
            File f = new File(filenameWithPath);
            if (!f.exists()) {
                Log.e(FILENAME, "File does not exists" + " filepath# " + filenameWithPath);
                permanentFailure = true;
            } else if (!f.canRead()) {
                Log.e(FILENAME, "File can't be read" + " filepath# " + filenameWithPath);
                permanentFailure = true;
            }
        }
        if (permanentFailure == true)
        {
            //update chat record for receiptID
            DataSource db = new DataSource(context);
            db.open();
            //Here update chat record for inserting receiptId based on timestamp:
            db.updateChatRecord(ChatXMPPService.mUserName, userAddress, timeStamp.toString(), ChatBubbleActivity.MESSAGE_FAILED_PERMANENT, timeStamp);
            db.close();
            //updated in db - now inform user as well.... by invalidating in case of false:
            sendUpdateChatRecord(userAddress, receiptId,ChatBubbleActivity.MESSAGE_FAILED_PERMANENT);
            return false;
        }
        if (connection == null)
        {
            //READY_TOSEND - will be sent automatically by monitoring thread - no further action required
            return false;
        }
        try
        {
            String fileUploadURL = FileSharing.MEDIAUPLOADURL + FileSharing.MEDIARESOURCE;
            FileSharing.uploadFile(fileUploadURL, filenameWithPath, mimeType);
            Log.d(FILENAME, "Sendfile Success");
            DataSource db = new DataSource(context);
            db.open();
            //Here update chat record for inserting receiptId based on timestamp:
            db.updateChatRecord(ChatXMPPService.mUserName, userAddress, timeStamp.toString(), ChatBubbleActivity.MESSAGE_FILE_UPLOADED, timeStamp);
            db.close();
            //updated in db - now inform user as well.... by invalidating in case of false:
            sendUpdateChatRecord(userAddress, receiptId, ChatBubbleActivity.MESSAGE_FILE_UPLOADED);
            //Here inform to peer about the sent message - do not store this temp message in db - as we have already stored image path in db

            //String mimeType = messageType;
            ///TODO: Need handling for groupchat, for now passed last parameter as false i.e. it is not a room.
            receiptId = ChatXMPPService.SendMessage(context, filenameWithPath, userAddress, ChatBubbleActivity.CHAT_MESSAGE_TYPE_IMAGE, timeStamp, false,false); //false means first time being sent
            if (receiptId != null)
            {
                //update chat record - receiptId based on timeStamp
                sendUpdateChatRecord(userAddress, receiptId, timeStamp);
            }

            return true;
        }
        catch (Exception e)
        {
            //READY_TOSEND - will be sent automatically by monitoring thread - no further action required
            Log.e(FILENAME, "Sending file exceptption" + " filepath# " + filenameWithPath + " filename# " + "Test" + e.getMessage());
        }
        return false;
    }

    public static void sendUpdateChatRecord(String remoteUserName, String receiptId, String receiptStatus)
    {
        //String timeStampStr = TimeClock.getEpocTimeLongToString(timeStamp);
        if (ChatXMPPService.mResponseMessengerChat != null)
        {//chat is open, so update activity if it is for currently open user:
            Log.d(FILENAME, "fileTransferRequest: update activity");
            //Send message to Bubble to update Adapter for the sent file
            Bundle data = new Bundle();
            data.putSerializable(ChatUsersListActivity.REMOTE_USER_NAME, remoteUserName);
            data.putSerializable(ChatXMPPService.RECEIPTID, receiptId);
            data.putSerializable(ChatXMPPService.RECEIPTSTATUS, receiptStatus);
            android.os.Message msg = android.os.Message.obtain(null, ChatXMPPService.INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYREPORT);
            msg.setData(data);
            try {
                ChatXMPPService.mResponseMessengerChat.send(msg);
            } catch (RemoteException e) {
                Log.d(FILENAME, "Exception in sending message to Chat Activity : " + e.getMessage());
                e.printStackTrace();

            }
        }
    }

    public static void sendUpdateChatRecord(String remoteUserName, String receiptId, long timeStamp)
    {
        //String timeStampStr = TimeClock.getEpocTimeLongToString(timeStamp);
        if (ChatXMPPService.mResponseMessengerChat != null)
        {//chat is open, so update activity if it is for currently open user:
            Log.d(FILENAME, "fileTransferRequest: update activity");
            //Send message to Bubble to update Adapter for the sent file
            Bundle data = new Bundle();
            data.putSerializable(ChatUsersListActivity.REMOTE_USER_NAME, remoteUserName);
            data.putSerializable(ChatXMPPService.RECEIPTID, receiptId);
            data.putSerializable(ChatXMPPService.TIMESTAMP, timeStamp);
            android.os.Message msg = android.os.Message.obtain(null, ChatXMPPService.INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYID);
            msg.setData(data);
            try
            {
                ChatXMPPService.mResponseMessengerChat.send(msg);
            }
            catch (RemoteException e) {
                Log.d(FILENAME, "Exception in sending message to Chat Activity : " + e.getMessage());
                e.printStackTrace();
            }
        }
    }

    /*
    public static void monitoringReliableDeliveryThread(final Context cntxt) {
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    boolean isNetworkConnected = ConnectivityReceiver.isConnected(cntxt);
                    if (isNetworkConnected) {
                        try {
                            Thread.sleep(30000L);
                            loadAndDeliverableMessagesFromDB(cntxt, ChatXMPPService.mUserName);
                        } catch (Exception e) {
                            Log.e(FILENAME, "Exception" + e.getMessage());
                        }
                    } else {
                        //sleep longer:
                        try {
                            Thread.sleep(60000L);
                        } catch (Exception e) {
                            Log.e(FILENAME, "Exception" + e.getMessage());
                        }
                    }
                }
            }
        }.start();
    }
    */

    //List<ChatRecord> ResendRequiredReqs = null;
    static List<ChatRecord> reqs = null;
    protected static void loadAndDeliverableMessagesFromDB(Context cntxt, String userName)
    {
        //First send text messages and then send files:
        if (reqs ==null)
        {
            DataSource db = new DataSource(cntxt);
            db.open();
            reqs = db.getChatRecords(userName, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            db.close();
        }
        if (reqs != null) {
            //Set resending as true;
            boolean resending = true;
            for (ChatRecord _req : reqs) {
                String mimeType = _req.getMimeType();
                String str = null;
                if (mimeType == null) {
                    continue;
                }
                //else if (mimeType.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT))
                {
                    //It is already loaded in adapter if adapter is not null
                    ///TODO: Need handling for groupchat, for now passed last parameter as false i.e. it is not a room.
                    String receiptId = ChatXMPPService.SendMessage(cntxt, _req.getMessageText(), _req.getRemoteUserName(), mimeType, _req.getCreationTimestamp(), true,false); //true means resending
                    // --? is update also required in userlist - for receiptID based on timeStamp?
                    if (receiptId != null) {
                        _req.setMessageReceiptStatus(ChatBubbleActivity.MESSAGE_ACK_AWAITED);
                        //update chat record - receiptId based on timeStamp
                        sendUpdateChatRecord(_req.getRemoteUserName(), receiptId, _req.getCreationTimestamp());
                    }
                }
                //Do not send any other thing
            }
            reqs.clear();
        }

        //Now process all the images in ready to send state::
        DataSource db = new DataSource(cntxt);
        db.open();
        reqs = db.getChatRecords(userName, ChatBubbleActivity.MESSAGE_READYTO_SEND);
        db.close();

        if (reqs != null)
        {
            boolean resending = true;
            for (ChatRecord _req : reqs)
            {
                String mimeType = _req.getMimeType();
                String str = null;
                if (mimeType == null) {
                    continue;
                }
                else
                {
                    SendFile(cntxt, _req.getMessageFileName(), _req.getRemoteUserName(), mimeType, _req.getCreationTimestamp(), resending, _req.getMessageReceiptId());
                }
            }
            reqs.clear();
            reqs = null;
        }
    }

    private static String SERVER_URL = MEDIAUPLOADURL + MEDIARESOURCE ; //"http://ec2-54-201-247-62.us-west-2.compute.amazonaws.com:8888/images/IMG_20150620_182044.jpg";

    public static String downloadFile(String fileName) {
        int count;
        String file_url = SERVER_URL;
        file_url+=fileName;
        final File file;
        try {
            URL url = new URL(file_url);
            URLConnection conection = url.openConnection();
            conection.connect();
            // this will be useful so that you can show a tipical 0-100% progress bar
            int lenghtOfFile = conection.getContentLength();
            // download the file
            InputStream input = new BufferedInputStream(url.openStream(), 8192);

            //String outputFileName = f_url.substring(0, f_url.indexOf("/"));
            String outputFileName = file_url.substring(file_url.indexOf("/images/")); //part1
            outputFileName = outputFileName.substring(8); //part2 - index of first char in filenam
            //outputFileName = outputFileName.substring(outputFileName.indexOf("/")); //part3
            //outputFileName = outputFileName.substring(outputFileName.indexOf("/")); //part4

            // Output stream

            File mf = Environment.getExternalStorageDirectory();
            String root = mf.toString();
            root = root + "/bitchat";
            File myDir = new File(root);
            myDir.mkdirs();
            outputFileName = root + "/" + outputFileName;
            file = new File(outputFileName);

            OutputStream output = new FileOutputStream( outputFileName);
            byte data[] = new byte[1024];
            long total = 0;
            while ((count = input.read(data)) != -1) {
                total += count;
                // publishing the progress....
                // After this onProgressUpdate will be called
                //publishProgress(""+(int)((total*100)/lenghtOfFile));
                // writing data to file
                output.write(data, 0, count);
            }
            // flushing output
            output.flush();
            // closing streams
            output.close();
            input.close();
        }
        catch (Exception e)
        {
            Log.e("Error: ", e.getMessage());
            return null;
        }
        return file.getAbsolutePath();
    }
}
