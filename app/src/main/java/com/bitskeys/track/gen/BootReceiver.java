package com.bitskeys.track.gen;

import android.app.ActivityManager;
import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.app.NotificationCompat;
import android.util.Log;

import com.bitskeys.track.chat.ChatBubbleActivity;
import com.bitskeys.track.db.DataSource;
import com.bitskeys.track.db.MyRecord;
import com.bitskeys.track.login.LoginActivity;

import java.util.List;

/**
 * Created by Manish on 31-01-2016.
 */
public class BootReceiver extends BroadcastReceiver
{
    private final String FILENAME = "GBC BootReceiver";
    /*
    @Override
    public void onReceive(Context context, Intent intent) {
        // TODO Auto-generated method stub
        Intent myIntent = new Intent(context, LoginActivity.class);
        myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(myIntent);
    }
    */

    private static Intent sMonitorServiceIntent = null;

    public static boolean toRunInForeground = true;

    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(FILENAME, "onReceiver");
        if (isServiceRunning(context, ChatXMPPService.class) == false) {
            String info = "starting ChatXmpp service";
            Log.d(FILENAME, info);

            toRunInForeground = false;
            Log.d(FILENAME, "onReceive - setting  - toRunInForeground = false");

            //Intent myIntent = new Intent(context, LoginActivity.class);
            //myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            //context.startActivity(myIntent);

            /*
            Intent myFakeIntent = new Intent(context, FakeService.class);
            myFakeIntent.setAction(ChatXMPPService.ACTION_BACKGROUND);
            context.startService(myFakeIntent);
            */

            //Intent myIntent = new Intent(context, );
            Intent myIntent = new Intent(context, ChatXMPPService.class);
            myIntent.setAction(ChatXMPPService.ACTION_BACKGROUND);
            context.startService(myIntent);

            //context.startForeground(myIntent);
            //startForegrouond()

            /*
            Intent myIntent = new Intent(context, LoginActivity.class);
            myIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(myIntent);
            */

            //sMonitorServiceIntent = new Intent(context, NetStatusMonitorService.class);
            //context.startService(sMonitorServiceIntent);
        }
    }

    // this method is very important
    private boolean isServiceRunning(Context context, Class<?> serviceClass) {
        ActivityManager am = (ActivityManager) context
                .getSystemService(Context.ACTIVITY_SERVICE);
        for (ActivityManager.RunningServiceInfo serviceInfo : am
                .getRunningServices(Integer.MAX_VALUE)) {
            String className1 = serviceInfo.service.getClassName();
            String className2 = serviceClass.getName();
            if (className1.equals(className2)) {
                return true;
            }
        }
        return false;
    }

    private AlarmManager alarmMgr;
    private PendingIntent alarmIntent;
    long ALARMREPEATTIME = 300000L;//reduced to 1 minute for testing

    public void SetAlarm(Context context)
    {
        alarmMgr =( AlarmManager)context.getSystemService(Context.ALARM_SERVICE);
        Intent i = new Intent("com.bitskeys.track.START_ALARM");
        //Intent i = new Intent("sss", BootReceiver.class);
        alarmIntent = PendingIntent.getBroadcast(context, 0, i, 0);
        alarmMgr.setInexactRepeating(AlarmManager.ELAPSED_REALTIME_WAKEUP, ALARMREPEATTIME, ALARMREPEATTIME, alarmIntent);
        //      AlarmManager.INTERVAL_HALF_HOUR,
          //      AlarmManager.INTERVAL_HALF_HOUR, alarmIntent);
        //am.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(), 1000 * 60 * 10, pi); // Millisec * Second * Minute
    }
}
