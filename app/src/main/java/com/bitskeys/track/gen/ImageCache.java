package com.bitskeys.track.gen;

import android.graphics.Bitmap;
import android.util.LruCache;

/**
 * Created by swarnkarn on 1/23/2016.
 */
public class ImageCache {
    private LruCache<String, Bitmap> mMemoryCache;
    private static Object mDiskCacheLock = new Object();
    private static ImageCache imgCache = null;
    private static int maxMemory = (int) (Runtime.getRuntime().maxMemory() / 1024);
    private static int cacheSize = maxMemory / 8;


    public static ImageCache getInstance()
    {
        synchronized (mDiskCacheLock){
            if(imgCache == null)
                imgCache = new ImageCache();
            imgCache.mMemoryCache = new LruCache<String, Bitmap>(cacheSize);
        }
        return imgCache;
    }

    public void addBitmapToMemoryCache(String userName, Bitmap bitmap) {
        if (getBitmapFromMemCache(userName) == null) {
            mMemoryCache.put(userName, bitmap);
        }
    }

    public Bitmap getBitmapFromMemCache(String userName) {
        return mMemoryCache.get(userName);
    }

}
