package com.bitskeys.track.gen;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.AsyncTask;
import android.util.Log;

import com.bitskeys.track.login.ConnectivityReceiver;
import com.bitskeys.track.map.LocationListenerImpl;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.io.StringWriter;
import java.io.UnsupportedEncodingException;
import java.io.Writer;
import java.lang.Thread.UncaughtExceptionHandler;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class CustomExceptionHandler implements UncaughtExceptionHandler {

    private final static String FILENAME = "GBC CustomException";

    private UncaughtExceptionHandler defaultUEH;
    public static String sendErrorLogsTo = "contactus@bitskeytechnologies.com" ;

    //Activity activity;
    static Context gContext = null;

    public CustomExceptionHandler(Context context) {
        this.defaultUEH = Thread.getDefaultUncaughtExceptionHandler();
        //this.activity = activity;
        gContext = context;
    }

    public void uncaughtException(Thread t, Throwable e) {

        final Writer result = new StringWriter();
        final PrintWriter printWriter = new PrintWriter(result);
        e.printStackTrace(printWriter);
        String stacktrace = result.toString();
        printWriter.close();
        String filename = "error" + System.nanoTime() + ".stacktrace";
        Log.e("Hi", "url != null");
        sendToServer(stacktrace, filename);

        StackTraceElement[] arr = e.getStackTrace();
        String report = e.toString() + "\n\n";
        report += "--------- Stack trace ---------\n\n";
        for (int i = 0; i < arr.length; i++) {
            report += "    " + arr[i].toString() + "\n";
        }
        report += "-------------------------------\n\n";
        report += "--------- Cause ---------\n\n";
        Throwable cause = e.getCause();
        if (cause != null) {
            report += cause.toString() + "\n\n";
            arr = cause.getStackTrace();
            for (int i = 0; i < arr.length; i++) {
                report += "    " + arr[i].toString() + "\n";
            }
        }
        report += "-------------------------------\n\n";
        defaultUEH.uncaughtException(t, e);
    }


    private void sendToServer(String stacktrace, String filename)
    {
        AsyncTaskClass async = new AsyncTaskClass(stacktrace, filename,
        getAppLable(gContext));
        async.execute("");
    }

    public String getAppLable(Context pContext)
    {
        PackageManager lPackageManager = pContext.getPackageManager();
        ApplicationInfo lApplicationInfo = null;
        try {
             lApplicationInfo = lPackageManager.getApplicationInfo(
            pContext.getApplicationInfo().packageName, 0);
        }
        catch (final NameNotFoundException e)
        {
        }
        return (String) (lApplicationInfo != null ? lPackageManager.getApplicationLabel(lApplicationInfo) : "Unknown");
     }

    public class AsyncTaskClass extends AsyncTask<String, String, InputStream>
    {
        InputStream is = null;
        String stacktrace;
        final String filename;
        String applicationName;

        AsyncTaskClass(final String stacktrace, final String filename, String applicationName)
        {
            this.applicationName = applicationName;
            this.stacktrace = stacktrace;
            this.filename = filename;
        }

        @Override
        protected InputStream doInBackground(String... params)
        {
            //HttpClient httpclient = new DefaultHttpClient();
            //HttpPost httppost = new HttpPost(
            //"http://suo-yang.com/books/sendErrorLog/sendErrorLogs.php?");

            Log.i(FILENAME, stacktrace);
            try {
                CustomExceptionHandler.uploadStackTrace(stacktrace,applicationName );
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return is;
        }

        @Override
        protected void onPostExecute(InputStream result)
        {
            super.onPostExecute(result);

            Log.e("Stream Data", getStringFromInputStream(is));
        }
    }


    // convert InputStream to String
    private static String getStringFromInputStream(InputStream is) {

        BufferedReader br = null;
        StringBuilder sb = new StringBuilder();

        String line;
        try {

        br = new BufferedReader(new InputStreamReader(is));
        while ((line = br.readLine()) != null) {
        sb.append(line);
        }

        } catch (IOException e) {
        e.printStackTrace();
        } finally {
        if (br != null) {
        try {
        br.close();
        } catch (IOException e) {
        e.printStackTrace();
        }
        }
        }
        return sb.toString();
    }

    public static List jsonMyCrashStacks = new ArrayList();
    public static void uploadStackTrace(String data, String subject) {
        Log.d(FILENAME, "captureLoc");
        JSONObject json = new JSONObject();
        try {
            json.put("userName", ChatXMPPService.mUserName);
            json.put("groupName", ChatXMPPService.GROUPNAME);
            long time = System.currentTimeMillis();
            String epochtime = Long.toString(time);
            json.put("recordedTime",epochtime);
            json.put("description", "TBD");
            json.put("data", data);
            json.put("subject", subject);
            //json.put("recordedTime", new Date());
            //json.put("recordedTime","1451217600000");
        } catch (JSONException e) {
            Log.e(FILENAME, e.getMessage());
            e.printStackTrace();
            return;
        }
        String jsonString = json.toString();
        jsonMyCrashStacks.add(jsonString);
        String jsonPath = "http://" + LocationListenerImpl.BASE_URI + LocationListenerImpl.PATH_SAVELOGS;
        if (ConnectivityReceiver.isConnected(gContext)) {
            for (Object jsonMessage : jsonMyCrashStacks) {
                //For every jsonString send it to server.
                invokeJsonRequestSaveLogs(jsonPath, (String) jsonMessage);
            }
            jsonMyCrashStacks.clear();
        }
        return;
    }

    public static void invokeJsonRequestSaveLogs(String jsonPath, String jsonString)
    {
        Log.d(FILENAME, "invokeJsonRequestCapture");
        AsyncHttpClient client = new AsyncHttpClient();
        client.removeHeader("Content-Type");
        client.addHeader("Content-Type", "application/json;charset=UTF-8");
        client.addHeader("Accept", "application/json");
        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        client.post(gContext,jsonPath,se,"application/json",new
            AsyncHttpResponseHandler() {
                @Override
                public void onSuccess ( int statusCode, org.apache.http.Header[] headers,
                                        byte[] responseBody){
                    Log.d(FILENAME, "http onSuccess: " + responseBody);
                    try {
                        // JSON Object
                        HashMap<String, String> hmap = new HashMap<String, String>();
                        for (org.apache.http.Header h : headers) {
                            hmap.put(h.getName(), h.getValue());
                            if (h.getName().equals("Content-Length")) {
                                if (h.getValue().equals("0")) {
                                    Log.d(FILENAME, "successfully response");
                                }
                            }
                        }
                        JSONObject obj = new JSONObject(responseBody.toString());
                        // When the JSON response has status boolean value assigned with true
                        if (obj.getBoolean("status")) {
                            Log.d(FILENAME, "successfully status");
                        }
                        // Else display error message
                        else {
                            Log.d(FILENAME, "Error in capturing Captured");
                        }
                    } catch (JSONException e) {
                        // TODO Auto-generated catch block
                        e.printStackTrace();
                        Log.d(FILENAME, "Error in capturing Captured");
                    }
                }
                // When the response returned by REST has Http response code other than '200'
                @Override
                public void onFailure ( int statusCode, org.apache.http.Header[] headers,
                                        byte[] responseBody, Throwable
                                                error){
                    // Hide Progress Dialog
                    //prgDialog.hide();
                    Log.d(FILENAME, "http onFailure");
                    // When Http response code is '404'
                    if (statusCode == 404) {
                        Log.d(FILENAME, "Failed:404" + error.getMessage() + ":" + responseBody.toString());
                    }
                    // When Http response code is '404'
                    else if (statusCode == 409) {
                        Log.d(FILENAME, "Failed:409" + error.getMessage() + ":" + responseBody.toString());
                    }
                    // When Http response code is '500'
                    else if (statusCode == 500) {
                        Log.d(FILENAME, "Failed:500" + error.getMessage() + ":" + responseBody.toString());
                    }
                    // When Http response code other than 404, 500
                    else {
                        Log.d(FILENAME, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]");
                    }
                }
            }
        );
        return;
    }
}

