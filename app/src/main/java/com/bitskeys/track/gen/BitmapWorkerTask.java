package com.bitskeys.track.gen;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.AsyncTask;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;

import com.bitskeys.track.profile.VCardWrappers;
import com.bitskeys.track.db.DataSource;
import com.bitskeys.track.db.ProfileImageRecord;
import com.bitskeys.track.login.ChatConnection;
import com.bitskeys.track.users.UserObject;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;

import java.util.List;

/**
 * Created by swarnkarn on 1/23/2016.
 */
public class BitmapWorkerTask extends AsyncTask<Integer, Void, Bitmap> {

    BitMapWorkerRequest bitMapWorkerRequest = null;
    public final static String FILENAME = "GBC BitmapWorkerTask";

    public BitmapWorkerTask(BitMapWorkerRequest req)
    {
        bitMapWorkerRequest = req;
    }


    boolean saveProfileInfoToDB(String firstName,String lastName,String emailId,String orgName)
    {
        boolean result = false;
        if( bitMapWorkerRequest.getAppContextReference().get() != null)
        {
            ProfileImageRecord.storeProfileImageRecord(bitMapWorkerRequest.getAppContextReference().get(),
                    bitMapWorkerRequest.getRemoteUserName(),firstName,lastName,
                    emailId, orgName,null);
            result = true;
        }
        return result;
    }

    boolean saveAvatarBitmapToDB(byte[] imageBytes,String firstName,String lastName,String emailId,String orgName)
    {
        boolean result = false;
        if( bitMapWorkerRequest.getAppContextReference().get() != null && imageBytes != null)
        {
            ProfileImageRecord.storeProfileImageRecord(bitMapWorkerRequest.getAppContextReference().get(),
                    bitMapWorkerRequest.getRemoteUserName(),firstName,lastName,
                    emailId, orgName,imageBytes);
            result = true;
        }
        return result;
    }

    boolean getProfileInfoFromDB()
    {
        Bitmap bitmap = null;
        boolean result = false;
        byte[] imageBytes = null;
        if(bitMapWorkerRequest.getAppContextReference() != null && bitMapWorkerRequest.getAppContextReference().get() != null)
        {
            Context ctxt = bitMapWorkerRequest.getAppContextReference().get();
            if(ctxt != null)
            {
                DataSource db = new DataSource(bitMapWorkerRequest.getAppContextReference().get());

                try
                {
                    db.open();
                    List<ProfileImageRecord> list = db.getProfileImageRecords(bitMapWorkerRequest.getRemoteUserName());
                    for (ProfileImageRecord _req : list) {
                        imageBytes = _req.getImageBytes();
                        bitMapWorkerRequest.setFirstName(_req.getFirstName());
                        bitMapWorkerRequest.setLastName(_req.getLastName());
                        bitMapWorkerRequest.setEmailId(_req.getEmailId());
                        bitMapWorkerRequest.setOrgName(_req.getOrgName());
                        result = true;
                        break;
                    }
                    db.close();
                }
                catch(Exception e)
                {
                    db.close();
                    Log.d(FILENAME, "error in opening in db");
                    e.printStackTrace();
                }
            }


        }



        if(imageBytes != null && imageBytes.length >0)
        {
            bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        }
        return result;
    }

    Bitmap getAvatarBitmapFromDB()
    {
        Bitmap bitmap = null;
        byte[] imageBytes = null;
        if(bitMapWorkerRequest.getAppContextReference() != null && bitMapWorkerRequest.getAppContextReference().get() != null)
        {
            DataSource db = new DataSource(bitMapWorkerRequest.getAppContextReference().get());
            db.open();
            List<ProfileImageRecord> list = db.getProfileImageRecords(bitMapWorkerRequest.getRemoteUserName());
            for(ProfileImageRecord _req : list) {
                imageBytes = _req.getImageBytes();
                bitMapWorkerRequest.setFirstName(_req.getFirstName());
                bitMapWorkerRequest.setLastName(_req.getLastName());
                bitMapWorkerRequest.setEmailId(_req.getEmailId());
                bitMapWorkerRequest.setOrgName(_req.getOrgName());
                break;
            }
            db.close();
        }



        if(imageBytes != null && imageBytes.length >0)
        {
            bitmap = BitmapFactory.decodeByteArray(imageBytes, 0, imageBytes.length);
        }
        return bitmap;
    }

    /* This will process all fields of vCard. In case it contains valid Bitmap,
       it will return the Bitmap, else null.
       In any case of return values, none is error condition
     */
    Bitmap processVCardRecieved(VCard vCard)
    {
        if(vCard == null)
            return null;

        Bitmap bitmap = null;

        byte[] imageBytes = null;
        imageBytes = vCard.getAvatar();

        ImageCache imgCache = ImageCache.getInstance();

        if(imageBytes != null && imageBytes.length >0)
        {
            bitmap = Utility.decodeSampledBitmapFromResource(
                    bitMapWorkerRequest.getBitmapLength(),
                    bitMapWorkerRequest.getBitmapHeight(),
                    imageBytes);

//            if (bitmap != null && bitmap.getByteCount() > 0)
//                imgCache.addBitmapToMemoryCache(bitMapWorkerRequest.getRemoteUserName(), bitmap);
        }


        bitMapWorkerRequest.setFirstName(vCard.getField(VCardWrappers.VCardFirstNameKey));
        if(bitMapWorkerRequest.getFirstName() == null)
        {
            bitMapWorkerRequest.setFirstName(vCard.getFirstName());
        }
        if(bitMapWorkerRequest.getFirstName() == null)
        {
            bitMapWorkerRequest.setFirstName("N.A");
        }

        bitMapWorkerRequest.setLastName(vCard.getField(VCardWrappers.VCardLastNameKey));
        if(bitMapWorkerRequest.getLastName() == null)
        {
            bitMapWorkerRequest.setLastName(vCard.getLastName());
        }

        if(bitMapWorkerRequest.getLastName() == null)
            bitMapWorkerRequest.setLastName("N.A");

        bitMapWorkerRequest.setEmailId(vCard.getField(VCardWrappers.VCardEmailKey));
        if(bitMapWorkerRequest.getEmailId() == null)
        {
            bitMapWorkerRequest.setEmailId(vCard.getEmailWork());
        }
        if(bitMapWorkerRequest.getEmailId() == null)
        {
            bitMapWorkerRequest.setEmailId("N.A");
        }

        bitMapWorkerRequest.setOrgName(vCard.getField(VCardWrappers.organizatioNameKey));
        if(bitMapWorkerRequest.getOrgName() == null)
        {
            bitMapWorkerRequest.setOrgName(vCard.getOrganization());
        }
        if(bitMapWorkerRequest.getOrgName() == null)
            bitMapWorkerRequest.setOrgName("N.A");

        if( bitMapWorkerRequest.getAppContextReference().get() != null)
        {
            //saveAvatarBitmapToDB(imageBytes,bitMapWorkerRequest.getFirstName(),bitMapWorkerRequest.getLastName(),bitMapWorkerRequest.getEmailId(),bitMapWorkerRequest.getOrgName());
            saveProfileInfoToDB(bitMapWorkerRequest.getFirstName(),bitMapWorkerRequest.getLastName(),bitMapWorkerRequest.getEmailId(),bitMapWorkerRequest.getOrgName());
        }

        return bitmap;

    }
    // Decode image in background.
    @Override
    protected Bitmap doInBackground(Integer... params)
    {
        Bitmap bitmap = null;
        Context ctxt = ( bitMapWorkerRequest.getAppContextReference()!= null )?bitMapWorkerRequest.getAppContextReference().get():null;

        /* Check for DB and return even if no entry exists */
        if(bitMapWorkerRequest.isTryOnlyFromDB())
        {
            return getAvatarBitmapFromDB();
        }

        /* Get profile Information from DB */
        ;

        /* Get Bitmap from Disk Cache.If both profile information is available in DB
        & disk cahce contains the bitmap, return from here only */
        if(getProfileInfoFromDB())
        {
            bitmap = Utility.getSavedBitmapForUser(bitMapWorkerRequest.getRemoteUserName(),ctxt);
            if(bitmap != null)
            {
                return bitmap;
            }
        }



//        /* Check if available in memory cache */
//        ImageCache imgCache = ImageCache.getInstance();
//        bitmap = imgCache.getBitmapFromMemCache(bitMapWorkerRequest.getRemoteUserName());
//        if(bitmap != null && bitmap.getByteCount() >0)
//            return bitmap;


        /* First load the VCard & get Image Bytes */
        XMPPTCPConnection mConnection = ChatConnection.getInstance().getConnection();
        VCard vCard = null;
        boolean isConnectionAvailable = false;
        if(mConnection != null && mConnection.isConnected())
        {
            int maxTry = bitMapWorkerRequest.getMaxTry();
            for(int i=0;i<maxTry;i++)
            {
                vCard = VCardWrappers.loadUserVCard(ChatConnection.getInstance().getConnection(), bitMapWorkerRequest.getRemoteUserName());
                if(vCard != null)
                    break;
            }
            isConnectionAvailable = true;

        }



        /* If connection is available & vCard is successfully fetched */
        Bitmap vCardBitmap = null;
        if( vCard != null)
        {
            vCardBitmap = processVCardRecieved(vCard);
        }

        /* Check if bitmap retrieved from diskCache was null, then take bitMap fetched from Openfire*/
        if(bitmap == null)
        {
            bitmap = vCardBitmap;
        }
        return  bitmap;
    }

    // Once complete, see if ImageView is still around and set bitmap.
    @Override
    protected void onPostExecute(Bitmap bitmap)
    {
        Context ctxt = (bitMapWorkerRequest.getAppContextReference() != null)? bitMapWorkerRequest.getAppContextReference().get():null;

        if(bitmap != null && ctxt != null)
        {
            if(Utility.getSavedBitmapForUser(bitMapWorkerRequest.getRemoteUserName(),ctxt) == null)
            {
                Utility.saveBitmapFileOnDisk(bitmap, ctxt, bitMapWorkerRequest.getRemoteUserName());
                UserObject uObj = bitMapWorkerRequest.getUserObject();
                if(uObj != null){
                    uObj.setMyPhoto(bitmap);
                }

            }
            else {
                //TODO: renew image if required
            }
        }

        if (bitMapWorkerRequest.getImageViewReference() != null && bitmap != null) {
            final ImageView imageView = bitMapWorkerRequest.getImageViewReference().get();
            if (imageView != null) {
                imageView.setBackgroundColor(Color.TRANSPARENT);
                imageView.setBackground(null);
                imageView.setImageBitmap(bitmap);
            }
        }

        if(bitMapWorkerRequest.getFirstNameTvReference() != null && bitMapWorkerRequest.getFirstNameTvReference().get() != null)
        {
            bitMapWorkerRequest.getFirstNameTvReference().get().setText(bitMapWorkerRequest.getFirstName());
        }

        if(bitMapWorkerRequest.getLastNameTvReference() != null && bitMapWorkerRequest.getLastNameTvReference().get() != null)
        {
            bitMapWorkerRequest.getLastNameTvReference().get().setText(bitMapWorkerRequest.getLastName());
        }

        if(bitMapWorkerRequest.getEmailIdTvReference() != null && bitMapWorkerRequest.getEmailIdTvReference().get() != null)
        {
            bitMapWorkerRequest.getEmailIdTvReference().get().setText(bitMapWorkerRequest.getEmailId());
        }

        if(bitMapWorkerRequest.getOrgNameTvReference() != null && bitMapWorkerRequest.getOrgNameTvReference().get() != null)
        {
            bitMapWorkerRequest.getOrgNameTvReference().get().setText(bitMapWorkerRequest.getOrgName());
        }

        if(bitMapWorkerRequest.getProgessBarReference() != null && bitMapWorkerRequest.getProgessBarReference().get() != null)
        {
            bitMapWorkerRequest.getProgessBarReference().get().setVisibility(View.GONE);
        }

        /* Free count for active BitMapWorkers*/
        BitMapWorkerRequest.decrementBitWorkerCount();
    }

}