package com.bitskeys.track.gen;

import android.content.Context;
import android.os.Bundle;
import android.os.Environment;
import android.os.RemoteException;
import android.util.Log;

import com.bitskeys.track.chat.ChatBubbleActivity;
import com.bitskeys.track.db.ChatRecord;
import com.bitskeys.track.db.DataSource;
import com.bitskeys.track.login.ChatConnection;
import com.bitskeys.track.login.ConnectivityReceiver;
import com.bitskeys.track.users.ChatUsersListActivity;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.XMPPConnection;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.disco.ServiceDiscoveryManager;
import org.jivesoftware.smackx.filetransfer.FileTransferListener;
import org.jivesoftware.smackx.filetransfer.FileTransferManager;
import org.jivesoftware.smackx.filetransfer.FileTransferRequest;
import org.jivesoftware.smackx.filetransfer.IncomingFileTransfer;
import org.jivesoftware.smackx.filetransfer.OutgoingFileTransfer;

import java.io.File;
import java.net.FileNameMap;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Manish on 07-08-2015.
 */

public class FileTransferWrapperObs {

    private static String FILENAME = "GBC FileTransferWrapper#";
    private List<FileTransferListener> listeners = new ArrayList<FileTransferListener>();
    private static Context context;

    public static boolean SendFile(Context context, String filenameWithPath, String userAddress, String mimeType, Long timeStamp, boolean isResending, String receiptId) {

        XMPPTCPConnection connection = ChatConnection.getInstance().getConnection();
        if (connection == null)
        {
            Log.d(FILENAME, "connection closed, returning");
            return false;
        }
        FileTransferManager manager = FileTransferManager.getInstanceFor(connection);

        //OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer("usre2@myHost/Smack");
        //OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(userAddress + "/Smack");

        if (filenameWithPath == null) {
            Log.d(FILENAME, "SendFile: Error: filename is null");
            return false;
        }
        String jid = ChatXMPPService.mUsersListMap.mUserJidMap.get(userAddress);

        OutgoingFileTransfer transfer;
        if (jid != null) {
            transfer = manager.createOutgoingFileTransfer(userAddress + jid);//User configured on home laptop
        } else {
            //For offline users - it will return from here.
            //Log.d(FILENAME, "SendFile: Error: Jid not valid - it is null for user, user may be offline:: " + userAddress);
            storeUpdateChatRecord(
                    userAddress,
                    ChatBubbleActivity.MESSAGEDIRECTIONSENT,
                    filenameWithPath,
                    mimeType,
                    filenameWithPath,
                    timeStamp,
                    receiptId,
                    ChatBubbleActivity.MESSAGE_RESEND_REQUIRED,
                    1000, // it is unused for now
                    1000  // it is unused for now
            );

            //updateChatRecordRequest(context, ChatXMPPService.mUserName, userAddress, receiptId, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            //DataSource db = new DataSource(context);
            //db.open();
            //db.updateChatRecord(ChatXMPPService.mUserName, userAddress, receiptId, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            //db.close();
            //ChatRecord.LoadAndUpdateChatRecord(db, context, ChatXMPPService.mUserName, userAddress, false, filenameWithPath, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            //Toast throws error:
            //Toast.makeText(context, "Sending Failed: user is offline - try again - once user is online", Toast.LENGTH_SHORT).show();
            return false;
            //transfer = manager.createOutgoingFileTransfer(userAddress + ChatXMPPService.DOMAIN + "/Smack");//User configured on home laptop
        }

        //OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(userAddress + "/Spark");//User configured on home laptop
        //OutgoingFileTransfer transfer = manager.createOutgoingFileTransfer(userAddress + "/Spark 2.7.0");//User configured on manish's laptop
        File f = new File(filenameWithPath);

        if (!f.exists()) {
            Log.e(FILENAME, "File does not exists" + " filepath# " + filenameWithPath);
            storeUpdateChatRecord(
                    userAddress,
                    ChatBubbleActivity.MESSAGEDIRECTIONSENT,
                    filenameWithPath,
                    mimeType,
                    filenameWithPath,
                    timeStamp,
                    receiptId,
                    ChatBubbleActivity.MESSAGE_FAILED_PERMANENT,
                    1000, // it is unused for now
                    1000  // it is unused for now
            );
            //updateChatRecordRequest(context, ChatXMPPService.mUserName, userAddress, receiptId, ChatBubbleActivity.MESSAGE_FAILED_PERMANENT);
            //ChatRecord.LoadAndUpdateChatRecord(context, ChatXMPPService.mUserName, userAddress, false, filenameWithPath, ChatBubbleActivity.MESSAGE_FAILED_PERMANENT);
            return false;
        }
        if (!f.canRead()) {
            Log.e(FILENAME, "File can't be read" + " filepath# " + filenameWithPath);
            storeUpdateChatRecord(
                    userAddress,
                    ChatBubbleActivity.MESSAGEDIRECTIONSENT,
                    filenameWithPath,
                    mimeType,
                    filenameWithPath,
                    timeStamp,
                    receiptId,
                    ChatBubbleActivity.MESSAGE_FAILED_PERMANENT,
                    1000, // it is unused for now
                    1000  // it is unused for now
            );
            //updateChatRecordRequest(context, ChatXMPPService.mUserName, userAddress, receiptId, ChatBubbleActivity.MESSAGE_FAILED_PERMANENT);
            //ChatRecord.LoadAndUpdateChatRecord(context, ChatXMPPService.mUserName, userAddress, false, filenameWithPath, ChatBubbleActivity.MESSAGE_FAILED_PERMANENT);
            return false;
        }
        try
        {
            //transfer.sendFile(f, "MyImage.png");
            String fileUploadURL = FileSharing.MEDIAUPLOADURL + FileSharing.MEDIARESOURCE;
            FileSharing.uploadFile(fileUploadURL,filenameWithPath, mimeType);
            //DownloadFileFromURL.execute(fileUploadURL);

        }
        catch (Exception e) {
            Log.e(FILENAME, "Sending file exceptption" + " filepath# " + filenameWithPath + " filename# " + "Test" + e.getMessage());
            storeUpdateChatRecord(
                    userAddress,
                    ChatBubbleActivity.MESSAGEDIRECTIONSENT,
                    filenameWithPath,
                    mimeType,
                    filenameWithPath,
                    timeStamp,
                    receiptId,
                    ChatBubbleActivity.MESSAGE_RESEND_REQUIRED,
                    1000, // it is unused for now
                    1000  // it is unused for now
            );
            //updateChatRecordRequest(context, ChatXMPPService.mUserName, userAddress, receiptId, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            //ChatRecord.LoadAndUpdateChatRecord(context, ChatXMPPService.mUserName, userAddress, false, filenameWithPath, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            //ChatRecord.storeMessage(context, ChatXMPPService.mUserName, userAddress, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, mimeType, filenameWithPath, filenameWithPath, timeStamp, filenameWithPath, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            return false;
        }
        while (!transfer.isDone()) {
            if (transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error)) {
                //System.out.println("ERROR!!! " + transfer.getError());
                Log.e(FILENAME, "ERROR!!! " + transfer.getError());
            } else if (transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.cancelled)
                    || transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.refused)) {
                //System.out.println("Cancelled!!! " + transfer.getError());
                Log.e(FILENAME, "Cancelled!!! " + transfer.getError());
            }
            try {
                Thread.sleep(1000L);
            } catch (InterruptedException e) {
                e.printStackTrace();
                Log.e(FILENAME, e.getMessage());
            }
        }
        if (transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.refused)
                || transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.cancelled)) {
            Log.e(FILENAME, "SendFile - refused cancelled " + transfer.getError());
            storeUpdateChatRecord(
                    userAddress,
                    ChatBubbleActivity.MESSAGEDIRECTIONSENT,
                    filenameWithPath,
                    mimeType,
                    filenameWithPath,
                    timeStamp,
                    receiptId,
                    ChatBubbleActivity.MESSAGE_FAILED_PERMANENT,
                    1000, // it is unused for now
                    1000  // it is unused for now
            );
        }
        else if (transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error))
        {
            Log.e(FILENAME, "SendFile - error " + transfer.getError());
            storeUpdateChatRecord(
                    userAddress,
                    ChatBubbleActivity.MESSAGEDIRECTIONSENT,
                    filenameWithPath,
                    mimeType,
                    filenameWithPath,
                    timeStamp,
                    receiptId,
                    ChatBubbleActivity.MESSAGE_RESEND_REQUIRED,
                    1000, // it is unused for now
                    1000  // it is unused for now
            );
            //updateChatRecordRequest(context, ChatXMPPService.mUserName, userAddress, receiptId, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            //ChatRecord.LoadAndUpdateChatRecord(context, ChatXMPPService.mUserName, userAddress, false, filenameWithPath, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            //ChatRecord.storeMessage(context, ChatXMPPService.mUserName, userAddress, ChatBubbleActivity.MESSAGEDIRECTIONSENT, null, mimeType, filenameWithPath, filenameWithPath, timeStamp, filenameWithPath, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            // Here update about failure
        } else {
            //Here update about success
            Log.d(FILENAME, "Sendfile Success");
            storeUpdateChatRecord(
                    userAddress,
                    ChatBubbleActivity.MESSAGEDIRECTIONSENT,
                    filenameWithPath,
                    mimeType,
                    filenameWithPath,
                    timeStamp,
                    receiptId,
                    ChatBubbleActivity.MESSAGE_ACKED,
                    1000, // it is unused for now
                    1000  // it is unused for now
            );
            return true;
            //reqs.contains()
            //updateChatRecordRequest(context, ChatXMPPService.mUserName, userAddress, receiptId, ChatBubbleActivity.MESSAGE_ACKED);
            //ChatRecord.LoadAndUpdateChatRecord(context, ChatXMPPService.mUserName, userAddress, false, filenameWithPath, ChatBubbleActivity.MESSAGE_ACKED);
        }
        return false;
    }

    public static void storeUpdateChatRecord(String remoteUserName, int direction, String messageSent, String mimeType, String fileSent, Long timeStamp, String receiptId, String receiptStatus, long fileSizeExpected, long fileSizeDisk) {
        DataSource db = new DataSource(context);
        db.open();
        db.updateChatRecord(ChatXMPPService.mUserName, remoteUserName, receiptId, receiptStatus);
        db.close();
        sendUpdateChatRecordObs(
                remoteUserName,
                direction,
                messageSent,
                mimeType,
                fileSent,
                timeStamp,
                receiptId,
                receiptStatus,
                fileSizeExpected, // it is unused for now
                fileSizeDisk  // it is unused for now
        );
    }


    public static void ReceiveFile(Context cntxt) {
        context = cntxt;
        XMPPConnection connection2 = ChatConnection.getInstance().getConnection();
        // TODO Auto-generated method stub
        if (connection2 != null) {

            //FileSharing.monitoringReliableDeliveryThread(cntxt);
            ServiceDiscoveryManager sdm = ServiceDiscoveryManager.getInstanceFor(connection2);
            sdm.addFeature("http://jabber.org/protocol/disco#info");
            sdm.addFeature("jabber:iq:privacy");
            sdm.addFeature("http://jabber.org/protocol/si");
            sdm.addFeature("http://jabber.org/protocol/bytestreams");
            //sdm.addFeature("http://jabber.org/protocol/ibb");
            FileTransferManager manager = FileTransferManager.getInstanceFor(connection2);
            manager.addFileTransferListener(new FileTransferListener() {
                public void fileTransferRequest(final FileTransferRequest request) {
                    Log.d(FILENAME, "allows file...");
                    if (request != null) {
                        // Accept it
                        final IncomingFileTransfer transfer = request.accept();
                        try {
                            //FileOutputStream fos = openFileOutput(request.getFileName(), Context.MODE_PRIVATE);
                            File mf = Environment.getExternalStorageDirectory();
                            String root = mf.toString();
                            root = root + "/bitchat";
                            File myDir = new File(root);
                            myDir.mkdirs();
                            final File file = new File(root + "/" + transfer.getFileName());
                            //final File file = new File(mf.getAbsoluteFile() + "/" + transfer.getFileName());
                            transfer.recieveFile(file);

                            new Thread(new Runnable() {
                                public void run() {
                                    boolean fileReceiveSuccess = true;
                                    while (!transfer.isDone()) {
                                        try {
                                            Thread.sleep(1000L);
                                        } catch (Exception e) {
                                            Log.e(FILENAME, "Exception# " + e.getMessage());
                                            fileReceiveSuccess = false;
                                        }
                                        if (transfer.getStatus().equals(org.jivesoftware.smackx.filetransfer.FileTransfer.Status.error)) {
                                            Log.e(FILENAME, "ERROR!!! " + transfer.getError() + "");
                                            fileReceiveSuccess = false;
                                        }
                                        if (transfer.getException() != null) {
                                            fileReceiveSuccess = false;
                                            Log.e(FILENAME, "Error!!!" + "exception in receiving file");
                                            transfer.getException().printStackTrace();
                                        }
                                    }

                                    if (fileReceiveSuccess == true) {
                                        Log.d(FILENAME, "file receive success");
                                        String from = request.getRequestor();
                                        String remoteUserName = from.substring(0, from.indexOf("@"));
                                        String url = file.getAbsolutePath();
                                        FileNameMap fileNameMap = URLConnection.getFileNameMap();
                                        String mimeType = fileNameMap.getContentTypeFor("file://" + url);

                                        long fileSizetoReceive = transfer.getFileSize();
                                        long fileSizeRecieved = transfer.getAmountWritten();
                                        Log.d(FILENAME, "FileSizetoReceive = " +fileSizetoReceive );
                                        Log.d(FILENAME, "FileSizeReceived = " +fileSizeRecieved );

                                        if (fileSizetoReceive <= fileSizeRecieved)
                                        {
                                            Log.d(FILENAME, "Full File Received");
                                            if (ChatXMPPService.mResponseMessengerChat == null) {
                                                Log.d(FILENAME, "fileTransferRequest: Cannot send message to activity - no activity registered to this service. So storing and showing notification intent");
                                                ChatXMPPService.count++;
                                                //String timeStamp = TimeClock.getDateTime();
                                                //final String receiptId = timeStamp;
                                                final long timeStamp = TimeClock.getEpocTime();
                                                final String receiptId = TimeClock.getEpocTimeLongToString(timeStamp);

                                                //Store message here.
                                                ChatRecord.storeMessage(context, ChatXMPPService.mUserName, remoteUserName, ChatBubbleActivity.MESSAGEDIRECTIONRECEIVED, null, mimeType, remoteUserName + ":" + file.getAbsolutePath(), file.getAbsolutePath(), timeStamp, receiptId, ChatBubbleActivity.MESSAGE_ACKED, fileSizetoReceive, fileSizeRecieved);
                                                //Notification action is just to inform user of messages:
                                                ChatXMPPService.prepareAndSendNotification(context, remoteUserName, file.getAbsolutePath(), true);
                                            } else {
                                                Log.d(FILENAME, "Sending message to CHAT activity: " + remoteUserName + " file: " + file.getAbsolutePath());
                                                ChatXMPPService.count = 0;
                                                Bundle data = new Bundle();
                                                data.putSerializable(ChatUsersListActivity.REMOTE_USER_NAME, remoteUserName);
                                                data.putSerializable(ChatXMPPService.CHAT_MESSAGETYPE, mimeType);
                                                data.putSerializable(ChatXMPPService.MESSAGERECEIVED, file.getAbsolutePath());
                                                data.putSerializable(ChatXMPPService.FILESIZEEXPECTED, fileSizetoReceive);
                                                data.putSerializable(ChatXMPPService.FILESIZEDISK, fileSizeRecieved);
                                                android.os.Message msg = android.os.Message.obtain(null, ChatXMPPService.INTER_TASK_MESSAGE_TYPE_MESSAGE);
                                                msg.setData(data);
                                                try {
                                                    ChatXMPPService.mResponseMessengerChat.send(msg);
                                                } catch (RemoteException e) {
                                                    Log.d(FILENAME, "Exception in sending message to Chat Activity : " + e.getMessage());
                                                    e.printStackTrace();
                                                }
                                            }
                                        }
                                        else
                                        {
                                            Log.e(FILENAME, "full file size could not be received");
                                        }
                                    }
                                }
                            }).start();

                            // The PendingIntent to launch our activity if the user selects this notification
                            //fos.write(IOUtils.toByteArray(stream));
                            //fos.close();

                            // String pathsd =    Environment.getExternalStorageDirectory().toString();
                            // transfer.recieveFile(new
                            // File(request.getFileName()));
                            //System.out.println("File " + request.getFileName() + "Received Successfully");
                            //                      Log.d("", "rece: " +    request.getFileName());
                            //                      Log.d("", "path: " + request.getFileName());
                            // InputStream input = transfer.recieveFile();

                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(FILENAME, e.getMessage());
                        }
                    } else {
                        Log.d(FILENAME, "reject file...");
                        try {

                            request.reject();
                        } catch (Exception e) {
                            e.printStackTrace();
                            Log.d(FILENAME, e.getMessage());
                        }
                    }
                }
            });
        }
    }


    //Relaible delivery of files:
    //initiate a new thread only once in life cycle of program
    //loop through all the resent rquired messages
    //send file
    //update chatmessagelist
    //refresh view
    //sleep
    /*
    private static void monitoringReliableDeliveryThread(final Context cntxt) {
        new Thread() {
            @Override
            public void run() {
                while (true) {
                    boolean isNetworkConnected = ConnectivityReceiver.isConnected(cntxt);
                    if (isNetworkConnected) {
                        try {
                            Thread.sleep(5000L);
                            FileSharing.loadAndDeliverableMessagesFromDB(cntxt, ChatXMPPService.mUserName);
                        } catch (Exception e) {
                            Log.e(FILENAME, "Exception" + e.getMessage());
                        }
                    } else {
                        //sleep longer:
                        try {
                            Thread.sleep(20000L);
                        } catch (Exception e) {
                            Log.e(FILENAME, "Exception" + e.getMessage());
                        }
                    }
                }
            }
        }.start();
    }
    */

    /*
    //List<ChatRecord> ResendRequiredReqs = null;
    static List<ChatRecord> reqs = null;
    protected static void loadAndDeliverableMessagesFromDB(Context cntxt, String userName) {
        if (reqs ==null)
        {
            DataSource db = new DataSource(cntxt);
            db.open();
            reqs = db.getChatRecords(userName, ChatBubbleActivity.MESSAGE_RESEND_REQUIRED);
            db.close();
        }

        if (reqs != null)
        {
            //Set resending as true;
            boolean resending = true;
            //int location
            int resentFiles = 0;
            for (ChatRecord _req : reqs) {

                String mimeType = _req.getMimeType();
                String str = null;
                if (mimeType == null) {
                    continue;
                }
                else if (mimeType.equals(ChatBubbleActivity.CHAT_MESSAGE_TYPE_TEXT))
                {
                    //chatArrayAdapter.add(new ChatMessage(_req.getMessageDirection(), _req.getMessageText(), CHAT_MESSAGE_TYPE_TEXT, null, _req.getCreationTimestamp(), _req.getMessageReceiptId(), _req.getMessageReceiptStatus(), _req.getFileSizeExpected(), _req.getFileSizeDisk()));
                }
                else if (_req.getMessageReceiptStatus() == ChatBubbleActivity.MESSAGE_RESEND_REQUIRED)
                {
                    resentFiles++;
                    boolean status = FileTransferWrapper.SendFile(cntxt, _req.getMessageFileName(), _req.getRemoteUserName(), mimeType, _req.getCreationTimestamp(), resending, _req.getMessageReceiptId());
                    if (status == true)
                    {
                        _req.setMessageReceiptStatus(ChatBubbleActivity.MESSAGE_ACKED);
                    }
                }
            }
            if (resentFiles == 0)
            {
                reqs.clear();
            }
        }
    }*/

    /*
    public static void sendUpdateChatRecord(String remoteUserName, String receiptId, String receiptStatus)
    {
        //String timeStampStr = TimeClock.getEpocTimeLongToString(timeStamp);
        if (ChatXMPPService.mResponseMessengerChat != null)
        {//chat is open, so update activity if it is for currently open user:
            Log.d(FILENAME, "fileTransferRequest: update activity");
            //Send message to Bubble to update Adapter for the sent file
            Bundle data = new Bundle();
            data.putSerializable(ChatUsersListActivity.REMOTE_USER_NAME, remoteUserName);
            //data.putSerializable(ChatXMPPService.MESSAGEDIRECTION, direction);
            //data.putSerializable(ChatXMPPService.MESSAGESENT, messageSent);
            //data.putSerializable(ChatXMPPService.CHAT_MESSAGETYPE, mimeType);
            //data.putSerializable(ChatXMPPService.FILESENT, fileSent);
            //data.putSerializable(ChatXMPPService.TIMESTAMP, timeStamp);
            data.putSerializable(ChatXMPPService.RECEIPTID, receiptId);
            data.putSerializable(ChatXMPPService.RECEIPTSTATUS, receiptStatus);
            //data.putSerializable(ChatXMPPService.FILESIZEEXPECTED, fileSizeExpected);
            //data.putSerializable(ChatXMPPService.FILESIZEDISK, fileSizeDisk);
            android.os.Message msg = android.os.Message.obtain(null, ChatXMPPService.INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYREPORT);
            msg.setData(data);
            try {
                ChatXMPPService.mResponseMessengerChat.send(msg);
            } catch (RemoteException e) {
                Log.d(FILENAME, "Exception in sending message to Chat Activity : " + e.getMessage());
                e.printStackTrace();

            }
        }
    }

    public static void sendUpdateChatRecord(String remoteUserName, String receiptId, long timeStamp)
    {
        //String timeStampStr = TimeClock.getEpocTimeLongToString(timeStamp);
        if (ChatXMPPService.mResponseMessengerChat != null)
        {//chat is open, so update activity if it is for currently open user:
            Log.d(FILENAME, "fileTransferRequest: update activity");
            //Send message to Bubble to update Adapter for the sent file
            Bundle data = new Bundle();
            data.putSerializable(ChatUsersListActivity.REMOTE_USER_NAME, remoteUserName);
            //data.putSerializable(ChatXMPPService.MESSAGEDIRECTION, direction);
            //data.putSerializable(ChatXMPPService.MESSAGESENT, messageSent);
            //data.putSerializable(ChatXMPPService.CHAT_MESSAGETYPE, mimeType);
            //data.putSerializable(ChatXMPPService.FILESENT, fileSent);

            data.putSerializable(ChatXMPPService.RECEIPTID, receiptId);
            data.putSerializable(ChatXMPPService.TIMESTAMP, timeStamp);
            //data.putSerializable(ChatXMPPService.RECEIPTSTATUS, receiptStatus);
            //data.putSerializable(ChatXMPPService.FILESIZEEXPECTED, fileSizeExpected);
            //data.putSerializable(ChatXMPPService.FILESIZEDISK, fileSizeDisk);
            android.os.Message msg = android.os.Message.obtain(null, ChatXMPPService.INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYID);
            msg.setData(data);
            try {
                ChatXMPPService.mResponseMessengerChat.send(msg);
            } catch (RemoteException e) {
                Log.d(FILENAME, "Exception in sending message to Chat Activity : " + e.getMessage());
                e.printStackTrace();

            }
        }
    }

*/


    //Following is obsolete now: Manish
    public static void sendUpdateChatRecordObs(String remoteUserName, int direction, String messageSent, String mimeType, String fileSent, Long timeStamp, String receiptId, String receiptStatus, long fileSizeExpected, long fileSizeDisk)
    {
        //String timeStampStr = TimeClock.getEpocTimeLongToString(timeStamp);
        if (ChatXMPPService.mResponseMessengerChat != null)
        {//chat is open, so update activity if it is for currently open user:
            Log.d(FILENAME, "fileTransferRequest: update activity");
            //Send message to Bubble to update Adapter for the sent file
            Bundle data = new Bundle();
            data.putSerializable(ChatUsersListActivity.REMOTE_USER_NAME, remoteUserName);
            data.putSerializable(ChatXMPPService.MESSAGEDIRECTION, direction);
            data.putSerializable(ChatXMPPService.MESSAGESENT, messageSent);
            data.putSerializable(ChatXMPPService.CHAT_MESSAGETYPE, mimeType);
            data.putSerializable(ChatXMPPService.FILESENT, fileSent);
            data.putSerializable(ChatXMPPService.TIMESTAMP, timeStamp);
            data.putSerializable(ChatXMPPService.RECEIPTID, receiptId);
            data.putSerializable(ChatXMPPService.RECEIPTSTATUS, receiptStatus);
            data.putSerializable(ChatXMPPService.FILESIZEEXPECTED, fileSizeExpected);
            data.putSerializable(ChatXMPPService.FILESIZEDISK, fileSizeDisk);
            android.os.Message msg = android.os.Message.obtain(null, ChatXMPPService.INTER_TASK_MESSAGE_TYPE_MESSAGEDELIVERYREPORT);
            msg.setData(data);
            try {
                ChatXMPPService.mResponseMessengerChat.send(msg);
            } catch (RemoteException e) {
                Log.d(FILENAME, "Exception in sending message to Chat Activity : " + e.getMessage());
                e.printStackTrace();

            }
        }
    }

}
