package com.bitskeys.track.settings;

import android.app.Activity;
import android.graphics.Color;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.bitskeys.track.R;

//import com.bitskeys.track.R;

public class ChildProfileSettingsActivity extends Activity {
    RelativeLayout profileRelativeLayout = null;
    RelativeLayout accountRelativeLayout = null;
    RelativeLayout chatRelativeLayout = null;
    RelativeLayout notificationsRelativeLayout = null;
    RelativeLayout dataUsageRelativeLayout = null;
    RelativeLayout contactsRelativeLayout = null;
    RelativeLayout aboutHelpRelativeLayout = null;

    public void setListenersForHighlightLayout(final RelativeLayout layout)
    {
        if(layout == null)
            return;

        layout.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if(event.getAction() == MotionEvent.ACTION_DOWN)
                {
                    layout.setBackgroundColor(Color.CYAN);
                }
                else if (event.getAction() == MotionEvent.ACTION_UP)
                {
                    layout.setBackgroundColor(getResources().getColor(android.R.color.transparent));
                }
                return false;
            }
        });
    }

    public void initializeActivity()
    {
        profileRelativeLayout= (  RelativeLayout) this.findViewById(R.id.profile_layout);
        setListenersForHighlightLayout(profileRelativeLayout);

        accountRelativeLayout =( RelativeLayout) this.findViewById(R.id.account_layout);
        setListenersForHighlightLayout(accountRelativeLayout);

        chatRelativeLayout =( RelativeLayout) this.findViewById(R.id.chat_layout);
        setListenersForHighlightLayout(chatRelativeLayout);

        notificationsRelativeLayout =( RelativeLayout) this.findViewById(R.id.notification_layout);
        setListenersForHighlightLayout(notificationsRelativeLayout);

        dataUsageRelativeLayout =( RelativeLayout) this.findViewById(R.id.data_usage_layout);
        setListenersForHighlightLayout(dataUsageRelativeLayout);

        contactsRelativeLayout =( RelativeLayout) this.findViewById(R.id.contacts_layout);
        setListenersForHighlightLayout(contactsRelativeLayout);

        aboutHelpRelativeLayout =( RelativeLayout) this.findViewById(R.id.about_help_layout);
        setListenersForHighlightLayout(aboutHelpRelativeLayout);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.profile_setting);
        this.setTitle("Settings");

        initializeActivity();

    }

    public void onClickAccount(View view)
    {
        Toast.makeText(this, "Account Setting Clicked", Toast.LENGTH_LONG).show();
    }
}
