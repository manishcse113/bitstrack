package com.bitskeys.track.login;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;
import android.widget.Toast;

import com.bitskeys.track.RestAPI.OpenfireRestAPI;
import com.bitskeys.track.profile.ProfileInfo;
import com.bitskeys.track.profile.VCardWrappers;
import com.bitskeys.track.db.MyRecord;

import org.jivesoftware.smackx.vcardtemp.packet.VCard;

/**
 * Created by GUR11219 on 22-09-2015.
 */

public class LoginBackgroundNoGUI extends AsyncTask<Void,Void,Void> {

    private final String FILENAME = "GBC LoginBackground#";
    private String userName;
    private String password;
    private Context context;
    LoginProcessCallback mLoginProgCallback;

    public interface LoginProcessCallback
    {
        public void LoginSuccess();
        public void LoginFailure(int statusCode, String errMessage);
    }

    public LoginBackgroundNoGUI(String uName,String pwd,Context appCtxt,LoginProcessCallback mCallback)
    {
        Log.d(FILENAME, "LoginBackground");
        userName = uName;
        password = pwd;
        context = appCtxt;
        mLoginProgCallback = mCallback;
    }

    @Override
    protected Void doInBackground(Void...arg){
        Log.d(FILENAME, "doInBackground");
        publishProgress();
        // do your processing here like sending data or downloading etc.
        ChatConnection connection = ChatConnection.getInstance();
        connection.connect(context, userName, password);
        boolean done = false;
        while(!done)
        {
            if(!(connection.getStatus() == ChatConnection.Status.RUNNING))
            {
                done=true;
            }
            /*
            else
            {
                Log.v(FILENAME, "Network not Available!");
            }*/
        }
        Log.d(FILENAME, "doInBackground exit");
        return null;
    }
    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
        Log.d(FILENAME, "onPreExecute");
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
    }

    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        Log.d(FILENAME, "onPostExecute starting login");
        ChatConnection connection = ChatConnection.getInstance();
        if(connection.getStatus() == ChatConnection.Status.FINISHED) {

           //Remove old records, persists new records
            MyRecord.clearFromDB(context);
            MyRecord.storeRecord(context, userName, userName, null, password, null, null);//group, mail, avatar are null for now
			Toast.makeText(context, "Login Success", Toast.LENGTH_SHORT).show();
            if(mLoginProgCallback != null)
            {
                //ProfileInfo pInfo = LoadProfileInfoFromServer();
                ProfileInfo pInfo = LoadMyProfileInfoFromServer();
                /* No need to check whether it is Null or Non Null */
                OpenfireRestAPI.setProfileInfo(pInfo);
                mLoginProgCallback.LoginSuccess();
            }
        }
        else
        {
            if(mLoginProgCallback != null)
            {
                mLoginProgCallback.LoginFailure(0,"Please check internet connectivity!!");
            }
        }
        //ChatXMPPService.startAction
    }

    //@Override
    public void onPause()
    {
        //super.onPause();
    }


    public void CreateAndSaveProfileOnServer(ProfileInfo profileInfo)
    {
        ChatConnection chatConnection = ChatConnection.getInstance();
        chatConnection.CreateandSaveVCard(profileInfo);
    }
    public ProfileInfo LoadMyProfileInfoFromServer()
    {
        //VCard vCard = VCardWrappers.loadMyVCard(ChatConnection.mConnection);
        VCard vCard = VCardWrappers.loadUserVCard(ChatConnection.mConnection,OpenfireRestAPI.getUserName());
        if(vCard == null)
            return null;

        ProfileInfo pInfo = null;
        String firstName = vCard.getField(VCardWrappers.VCardFirstNameKey);
        if(firstName == null)
        {
            firstName = vCard.getFirstName();
        }
        String lastName = vCard.getField(VCardWrappers.VCardLastNameKey);
        if(lastName == null)
        {
            lastName = vCard.getLastName();
        }
        String orgName = vCard.getField(VCardWrappers.organizatioNameKey);
        if(orgName == null)
        {
            orgName = vCard.getOrganization();
        }
        String emailId = vCard.getField(VCardWrappers.VCardEmailKey);
        if(emailId == null)
        {
            emailId = vCard.getEmailWork();
        }

        String displayName = vCard.getField(VCardWrappers.VCardDisplayName);
        if(displayName == null)
        {
            displayName = vCard.getNickName();
        }

        byte[] imageByte = vCard.getAvatar();

        pInfo = new ProfileInfo(firstName,lastName,displayName,emailId);
        pInfo.setAddressFieldCity(vCard.getField(VCardWrappers.VCardCityAddressKey));
        pInfo.setAddressFieldCountry(vCard.getField(VCardWrappers.VCardCountryAddressKey));
        pInfo.setAddressFieldState(vCard.getField(VCardWrappers.VCardStateAddressKey));
        pInfo.setAddressFieldStreet(vCard.getField(VCardWrappers.VCardStreetAddressKey));
        pInfo.setOrganizationName(orgName);
        if(imageByte != null && imageByte.length>0) {
            pInfo.setAvatarImage(imageByte);
        }

        return pInfo;

    }

}