package com.bitskeys.track.login;

import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;

import com.bitskeys.track.gen.ChatXMPPService;

/**
 * Created by GUR11219 on 22-09-2015.
 */

class LoginBackground { //extends AsyncTask<Void,Void,Void> {
    public static ProgressDialog progress;
    private final String FILENAME = "GBC LoginBackground#";
    private String userName;
    private String password;
    private Context context;
    //static boolean autologin = false;

    LoginBackground(LoginActivity activity)
    {
        Log.d(FILENAME, "LoginBackground");
        progress = new ProgressDialog(activity);
        userName = activity.mUserName;
        password = activity.mPassword;
        context = activity.getApplicationContext();

        //Following is added - to login in backgrounds
        if (LoginActivity.autologin == true) {
            ChatXMPPService.startActionLogin(context, userName, password, "anything");
        }
        ChatConnection connection = ChatConnection.getInstance();
        connection.connect(context, userName, password);
        boolean done = false;
        while(!done)
        {
            if(!(connection.getStatus() == ChatConnection.Status.RUNNING))
            {
                done=true;
            }
            else
            {
                Log.v(FILENAME, "Network not Available!");
            }
        }
        Log.d(FILENAME, "doInBackground exit");
    }

    /*
    @Override
    protected Void doInBackground(Void...arg){
        Log.d(FILENAME, "doInBackground");
        publishProgress();
        // do your processing here like sending data or downloading etc.
        ChatConnection connection = ChatConnection.getInstance();
        connection.connect(context, userName, password);
        boolean done = false;
        while(!done)
        {
            if(!(connection.getStatus() == ChatConnection.Status.RUNNING))
            {
                done=true;
            }
            else
            {
                Log.v(FILENAME, "Network not Available!");
            }
        }
        Log.d(FILENAME, "doInBackground exit");
        return null;
    }

    @Override
    protected void onPreExecute()
    {
        super.onPreExecute();
        Log.d(FILENAME, "onPreExecute");
        //progress = new ProgressDialog();
        progress.setMessage("Loging in ...");
        progress.setIndeterminate(false);
        progress.setCancelable(false);
        try {
            progress.show();// It needs to be fixed. It crashed here
        }
        catch(Exception e)
        {
            Log.e(FILENAME, "It crashed here- handled the exception");
            e.printStackTrace();
        }
    }

    @Override
    protected void onProgressUpdate(Void... values) {
        super.onProgressUpdate(values);
        Log.d(FILENAME, "onProgressUpdate");
        if (progress == null)
        {
            return;
        }
        progress.show();
    }
    @Override
    protected void onPostExecute(Void result) {
        super.onPostExecute(result);
        Log.d(FILENAME, "onPostExecute starting login");
        try {
            if (progress != null && progress.isShowing()) {
                progress.dismiss(); //Manish:: TBD: Crashed on tab here: Nexus 7: Harsh
                //Fix is discussed in : http://stackoverflow.com/questions/2224676/android-view-not-attached-to-window-manager

            }
            progress = null;
        }
        catch (Exception e)
        {
            Log.e(FILENAME, "It sometimes crashes here- no work around for now");
            e.printStackTrace();
        }

        ChatConnection connection = ChatConnection.getInstance();
        if(connection.getStatus() == ChatConnection.Status.FINISHED) {

            //Remove old records, persists new records
            MyRecord.clearFromDB(context);
            MyRecord.storeRecord(context, userName, null, password, null, null);//group, mail, avatar are null for now
			Toast.makeText(context, "Login Success", Toast.LENGTH_SHORT).show();

            Log.d(FILENAME, "username and password are " + userName + " " + password);
            //autologin = true;
            ChatXMPPService.startActionLogin(context, userName, password, ChatXMPPService.STR_LOGGEDIN);
        }
        // first we will move to user-screen - while login will happen in background.
        else
        {
            Toast.makeText(context, "Login Failed: Taking long time - Check Server/Network or login credentials - continuing in offline mode", Toast.LENGTH_SHORT).show();
            //Show tost to retry login: again: warning message:
            if (LoginActivity.autologin == true) {
                ChatXMPPService.startActionLogin(context, userName, password, "anything");
            }
        }

        //ChatXMPPService.startAction
    }
*/


}