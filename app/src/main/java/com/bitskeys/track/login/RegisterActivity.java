package com.bitskeys.track.login;

/**
 * Created by GUR11219 on 02-07-2015.
 */

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.bitskeys.track.R;
import com.bitskeys.track.RestAPI.OpenfireRestAPI;
import com.bitskeys.track.gen.ChatXMPPService;
//import com.bitskeys.track.R;
import com.bitskeys.track.gen.Utility;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;

/**
 * Register Activity Class
 */
public class RegisterActivity extends Activity {
    // Progress Dialog Object
    ProgressDialog prgDialog;
    // Error Msg TextView Object
    TextView errorMsg;
    // Name Edit View Object
    EditText nameET;
    // Email Edit View Object
    EditText emailET;
    // Passwprd Edit View Object
    EditText pwdET;

    private final String FILENAME = "GBC: RegisterActivity#";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        Log.d(FILENAME, "onCreate");
        setContentView(R.layout.register);
        // Find Error Msg Text View control by ID
        errorMsg = (TextView) findViewById(R.id.register_error);
        // Find Name Edit View control by ID
        nameET = (EditText) findViewById(R.id.registerName);
        // Find Email Edit View control by ID
        emailET = (EditText) findViewById(R.id.registerEmail);
        // Find Password Edit View control by ID
        pwdET = (EditText) findViewById(R.id.registerPassword);
        // Instantiate Progress Dialog object
        prgDialog = new ProgressDialog(this);
        // Set Progress Dialog Text
        prgDialog.setMessage("Please wait...");
        // Set Cancelable as False
        prgDialog.setCancelable(false);
    }

    void invokeCreateUser(String name, String userName, String password, String mail) {
/*
        //String url = new String ("http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users");
        String url = new String("http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com");
        AuthenticationToken authToken = new AuthenticationToken("664Rk33SFjfs2m7R");
        RestApiClient restApiClient = new RestApiClient(url, 9090, authToken);
        // Create a new user (username, name, email, passowrd). There are more user settings available.
        UserEntity userEntity = new UserEntity(name, userName, password, mail);
        restApiClient.createUser(userEntity);
  */
    }

    /**
     * Method gets triggered when Register button is clicked
     *
     * @param view
     */
    public void registerUser(View view) {
        // Get NAme ET control value
        String name = nameET.getText().toString();
        // Get Email ET control value
        String email = emailET.getText().toString();
        // Get Password ET control value
        String password = pwdET.getText().toString();
        // Instantiate Http Request Param Object
        RequestParams params = new RequestParams();
        // When Name Edit View, Email Edit View and Password Edit View have values other than Null

        Log.d(FILENAME, "registerUser name#" + name + "email#" + email + "password#" + password);

        if (Utility.isNotNull(name) && Utility.isNotNull(email) && Utility.isNotNull(password)) {
            // When Email entered is Valid
            //if(Utility.validate(email)){
            // Put Http parameter name with value of Name Edit View control
            //params.put("username", name);
            // Put Http parameter username with value of Email Edit View control
            //params.put("password", password);
            // Invoke RESTful Web Service with Http parameters
            //params.put("username", email);
            // Put Http parameter password with value of Password Edit View control


            JSONObject json = new JSONObject();
            try {

                json.put("username",name);
                json.put("password", password);

            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }
            //invokeCreateUser(name, name, password, email);
            invokeWS2(json.toString());

            JSONObject jsonGrp = new JSONObject();
            String group = ChatXMPPService.GROUPNAME;
            try {
                jsonGrp.put("groupname", group);

            } catch (JSONException e) {
                e.printStackTrace();
                return;
            }

            addToDefaultGroup(jsonGrp.toString(), name , group);
            //}
            // When Email is invalid
            /*
            else{
                Toast.makeText(getApplicationContext(), "Please enter valid email", Toast.LENGTH_LONG).show();
            }*/
        }
        // When any of the Edit View control left blank
        else {
            Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
        }

    }

    // public void invokeWS(RequestParams params) {
    public void invokeWS2(String jsonString) {
        String st = null;
        // Show Progress Dialog
        prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        //String  contentType = "string/xml;UTF-8";
        client.removeHeader("Content-Type");
        //client.addHeader("Content-Type", "application/json");
        //client.addHeader("Authorization", "664Rk33SFjfs2m7R");
        client.addHeader("Authorization", OpenfireRestAPI.restApiAutherizationKey);
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Accept", "application/json");
        //client.setBasicAuth("manish", "nswdel10");

        Log.d(FILENAME, "invokeWS2");

        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        //client.post(this.getApplicationContext(), "http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users", se, "application/json", new AsyncHttpResponseHandler() {
        client.post(this.getApplicationContext(), "http://" + ChatXMPPService.DOMAIN + "/plugins/restapi/v1/users", se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {
                // Hide Progress Dialog
                prgDialog.hide();
                Log.d(FILENAME, "http onSuccess: " + responseBody);

                try {
                    // JSON Object
                    HashMap<String, String> hmap = new HashMap<String, String>();
                    for (org.apache.http.Header h : headers) {
                        hmap.put(h.getName(), h.getValue());
                        if (h.getName().equals("Content-Length")) {
                            if (h.getValue().equals("0")) {
                                Toast.makeText(getApplicationContext(), "You are successfully registered!", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }
                    JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        // Set Default Values for Edit View controls
                        setDefaultValues();
                        // Display successfully registered message using Toast
                        Toast.makeText(getApplicationContext(), "You are successfully registered!", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                        errorMsg.setText(obj.getString("error_msg"));
                        Toast.makeText(getApplicationContext(), obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                // Hide Progress Dialog
                prgDialog.hide();
                Log.d(FILENAME, "http onFailure");

                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Failed:404" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '404'
                else if (statusCode == 409) {

                    Toast.makeText(getApplicationContext(), "Failed:409" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Failed:500" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });



    }

    public void addToDefaultGroup(String jsonString, String userName, String groupName) {
        String st = null;
        // Show Progress Dialog
        prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
        //String  contentType = "string/xml;UTF-8";
        client.removeHeader("Content-Type");
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", "664Rk33SFjfs2m7R");
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Accept", "application/json");
        //client.setBasicAuth("manish", "nswdel10");

        String userUrl = "http://" + ChatXMPPService.DOMAIN + "/plugins/restapi/v1/users";
        String grpUrl = userUrl + "/" + userName + "/groups";
        Log.d(FILENAME, "addToDefaultGroup");

        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }
        client.post(this.getApplicationContext(),grpUrl, se, "application/json",new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {
                // Hide Progress Dialog
                prgDialog.hide();
                Log.d(FILENAME, "http onSuccess: " + responseBody);

                try {
                    // JSON Object
                    HashMap<String,String> hmap=new HashMap<String,String>();
                    for (  org.apache.http.Header h : headers) {
                        hmap.put(h.getName(),h.getValue());
                        if (h.getName().equals("Content-Length"))
                        {
                            if (h.getValue().equals("0"))
                            {
                                Toast.makeText(getApplicationContext(), "You are successfully added to group!", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }
                    JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        // Set Default Values for Edit View controls
                        setDefaultValues();
                        // Display successfully registered message using Toast
                        Toast.makeText(getApplicationContext(), "You are successfully registered!", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                        errorMsg.setText(obj.getString("error_msg"));
                        Toast.makeText(getApplicationContext(), obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                // Hide Progress Dialog
                prgDialog.hide();
                Log.d(FILENAME, "http onFailure");

                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Failed:404"  + error.getMessage()+ ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '404'
                else if (statusCode == 409) {

                    Toast.makeText(getApplicationContext(), "Failed:409" + error.getMessage()+ ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Failed:500"  + error.getMessage()+ ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Failed:Unexpected Error occcured!" +  error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]"+ responseBody.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });



    }




    /// Code below this is not used for now///////////////////////

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param params
     */
   // public void invokeWS(RequestParams params) {
    public void invokeWS(String jsonString) {
        String st = null;
        // Show Progress Dialog
        prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        AsyncHttpClient client = new AsyncHttpClient();
    /*
        Context context = this.getApplicationContext();
        String  url = null;
        String  xml = null;
        url = new String ("http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users");
        xml = new String ("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?> <user> <username>manish</username> <password>nswdel10</password></user>");

        HttpEntity entity;
        try {
            entity = new StringEntity(xml, "UTF-8");
        } catch (IllegalArgumentException e) {
            Log.d("HTTP", "StringEntity: IllegalArgumentException");
            return;
        } catch (UnsupportedEncodingException e) {
            Log.d("HTTP", "StringEntity: UnsupportedEncodingException");
            return;
        }
        */
        //String  contentType = "string/xml;UTF-8";
        client.removeHeader("Content-Type");
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Authorization", "664Rk33SFjfs2m7R");
        //client.addHeader("Content-Type", "application/json");
        client.addHeader("Accept", "application/json");
        //client.setBasicAuth("manish", "nswdel10");

        /*
         HttpEntity httpEntity = new HttpEntity() {
             @Override
             public boolean isChunked() {
                 return false;
             }

             @Override
             public long getContentLength() {
                 return 0;
             }

             @Override
             public Header getContentType() {
                 return null;
             }

             @Override
             public Header getContentEncoding() {
                 return null;
             }

             @Override
             public InputStream getContent() throws IOException, IllegalStateException {
                 return null;
             }

             @Override
             public void writeTo(OutputStream outputStream) throws IOException {

             }

             @Override
             public boolean isStreaming() {
                 return false;
             }

             @Override
             public void consumeContent() throws IOException {

             }

             @Override
             public boolean isRepeatable() {
                 return false;
             }
         };
        */
        //client.post( context, url, entity, contentType, new AsyncHttpResponseHandler(){
        //st =new String ("<?xml version=\"1.0\" encoding=\"UTF-8\" standalone=\"yes\"?> <user> <username>manish</username> <password>nswdel10</password></user>");
        //client.post("http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users", params, new AsyncHttpResponseHandler() {

        StringEntity se = null;

        try {
            //se = new StringEntity(params.toString());
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
          e.printStackTrace();
          return;
        }
        client.post(this.getApplicationContext(),"http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users", se, "application/json",new AsyncHttpResponseHandler() {
        //client.post("http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users", params, new AsyncHttpResponseHandler() {

        //client.post(this.getBaseContext(), "http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users", httpEntity, params.toString(), "application/json",new AsyncHttpResponseHandler() {
            //client.get("http://192.168.2.2:9999/useraccount/register/doregister",params ,new AsyncHttpResponseHandler() {
            // When the response returned by REST has Http response code '200'
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {
                // Hide Progress Dialog
                prgDialog.hide();
                Log.d("HTTP", "onSuccess: " + responseBody);

                try {
                    // JSON Object
                    JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        // Set Default Values for Edit View controls
                        setDefaultValues();
                        // Display successfully registered message using Toast
                        Toast.makeText(getApplicationContext(), "You are successfully registered!", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                        errorMsg.setText(obj.getString("error_msg"));
                        Toast.makeText(getApplicationContext(), obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }

                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();

                }

            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                // Hide Progress Dialog
                prgDialog.hide();
                Log.d("HTTP", "onFailure" + statusCode);

                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Requested resource not found", Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Something went wrong at server end", Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Unexpected Error occcured! [Most common Error: Device might not be connected to Internet or remote server is not up and running]", Toast.LENGTH_LONG).show();
                }
            }
        });
    }

    /**
     * Method which navigates from Register Activity to Login Activity
     */
    public void navigatetoLoginActivity(View view) {
        Intent loginIntent = new Intent(getApplicationContext(), LoginActivity.class);
        // Clears History of Activity
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }

    /**
     * Set degault values for Edit View controls
     */
    public void setDefaultValues() {
        nameET.setText("");
        emailET.setText("");
        pwdET.setText("");
    }

}

