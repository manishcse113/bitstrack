package com.bitskeys.track.login;

/**
 * Created by swarnkarn on 1/2/2016.
 */
public class AvatarInfo {
    private byte[] imageBytes = null;
    private String userNameJid = null;

    public AvatarInfo(String userNameJid, byte[] imageBytes) {
        this.userNameJid = userNameJid;
        this.imageBytes = imageBytes;
    }

    public String getUserNameJid() {
        return userNameJid;
    }

    public void setUserNameJid(String userNameJid) {
        this.userNameJid = userNameJid;
    }

    public byte[] getImageBytes() {
        return imageBytes;
    }

    public void setImageBytes(byte[] imageBytes) {
        this.imageBytes = imageBytes;
    }
}
