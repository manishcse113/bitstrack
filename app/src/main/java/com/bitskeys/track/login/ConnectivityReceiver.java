package com.bitskeys.track.login;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;

/**
 * Created by Manish on 10-10-2015.
 */
public class ConnectivityReceiver extends BroadcastReceiver {

    private static final String FILENAME = "GBC ChatConnectio#";
    static boolean IS_NETWORK_AVAILABLE = false;

    static boolean firstTimeCheck = true;
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(FILENAME, "onReceive  action: "
                + intent.getAction());
        IS_NETWORK_AVAILABLE = haveNetworkConnection(context);
        //IS_NETWORK_AVAILABLE this variable in your activities to check networkavailability.
        if (IS_NETWORK_AVAILABLE)
        {
            //Send thread about Network available:
        }
    }

    static boolean haveConnectedWifi = false;
    static boolean haveConnectedMobile = false;
    private static boolean haveNetworkConnection(Context context) {
        try {
            ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo[] netInfo = cm.getAllNetworkInfo();
            for (NetworkInfo ni : netInfo) {
                if (ni.getTypeName().equalsIgnoreCase("WIFI"))
                    if (ni.isConnected())
                        haveConnectedWifi = true;
                if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
                    if (ni.isConnected())
                        haveConnectedMobile = true;
            }
        }
        catch (Exception e)
        {
            Log.e(FILENAME, "Exception occurred");
                    e.printStackTrace();
        }
        IS_NETWORK_AVAILABLE = haveConnectedWifi || haveConnectedMobile;
        return IS_NETWORK_AVAILABLE; // || haveConnectedMobile;
    }

    public static boolean isConnected(final Context context)
    {
        if (firstTimeCheck == true)
        {
           IS_NETWORK_AVAILABLE = haveNetworkConnection(context);
        }
        return IS_NETWORK_AVAILABLE;
    }

    public static void setConnectionStatus(boolean connectionStatus)
    {
        IS_NETWORK_AVAILABLE = connectionStatus;
    }
}