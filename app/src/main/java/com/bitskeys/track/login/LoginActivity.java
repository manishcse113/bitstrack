package com.bitskeys.track.login;

/**
 * Created by Manish on 02-07-2015.
 */

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.os.Message;
import android.os.Messenger;
import android.os.RemoteException;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.RestAPI.OpenfireRestAPI;
import com.bitskeys.track.profile.ProfileInfo;
import com.bitskeys.track.db.DataSource;
import com.bitskeys.track.db.MyRecord;
import com.bitskeys.track.fragments.LoginFragment;
import com.bitskeys.track.fragments.PostRegInitFragment;
import com.bitskeys.track.fragments.ProfileCreateFragment;
import com.bitskeys.track.fragments.RegisterFragment;
import com.bitskeys.track.fragments.WelcomeFragment;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.Utility;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.nio.ByteBuffer;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
/**
 * Login Activity Class
 */
public class LoginActivity extends Activity implements LoginFragment.LoginFragmentCallBack,ProfileCreateFragment.ProfileCreateFragmentCallBack{
        //extends Activity {
    // Progress Dialog Object
    ProgressDialog prgDialog;
    // Error Msg TextView Object
//    TextView errorMsg;
//    // Email Edit View Object
//    EditText emailET;
//    // Passwprd Edit View Object
//    EditText pwdET;

    public static boolean autologin = false;
    String mUserName = null;
    String mPassword= null;

    //AbstractXMPPConnection mConnection = null;
    private final String FILENAME = "GBC LoginActivity#";

    public String getUserName()
    {
        return mUserName;
    }

    public void setUserName(String userName)
    {
        mUserName = userName;
    }

    public String getPassword()
    {
        return mPassword;
    }

    public void setPassword(String password)
    {
        mPassword = password;
    }


    public void createLoginFragment()
    {
        FragmentManager managerF = getFragmentManager();
        if (managerF != null) {
            FragmentTransaction transaction = managerF.beginTransaction();
            LoginFragment pLoginFrag = new LoginFragment();
            transaction.replace(R.id.base_fragment_layout, pLoginFrag);
            transaction.addToBackStack(null);
            //transaction.add(R.id.base_fragment_layout,pLoginFrag,"LoginFragment");
            transaction.commit();
        }
    }

    public void createRegisterFragment()
    {
        FragmentManager managerF = getFragmentManager();
        if (managerF != null) {
            FragmentTransaction transaction = managerF.beginTransaction();
            RegisterFragment pRegFrag = new RegisterFragment();
            transaction.replace(R.id.base_fragment_layout, pRegFrag);
            transaction.addToBackStack(null);
            //transaction.add(R.id.base_fragment_layout,pLoginFrag,"LoginFragment");
            transaction.commit();
        }
    }

    public void createWelcomeFragment()
    {
        FragmentManager managerF = getFragmentManager();
        if (managerF != null) {
            FragmentTransaction transaction = managerF.beginTransaction();
            WelcomeFragment pWelcomeFrag = new WelcomeFragment();
            transaction.replace(R.id.base_fragment_layout, pWelcomeFrag);
            transaction.addToBackStack(null);
            //transaction.add(R.id.base_fragment_layout,pLoginFrag,"LoginFragment");
            transaction.commit();
        }
    }

    public void PostRegInitFragment()
    {
        FragmentManager managerF = getFragmentManager();
        if (managerF != null) {
            FragmentTransaction transaction = managerF.beginTransaction();
            PostRegInitFragment pPostRegFrag = new PostRegInitFragment();
            transaction.replace(R.id.base_fragment_layout, pPostRegFrag);
            transaction.addToBackStack(null);
            //transaction.add(R.id.base_fragment_layout,pLoginFrag,"LoginFragment");
            transaction.commit();
        }
    }




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.root_fragment_layout);
        doBindService();
        //autoLogin functionality to be done:
        //read DB, login automatically - if record is found:
        autoLogin();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        Log.d(FILENAME, "onDestroy");
        finishActivity(0);
        //doUnbindService();
    }

    public ProfileInfo createProfileInfo()
    {
        EditText firstName =(EditText) findViewById(R.id.firstName);
        EditText lastName = (EditText) findViewById(R.id.lastName);
        EditText nickName = (EditText) findViewById(R.id.nick_name);
        EditText emailId = (EditText)  findViewById(R.id.email_id);
        EditText orgName = (EditText)  findViewById(R.id.org_name);
        ProfileInfo pInfo = null;

        pInfo = OpenfireRestAPI.getProfileInfo();

        if(pInfo == null)
        {
            pInfo = new ProfileInfo(firstName.getText().toString(),
                    lastName.getText().toString(),
                    nickName.getText().toString(),
                    emailId.getText().toString());
        }
        else
        {
            pInfo.setFirstName(firstName.getText().toString());
            pInfo.setLastName(lastName.getText().toString());
            pInfo.setNickName(nickName.getText().toString());
            pInfo.setEmaildIdOff(emailId.getText().toString());
            pInfo.setEmailIdHome(emailId.getText().toString());
        }


        pInfo.setOrganizationName(orgName.getText().toString());
        pInfo.setEmaildIdOff(emailId.getText().toString());

//        ImageView imageView = (ImageView)findViewById(R.id.photo_img_view);
//        imageView.setDrawingCacheEnabled(true);
//        imageView.buildDrawingCache();
//        Bitmap bm = imageView.getDrawingCache();
//        ByteArrayOutputStream stream = new ByteArrayOutputStream();
//        bm.compress(Bitmap.CompressFormat.PNG, 100, stream);
//        byte[] byteArray = stream.toByteArray();
//        pInfo.setAvatarImage(byteArray);

        if(OpenfireRestAPI.tempAvatarByte != null)
            pInfo.setAvatarImage(OpenfireRestAPI.tempAvatarByte);

        OpenfireRestAPI.setProfileInfo(pInfo);
        return pInfo;
    }

    public void OnProfileImageClick(View view)
    {
        boolean isProfilePickDisabled = false;

        /* It will create profile info from Fragment Screen & save it static ProfileInfo in
        calss OpenfireRestAPI
         */
        if(!isProfilePickDisabled)
        {
            ProfileInfo pInfo = OpenfireRestAPI.getProfileInfo();
            if(pInfo == null)
                createProfileInfo();

            Intent intent = new Intent();
            intent.setType("image/*");
            intent.setAction(Intent.ACTION_GET_CONTENT);
            intent.addCategory(Intent.CATEGORY_OPENABLE);
            startActivityForResult(intent, 1);
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode ==1 && resultCode == Activity.RESULT_OK)
        {
            Bitmap bitmap = null;
            ImageView imageView = (ImageView)findViewById(R.id.photo_img_view);
            if(imageView != null)
            {
                imageView.setBackground(null);
                imageView.setImageBitmap(null);
            }
            try {

                // We need to recyle unused bitmaps
                if (bitmap != null) {
                    bitmap.recycle();
                }
                InputStream stream = getContentResolver().openInputStream(
                        data.getData());
                bitmap = BitmapFactory.decodeStream(stream);
                stream.close();



                Bitmap newBitmap = Utility.getResizedBitmap(bitmap, 100, 100);
                int newBytesCount = newBitmap.getByteCount();

                Bitmap compressedBitmap = Utility.codec(newBitmap, Bitmap.CompressFormat.JPEG,100);

                imageView.setImageBitmap(compressedBitmap);
                //imageView.setImageBitmap(bitmap);

//                int byteCount = bitmap.getByteCount();
//                ByteBuffer buffer = ByteBuffer.allocate(byteCount);
//                bitmap.copyPixelsToBuffer(buffer);

                int byteCount = compressedBitmap.getByteCount();
                ByteBuffer buffer = ByteBuffer.allocate(byteCount);
                compressedBitmap.copyPixelsToBuffer(buffer);

                byte[] imageByte = buffer.array();
                OpenfireRestAPI.tempAvatarByte = imageByte;
//                ProfileInfo pInfo = OpenfireRestAPI.getProfileInfo();
//                if(pInfo != null)
//                {
//                    pInfo.setAvatarImage(imageByte);
//                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    /**
     * Method gets triggered when Login button is clicked
     *
     * @param view
     */
    public void loginUser(View view) {

        Log.d(FILENAME, "loginUser");
        TextView errorMsg = (TextView) findViewById(R.id.login_error);
        // Find Email Edit View control by ID
        EditText emailET = (EditText) findViewById(R.id.loginEmail);
        // Find Password Edit View control by ID
        EditText pwdET = (EditText) findViewById(R.id.loginPassword);

        // Get Email Edit View Value
        String email = emailET.getText().toString();
        mUserName = email;
        // Get Password Edit View Value
        mPassword = pwdET.getText().toString();

        // Instantiate Http Request Param Object
        RequestParams params = new RequestParams();
        // When Email Edit View and Password Edit View have values other than Null
        if (Utility.isNotNull(email) && Utility.isNotNull(mPassword)) {
            // When Email entered is Valid
            //if(Utility.validate(email)){
            // Put Http parameter username with value of Email Edit View control
            //params.put("username", email);
            // Put Http parameter password with value of Password Edit Value control
            //params.put("password", mPassword);
            // Invoke RESTful Web Service with Http parameters
            new LoginBackground(LoginActivity.this);//.execute();
            //ChatXMPPService.startActionLogin(this, mUserName, mPassword );

        } else {
            Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
        }
    }


    //////////////// Code below this is not used for now ////////////////
    /**
     * Method which navigates from Login Activity to Home Activity
     */
    public void navigatetoHomeActivity() {
        Log.d(FILENAME, "navigatetoHomeActivity");
        Intent homeIntent = new Intent(getApplicationContext(), HomeActivity.class);
        homeIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(homeIntent);
    }

    /**
     * Method gets triggered when Register button is clicked
     *
     * @param view
     */
    public void navigatetoRegisterActivity(View view) {
        Log.d(FILENAME, "navigatetoRegisterActivity");
        Intent loginIntent = new Intent(getApplicationContext(), RegisterActivity.class);
        loginIntent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(loginIntent);
    }

    /**
     * Method that performs RESTful webservice invocations
     *
     * @param
     */
    //public void getConnections(RequestParams params) {
    public void getUsers() {
        // Show Progress Dialog
        //prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        Log.d(FILENAME, "getUsers");
        AsyncHttpClient client = new AsyncHttpClient();
        client.addHeader("Authorization", "664Rk33SFjfs2m7R");
        client.addHeader("Accept", "application/json");
        client.get("http://" + ChatXMPPService.DOMAIN + "/plugins/restapi/v1/users", new AsyncHttpResponseHandler() {
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {
                // Hide Progress Dialog
                prgDialog.hide();
                String str = new String(responseBody);
                //str = responseBody;
                Log.d("HTTP", "onSuccess: " + str);
                try {
                    // JSON Object
                    HashMap<String, String> hmap = new HashMap<String, String>();
                    for (org.apache.http.Header h : headers) {
                        hmap.put(h.getName(), h.getValue());
                        if (h.getName().equals("Content-Length")) {
                            if (h.getValue().equals("0")) {
                                Toast.makeText(getApplicationContext(), "No records found!", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }
                    JSONObject obj = new JSONObject(str);
                    Iterator<String> iter = obj.keys();
                    while (iter.hasNext()) {
                        String key = iter.next();
                        Object value = obj.get(key);
                        Log.d("HTTP", "onSuccess" + value);
                        /*
                        try {
                            Object value = obj.get(key);
                            //JSONObject userObj = new JSONObject(value);
                            Iterator<String> iter2 = value.keys();
                            while (iter2.hasNext()) {
                                String key2 = iter2.next();
                                Object userRecord = userObj.get(key2);
                                Toast.makeText(getApplicationContext(), key2.toString() + userRecord.toString(), Toast.LENGTH_LONG).show();
                            }
                        } catch (JSONException e) {
                            // Something went wrong!
                        }
                        */
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(getApplicationContext(), "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable error) {
                // Hide Progress Dialog
                prgDialog.hide();
                Log.d("HTTP", "onFailure" + statusCode);

                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(getApplicationContext(), "Failed:404" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '404'
                else if (statusCode == 409) {

                    Toast.makeText(getApplicationContext(), "Failed:409" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(getApplicationContext(), "Failed:500" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(getApplicationContext(), "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });
    }



    /**
     * Messenger used for communicating with service.
     */
    Messenger mService = null;
    boolean mServiceConnected = false;
    /**
     * Class for interacting with the main interface of the service.
     */
    final Messenger mMessenger = new Messenger(new IncomingHandler());
    boolean mIsBound;

    class IncomingHandler extends Handler {
        @Override
        public void handleMessage(Message msg) {
            /*
            if (msg.what == ChatXMPPService.MESSAGE_TYPE_USERLIST) {
                Log.d(FILENAME,"IncomingHandler : MESSAGE_TYPE_USERLIST " );
                Bundle b = msg.getData();
                CharSequence text = null;
                if (b != null) {
                    processBundleMsg(b);
                    //text = b.getCharSequence("data");
                } else {
                    text = "Service responded with empty message";
                }
                Log.d("MessengerActivity", "Response: " + text);
            } else {
                super.handleMessage(msg);
            }
            */
        }
    }

    /**
     * Class for interacting with the main interface of the service.
     */
    private ServiceConnection mConn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName className, IBinder service) {
            mService = new Messenger(service);
            mServiceConnected = true;
            // Register our messenger also on Service side:
            Message msg = Message.obtain(null,
                    ChatXMPPService.INTER_TASK_MESSAGE_TYPE_REGISTER_LOGIN);
            msg.replyTo = mMessenger;
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                // We always have to trap RemoteException
                // (DeadObjectException
                // is thrown if the target Handler no longer exists)
                e.printStackTrace();
            }
        }

        /**
         * Connection dropped.
         */
        @Override
        public void onServiceDisconnected(ComponentName className) {
            Log.d(FILENAME, "Disconnected from service.");
            mService = null;
            mServiceConnected = false;
        }
    };
    /**
     * Sends message with text stored in bundle extra data ("data" key).
     *
     * @param code to send
     */
    void sendToService(int code) {
        if (mServiceConnected) {
            Message msg = null;
            if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_LOGIN) {
                Log.d(FILENAME, "sendToService: MESSAGE_TYPE_UNREGISTER_LOGIN");
                //Manish: Send from here, whatever to send to service from activity:
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_LOGIN);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            } else if (code == ChatXMPPService.INTER_TASK_MESSAGE_TYPE_LOGIN_QUERY) {
                Log.d(FILENAME, "sendToService: MESSAGE_TYPE_LOGIN_QUERY");
                msg = Message.obtain(null,
                        ChatXMPPService.INTER_TASK_MESSAGE_TYPE_LOGIN_QUERY);
                Bundle b = new Bundle();
                b.putInt("data", code);
                msg.setData(b);
            }
            try {
                mService.send(msg);
            } catch (RemoteException e) {
                // We always have to trap RemoteException
                // (DeadObjectException
                // is thrown if the target Handler no longer exists)
                e.printStackTrace();
            }
        } else {
            Log.d(FILENAME, "Cannot send - not connected to service.");
        }
    }


    void doBindService() {
        // Establish a connection with the service.  We use an explicit
        // class name because there is no reason to be able to let other
        // applications replace our component.
        Log.d(FILENAME, "binding initiated");
        mIsBound = getApplicationContext().bindService(new Intent(getApplicationContext(), ChatXMPPService.class), mConn, Context.BIND_AUTO_CREATE);
        Log.d(FILENAME, "binding done");
        //mIsBound = true;
    }

    void doUnbindService() {
        if (mIsBound) {
            Log.d(FILENAME, "doUnbindService");
            // If we have received the service, and hence registered with
            // it, then now is the time to unregister.
            if (mService != null) {
                sendToService(ChatXMPPService.INTER_TASK_MESSAGE_TYPE_UNREGISTER_LOGIN);
            }
            // Detach our existing connection.
            unbindService(mConn);
            mIsBound = false;
        }
    }

    //db is expected to have only one record.
    //If record is present then attempt-auto login on the record.
    protected void autoLogin()
    {
        DataSource db = new DataSource(this);
        db.open();
        List<MyRecord> reqs = db.getMyRecords();
        autologin = false;
        String userName = null;
        String password = null;
        String displayName = null;
        for(MyRecord _req : reqs) {
            userName = _req.getUserName();
            password = _req.getPassword();
            OpenfireRestAPI.setUserName(userName);
            ChatXMPPService.mUserName = userName;
            displayName = ChatXMPPService.mDisplayName = _req.getDisplayName();
            OpenfireRestAPI.setPassWord(password);
            OpenfireRestAPI.setDisplayName(_req.getDisplayName());
            /* When App is run first time, record is created in order
             * 1. with valid username and password but with displayname=""
              *2. In profile create screen, displayname="" is replaced by actual displayname and entry is updated.
              * Before completing 2 seconds, if application is stopped, then user record with empty displayname
              * will be found and crash will happen. In case, second step is not completed, it is better
              * to prompt the user for registration again.
              *
              *
              *
             */
            autologin = true;
        }
        db.close();
        if ( autologin == true)
        {
            setUserName(userName);
            setPassword(password);
            startAutoLogin();
        }
        else
        {
            createWelcomeFragment();
            //createLoginFragment();
        }

    }

    public void startAutoLogin()
    {
        Log.d(FILENAME, "loginUser");

        autologin = true;
        // When Email Edit View and Password Edit View have values other than Null
        if (Utility.isNotNull(mUserName) && Utility.isNotNull(mPassword)) {
            new LoginBackground(LoginActivity.this);//.execute();
            finish();
        } else {
            Toast.makeText(getApplicationContext(), "Please fill the form, don't leave any field blank", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public void onPause() {
        super.onPause();
        if ((LoginBackground.progress != null) && LoginBackground.progress.isShowing())
            LoginBackground.progress.dismiss();
        LoginBackground.progress = null;
    }

    @Override
    protected void onNewIntent(Intent intent) {

        Log.d(FILENAME, "onNewIntent");
        handleIntent(intent);
    }

    public void handleIntent(Intent intent)
    {
        Log.d(FILENAME, "handleIntent");
        String action = intent.getAction();
        String type = intent.getType();

        if (ChatXMPPService.isUserListActivityActive() == false)
        {
            autoLogin();
        }
        //if (Intent.ACTION_SEND.equals(action) && type != null) { //If external sharing events received:
        //}
    }

}

