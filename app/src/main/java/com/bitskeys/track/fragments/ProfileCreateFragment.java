package com.bitskeys.track.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.RestAPI.OpenfireRestAPI;
import com.bitskeys.track.profile.ProfileInfo;
import com.bitskeys.track.db.MyRecord;
import com.bitskeys.track.db.ProfileImageRecord;

/**
 * Created by Mr.Neeraj on 04-06-2015.
 */
public class ProfileCreateFragment extends Fragment implements OpenfireRestAPI.UpdateUserCallback {

    OpenfireRestAPI.UpdateUserCallback mUpdateUserCallback;
    ProfileCreateFragmentCallBack mCallback;

    public interface ProfileCreateFragmentCallBack {
        public void setUserName(String userName);
        public String getUserName();
        public String getPassword();
        public void setPassword(String password);
        public void startAutoLogin();
        public ProfileInfo createProfileInfo();
    }

//    public interface UpdateUserCallback
//    {
//        public void onSuccess();
//        public void onFailure(int statusCode);
//        public String getUserName();
//        public String getNickName();
//        public String getPassword();
//        public String getEmailId();
//        public String getUserDisplayName();
//        public String getGroupName();
//    }


    public void onSuccess()
    {
        disableProgressBar();
        mCallback.startAutoLogin();
    }

    public void onFailure(int statusCode)
    {
        //TODO
        disableProgressBar();
        switch (statusCode)
        {
            case 400://bad request, for now ignoring it
                disableProgressBar();
                mCallback.startAutoLogin();
                break;
             default:
                ShowErrorOnFragment("Error Occured with Code:" + String.valueOf(statusCode));
                break;
        }
    }

//    public String getUserName()
//    {
//        if(OpenfireRestAPI.getProfileInfo() != null)
//            return OpenfireRestAPI.getProfileInfo().getFirstName();
//        return null;
//    }
//
//    public String getNickName()
//    {
//        if(OpenfireRestAPI.getProfileInfo() != null)
//            return OpenfireRestAPI.getProfileInfo().getNickName();
//        return  null;
//    }

//    public String getPassword()
//    {
//        return OpenfireRestAPI.getPassword();
//    }

//    public String getEmailId()
//    {
//        if(OpenfireRestAPI.getProfileInfo() != null)
//            return OpenfireRestAPI.getProfileInfo().getEmailIdHome();
//        return  null;
//    }
//
//    /* Nick name is Display Name */
//    public String getUserDisplayName()
//    {
//        if(OpenfireRestAPI.getProfileInfo() != null)
//            return OpenfireRestAPI.getProfileInfo().getNickName();
//        return  null;
//    }
//
//    public String getGroupName()
//    {
//        if(OpenfireRestAPI.getProfileInfo() != null)
//            return OpenfireRestAPI.getProfileInfo().getGroupName();
//        return  null;
//    }


    public void disableProgressBar()
    {
        ProgressBar verifyPB = (ProgressBar)rootView.findViewById(R.id.progressBar_create_prof);
        verifyPB.setVisibility(View.GONE);
    }

    public void enableProgressBar()
    {
        ProgressBar verifyPB = (ProgressBar)rootView.findViewById(R.id.progressBar_create_prof);
        if(verifyPB != null) {
            verifyPB.setVisibility(View.VISIBLE);
//            verifyPB.setProgress(50);
        }

    }

    public void ShowErrorOnFragment(String errorStr)
    {
        TextView errmsg_tv = (TextView)rootView.findViewById(R.id.profile_err_tv);
        errmsg_tv.setText(errorStr);
        errmsg_tv.setTextColor(Color.RED);
    }

    public void ShowInfoOnFragment(String errorStr)
    {
        TextView errmsg_tv = (TextView)rootView.findViewById(R.id.profile_err_tv);
        errmsg_tv.setText(errorStr);
        errmsg_tv.setTextColor(Color.GREEN);
    }





    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        try {
            mCallback = (ProfileCreateFragmentCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement ProfileCreateFragmentCallBack");
        }

        try {
             mUpdateUserCallback = (OpenfireRestAPI.UpdateUserCallback)this;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement UpdateUserCallback");
        }




    }

    private View rootView;
//    ArrayList<ChatInfo> chatInfoArray = new ArrayList<ChatInfo>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.create_profile_layout,container,false);

        return rootView;
    }

    public ProfileInfo  PopulateScreenFromProfileInfo()
    {
        ProfileInfo pInfo = OpenfireRestAPI.getProfileInfo();
        if(pInfo == null)
        {
            return pInfo;
        }
        EditText firstName =(EditText) rootView.findViewById(R.id.firstName);
        firstName.setText(pInfo.getFirstName());
        EditText lastName = (EditText) rootView.findViewById(R.id.lastName);
        lastName.setText(pInfo.getLastName());
        EditText nickName = (EditText) rootView.findViewById(R.id.nick_name);
        nickName.setText(pInfo.getNickName());
        EditText emailId = (EditText)  rootView.findViewById(R.id.email_id);
        emailId.setText(pInfo.getEmaildIdOff());
        EditText orgName = (EditText)  rootView.findViewById(R.id.org_name);
        orgName.setText(pInfo.getOrganizationName());

        ImageView imageView = (ImageView)rootView.findViewById(R.id.photo_img_view);
        byte[] imageByte = pInfo.getImageBytes();
        if(imageByte != null)
        {
            Bitmap bmp = BitmapFactory.decodeByteArray(imageByte,0,imageByte.length);
            imageView.setImageBitmap(bmp);
        }

        return pInfo;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        /*
        If profile already exists on server, profileInfo is retrieved and saved in memory.
        Following call loads the saved profile info and populate the textfields.
        */
        ProgressBar verifyPB = (ProgressBar)rootView.findViewById(R.id.progressBar_create_prof);
        if(verifyPB != null)
            verifyPB.setVisibility(View.GONE);

        ProfileInfo pInfo = PopulateScreenFromProfileInfo();
        Button submitButton = (Button)rootView.findViewById(R.id.submit_profile_button);
        submitButton.setOnClickListener(new Button.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                    enableProgressBar();
                    mCallback.setUserName(OpenfireRestAPI.getUserName());
                    mCallback.setPassword(OpenfireRestAPI.getPassWord());

                    /* Load the Profile Information from Text Fields */
                    ProfileInfo pInfo = mCallback.createProfileInfo();
                    /* Update Entry in DB */
                    MyRecord.storeRecord(
                            getActivity(),
                            OpenfireRestAPI.getUserName(),
                            pInfo.getNickName(),
                            null,
                            OpenfireRestAPI.getPassWord(),
                            null,
                            null);
                    OpenfireRestAPI.setDisplayName(pInfo.getNickName());



                    /* Save to DB */
                    ProfileImageRecord.storeProfileImageRecord(getActivity(),
                            OpenfireRestAPI.getUserName(), pInfo.getFirstName(), pInfo.getLastName(),
                            pInfo.getEmaildIdOff(), pInfo.getOrganizationName(), pInfo.getImageBytes());

                    /* Save the Profile Info on Server */
                    boolean result = OpenfireRestAPI.SaveProfileInfoOnServer(pInfo);
                    if(result == true)
                    {
                        //mCallback.startAutoLogin();
                        OpenfireRestAPI.updateUser(getActivity(),pInfo,mUpdateUserCallback);
                    }
                    else
                    {
                        //TODO Show Error on Screen
                        ShowErrorOnFragment("Please check internet connectivity!!");
                        disableProgressBar();
                    }

                }
            }
        );
    }


}
