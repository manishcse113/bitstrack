package com.bitskeys.track.fragments;

import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.telephony.SmsManager;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

//import com.bitskeys.track.R;

import com.bitskeys.track.R;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Mr.Neeraj on 04-06-2015.
 */
public class LoginFragment extends Fragment {

    LoginFragmentCallBack mCallback;

    // The container Activity must implement this interface so the frag can deliver messages
    public interface LoginFragmentCallBack {
        /** Called by HeadlinesFragment when a list item is selected */
        public void setUserName(String userName);
        public String getUserName();
        public String getPassword();
        public void setPassword(String password);
        public void loginUser(View view);
    }


    private View rootView;
    ProgressDialog prgDialog;

//    ArrayList<ChatInfo> chatInfoArray = new ArrayList<ChatInfo>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.login,container,false);
        return rootView;
    }





    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

        if(mCallback != null)
        {
            EditText emailET = (EditText)rootView.findViewById(R.id.loginEmail);
            EditText pwdET = (EditText)rootView.findViewById(R.id.loginPassword);
            emailET.setText(mCallback.getUserName());
            pwdET.setText(mCallback.getPassword());
            mCallback.setPassword(pwdET.getText().toString());
            mCallback.setUserName(emailET.getText().toString());

            Button loginButton = (Button)rootView.findViewById(R.id.btnLogin);
            mCallback.loginUser(loginButton);

        }
//
//        TextView errorMsg = (TextView)rootView.findViewById(R.id.login_error);
//        EditText emailET = (EditText)rootView.findViewById(R.id.loginEmail);
//        EditText pwdET = (EditText)rootView.findViewById(R.id.loginPassword);
//        if(mCallback != null)
//        {
//            mCallback.setPassword(pwdET.getText().toString());
//            mCallback.setUsername(emailET.getText().toString());
//        }
//        prgDialog = new ProgressDialog(rootView.getContext());
//        prgDialog.setMessage("Please wait...");
//        // Set Cancelable as False
//        prgDialog.setCancelable(false);


    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);

        // This makes sure that the container activity has implemented
        // the callback interface. If not, it throws an exception.
        try {
            mCallback = (LoginFragmentCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
    }
}
