package com.bitskeys.track.fragments;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.profile.RemoteUserProfileActivity;
import com.bitskeys.track.gen.BitMapWorkerRequest;
import com.bitskeys.track.gen.ChatXMPPService;

/**
 * Created by Mr.Neeraj on 04-06-2015.
 */
public class UserActionFragment extends Fragment {

    private View rootView;
    String userName = null;
    String displayName = null;
    TextView displayNameTv = null;
    ImageView avatarImageView = null;
    ProgressBar pb = null;
    ImageButton emailMeButton = null;
    ImageButton trackMeButton = null;
    ImageButton callMeButton = null;
    ImageButton myProfileButton = null;
    ImageButton myHistoryButton = null;
    ImageButton msgMeButton = null;
    boolean isItARoom = false;


    UserActionCallBack mCallback;
    public interface UserActionCallBack {
        public void deleteUserActionFragment();
        public void onNavigateToTrackUserMap();
        public void onClickTrackMe(String userName);
        public void startActionChat(Context context, String param1, String param2);
        public void Vibrate();
        public void userCallSelected(String selectedUserName);

    }

    public static boolean startRemoteProfileActivity(Context ctxt, String userName, String displayName, boolean isItARoom) {
        if (userName == null || userName.length() == 0)
            return false;
        Intent intent = new Intent(ctxt, RemoteUserProfileActivity.class);
        Bundle b = new Bundle();
        b.putString("USERNAME", userName);
        b.putString("DISPLAYNAME", displayName);
        b.putBoolean("ISITAROOM", isItARoom);
        intent.putExtras(b);
        ctxt.startActivity(intent);
        return true;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.chat_user_click, container, false);

        Bundle args = getArguments();
        userName = args.getString("USER_NAME");
        displayName = args.getString("DISPLAY_NAME");
        isItARoom = args.getBoolean("ISITAROOM");

        displayNameTv = (TextView)rootView.findViewById(R.id.display_name_tv);
        displayNameTv.setText(displayName);

        emailMeButton = (ImageButton)rootView.findViewById(R.id.email_me_button);
        trackMeButton = (ImageButton)rootView.findViewById(R.id.trackeme_button);
        callMeButton = (ImageButton)rootView.findViewById(R.id.call_me_button);
        myProfileButton = (ImageButton)rootView.findViewById(R.id.my_profile_button);
        myHistoryButton = (ImageButton)rootView.findViewById(R.id.my_his_button);
        msgMeButton = (ImageButton)rootView.findViewById(R.id.my_msg_button);

        emailMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(mCallback != null)
                {
                    mCallback.deleteUserActionFragment();
                }
            }
        });

        trackMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mCallback != null)
                {
                    mCallback.deleteUserActionFragment();
                    mCallback.onClickTrackMe(userName);

                }
            }
        });

        callMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mCallback != null)
                {
                    mCallback.deleteUserActionFragment();
                    mCallback.userCallSelected(userName);
                }
            }
        });

        myProfileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mCallback != null)
                {
                    mCallback.deleteUserActionFragment();
                    startRemoteProfileActivity(getActivity(), userName, displayName, isItARoom);
                }
            }
        });


        myHistoryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mCallback != null)
                {
                    mCallback.deleteUserActionFragment();
                }
            }
        });

        msgMeButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                if(mCallback != null)
                {
                    mCallback.deleteUserActionFragment();
                    mCallback.Vibrate();
                    mCallback.startActionChat(getActivity(), ChatXMPPService.mUserName.toString(), userName);
                }
            }
        });


//
//        ChatConnection chatConnection = ChatConnection.getInstance();
//        XMPPTCPConnection mConnection = chatConnection.getConnection();
//        if(mConnection != null && mConnection.isConnected())
//        {
//
//        }

        avatarImageView = (ImageView)rootView.findViewById(R.id.profile_img_view);
        int length = avatarImageView.getWidth();
        int height = avatarImageView.getHeight();

        Bitmap bitmap = Utility.getSavedBitmapForUser(userName, getActivity());
        if(bitmap != null)
        {
            avatarImageView.setImageBitmap(null);
            avatarImageView.setImageBitmap(bitmap);
        }
        else
        {
            avatarImageView.setImageResource(R.drawable.avatar);
            BitMapWorkerRequest.AddAndStartBitMapWorker(getResources(),
                    userName,
                    null,
                    avatarImageView.getWidth(),
                    avatarImageView.getHeight(),
                    null,
                    null,
                    null,
                    null,
                    null,
                    R.id.profile_img_view,
                    rootView.getContext(), null);
        }

        /*

        pb = (ProgressBar)rootView.findViewById(R.id.user_action_pbar);
        if(pb != null)
        {
            pb.setVisibility(View.VISIBLE);
        }
        BitMapWorkerRequest.AddAndStartBitMapWorker(getResources(),
                userName,
                avatarImageView,
                length,
                height,
                null,
                null,
                null,
                null,
                pb,
                R.id.profile_img_view,
                rootView.getContext(), null);
         */

        return rootView;
    }


    @Override
    public void onActivityCreated(Bundle savedInstanceState) {

        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mCallback = (UserActionCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }

    }
}
