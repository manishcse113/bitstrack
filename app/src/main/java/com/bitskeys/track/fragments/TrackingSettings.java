package com.bitskeys.track.fragments;

//import android.app.v4.Fragment;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.Spinner;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.db.SharedPreference;
import com.bitskeys.track.map.LocationListenerImpl;
import com.bitskeys.track.gen.TimeClock;

/**
 * Created by Manish on 5/29/2016.
 */
public class TrackingSettings extends Fragment
{
    private static final String FILENAME = "GBC TrackingSettings";

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {


        final View thisDialog = inflater.inflate(R.layout.tracktime_settings, container, false);
        Button btnDateTimeOK = (Button)thisDialog.findViewById(R.id.btnDateTimeOK);
        Button btnDateTimeCancel = (Button)thisDialog.findViewById(R.id.btnDateTimeCancel);

        SharedPreference.getInstance().retreiveSharedPreference(getActivity());
        boolean canGetLocation = SharedPreference.getInstance().getTrackingOn();
        int startDay = SharedPreference.getInstance().getStartTrackingDay();
        int endDay = SharedPreference.getInstance().getLastTrackingDay();
        int startTime = SharedPreference.getInstance().getStartTrackingTime();
        int endTime = SharedPreference.getInstance().getLastTrackingTime();
        int trackingInterval = SharedPreference.getInstance().getTrackingInterval();

        final Spinner spinSelectTrackingPreference = (Spinner)thisDialog.findViewById(R.id.spinSelectTrackingPreference);
        ArrayAdapter<String> startTrackingPreference = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_tracking_picker, getResources().getStringArray(R.array.tracking_preferece));
        //startTrackingPreference.
        spinSelectTrackingPreference.setAdapter(startTrackingPreference);
        if (canGetLocation)
        {
            spinSelectTrackingPreference.setSelection(0);
        }
        else
        {
            spinSelectTrackingPreference.setSelection(1);
        }

        final Spinner spinSelectStartDay = (Spinner)thisDialog.findViewById(R.id.spinSelectStartDay);
        ArrayAdapter<String> startDayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_day_picker, getResources().getStringArray(R.array.days_of_week));
        spinSelectStartDay.setAdapter(startDayAdapter);
        spinSelectStartDay.setSelection(startDay - 1);

        final Spinner spinSelectEndDay = (Spinner)thisDialog.findViewById(R.id.spinSelectEndDay);
        ArrayAdapter<String> endDayAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_day_picker, getResources().getStringArray(R.array.days_of_week));
        spinSelectEndDay.setAdapter(endDayAdapter);
        spinSelectEndDay.setSelection(endDay - 1);

        final Spinner spinSelectStartTime = (Spinner)thisDialog.findViewById(R.id.spinDayStartTime);
        ArrayAdapter<String> startTimeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_hrs_picker, getResources().getStringArray(R.array.hrs_of_day));
        spinSelectStartTime.setAdapter(startTimeAdapter);
        spinSelectStartTime.setSelection(startTime);

        final Spinner spinSelectEndTime = (Spinner)thisDialog.findViewById(R.id.spinDayEndTime);
        ArrayAdapter<String> endTimeAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_hrs_picker, getResources().getStringArray(R.array.hrs_of_day));
        spinSelectEndTime.setAdapter(endTimeAdapter);
        spinSelectEndTime.setSelection(endTime);

        /*
        <string-array name="tracking_interval">
        <item>30s	</item>
        <item>60s	</item>
        <item>2 mins	</item>
        <item>3 mins</item>
        <item>5 mins	</item>
        <item>7 mins	</item>
        <item>10 mins	</item>
        <item>15 mins	</item>
        <item>20 mins	</item>
        <item>30 mins	</item>
        <item>1 hr</item>
        </string-array>
        */
        final Spinner spinSelectTrackingInterval = (Spinner)thisDialog.findViewById(R.id.spinIntervalSeconds);
        ArrayAdapter<String> trackingIntervalAdapter = new ArrayAdapter<String>(getActivity(), R.layout.spinner_item_tracking_interval, getResources().getStringArray(R.array.tracking_interval));
        spinSelectTrackingInterval.setAdapter(trackingIntervalAdapter);
        int intervalindex = convertTrackingIntervalToIndex(trackingInterval);
        spinSelectTrackingInterval.setSelection(intervalindex);

        //final TimePicker tpSelectedTime = (TimePicker)thisDialog.findViewById(R.id.tpSelectedTime);

        btnDateTimeCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //ActionBarActivity thisActivity = (ActionBarActivity) getActivity();
                //thisDialog.dismiss();
                callback.trackingPrefcancelled();
            }
        });

        btnDateTimeOK.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                //ActionBarActivity thisActivity = (ActionBarActivity) getActivity();

                //String strStartDay = null, strEndDay = null, strStartHour = null, strEndHour = null, trackingPref = null;
                boolean canGetLocation;
                int startDay;
                int endDay;
                int startTime;
                int endTime;
                int trackingInterval;

                int trackingPref = spinSelectTrackingPreference.getSelectedItemPosition();
                if (trackingPref==0)
                {
                    canGetLocation = true;
                }
                else
                {
                    canGetLocation = false;
                }
                startDay = spinSelectStartDay.getSelectedItemPosition() + 1;
                endDay = spinSelectEndDay.getSelectedItemPosition() + 1;

                startTime = spinSelectStartTime.getSelectedItemPosition();
                endTime = spinSelectEndTime.getSelectedItemPosition();

                int trackingIntervalIndex = spinSelectTrackingInterval.getSelectedItemPosition();
                trackingInterval = convertTrackingIndexToInterval(trackingIntervalIndex);

                //SharedPreference.getInstance().retreiveSharedPreference(getActivity());
                Context cntxt = getActivity();
                SharedPreference.saveSharedPreferenceTracking(cntxt, canGetLocation);
                SharedPreference.saveSharedPreferenceTrackingStartDay(cntxt, startDay);
                SharedPreference.saveSharedPreferenceTrackingLastDay(cntxt, endDay);
                SharedPreference.saveSharedPreferenceTrackingStartTime(cntxt, startTime);
                SharedPreference.saveSharedPreferenceTrackingStopTime(cntxt, endTime);
                SharedPreference.saveSharedPreferenceTrackingInterval(cntxt, trackingInterval);

                LocationListenerImpl.setTrackingInterval(trackingInterval);
                TimeClock.setTrackingPreference(canGetLocation, startDay, endDay, startTime, endTime);
                callback.trackingPrefOk(canGetLocation);
            }
        });

        return thisDialog;
    }

    //returns the conversion from index to value for tracking Interval
    int convertTrackingIndexToInterval(int trackingIntervalIndex)
    {
        int trackingInterval = 120;
        if (trackingIntervalIndex == 0)
        {
            trackingInterval = 30;
        }
        else if (trackingIntervalIndex == 1)
        {
            trackingInterval = 60;
        }
        else if (trackingIntervalIndex == 2)
        {
            trackingInterval = 120;
        }
        else if (trackingIntervalIndex == 3)
        {
            trackingInterval = 180;
        }
        else if (trackingIntervalIndex == 4)
        {
            trackingInterval = 300;
        }
        else if (trackingIntervalIndex == 5)
        {
            trackingInterval = 420;
        }
        else if (trackingIntervalIndex == 6)
        {
            trackingInterval = 600;
        }
        else if (trackingIntervalIndex == 7)
        {
            trackingInterval = 900;
        }
        else if (trackingIntervalIndex == 8)
        {
            trackingInterval = 1200;
        }
        else if (trackingIntervalIndex == 9)
        {
            trackingInterval = 1800;
        }
        else if (trackingIntervalIndex == 10)
        {
            trackingInterval = 3600;
        }

        return trackingInterval;
    }


    int convertTrackingIntervalToIndex(int trackingInterval)
    {
        int trackingIntervalIndex = 2;
        if (trackingInterval == 30)
        {
            trackingIntervalIndex = 0;
        }
        else if (trackingInterval == 60)
        {
            trackingIntervalIndex = 1;;
        }
        else if (trackingInterval == 120)
        {
            trackingIntervalIndex = 2;;
        }
        else if (trackingInterval == 180)
        {
            trackingIntervalIndex = 3;
        }
        else if (trackingInterval == 300)
        {
            trackingIntervalIndex = 4;
        }
        else if (trackingInterval == 420)
        {
            trackingIntervalIndex = 5;
        }
        else if (trackingInterval == 600)
        {
            trackingIntervalIndex = 6;
        }
        else if (trackingInterval == 900)
        {
            trackingIntervalIndex = 7;
        }
        else if (trackingInterval == 1200)
        {
            trackingIntervalIndex = 8;
        }
        else if (trackingInterval == 1800)
        {
            trackingIntervalIndex = 9;
        }
        else if (trackingInterval == 3600)
        {
            trackingIntervalIndex = 10;
        }

        return trackingIntervalIndex;
    }

    TrackingSettingsCallback callback;
    public interface TrackingSettingsCallback {
        public void trackingPrefcancelled();
        public void trackingPrefOk(boolean canGetLocation);
    }

    public void setCallback(TrackingSettingsCallback callback) {
        Log.d(FILENAME, "setCallback");
        this.callback = callback;
    }
}
