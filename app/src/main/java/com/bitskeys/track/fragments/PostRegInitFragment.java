package com.bitskeys.track.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.db.DataSource;
import com.bitskeys.track.db.MyRecord;
import com.bitskeys.track.gen.ChatXMPPService;

import java.util.List;

/**
 * Created by Mr.Neeraj on 04-06-2015.
 */
public class PostRegInitFragment extends Fragment {


    private View rootView;
//    ArrayList<ChatInfo> chatInfoArray = new ArrayList<ChatInfo>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.initializing_post_reg_layout,container,false);
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        DataSource db = null;
        try{
            db = new DataSource(this.getActivity());
            db.open();
            List<MyRecord> reqs = db.getMyRecords();
            String userName = null;
            String password = null;
            for(MyRecord _req : reqs) {
                userName = _req.getUserName();
                password = _req.getPassword();
            }
            db.close();
            ChatXMPPService.startActionLogin(rootView.getContext(), userName, password, "anything");
        }catch (Exception ex)
        {
            String msg = ex.getMessage();
            Log.d("Exception", msg);
            db.close();
        }


        Button nextButton = (Button)rootView.findViewById(R.id.init_app_next_button);
        nextButton.setOnClickListener(new Button.OnClickListener() {
            @Override
            public void onClick(View v) {
                FragmentManager managerF = getFragmentManager();
                managerF.popBackStack(null, managerF.POP_BACK_STACK_INCLUSIVE);
                FragmentTransaction transaction = managerF.beginTransaction();
                //FOr Testing enabling Country Code Fragment, otherwise it mustbe RegisterFragment
              //  CountryCodeFragment pCCFrag = new CountryCodeFragment();
               // transaction.add(R.id.main_activity_layout, pCCFrag, "CountryCodeFragment");

                //TODO ChatUserListActivity should come here, commenting below lines:Neeraj
//                ChatFragment pChatFrag = new ChatFragment();
//                transaction.add(R.id.main_activity_layout, pChatFrag, "ChatFragment");
//                transaction.commit();
                Button nextButton = (Button)rootView.findViewById(R.id.init_app_next_button);
                nextButton.setVisibility(View.INVISIBLE);
            }
        }
        );
//        chatInfoArray.add(new ChatInfo("Neeraj", "Hi Darling", "Yesterday", 0));
//        chatInfoArray.add(new ChatInfo("Manish","Meeting Kab Hai Bhai!!","Yesterday",1));
//        chatInfoArray.add(new ChatInfo("Amarnath","Darjeeling mein hoon..","Yesterday",2));
//        chatInfoArray.add(new ChatInfo("Preeti","Are you there?","Yesterday",3));
//        chatInfoArray.add(new ChatInfo("Rahul","Dost kal chalte hain","12:35 am",0));
//        chatInfoArray.add(new ChatInfo("Rphit","Project Mujhe outsource karo","15-Aug",0));
//        chatInfoArray.add(new ChatInfo("Praveen","Chandan ko pakad kar rakhna","24-Aug",0));
//
//
//        ListView chatListView  = (ListView)rootView.findViewById(R.id.chat_listview);
//        ChatInfoAdapter contactAdapter = new ChatInfoAdapter(rootView.getContext(),chatInfoArray);
//        chatListView.setAdapter(contactAdapter);
    }

//    public class ChatInfoAdapter extends ArrayAdapter<ChatInfo> {
//
//        public ChatInfoAdapter(Context context, ArrayList<ChatInfo> info) {
//            super(context, R.layout.chat_info_listview_layout, info);
//        }
//
//        @Override
//        public View getView(int position, View convertView, ViewGroup parent) {
//
//            // Get the data item for this position
//            ChatInfo info = getItem(position);
//            // Check if an existing view is being reused, otherwise inflate the view
//            if (convertView == null) {
//                convertView = LayoutInflater.from(getContext()).inflate(R.layout.chat_info_listview_layout, parent, false);
//            }
//
//            // Lookup view for data population
//            TextView contactName = (TextView) convertView.findViewById(R.id.chat_info_name);
//            TextView timeLastMsgRecvd = (TextView) convertView.findViewById(R.id.chat_info_time_last_msg_recvd);
//            TextView lastMsgRecvd = (TextView)convertView.findViewById(R.id.chat_info_last_chat_msg);
//            ImageView statusImageFirst = (ImageView)convertView.findViewById(R.id.msg_status_first);
//            ImageView statusImageSecond = (ImageView)convertView.findViewById(R.id.msg_status_second);
//            ImageView statusImageThird = (ImageView)convertView.findViewById(R.id.msg_status_third);
//            //TextView contactStatus = (TextView) convertView.findViewById(R.id.contact_info_status);
//            //TextView contactLocation = (TextView) convertView.findViewById(R.id.contact_info_location);
//
//            statusImageFirst.setVisibility(View.INVISIBLE);
//            statusImageSecond.setVisibility(View.INVISIBLE);
//            statusImageThird.setVisibility(View.INVISIBLE);
//
//            contactName.setText(info.getName());
//            timeLastMsgRecvd.setText(info.getTimeOfLastMessage());
//            lastMsgRecvd.setText(info.getShortChatText());
//            if(info.getDelivery_status() == 1)
//            {
//                statusImageFirst.setVisibility(View.VISIBLE);
//
//            } else if(info.getDelivery_status() == 2)
//            {
//                statusImageFirst.setVisibility(View.VISIBLE);
//                statusImageSecond.setVisibility(View.VISIBLE);
//            }else if(info.getDelivery_status() == 3)
//            {
//                statusImageFirst.setVisibility(View.VISIBLE);
//                statusImageSecond.setVisibility(View.VISIBLE);
//                statusImageThird.setVisibility(View.VISIBLE);
//            }
////            contactDeviceInof.setText(info.getLoginDeviceInfo());
////            contactStatus.setText(info.getStatus());
////            contactLocation.setText(info.getLocation());
//            return convertView;
//        }
//
//
//    }
}
