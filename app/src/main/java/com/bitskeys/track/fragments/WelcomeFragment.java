package com.bitskeys.track.fragments;

import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.gen.AppGeneralSettings;

/**
 * Created by Mr.Neeraj on 04-06-2015.
 */
public class WelcomeFragment extends Fragment {


    private View rootView;
//    ArrayList<ChatInfo> chatInfoArray = new ArrayList<ChatInfo>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        //rootView = inflater.inflate(R.layout.welcome_fragment_layout,container,false);
        rootView = inflater.inflate(R.layout.welcome_frag_layout,container,false);
        AppGeneralSettings.setFirstTimeRun();
        return rootView;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        Button agreeButton = (Button)rootView.findViewById(R.id.agree_button);
        agreeButton.setOnClickListener(new Button.OnClickListener() {
                                           @Override
                                           public void onClick(View v) {
                                               FragmentManager managerF = getFragmentManager();
                                               managerF.popBackStack(null, managerF.POP_BACK_STACK_INCLUSIVE);

                                               FragmentTransaction transaction = managerF.beginTransaction();
                                               //FOr Testing enabling Country Code Fragment, otherwise it mustbe RegisterFragment
                                               //  CountryCodeFragment pCCFrag = new CountryCodeFragment();
                                               // transaction.add(R.id.main_activity_layout, pCCFrag, "CountryCodeFragment");
                                               RegisterFragment pRegFrag = new RegisterFragment();
                                               transaction.add(R.id.base_fragment_layout, pRegFrag, "RegisterFragment");
                                               transaction.commit();
                                           }
                                       }
        );

    }


}
