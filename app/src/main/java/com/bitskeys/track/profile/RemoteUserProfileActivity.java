package com.bitskeys.track.profile;

import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.app.Activity;
import android.view.View;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.gen.BitMapWorkerRequest;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.login.ChatConnection;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.w3c.dom.Text;

public class RemoteUserProfileActivity extends Activity {



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        //setContentView(R.layout.remote_user_profile);
        setContentView(R.layout.view_user_profile);
        String remoteUserName = null;
        String displayName = null;
        Bundle b = getIntent().getExtras();
        remoteUserName = b.getString("USERNAME");
        displayName = b.getString("DISPLAYNAME");
        boolean isItARoom = b.getBoolean("ISITAROOM");

        TextView displayNameTv = (TextView)findViewById(R.id.remote_profile_display_name);
        if(displayNameTv != null)
            displayNameTv.setText(displayName);

        TextView firstNameTv = (TextView)findViewById(R.id.remote_profile_first_name);
        TextView lastNameTv = (TextView)findViewById(R.id.remote_profile_last_name);
        TextView emailIdTv = (TextView)findViewById(R.id.remote_profile_email);
        TextView orgNameTv = (TextView)findViewById(R.id.remote_profile_org);
        TextView phoneTv = (TextView)findViewById(R.id.remote_profile_phone);
        phoneTv.setText(remoteUserName);

        /*
        ProgressBar pb = (ProgressBar)findViewById(R.id.remote_profile_progress_bar);
        if(pb != null)
        {
            pb.setVisibility(View.VISIBLE);
        }
        */

//        ChatConnection chatConnection = ChatConnection.getInstance();
//        XMPPTCPConnection mConnection = chatConnection.getConnection();
//        if(mConnection != null && mConnection.isConnected())
//        {
//            //String remoteUserName = null;
//
//
//            //BitMapWorkerRequest.AddAndStartBitMapWorker(getResources(), remoteUserName, length, height, avatarImageView, R.id.remote_profile_avatar);
//        }
//        else
//        {
//            // In case no connectivity, disable progressbar
//            if(pb != null)
//            {
//                pb.setVisibility(View.GONE);
//            }
//        }

        ImageView avatarImageView =(ImageView)findViewById(R.id.remote_profile_avatar);
        int length = avatarImageView.getWidth();
        int height = avatarImageView.getHeight();

        Bitmap bitmap = Utility.getSavedBitmapForUser(remoteUserName, this);
        if(bitmap != null)
        {
            avatarImageView.setImageBitmap(null);
            avatarImageView.setImageBitmap(bitmap);
        }
        else {
            BitMapWorkerRequest.AddAndStartBitMapWorker(getResources(),
                    remoteUserName,
                    avatarImageView,
                    length,
                    height,
                    firstNameTv,
                    lastNameTv,
                    emailIdTv,
                    orgNameTv,
                    null,
                    R.id.remote_profile_avatar,
                    this, null);
        }

    }



}
