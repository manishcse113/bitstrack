package com.bitskeys.track.profile;

import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.gen.BitMapWorkerRequest;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.users.UserListLayout;
import com.bitskeys.track.users.UserObject;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Created by Manish on 3/27/2016.
 */
public class GroupUsersListLayout  extends ArrayAdapter {

    public static List groupUsersList = new ArrayList();
    //public static List userObjRoomsList = new ArrayList();
    Context context;
    int groupid;
    private final String FILENAME = "GBC GroupUsersList";
    private UserListLayout.UserListActivityCallback callbackActivity;

    public void setUserListActivityCallback(UserListLayout.UserListActivityCallback callback) {
        Log.d(FILENAME, "setUserListActivityCallback");
        this.callbackActivity = callback;
    }

    public GroupUsersListLayout(Context context, int vg, List groupProfileList) {
        super(context, vg, groupProfileList);
        this.context = context;
        groupid = vg;
        this.groupUsersList.clear();
        this.groupUsersList.addAll(groupProfileList);
        sortUsersList();
    }


    public void sortUsersList() {
        //first sort based on displayname
        Collections.sort(groupUsersList, new Comparator<UserObject>() {
            @Override
            public int compare(final UserObject object1, final UserObject object2) {
                return object1.userDisplayName.compareTo(object2.userDisplayName);
            }
        });

        //now sort on imageId
        Collections.sort(groupUsersList, new Comparator<UserObject>() {
            @Override
            public int compare(final UserObject object1, final UserObject object2) {
                return object1.image.compareTo(object2.image);
            }
        });
        //userObjList.t
    }

    //@Override
    public void add(UserObject object) {
        groupUsersList.add(object);
        super.add(object);
    }


    public void addUsersList(List groupUsersList) {
        //updateUsersList(objectList);
        //search message based on unique id: receiptId and replace the contents.
        for (Object userObject : groupUsersList)
        {
            add((UserObject) (userObject));
        }
        sortUsersList();
        //super.notify();
    }

    public void clear() {
        groupUsersList.clear();
        super.clear();
    }

    public int getCount() {
        return this.groupUsersList.size();
    }

    public UserObject getItem(int index) {
        UserObject userObject = (UserObject) groupUsersList.get(index);
        return userObject;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
        LayoutInflater inflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        // View itemView = inflater.inflate(groupid, parent, false);
        View fragment_list_view = inflater.inflate(R.layout.view_group_profile, null);
        //Log.d(FILENAME, "getView position#"+ position + " view#"+convertView);
        View row = convertView;
        if (row == null) {
            row = inflater.inflate(R.layout.view_group_listlayout, (ViewGroup) fragment_list_view, false);
            populateGroupListLayout(inflater, row, position);

        } else {
            //You get here in case of scroll
            populateGroupListLayout(inflater, row, position);

        }
        //((ViewGroup) fragment_list_view).addView(row);
        return row;
    }

    public View populateGroupListLayout(LayoutInflater inflater, final View user_view, final int position)
    {
        try {
            ImageView avatarView = (ImageView) user_view.findViewById(R.id.avatar);
            boolean isAvatarApplied = false;

            TextView textuserName = (TextView) user_view.findViewById(R.id.username);
            //TextView textStatus = (TextView)user_view.findViewById(R.id.status);
            //textStatus.setText("");

            //String userDisplayName = textuserName.getText().toString();
            //String remoteUserName = ChatXMPPService.getUserName(userDisplayName);

            UserObject uObj = (UserObject)(groupUsersList.get(position));
            //final boolean isItARoom = uObj.isItARoom;

            avatarView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    TextView textuserName = (TextView) user_view.findViewById(R.id.username);

                    String userDisplayName = textuserName.getText().toString();
                    String userName = ChatXMPPService.getUserName(userDisplayName);
                    // Convert the first Character to uppercase.
                    String formatDisplayName = Utility.FormatStringToUserName(userDisplayName);

                    //startRemoteProfileActivity(context, userName, userDisplayName);
                    callbackActivity.onClickForUserAction(userName,userDisplayName, false);

//                    Intent myIntent = new Intent(getContext(), RemoteUserProfileActivity.class);
                  //  TextView textuserName = (TextView) user_view.findViewById(R.id.username);

                  //  String userDisplayName = textuserName.getText().toString();
                  //  String userName = ChatXMPPService.getUserName(userDisplayName);
                    // Convert the first Character to uppercase.
                  //  String formatDisplayName = Utility.FormatStringToUserName(userDisplayName);
                   // callbackActivity.onClickForUserAction(userName,userDisplayName, isItARoom);
                }
            });

            //Bitmap avatarBitmap = Utility.getSavedBitmapForUser(remoteUserName, context);
            Bitmap avatarBitmap = uObj.getMyPhoto();
            if (avatarBitmap != null) {
                avatarView.setImageBitmap(null);
                avatarView.setImageBitmap(avatarBitmap);
            }
            else
            {
                avatarView.setImageResource(R.drawable.avatar);
                BitMapWorkerRequest.AddAndStartBitMapWorker(context.getResources(),
                        uObj.userName,
                        null,
                        avatarView.getWidth(),
                        avatarView.getHeight(),
                        null,
                        null,
                        null,
                        null,
                        null,
                        R.id.avatar, user_view.getContext(), null);
            }
            textuserName = (TextView) user_view.findViewById(R.id.username);
            String userDisplayName = ((UserObject) (groupUsersList.get(position))).userDisplayName; //(String)userDisplayNamelist.get(position);
            // Convert the first Character to uppercase.

            String formatDisplayName = Utility.FormatStringToUserName(userDisplayName);

            if (userDisplayName != null && userDisplayName.length() > 0)
            {
//                textuserName.setText(userDisplayName);
                textuserName.setText(formatDisplayName);
            } else
            {
                textuserName.setText(((UserObject) (groupUsersList.get(position))).userName);//(String)userNamelist.get(position));
            }

            textuserName = (TextView) user_view.findViewById(R.id.username);
            textuserName.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0)
                {
                    Log.d(FILENAME, "onClickListener");
                    String displayName = ((TextView) (arg0)).getText().toString();
                    String lremoteUserName = ChatXMPPService.getUserName(displayName);
                    return;
                }

            });
        }catch (Exception e)
        {

            Log.d(FILENAME, " Exception " + e.getMessage());
        }
        return user_view;
    }

}
