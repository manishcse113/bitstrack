/**
 * Created by Manish on 3/26/2016.
 */

package com.bitskeys.track.profile;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
//import com.bitskeys.track.R;
import com.bitskeys.track.R;
import com.bitskeys.track.chatRoom.MultiChat;
import com.bitskeys.track.gen.BitMapWorkerRequest;
import com.bitskeys.track.gen.ChatXMPPService;
import com.bitskeys.track.gen.Utility;
import com.bitskeys.track.login.ChatConnection;
import com.bitskeys.track.users.ChatUsersListActivity;
import com.bitskeys.track.users.UserListLayout;
import com.bitskeys.track.users.UserObject;

import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.muc.Affiliate;
import org.jivesoftware.smackx.muc.MUCRole;
import org.jivesoftware.smackx.muc.packet.MUCItem;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

public class FragmentRemoteGroupProfile extends ListFragment
{
    private String FILENAME = "RemoteGroupProfileActivity";
    GroupProfileEditCallBack mCallback;

    View myFragmentView;
    View mheaderView;

    //private ListView lv = null;
    GroupUsersListLayout simpleAdpt = null;
    String mRoomName;

    // Container Activity must implement this interface
    public interface GroupProfileEditCallBack {
        public void onGroupProfileEdit(String roomName);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        Log.d(FILENAME, "onCreateView");
        myFragmentView = inflater.inflate(R.layout.view_group_profile, container, false);
        //setContentView(R.layout.view_group_profile);
        //mheaderView = inflater.inflate(R.layout.create_chatroom_header, null);
        //lv = (ListView)(myFragmentView.findViewById(R.id.list));
        //usersListinRoom = (ListView)(myFragmentView.findViewById(R.id.list));

        mheaderView = inflater.inflate(R.layout.view_group_profile_header, null);


        //view = (ListView) inflater.inflate(R.layout.fragment_chatusers_list, container, false);
        // We get the ListView component from the layout
        //lv = (ListView) view.findViewById(R.id.list);

        //registerForContextMenu(lv);
        //return view;

        return myFragmentView;
    }

    @Override
    public void onAttach(Activity activity) {
        Log.d(FILENAME, "onAttach");
        super.onAttach(activity);
        try {
            //mCallback = (UserFragCallBack) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnHeadlineSelectedListener");
        }
        /*
        if (myFragmentView != null) {
            displayNameTv = (TextView) myFragmentView.findViewById(R.id.remote_profile_display_name);
            profileEdit = (TextView) (myFragmentView.findViewById(R.id.remote_profile_edit));
            //pb = (ProgressBar) (myFragmentView.findViewById(R.id.remote_profile_progress_bar));
            avatarImageView = (ImageView) (myFragmentView.findViewById(R.id.remote_profile_avatar));
        }
        */
    }

    @Override
    public void onDetach() {
        Log.d(FILENAME, "onDetach");
        super.onDetach();
    }

    TextView profileEdit;
    TextView displayNameTv;
    //ProgressBar pb;
    ImageView avatarImageView;

    @Override
    public void onActivityCreated(Bundle savedInstanceState)
    {
        super.onActivityCreated(savedInstanceState);
        if (mheaderView != null)
        {
            this.getListView().addHeaderView(mheaderView);
        }

        Log.d(FILENAME, "onActivityCreated");
        mCallback = (GroupProfileEditCallBack)getActivity();

        /*
        if (myFragmentView != null)
        {

            displayNameTv = (TextView) myFragmentView.findViewById(R.id.remote_profile_display_name);
            profileEdit = (TextView) (myFragmentView.findViewById(R.id.remote_profile_edit));
            //pb = (ProgressBar) (myFragmentView.findViewById(R.id.remote_profile_progress_bar));
            avatarImageView = (ImageView) (myFragmentView.findViewById(R.id.remote_profile_avatar));
        }
        */
        //this.setListAdapter(listAdapter);
    }

    public void onProfileView(final String roomName) {

        Log.d(FILENAME, "onProfileView");
        if (myFragmentView == null)
        {
            Log.d(FILENAME, "myFragmentView is null");
            return;
        }

        if (mheaderView == null)
        {
            Log.d(FILENAME, "mHeaderView is null");
            return;
        }

        displayNameTv = (TextView) mheaderView.findViewById(R.id.remote_profile_display_name);
        profileEdit = (TextView) (mheaderView.findViewById(R.id.remote_profile_edit));
        //pb = (ProgressBar) (myFragmentView.findViewById(R.id.remote_profile_progress_bar));
        avatarImageView = (ImageView) (mheaderView.findViewById(R.id.remote_profile_avatar));

        //lv = (ListView)(myFragmentView.findViewById(R.id.list));
        String displayName = null;
        //TextView displayNameTv = (TextView)findViewById(R.id.remote_profile_display_name);
        Log.d(FILENAME, "onProfileView");
        if(displayNameTv != null)
            displayNameTv.setText(roomName);
        //TextView profileEdit = (TextView)findViewById(R.id.remote_profile_edit);
        if (profileEdit!=null)
        {
            profileEdit.setOnClickListener(new View.OnClickListener() {
                public void onClick(View arg0) {
                    Log.d(FILENAME, "" +
                            "profile edit onClick");
                    mCallback.onGroupProfileEdit(roomName);
                    //callbackActivity.userMapSelected(position, ((UserObject) (userObjList.get(position))).userName);//(String)(userNamelist.get(position)));
                }
            });
        }
        //ProgressBar pb = (ProgressBar)findViewById(R.id.remote_profile_progress_bar);
        //if(pb != null)
        //{
        //    pb.setVisibility(View.VISIBLE);
        //}
        //ImageView avatarImageView =(ImageView)findViewById(R.id.remote_profile_avatar);
        int length = avatarImageView.getWidth();
        int height = avatarImageView.getHeight();
        BitMapWorkerRequest.AddAndStartBitMapWorker(getResources(),
                roomName,
                avatarImageView,
                length,
                height,
                null,
                null,
                null,
                null,
                null,
                R.id.remote_profile_avatar,
                getActivity(), null);

        XMPPTCPConnection connection = ChatConnection.getInstance().getConnection();
        if( connection == null)
        {
            Log.d(FILENAME, " Connection is null, group could not be edited");
            return;
        }
        mRoomName = roomName;
        List<Affiliate> usersInRoomList = MultiChat.getMembersOfChatRoom(connection, roomName);
        if (usersInRoomList !=null) {
            addUsers(getActivity(), usersInRoomList);
        }
        addUsers(getActivity(), usersInRoomList);
    }

    public void addUsers(Context context,  List<Affiliate> usersInRoomList) {
        Log.d(FILENAME, "addUsers");
        if (myFragmentView != null)
        {
            // We get the ListView component from the layout
            //ListView lv = (ListView) view.findViewById(R.id.listView);
            //userNameList = ulist;
            //mUsersMap = listobj;
            List userObjList = createUserObjectList(usersInRoomList, context);
            // Very if these users are already present or not..
            // if already present then -- those users shall be removed from the list

            if (simpleAdpt == null) {
                //if (lv !=null) {

                    //simpleAdpt = new UserListLayout(context, vg, id, userNameList, mUsersMap, allUsersOffline);
                    Log.d(FILENAME, "Creating new UserList");
                    simpleAdpt = new GroupUsersListLayout(context,R.id.list, userObjList);
                simpleAdpt.setUserListActivityCallback((UserListLayout.UserListActivityCallback)getActivity());
                setListAdapter(simpleAdpt);
                    //simpleAdpt.setCallback(this);
                    //simpleAdpt.setAmIOffline(amIOffline);
                    //simpleAdpt.setUserListActivityCallback((ChatUsersListActivity) (getActivity()));
                /*
                }
                else
                {
                    Log.d(FILENAME, "Invalid case");
                }
                */
            } else {
                Log.d(FILENAME, "calling addUsersList");
                //simpleAdpt.removeUsersList(userObjList);
                simpleAdpt.addUsersList(userObjList);
                //simpleAdpt.setAmIOffline(amIOffline);
                simpleAdpt.notifyDataSetInvalidated();
            }
        }
    }

    //CreateUserObjectList
    public List createUserObjectList(List<Affiliate> usersInRoomList,Context context)
    {
        List userObjList= new ArrayList();
        Log.d(FILENAME, "createUserObjectList");
        //Map<String, String> map = listobj.mUserPresenceMap;
        int i = 0;
        String userName;
        String presence;
        Integer presenceId;
        for(int j = 0; j < 5; j++)
        {
        /*
        for(Affiliate userAffiliation: usersInRoomList)
        {
           userName = userAffiliation.getNick();
        */
            userName = "+919871363142";
            presenceId = R.drawable.ic_2away; //Hard-coded for now:
            /*
            presence = entry.getValue();
            if (presence.equals(ChatXMPPService.OFFLINE))
            {
                presenceId = R.drawable.ic_3offline_icon;
            }
            else if (presence.equals(ChatXMPPService.ONLINE))
            {
                presenceId = R.drawable.ic_1online_icon;
            }
            else
            {
                presenceId = R.drawable.ic_2away;
            }
            */

            UserObject userObj = new UserObject(userName,ChatXMPPService.getDisplayName(userName), ChatXMPPService.getAvatarId(userName), presenceId, "" , false, false);
            Bitmap bitmap = Utility.getSavedBitmapForUser(userName, context);
            if(bitmap != null)
                userObj.setMyPhoto(bitmap);
            userObjList.add(userObj);
            i++;
        }
        /*
        if (users!=null) {
            updateLocationsList(users,context);
        }
        */
        Log.d(FILENAME, "New/Update Users count## " + i);
        return userObjList;
    }
}
