package com.bitskeys.track.profile;

import android.util.Log;

import com.bitskeys.track.gen.ChatXMPPService;

import org.jivesoftware.smack.SmackException;
import org.jivesoftware.smack.tcp.XMPPTCPConnection;
import org.jivesoftware.smackx.vcardtemp.VCardManager;
import org.jivesoftware.smackx.vcardtemp.packet.VCard;

import java.util.ArrayList;


/**
 * Created by swarnkarn on 1/22/2016.
 */

public class VCardWrappers {
    public static String VCardEmailKey = "VCARD_EMAIL_ID";
    public static String VCardApptAddressKey ="VCARD_APPT_ADDRESS";
    public static String VCardStreetAddressKey ="VCARD_STREET_ADDRESS";
    public static String VCardCityAddressKey ="VCARD_CITY_ADDRESS";
    public static String VCardStateAddressKey ="VCARD_STATE_ADDRESS";
    public static String VCardCountryAddressKey ="VCARD_COUNTRY_ADDRESS";

    public static String organizatioNameKey = "VCARD_ORG_NAME";
    public static String VCardFirstNameKey = "VCARD_FIRST_NAME";
    public static String VCardLastNameKey = "VCARD_LAST_NAME";
    public static String VCardDisplayName = "VCARD_DISPLAY_NAME";

    public static boolean addPropertieSToUser(XMPPTCPConnection mConnection,String userName,String firstName,String lastName,String displayName,
                                              String emailAddress, String orgName,
                                              String apptAddress,
                                              String streetAddress,
                                              String cityAddress,
                                              String stateAddress,
                                              String countryAddress,
                                              byte[] ImageByte)
    {
        ArrayList<VCardProperty> vCardList = new ArrayList<VCardProperty>();

        if(apptAddress != null && apptAddress.length() >0)
        {
            VCardProperty apptAddressProp = new VCardProperty(VCardWrappers.VCardApptAddressKey,apptAddress);
            vCardList.add(apptAddressProp);
        }

        if(streetAddress != null && streetAddress.length() >0)
        {
            VCardProperty streetAddressProp = new VCardProperty(VCardWrappers.VCardStreetAddressKey,streetAddress);
            vCardList.add(streetAddressProp);
        }

        if(cityAddress != null && cityAddress.length() >0)
        {
            VCardProperty cityAddressProp = new VCardProperty(VCardWrappers.VCardCityAddressKey,cityAddress);
            vCardList.add(cityAddressProp);
        }

        if(stateAddress != null && stateAddress.length() >0)
        {
            VCardProperty stateAddressProp = new VCardProperty(VCardWrappers.VCardStateAddressKey,stateAddress);
            vCardList.add(stateAddressProp);
        }

        if(countryAddress != null && countryAddress.length() >0)
        {
            VCardProperty countryAddressProp = new VCardProperty(VCardWrappers.VCardCountryAddressKey,countryAddress);
            vCardList.add(countryAddressProp);
        }

        if(displayName != null && displayName.length() >0)
        {
            VCardProperty displayNameProp = new VCardProperty(VCardWrappers.VCardDisplayName,displayName);
            vCardList.add(displayNameProp);
        }
        if(firstName != null && firstName.length()>0)
        {
            VCardProperty fnameProp = new VCardProperty(VCardFirstNameKey,firstName);
            vCardList.add(fnameProp);
        }

        if(lastName != null && lastName.length() >0)
        {
            VCardProperty lnameProp = new VCardProperty(VCardLastNameKey,lastName);
            vCardList.add(lnameProp);
        }

        if(emailAddress != null && emailAddress.length() >0)
        {
            VCardProperty emailProp = new VCardProperty(VCardEmailKey, emailAddress);
            vCardList.add(emailProp);
        }

        if(orgName != null && orgName.length() >0)
        {
            VCardProperty orgProp = new VCardProperty(organizatioNameKey, orgName);
            vCardList.add(orgProp);
        }

        if( addPropertiesToUser(mConnection,userName,vCardList,ImageByte) != null)
            return true;

        return false;
    }

    public static ArrayList<VCardProperty> addPropertiesToUser(XMPPTCPConnection mConnection,String userName,ArrayList<VCardProperty> vCardList,byte[] ImageByte)
    {
        if(mConnection == null || !mConnection.isConnected())
            return null;
        VCardManager vCardManager = VCardManager.getInstanceFor(mConnection);
        if(vCardManager == null )
            return null;

        if(vCardList == null || vCardList.size() == 0)
            return null;

        VCard vcard = null;

        ArrayList<VCardProperty> tempList = new ArrayList<VCardProperty>();

        for(int i=0;i<vCardList.size();i++)
        {
            try
            {
                // Load vCard for JID
                String JID = userName + "@" + ChatXMPPService.DOMAINJID_BARE;
                vcard = vCardManager.loadVCard(JID);

                //VCardProperty
                VCardProperty prop = vCardList.get(i);

                /* This must never come */
                if(prop == null)
                    break;

                if( prop.getProperty() == null || prop.getProperty().length() ==0 ||
                        prop.getValue() == null || prop.getValue().length() == 0
                        )
                {
                    /* try other property in list*/
                    continue;
                }

                //Reaching here means, all validation passed
                // Set Property in VCard
                if(prop.getProperty().equals(VCardWrappers.VCardDisplayName))
                {
                    vcard.setNickName(prop.getValue());

                }else if( prop.getProperty().equals(VCardWrappers.VCardEmailKey))
                {
                    vcard.setEmailWork(prop.getValue());
                }
                else
                {
                    vcard.setField(prop.getProperty(),prop.value);
                }




                //add to tempList, which will returned by this method so that user of this function
                // can verify which property were added successfully by comparing with input list
                tempList.add(i,prop);

            }
            catch (Exception ex)
            {

            }
        }/* for */

        if(tempList.size() >0)
        {
            try
            {
                if(ImageByte.length >0)
                    vcard.setAvatar(ImageByte);

                vCardManager.saveVCard(vcard);

            }
            catch (Exception ex)
            {
                String msg = ex.getMessage();
                Log.d("DEBUG",msg);
                tempList = null;
            }

            return tempList;
        }

        return null;
    }

    public static ArrayList<VCardProperty> addPropertiesToUser(XMPPTCPConnection mConnection,String userName,ArrayList<VCardProperty> vCardList)
    {
        if(mConnection == null || !mConnection.isConnected())
            return null;

        VCardManager vCardManager = VCardManager.getInstanceFor(mConnection);
        if(vCardManager == null )
            return null;

        if(vCardList == null || vCardList.size() == 0)
            return null;

        VCard vcard = null;

        ArrayList<VCardProperty> tempList = new ArrayList<VCardProperty>();

        for(int i=0;i<vCardList.size();i++)
        {
            try
            {
                // Load vCard for JID
                String JID = userName + "@" + ChatXMPPService.DOMAINJID_BARE;
                vcard = vCardManager.loadVCard(JID);

                //VCardProperty
                VCardProperty prop = vCardList.get(i);

                /* This must never come */
                if(prop == null)
                    break;

                if( prop.getProperty() == null || prop.getProperty().length() ==0 ||
                        prop.getValue() == null || prop.getValue().length() == 0
                        )
                {
                    /* try other property in list*/
                    continue;
                }

                //Reaching here means, all validation passed
                // Set Property in VCard
                vcard.setField(prop.getProperty(), prop.value);


                //add to tempList, which will returned by this method so that user of this function
                // can verify which property were added successfully by comparing with input list
                tempList.add(i,prop);

            }
            catch (Exception ex)
            {

            }
        }/* for */

        if(tempList.size() >0)
        {
            try
            {
                vCardManager.saveVCard(vcard);

            }
            catch (Exception ex)
            {
                String msg = ex.getMessage();
                Log.d("DEBUG",msg);
                tempList = null;
            }

            return tempList;
        }

        return null;
    }

    public static boolean addPropertiesToUser(XMPPTCPConnection mConnection,String userName,String property,String Value)
    {
        if(mConnection == null || !mConnection.isConnected())
            return false;
        VCardManager vCardManager = VCardManager.getInstanceFor(mConnection);
        if(vCardManager == null )
            return false;

        try
        {
            // Load vCard for JID
            String JID = userName + "@" + ChatXMPPService.DOMAINJID_BARE;
            VCard vcard = vCardManager.loadVCard(JID);

            //Set Property in VCard
            vcard.setField(property,Value);

            //Save VCard
            vCardManager.saveVCard(vcard);

        }
        catch (Exception ex)
        {

        }
        return false;
    }
    public static VCard loadUserVCard(XMPPTCPConnection mConnection,String userName)
    {
        if(mConnection == null || !mConnection.isConnected())
            return null;
        VCardManager vCardManager = VCardManager.getInstanceFor(mConnection);
        if(vCardManager == null )
            return null;

        boolean isTimeOut=false;
        for(int i =0;i<5;i++)
        {

            if(isTimeOut)
            {
                mConnection.setPacketReplyTimeout(10000);
            }

            try
            {
                String JID = userName + "@" + ChatXMPPService.DOMAINJID_BARE;
                VCard vcard = vCardManager.loadVCard(JID);
                return vcard;
            }
            catch (SmackException.NoResponseException ex)
            {
                isTimeOut = true;

            }
            catch (Exception ex)
            {
                String msg = ex.getMessage();
                Log.e("VCardExc:",msg);
            }
        }

        return  null;
    }

    public static VCard loadMyVCard(XMPPTCPConnection mConnection)
    {
        if(mConnection == null || !mConnection.isConnected())
            return null;

        VCardManager vCardManager = VCardManager.getInstanceFor(mConnection);
        try
        {
            VCard vcard = vCardManager.loadVCard();
            return vcard;
        }
        catch (Exception ex)
        {
            //TODO
        }
        return null;
    }

    public static boolean saveVCard(XMPPTCPConnection mConnection,VCard vCard)
    {
        if(mConnection == null || !mConnection.isConnected())
            return false;

        VCardManager vCardManager = VCardManager.getInstanceFor(mConnection);
        if(vCardManager == null)
            return false;

        try
        {
            VCard oldVCard = vCardManager.loadVCard();
            String displayName = vCard.getNickName();
            String firstName = vCard.getField(VCardWrappers.VCardFirstNameKey);
            String lastName = vCard.getField(VCardWrappers.VCardLastNameKey);
            String orgName = vCard.getField(VCardWrappers.organizatioNameKey);
            String addrStreet = vCard.getField(VCardStreetAddressKey);
            String addrCity = vCard.getField(VCardCityAddressKey);
            String email = vCard.getEmailWork();

            oldVCard.setNickName(displayName);
            oldVCard.setNickName(displayName);
            oldVCard.setField(VCardWrappers.VCardFirstNameKey, firstName);
            oldVCard.setFirstName(firstName);
            oldVCard.setField(VCardWrappers.VCardLastNameKey, lastName);
            oldVCard.setLastName(lastName);
            oldVCard.setField(VCardWrappers.organizatioNameKey, orgName);
            oldVCard.setOrganization(orgName);
            oldVCard.setField(VCardStreetAddressKey, addrStreet);
            oldVCard.setField(VCardCityAddressKey, addrCity);
            oldVCard.setEmailWork(email);
            oldVCard.setAvatar(vCard.getAvatar());

            vCardManager.saveVCard(oldVCard);
            return true;
        }
        catch (Exception ex)
        {
            //TODO
        }
        return false;
    }

}
