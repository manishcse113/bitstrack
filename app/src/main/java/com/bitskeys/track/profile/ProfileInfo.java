package com.bitskeys.track.profile;

import com.bitskeys.track.gen.ChatXMPPService;

import org.jivesoftware.smackx.vcardtemp.packet.VCard;

/**
 * Created by swarnkarn on 12/27/2015.
 */
public class ProfileInfo {

    String firstName;
    String lastName;
    String EmailIdHome;
    String EmaildIdOff;
    String OrganizationName;
    String AddressFieldStreet;
    String AddressFieldCity;
    String AddressFieldState;
    String AddressFieldCountry;
    String AddressFieldPinCode;
    String WorkPhone;
    String HomePhone;
    String NickName;//Display Name;
    String GroupName;
    byte[] ImageBytes;

    public ProfileInfo(String firstName, String lastName,String nickName, String emailId) {
        this.firstName = firstName;
        this.lastName = lastName;
        EmailIdHome = emailId;
        EmaildIdOff=emailId;
        OrganizationName="";
        AddressFieldStreet="";
        AddressFieldCity="";
        AddressFieldState="";
        AddressFieldCountry="";
        AddressFieldPinCode="";
        WorkPhone="";
        HomePhone="";
        NickName=nickName;//Display Name;
        GroupName = ChatXMPPService.GROUPNAME;
    }


    public void setGroupName(String groupName)
    {
        GroupName = groupName;
    }
    public String getGroupName()
    {
        return GroupName;
    }
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getOrganizationName() {
        return OrganizationName;
    }

    public void setOrganizationName(String organizationName) {
        OrganizationName = organizationName;
    }

    public String getEmailIdHome() {
        return EmailIdHome;
    }

    public void setEmailIdHome(String emailId) {
        EmailIdHome = emailId;
    }

    public String getEmaildIdOff() {
        return EmaildIdOff;
    }

    public void setEmaildIdOff(String emailId) {
        EmaildIdOff = emailId;
    }

    public String getAddressFieldStreet() {
        return AddressFieldStreet;
    }

    public void setAddressFieldStreet(String addressFieldStreet) {
        AddressFieldStreet = addressFieldStreet;
    }

    public String getAddressFieldCity() {
        return AddressFieldCity;
    }

    public void setAddressFieldCity(String addressFieldCity) {
        AddressFieldCity = addressFieldCity;
    }

    public String getAddressFieldCountry() {
        return AddressFieldCountry;
    }

    public void setAddressFieldCountry(String addressFieldCountry) {
        AddressFieldCountry = addressFieldCountry;
    }

    public String getAddressFieldState() {
        return AddressFieldState;
    }

    public void setAddressFieldState(String addressFieldState) {
        AddressFieldState = addressFieldState;
    }

    public String getAddressFieldPinCode() {
        return AddressFieldPinCode;
    }

    public void setAddressFieldPinCode(String addressFieldPinCode) {
        AddressFieldPinCode = addressFieldPinCode;
    }

    public String getWorkPhone() {
        return WorkPhone;
    }

    public void setWorkPhone(String workPhone) {
        WorkPhone = workPhone;
    }


    public String getHomePhone() {
        return HomePhone;
    }

    public void setHomePhone(String workPhone) {
        HomePhone = workPhone;
    }

    public String getNickName() {
        return NickName;
    }

    public void setNickName(String nickName) {
        NickName = nickName;
    }

    public void setAvatarImage(byte[] Image)
    {
        ImageBytes = Image;
    }

    public byte[] getImageBytes()
    {
        return  ImageBytes;
    }

    public VCard getVCard()
    {
        VCard vcard = new VCard();
        vcard.setField(VCardWrappers.VCardFirstNameKey,firstName);
        vcard.setFirstName(firstName);
        vcard.setField(VCardWrappers.VCardLastNameKey,lastName);
        vcard.setLastName(lastName);
        vcard.setField(VCardWrappers.VCardEmailKey,getEmaildIdOff());
        vcard.setEmailWork(getEmaildIdOff());
        vcard.setEmailHome(getEmailIdHome());
        vcard.setField(VCardWrappers.organizatioNameKey,getOrganizationName());
        vcard.setOrganization(getOrganizationName());
        vcard.setField(VCardWrappers.VCardStreetAddressKey,getAddressFieldStreet());
        vcard.setField(VCardWrappers.VCardCityAddressKey, getAddressFieldCity());
        vcard.setField(VCardWrappers.VCardStateAddressKey,getAddressFieldState());
        vcard.setField(VCardWrappers.VCardCountryAddressKey,getAddressFieldCountry());
        vcard.setAvatar(ImageBytes);
        return vcard;
    }
}
