package com.bitskeys.track.profile;

import android.content.Context;
import android.os.CountDownTimer;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.bitskeys.track.db.MyRecord;
import com.bitskeys.track.fragments.RegisterFragment;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;

import org.apache.http.entity.StringEntity;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.util.HashMap;


/**
 * Created by swarnkarn on 11/20/2015.
 */
public class UserRegistration {

    private Context appContext;
    String userName;
    String displayName;
    String password;
    String group;
    private View rootView;
    CountDownTimer cTimer = null;
    boolean isTimerExpired=false;

    /* STATE MACHINE
    Valid states transition can only be :
    * RMS_REGISTRATION_NOT_STARTED==>RMS_REGISTRATION_IN_PROGRESS
    *
    * RMS_REGISTRATION_IN_PROGRESS==>RMS_REGISTRATION_FINISHED OR
    * RMS_REGISTRATION_IN_PROGRESS==>RMS_REGISTRATION_CANCELLED
    *
    * RMS_REGISTRATION_CANCELLED==>RMS_REGISTRATION_IN_PROGRESS
    *
    * RMS_REGISTRATION_FINISHED , no further state transition from this state
    *
    *
    * */

    enum reg_state_machine {
        RMS_REGISTRATION_NOT_STARTED,
        RMS_REGISTRATION_IN_PROGRESS,
        RMS_REGISTRATION_CANCELLED,
        RMS_REGISTRATION_FINISHED
    };

    reg_state_machine rms_curr_state= reg_state_machine.RMS_REGISTRATION_NOT_STARTED;



    AsyncHttpClient clientUser = null;
    AsyncHttpClient clientDisplay=null;
    AsyncHttpClient clientGroup=null;

    RegisterFragment regFrag;
    String serverAddress = "http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users";
    GrocHttpError errCode = new GrocHttpError();
    public UserRegistration(RegisterFragment regFrag, Context appContext, View rootView, String userName,String displayName, String password, String group) {
        this.appContext = appContext;
        this.userName = userName;
        this.displayName = displayName;
        this.password = password;
        this.group = group;
        this.rootView = rootView;
        this.regFrag = regFrag;
    }

    public boolean isUserExists()
    {
        /* TODO */
        return false;
    }

    public void CancelAllHttpRequests()
    {
        if(clientUser != null)
            clientUser.cancelRequests(appContext,true);
        if(clientDisplay != null)
            clientDisplay.cancelRequests(appContext,true);
        if(clientGroup != null)
            clientGroup.cancelRequests(appContext,true);
    }

    public void handleTimerExpiry()
    {
        ChangeRMSState(reg_state_machine.RMS_REGISTRATION_CANCELLED);
        CancelAllHttpRequests();
        regFrag.disableProgressBar();
        regFrag.ShowErrorOnFragment("Unable to recieve SMS within 30 seconds, please check Mobile Network!!");
    }

    public void CancelTimer()
    {
        if(cTimer != null)
        {
            cTimer.cancel();
        }
    }
    public void Initialize()
    {

        ChangeRMSState(reg_state_machine.RMS_REGISTRATION_IN_PROGRESS);
        isTimerExpired = false;
//        cTimer = new CountDownTimer(30000, 1000) {
//
//            public void onTick(long millisUntilFinished) {
//                //mTextField.setText("seconds remaining: " + millisUntilFinished / 1000);
//            }
//
//            public void onFinish() {
//                handleTimerExpiry();
//            }
//        }.start();

    }

    public boolean ChangeRMSState(reg_state_machine new_state)
    {
        boolean state_changed = false;


        switch(rms_curr_state)
        {
            case  RMS_REGISTRATION_NOT_STARTED:
                if(new_state == reg_state_machine.RMS_REGISTRATION_IN_PROGRESS)
                    rms_curr_state = new_state;
                    state_changed= true;
                break;
            case RMS_REGISTRATION_IN_PROGRESS:
                if(new_state == reg_state_machine.RMS_REGISTRATION_CANCELLED ||
                        new_state == reg_state_machine.RMS_REGISTRATION_FINISHED
                        ) {
                    rms_curr_state = new_state;
                    state_changed = true;
                }

                break;
            case RMS_REGISTRATION_CANCELLED:
                if(new_state == reg_state_machine.RMS_REGISTRATION_IN_PROGRESS)
                    state_changed= true;
                break;
            case RMS_REGISTRATION_FINISHED:
                break;

        }
        if( !state_changed )
            Log.d("Invalid Reg State", rms_curr_state.toString() + "==>" + new_state.toString());

        return state_changed;
    }

    public void SaveUserInfoIntoDB()
    {
        MyRecord.clearFromDB(appContext);
        MyRecord.storeRecord(appContext, userName,displayName, null, password, null, null);//group, mail, avatar are null for now
    }
    public void RegisterUser()
    {

        if( !userName.isEmpty()&& !password.isEmpty())
        {
            JSONObject json = new JSONObject();
            try
            {
                json.put("username",userName);
                json.put("password",password);
                /* Start a timer here for 30 seconds */

                invokeWS2(json.toString());

                JSONObject jsonGrp = new JSONObject();
                try {
                    jsonGrp.put("groupname", this.group);

                } catch (JSONException e) {
                    e.printStackTrace();
                    return;
                }

                addToDefaultGroup(jsonGrp.toString(), this.userName , group);
                addDisplayName(this.userName, this.displayName);
                //addDisplayName(this.userName, "Dummy");
            }
            catch (Exception ex)
            {
                Log.d("json", "Unable to create JSON object");
                ex.printStackTrace();
                errCode.setErrorCode(400);
                errCode.setErrorMessage(ex.getMessage());
            }


        }
        else
        {
            errCode.setErrorCode(400);
            errCode.setErrorMessage("Invalid Parameter: username or password is empty string");
        }

        return;
    }

    public void addDisplayName(String userName,String displayName)
    {
        JSONObject jsonObj = new JSONObject();
        try {
            jsonObj.put("name", displayName);

        } catch (JSONException e) {
            e.printStackTrace();
            return;
        }

        String st = jsonObj.toString();
        clientDisplay = new AsyncHttpClient(true,9090,443);
        clientDisplay.addHeader("Authorization", "664Rk33SFjfs2m7R");
        clientDisplay.addHeader("Accept", "application/json");
        String userUrl = "http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users/" + userName;
        StringEntity se = null;
        try {
            se = new StringEntity(st);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }

        clientDisplay.put(appContext, userUrl, se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {
                try {
                    // JSON Object
                    HashMap<String, String> hmap = new HashMap<String, String>();
                    for (org.apache.http.Header h : headers) {
                        hmap.put(h.getName(), h.getValue());
                        if (h.getName().equals("Content-Length")) {
                            if (h.getValue().equals("0")) {
                                Toast.makeText(appContext, "Your name successfully updated!", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }
                    JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        // Set Default Values for Edit View controls
                        //setDefaultValues();
                        // Display successfully registered message using Toast
                        Toast.makeText(appContext, "Your name successfully updated!", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                        //errorMsg.setText(obj.getString("error_msg"));
                        Toast.makeText(appContext, obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(appContext, "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {


                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(appContext, "Failed:404" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '404'
                else if (statusCode == 409) {
                    regFrag.ShowInfoOnFragment("Existing Account Found");
                    Toast.makeText(appContext, "Failed:409" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(appContext, "Failed:500" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(appContext, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });


    }
    public void addToDefaultGroup(String jsonString, String userName, String groupName) {
        String st = null;
//        // Show Progress Dialog
//        prgDialog.show();
        // Make RESTful webservice call using AsyncHttpClient object
        clientGroup = new AsyncHttpClient();
        //String  contentType = "string/xml;UTF-8";
        clientGroup.removeHeader("Content-Type");
        //client.addHeader("Content-Type", "application/json");
        clientGroup.addHeader("Authorization", "664Rk33SFjfs2m7R");
        //client.addHeader("Content-Type", "application/json");
        clientGroup.addHeader("Accept", "application/json");
        //client.setBasicAuth("manish", "nswdel10");

        String userUrl = "http://ec2-54-148-100-240.us-west-2.compute.amazonaws.com:9090/plugins/restapi/v1/users/";

        String grpUrl = userUrl + "/" + userName + "/groups";
//        Log.d(FILENAME, "addToDefaultGroup");

        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return;
        }


        clientGroup.post(appContext,grpUrl, se, "application/json",new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {
                // Hide Progress Dialog
//                prgDialog.hide();
//                Log.d(FILENAME, "http onSuccess: " + responseBody);

                try {
                    // JSON Object
                    HashMap<String,String> hmap=new HashMap<String,String>();
                    for (  org.apache.http.Header h : headers) {
                        hmap.put(h.getName(),h.getValue());
                        if (h.getName().equals("Content-Length"))
                        {
                            if (h.getValue().equals("0"))
                            {
                                Toast.makeText(appContext, "You are successfully added to group!", Toast.LENGTH_LONG).show();
                                return;
                            }
                        }
                    }
                    JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        // Set Default Values for Edit View controls
                        //setDefaultValues();
                        // Display successfully registered message using Toast
                        Toast.makeText(appContext, "You are successfully registered!", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                        //errorMsg.setText(obj.getString("error_msg"));
                        Toast.makeText(appContext, obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(appContext, "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }
            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {


                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(appContext, "Failed:404" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '404'
                else if (statusCode == 409) {
                    regFrag.ShowInfoOnFragment("Existing Account Found");
                    Toast.makeText(appContext, "Failed:409" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(appContext, "Failed:500" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(appContext, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });



    }


    // public void invokeWS(RequestParams params) {
    public void invokeWS2(String jsonString) {
        String st = null;
        // Show Progress Dialog
        regFrag.enableProgressBar();

        // Make RESTful webservice call using AsyncHttpClient object
        clientUser = new AsyncHttpClient();
        //String  contentType = "string/xml;UTF-8";
        clientUser.removeHeader("Content-Type");
        //client.addHeader("Content-Type", "application/json");
        clientUser.addHeader("Authorization", "664Rk33SFjfs2m7R");
        //client.addHeader("Content-Type", "application/json");
        clientUser.addHeader("Accept", "application/json");
        //client.setBasicAuth("manish", "nswdel10");

        //Log.d(FILENAME, "invokeWS2");

        StringEntity se = null;
        try {
            se = new StringEntity(jsonString);
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            errCode.setErrorCode(400);
            errCode.setErrorMessage(e.getMessage());
            return;
        }
        clientUser.post(appContext, serverAddress, se, "application/json", new AsyncHttpResponseHandler() {
            @Override
            public void onSuccess(int statusCode, org.apache.http.Header[] headers, byte[] responseBody) {

                regFrag.disableProgressBar();
                errCode.setErrorCode(200);
                errCode.setErrorMessage("Succes");
                // Hide Progress Dialog
                //prgDialog.hide();
                //Log.d(FILENAME, "http onSuccess: " + responseBody);

                try {
                    // JSON Object
                    HashMap<String, String> hmap = new HashMap<String, String>();
                    for (org.apache.http.Header h : headers) {
                        hmap.put(h.getName(), h.getValue());
                        if (h.getName().equals("Content-Length")) {
                            if (h.getValue().equals("0")) {
                                Toast.makeText(appContext, "You are successfully registered!", Toast.LENGTH_LONG).show();
                                regFrag.launchPostRegFragment();
                                if(cTimer != null && !isTimerExpired )
                                {
                                    cTimer.cancel();
                                }
                                return;
                            }
                        }
                    }
                    JSONObject obj = new JSONObject(responseBody.toString());
                    // When the JSON response has status boolean value assigned with true
                    if (obj.getBoolean("status")) {
                        // Set Default Values for Edit View controls
                    //    setDefaultValues();
                        // Display successfully registered message using Toast
                        SaveUserInfoIntoDB();
                        Toast.makeText(appContext, "You are successfully registered!", Toast.LENGTH_LONG).show();
                    }
                    // Else display error message
                    else {
                  //      errorMsg.setText(obj.getString("error_msg"));
                        Toast.makeText(appContext, obj.getString("error_msg"), Toast.LENGTH_LONG).show();
                    }
                } catch (JSONException e) {
                    // TODO Auto-generated catch block
                    Toast.makeText(appContext, "Error Occured [Server's JSON response might be invalid]!", Toast.LENGTH_LONG).show();
                    e.printStackTrace();
                }
            }

            // When the response returned by REST has Http response code other than '200'
            @Override
            public void onFailure(int statusCode, org.apache.http.Header[] headers, byte[] responseBody, Throwable
                    error) {
                // Hide Progress Dialog
                regFrag.disableProgressBar();


                errCode.setErrorCode(statusCode);
                errCode.setErrorMessage(error.getMessage());

                // When Http response code is '404'
                if (statusCode == 404) {
                    Toast.makeText(appContext, "Failed:404" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code is '404'
                else if (statusCode == 409) {

                    /* This means user already registered with us */
                    SaveUserInfoIntoDB();
                    Toast.makeText(appContext, "Failed:409" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                    regFrag.launchPostRegFragment();
                }
                // When Http response code is '500'
                else if (statusCode == 500) {
                    Toast.makeText(appContext, "Failed:500" + error.getMessage() + ":" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
                // When Http response code other than 404, 500
                else {
                    Toast.makeText(appContext, "Failed:Unexpected Error occcured!" + error.getMessage() + ":[Most common Error: Device might not be connected to Internet or remote server is not up and running]" + responseBody.toString(), Toast.LENGTH_LONG).show();
                }
            }
        });

    }

}
