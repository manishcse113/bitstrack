/**
 * Created by GUR11219 on 3/28/2016.
 */
public class test {
}


{
        "swagger": "2.0",
        "info": {
        "description": "",
        "version": "1.0",
        "title": "Field Status Report"
        },
        "basePath": "/jobManagement",
        "schemes": [
        "http"
        ],
        "consumes": [
        "application/json"
        ],
        "produces": [
        "application/json"
        ],
        "paths": {
        "/job": {
        "post": {
        "tags": [
        "job"
        ],
        "operationId": "jobCreate",
        "summary": "create",
        "description": "",
        "deprecated": false,
        "parameters": [
        {
        "name": "job",
        "in": "body",
        "required": true,
        "schema": {
        "$ref": "#/definitions/Job"
        }
        }
        ],
        "responses": {
        "201": {
        "description": "Success",
        "schema": {
        "$ref": "#/definitions/Job"
        }
        }
        }
        },
        "get": {
        "tags": [
        "job"
        ],
        "operationId": "jobList",
        "summary": "find",
        "description": "",
        "deprecated": false,
        "responses": {
        "200": {
        "description": "Success",
        "schema": {
        "type": "array",
        "items": {
        "$ref": "#/definitions/Job"
        }
        }
        }
        }
        }
        },
        "/job/{jobId}": {
        "get": {
        "tags": [
        "job"
        ],
        "operationId": "jobfindById",
        "summary": "findById",
        "description": "",
        "deprecated": false,
        "parameters": [
        {
        "name": "jobId",
        "in": "path",
        "required": true,
        "type": "string"
        }
        ],
        "responses": {
        "200": {
        "description": "Success",
        "schema": {
        "$ref": "#/definitions/Job"
        }
        }
        }
        },
        "put": {
        "tags": [
        "job"
        ],
        "operationId": "jobUpdate",
        "summary": "update",
        "description": "",
        "deprecated": false,
        "parameters": [
        {
        "name": "jobId",
        "in": "path",
        "required": true,
        "type": "string"
        },
        {
        "name": "job",
        "in": "body",
        "required": true,
        "schema": {
        "$ref": "#/definitions/Job"
        }
        }
        ],
        "responses": {
        "201": {
        "description": "Success",
        "schema": {
        "$ref": "#/definitions/Job"
        }
        }
        }
        }
        }
        },
        "definitions": {
        "Job": {
        "required": [
        "jobId",
        "status",
        "jobOwnedBy",
        "createDate",
        "site"
        ],
        "type": "object",
        "properties": {
        "jobId": {
        "type": "string"
        },
        "assignDate": {
        "type": "string",
        "format": "date-time"
        },
        "createDate": {
        "type": "string",
        "format": "date-time"
        },
        "completeDate": {
        "type": "string",
        "format": "date-time"
        },
        "jobOwnedBy": {
        "type": "string"
        },
        "jobFullfilledBy": {
        "type": "string"
        },
        "jobTitle": {
        "type": "string"
        },
        "status": {
        "type": "string",
        "enum": [
        "New",
        "Accepted",
        "InProgress",
        "Completed",
        "Rejected"
        ]
        },
        "site": {
        "$ref": "#/definitions/Site"
        },
        "consumedItem": {
        "type": "array",
        "items": {
        "$ref": "#/definitions/ConsumedItem"
        }
        },
        "description": {
        "type": "string"
        },
        "remarks": {
        "type": "string"
        },
        "attachement": {
        "$ref": "#/definitions/Attachement"
        }
        }
        },
        "Attachement": {
        "type": "object",
        "properties": {
        "id": {
        "type": "string"
        },
        "href": {
        "type": "string"
        },
        "externalId": {
        "type": "string"
        }
        }
        },
        "ConsumedItem": {
        "type": "object",
        "properties": {
        "id": {
        "type": "string"
        },
        "name": {
        "type": "string"
        },
        "quantity": {
        "type": "string"
        }
        }
        },
        "Site": {
        "type": "object",
        "properties": {
        "id": {
        "type": "string"
        },
        "customerSiteName": {
        "type": "string"
        }
        }
        }
        }
        }